@extends('layouts.app')
@section('vars')
{!! $page_title = 'Error 419';
!!}
@endsection
@section('style')
<style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 36px;
                padding: 20px;
            }
        </style>
@endsection
@section('barra_buscar_cuerpo')
@endsection
@section('scripts')

@endsection
@section('content')
<div class="container">
  <center>
  <h1>{{$page_title}}</h1>
  <div class="title">
La página ha expirado debido a la inactividad.
<br>
Por favor, actualice y pruebe de nuevo.</div>
    </center>
</div>
@endsection
