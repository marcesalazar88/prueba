@extends('layouts.app')
@section('vars')
{!! $page_title = 'Error 403'; 
!!}
@endsection
@section('style')
@endsection
@section('barra_buscar_cuerpo')
@endsection
@section('scripts')

@endsection
@section('content')
<div class="container">
  <h1>{{$page_title}}</h1>
  <div class="title">Lo sentimos, no tienes permisos para ingresar a la página que estás buscando</div>
</div>
@endsection
