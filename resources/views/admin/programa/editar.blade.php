@extends('layouts.app')
@section('vars')
{!!
$page_title = 'Programas'; 
$route = 'programa'; 
!!}
@endsection
@section('ruta_de_migas')
<ol class="breadcrumb">
    <li><a href="{{ route('home') }}"> Inicio</a></li>
    <li><a href="{{ secure_asset('admin/programa') }}"> {{ $page_title }}</a></li>
    <li class="active"> Modificar</li>
</ol>
@endsection
@section('scripts')
<script>
    $(document).ready(function() {
        password_en_formulario("admin_programa_create");
        required_en_formulario_for("admin_programa_create","red","*")
    });
    
    function validar_area_password(obj){
        document.getElementById('area_password').style.display = obj.checked ? 'block' : 'none';
        document.getElementById('password').value='';
        document.getElementById('password-confirm').value='';
        $('#password').prop('required',obj.checked);
        $('#password-confirm').prop('required',obj.checked);
    }
</script>
@endsection
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"></div>

                <div class="card-body">
                    <form method="POST" id="admin_programa_create" action="{{ route('admin.programa.update',[
                    'usuario' => $prog->id,
                    ]) }}">
                
                         <meta name="csrf-token" content="{{ csrf_token() }}">
                         <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <div class="form-group row">
                            <label for="nom_prog" class="col-md-4 col-form-label text-md-right">{{ __('Nombre Programa') }}</label>

                            <div class="col-md-6">
                                <input id="id" type="hidden" class="form-control{{ $errors->has('id') ? ' is-invalid' : '' }}" name="id" value="{{
                                ($errors->has('id')) ?  old('id') : $prog->id }}" required>
                                <input id="nom_prog" type="text" class="form-control{{ $errors->has('nom_prog') ? ' is-invalid' : '' }}" name="nom_prog" value="{{
                                ($errors->has('nom_prog')) ?  old('nom_prog') : $prog->nom_prog }}" required autofocus>
                                @if ($errors->has('nom_prog'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('nom_prog') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label for="cod_prog" class="col-md-4 col-form-label text-md-right">{{ __('Código Programa') }}</label>

                            <div class="col-md-6">
                                <input id="cod_prog" type="text" class="form-control{{ $errors->has('cod_prog') ? ' is-invalid' : '' }}" name="cod_prog" value="{{
                                ($errors->has('cod_prog')) ?  old('cod_prog') : $prog->cod_prog }}" required autofocus>
                                @if ($errors->has('cod_prog'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('cod_prog') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">

                            <label for="depto_id" class="col-md-4 col-form-label text-md-right">{{ __('Departamento') }}</label>
                            <div class="col-md-6">
                         
                                {!!Form::select('depto_id', $depto, 
                                ($errors->has('depto_id')) ?  old('depto_id') : $prog->depto_id
                                , [
                                'id' => 'depto_id',
                                'class' => 'form-control js-example-basic-single form-control',
                                'placeholder' => 'Seleccione un Departamento',
                                'required',
                                ])!!}
                                
                                
                                @if ($errors->has('depto_id'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('depto_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="sede" class="col-md-4 col-form-label text-md-right">{{ __('Sede') }}</label>

                            <div class="col-md-6">
                                <input id="sede" type="text" class="form-control{{ $errors->has('sede') ? ' is-invalid' : '' }}" name="sede" value="{{
                                ($errors->has('sede')) ?  old('sede') : $prog->sede }}" required >

                                @if ($errors->has('sede'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('sede') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="periodo_ingreso" class="col-md-4 col-form-label text-md-right">{{ __('Periodo de ingreso') }}</label>

                            <div class="col-md-6">
                                {!!Form::select('periodo_ingreso', ['A'=>'A','B'=>'B'], 
                                ($errors->has('periodo_ingreso')) ?  old('periodo_ingreso') : $prog->periodo_ingreso
                                , [
                                'id' => 'periodo_ingreso',
                                'class' => 'form-control js-example-basic-single form-control',
                                'placeholder' => 'Seleccione una periodo',
                                'required',
                                ])!!}
                                @if ($errors->has('sede'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('sede') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                      
                       
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                              <a href="{{ route('admin.programa.index') }}" class="btn btn-secondary hidden-print">Regresar</a>
                                <input type="submit" name="submit" value="Actualizar" class="btn btn-primary">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
