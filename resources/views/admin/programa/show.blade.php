@extends('layouts.app')
@section('barra_buscar')
<li><a onclick="window.print()" title="Imprimir">
<span class="glyphicon glyphicon-print"></span></a></li>
@endsection
@section('ruta_de_migas')
<ol class="breadcrumb navbar-left">
    <li><a href="{{ route('home') }}"> Inicio</a></li>
    <li><a href="{{ route('admin.programa.index') }}"> Programas</a></li>
    <li class="active"> Detalles {{ $prog->nom_prog }}</li>
</ol>
@endsection
@section('scripts')
@include('reportes.css')
@endsection
@section('content')
<span id="reporte">
<center>
<table border="1">
    <tr>
        <td class="alineado_izquierda"><strong>Facultad:</strong><br>
        {{ $prog->depto->fac->nom_fac  }}
        </td>
    </tr>
    <tr>
        <td class="alineado_izquierda"><strong>Departamento:</strong><br>
        {{ $prog->depto->nom_depto  }}
        </td>
    </tr>
    <tr>
        <td class="alineado_izquierda"><strong>Programa:</strong><br>
        {{ $prog->nom_prog }}
        </td>
    </tr>
</table>
<br>
<table border="1">
    <tr>
        <td class="alineado_izquierda" colspan="8"><center><strong>Planes del Programa</strong></center></td>
    </tr>
    <tr>
        <th><center>Nombre</center></th>
        <th colspan="2"><center>Fecha / Documento Acuerdo</center></th>
@if (Auth::check())
@if (Auth::user()->rol=='admin')
        <th colspan="2" class="noprint"><center>Acciones</center></th>
        <th colspan="2" class="noprint"><center><a class="btn btn-primary" href="{{ route('admin.planes.create',[$prog->cod_prog]) }}">Nuevo</a></center></th>
@endif
@endif    
    </tr>
    @foreach ($prog->planes as $planes_i)
    <tr>
        <td>{{ $planes_i->nombre }}</td>
        <td colspan="2">{{ Fecha::formato_fecha($planes_i->fecha) }}&nbsp;&nbsp;
        
          <a href="<?php echo env('APP_URL').$planes_i->adjunto ?>"  title="Acuerdo"target="_blank"><img src="{{ secure_asset('/img/pdf.png')}}"></a>
      </td>
@if (Auth::check())
@if (Auth::user()->rol=='admin')
        <td colspan="2" class="noprint">
          <a class="btn btn-secondary noprint"  
        href="{{ route('admin.planes.show',[$planes_i->id]) }}"
        >Detalles</a>
         <br>
          <a class="btn btn-info noprint"  
        href="{{ route('admin.planes.edit',[$planes_i->id]) }}"
        >Modificar</a>
          </td>
       <td colspan="2" class="noprint">
      <a class="btn btn-danger noprint"  
        onclick="return confirm('¿Esta ud seguro que quiere eliminar {{ $planes_i->nombre }}?')" 
        href="{{ route('admin.planes.delete',[$planes_i->id]) }}"
        >Eliminar</a></td>
@endif
@endif 
    </tr>
    @endforeach
</table>
<br>
<table border="1">
    <tr>
        <td class="alineado_izquierda" colspan="4"><center><strong>Asignaciones del programa</strong></center></td>
    </tr>
    <tr>
        <th><center>Asignatura</center></th>
        <th><center>Docente</center></th>
        <th><center>Periodo Académico</center></th>
        <th><center>Estudiante</center></th>
    </tr>
    @foreach ($prog->asigcs as $asigc_i)
    <tr>
        <td>{{ $asigc_i->asigt->nom_asigt}}</td>
        <td>{{ $asigc_i->docente_fk->name }}</td>
        <td>{{ $asigc_i->per_acad }}</td>
        <td>
            @if(isset($asigc_i->estudiante_fk->name))
            {{ $asigc_i->estudiante }}<br>{{ $asigc_i->estudiante_fk->name }}
            @endif        
        </td>
    </tr>
    @endforeach
</table>
<br>
</span>
  <a href="{{ route('admin.programa.index') }}" class="btn btn-secondary hidden-print">Regresar</a>
@endsection