@extends('layouts.app')
@section('vars')
{!! $page_title = 'Asignación de asignaturas'; 
!!}
@endsection
@section('barra_buscar_cuerpo')
<nav class="navbar navbar-default">
<div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
<ul class="nav navbar-nav navbar-right">
      <li><a style="margin: 0; padding: 8px;">
    <div class="input-group sidebar-form">
    <label for="buscar">Buscar por asignatura&nbsp;</label><br>
    <select style="background-color: #FFF;color:black;width:170px;display:inline" id="buscar_asignaturas" name="cod_asigt" class="js-example-basic-single form-control input_buscar">
      <option value="">Todas las asignaturas</option>
      @foreach ($asignaturas as $id => $asignaturas_i)
      <option value="{{$asignaturas_i->codigo_asigt}}" <?php 
      if (isset($_POST['buscar_asignaturas']) and $_POST['buscar_asignaturas'] == $asignaturas_i->codigo_asigt){
      echo " selected ";
    } ?> >{{$asignaturas_i->nom_asigt}}</option>
      @endforeach
    </select>
    </div>
    </a></li>
    <li><a style="margin: 0; padding: 8px;">
    <div class="input-group sidebar-form">
    <label for="buscar">Buscar por docente&nbsp;</label><br>
    <select style="background-color: #FFF;color:black;width:170px;display:inline" id="buscar_docentes" name="ident_docnt" class="js-example-basic-single form-control input_buscar">
      <option value="">Todos los docentes</option>
      @foreach ($docentes as $id => $docentes_i)
      <option value="{{$docentes_i->ident_usu}}" <?php 
      if (isset($_POST['buscar_docentes']) and $_POST['buscar_docentes'] == $docentes_i->ident_usu){
      echo " selected ";
    } ?> >{{$docentes_i->nombre}} {{$docentes_i->apellido}}</option>
      @endforeach
    </select>
    </div>
    </a></li>
    <li><a style="margin: 0; padding: 8px;">
    <div class="input-group sidebar-form">
    <label for="buscar">Buscar por periodo&nbsp;</label><br>
    <select style="background-color: #FFF;color:black;width:170px;display:inline" id="buscar_periodos" name="per_acad" class="js-example-basic-single form-control input_buscar">
      <option value="">Todos los periodos</option>
      @foreach ($periodos as $id => $periodos_i)
      <option value="{{$periodos_i->periodo}}" <?php 
      if (isset($_POST['buscar_periodos']) and $_POST['buscar_periodos'] == $periodos_i->periodo){
      echo " selected ";
      }
      if (empty($_POST) and $periodo_activo->periodo == $periodos_i->periodo){
        echo " selected ";
      }
      ?>>{{$periodos_i->periodo}}</option>
      @endforeach
    </select>
    </div>
    </a></li>
    <li><a style="margin: 0; padding: 8px;">
    <div class="input-group sidebar-form">
    <label for="buscar">Buscar por programa&nbsp;</label><br>
    <select style="background-color: #FFF;color:black;width:170px;display:inline" id="buscar_programa" name="cod_prog" class="js-example-basic-single form-control input_buscar">
      <option value="">Todos los programas</option>
      @foreach ($progs as $id => $prog_i)
      <option value="{{$prog_i->cod_prog}}" <?php 
      if (isset($_POST['buscar_programa']) and $_POST['buscar_programa'] == $prog_i->cod_prog){
      echo " selected ";
    } ?> >{{$prog_i->nom_prog}}</option>
      @endforeach
    </select>
    </div>
    </a></li>
     <li><a style="margin: 0; padding: 8px;">
    <div class="input-group sidebar-form">
    <label for="buscar">Buscar por estudiante&nbsp;</label><br>
    <select style="background-color: #FFF;color:black;width:170px;display:inline" id="buscar_estudiantes" name="estudiante" class="js-example-basic-single form-control input_buscar">
      <option value="">Todos los estudiantes</option>
      @foreach ($estudiantes as $id => $estudiantes_i)
      <option value="{{$estudiantes_i->ident_usu}}" <?php 
      if (isset($_POST['buscar_estudiantes']) and $_POST['buscar_estudiantes'] == $estudiantes_i->ident_usu){
      echo " selected ";
    } ?> >{{$estudiantes_i->nombre}} {{$estudiantes_i->apellido}}</option>
      @endforeach
    </select>
    </div>
    </a></li>
    <li>
    <a> <input type="hidden" id="buscar" class="form-control" style="">
    <input type="hidden" id="buscar_arreglo" value="SI">
    <input type="hidden" name="flex" class="input_buscar2" value="SI">
    </a></li>
    <li>
      <div class="input-group sidebar-form">
          <label for="num_resultados">Resultados&nbsp;</label><br>
          <span class="input-group-btn" style="display:inline">
          <input title="Número de resultados" class="form-control" style="height: 27px;
    margin-top: 9px;background-color: rgb(255, 255, 255);color: black;width: 70px;border-bottom: 1px none transparent;border-left: 0px solid black;margin-right: 1px;border-right: 1px solid #ccd0d2;border-bottom: 1px solid #ccd0d2;background-color: #fff;
    border: 1px solid #aaa;
    border-radius: 4px;" min="1" id="num_resultados" name="num_resultados"  type="number" value="<?php 
    if (isset($_POST['num_resultados'])){
      echo $_POST['num_resultados'];
    }else{
      echo 3;
    }
    ?>" solonumeros>
          </span>
      </div>
    </li>
    <li style="
    margin: 0px;
    padding: 5px;
">
    </li>
</ul>

</nav>
@endsection
@section('fff')
<ul class="nav navbar-nav navbar-right">
    @include('layouts.partials.buscar')
</ul>
@endsection
@section('scripts')
<script>
function preparar_buscar_asignacion(){
  var contenido = '{';
  var cont = 0;
  var total = $(".input_buscar").length;
  $(".input_buscar").each(function( index ) {
        $(this).each(function( index2 ) {
          contenido += '"'+this.name+'":"'+$(this).val()+'"';
          cont++;
          if(cont != total) contenido += ',';
        });
    });
    contenido += '}'
  $("#buscar").val(btoa(contenido));
  
}
$(document).ready(function(){
$(".input_buscar").keyup(function(){
  preparar_buscar_asignacion();
  buscar_asignacion();
});
$(".input_buscar").change(function(){
  preparar_buscar_asignacion();
  buscar_asignacion();
});

  
$("#buscar").change(function(){
  buscar_asignacion();
});
$("#search-btn").click(function(){
  buscar_asignacion();
});
$("#num_resultados").keyup(function(){
  buscar_asignacion();
});
$("#num_resultados").change(function(){
  buscar_asignacion();
});
$("#menu_configuraciones").addClass('active');
$("#menu_asignacion").addClass('active');
  preparar_buscar_asignacion();
  buscar_asignacion();
});
</script>
@endsection
@section('ruta_de_migas')
<ol class="breadcrumb">
    <li><a href="{{ route('home') }}"> Inicio</a></li>
    <li class="active"> {{ $page_title }}</li>
</ol>
@endsection
@section('content')
<center><h1>{{ $page_title }}</h1></center>
<div class='row'>
  <div class='col-md-12'>
  <span id="txt_resultados">
  </span>
  <div id="area_pagination"></div>
    
    
  </div><!-- /.col -->
  <div class='col-md-6'>

  </div><!-- /.col -->

</div><!-- /.row -->

@endsection