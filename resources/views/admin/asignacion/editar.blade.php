@extends('layouts.app')
@section('vars')
{!!
$page_title = 'Asignación de asignaturas'; 
$route = 'asignacion'; 
!!}
@endsection
@section('ruta_de_migas')
<ol class="breadcrumb">
    <li><a href="{{ route('home') }}"> Inicio</a></li>
    <li><a href="{{ secure_asset('admin/asignacion') }}"> {{ $page_title }}</a></li>
    <li class="active"> Modificar</li>
</ol>
@endsection
@section('scripts')
<script>
    $(document).ready(function() {
        password_en_formulario("admin_asignacion_create");
        required_en_formulario_for("admin_asignacion_create","red","*")
    });
    </script>
@endsection
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <center><h1>{{ $page_title }}</h1></center>
            <div class="card">
                <div class="card-header"></div>

                <div class="card-body">
                    <form method="POST" id="admin_asignacion_create" action="{{ route('admin.asignacion.update',[
                    'id' => $asigc->id,
                    ]) }}">
                         <meta name="csrf-token" content="{{ csrf_token() }}">
                         <input type="hidden" name="_token" value="{{ csrf_token() }}">

                       <div class="form-group row">
                            <label for="ident_docnt" class="col-md-4 col-form-label text-md-right">{{ __('Docente') }}</label>

                            <div class="col-md-6">
                                <select id="ident_docnt" class="js-example-basic-single form-control{{ $errors->has('ident_docnt') ? ' is-invalid' : '' }}" name="ident_docnt" required autofocus>
                                    <option value="">Seleccione un docente</option>
                                    @foreach($usuarios as $ident_docnt_i)
                                    @if($ident_docnt_i->rol == "docente")
                                    <option value="{{ $ident_docnt_i->ident_usu }}"
                                        @if (count($errors)>0)
                                            @if (old('ident_docnt')==$ident_docnt_i->ident_usu) {{ __(' selected ') }} @endif;
                                        @else
                                            @if ($asigc->ident_docnt==$ident_docnt_i->ident_usu) {{ __(' selected ') }} @endif;
                                        @endif;
                                    >{{ $ident_docnt_i->name." (".$ident_docnt_i->ident_usu.")" }}</option>
                                    @endif
                                    @endforeach
                                </select>
                                @if ($errors->has('ident_docnt'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('ident_docnt') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                       <!--div class="form-group row">
                            <label for="estudiante" class="col-md-4 col-form-label text-md-right">{{ __('Estudiante') }}</label>

                            <div class="col-md-6">
                                <select id="estudiante" class="js-example-basic-single form-control{{ $errors->has('estudiante') ? ' is-invalid' : '' }}" name="estudiante" required autofocus>
                                    <option value="">Seleccione un estudiante</option>
                                    @foreach($usuarios as $estudiante_i)
                                    @if($estudiante_i->rol == "estudiante")
                                    <option  value="{{ $estudiante_i->ident_usu }}"
                                    @if (old('estudiante')==$estudiante_i->ident_usu) {{ __(' selected ') }} 
                                    @endif;
                                    >{{ $estudiante_i->name." (".$estudiante_i->ident_usu.")" }}</option>
                                    @endif
                                    @endforeach
                                </select>
                                @if ($errors->has('estudiante'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('estudiante') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div-->
                        
                       <div class="form-group row">
                            <label for="cod_asigt" class="col-md-4 col-form-label text-md-right">{{ __('Asignatura') }}</label>

                            <div class="col-md-6">
                                <select id="cod_asigt" class="js-example-basic-single form-control{{ $errors->has('cod_asigt') ? ' is-invalid' : '' }}" name="cod_asigt" required autofocus>
                                    <option value="">Seleccione una asignatura</option>
                                    @foreach($asigts as $asigt_i)
                                    <option value="{{ $asigt_i->codigo_asigt }}" 
                                        @if (count($errors)>0)
                                            @if (old('cod_asigt')==$asigt_i->codigo_asigt) {{ __(' selected ') }} @endif;
                                        @else
                                            @if ($asigc->cod_asigt==$asigt_i->codigo_asigt) {{ __(' selected ') }} @endif;
                                        @endif;
                                    >{{ $asigt_i->nom_asigt." (".$asigt_i->codigo_asigt.")" }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('cod_asigt'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('cod_asigt') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="per_acad" class="col-md-4 col-form-label text-md-right">{{ __('Periodo Académico') }}</label>

                            <div class="col-md-6">
                                <?php
                                #$sug_per_acad = date("Y");
                                #$sug_per_acad .= (date("m")<=6) ? 'A' : 'B';
                                ?>
                                
                                <select id="per_acad" class="js-example-basic-single form-control{{ $errors->has('per_acad') ? ' is-invalid' : '' }}" name="per_acad" required autofocus>
                                    <option value="">Seleccione un periodo académico</option>
                                    @foreach($periodos as $periodo)
                                    <option value="{{ $periodo->periodo }}"
                                    @if (count($errors)>0)
                                        @if (old('per_acad')==$periodo->periodo) 
                                            {{ __(' selected ') }} 
                                        @endif;
                                    @else
                                         @if ($asigc->per_acad==$periodo->periodo) 
                                            {{ __(' selected ') }} 
                                        @endif;
                                    @endif;
                                    >{{ $periodo->periodo }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('per_acad'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('per_acad') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                       <div class="form-group row">
                            <label for="cod_prog" class="col-md-4 col-form-label text-md-right">{{ __('Programa') }}</label>

                            <div class="col-md-6">
                                <select id="cod_prog" class="js-example-basic-single form-control{{ $errors->has('cod_prog') ? ' is-invalid' : '' }}" name="cod_prog" required autofocus>
                                    <option value="">Seleccione un programa</option>
                                    @foreach($progs as $prog_i)
                                    <option value="{{ $prog_i->cod_prog }}" 
                            <?php
                            if (count($errors)>0):
                                if (old('cod_prog')==$prog_i->cod_prog): echo ' selected '; endif;
                            else:
                                if ($asigc->cod_prog==$prog_i->cod_prog): echo ' selected '; endif;
                            endif;
                            ?>
                                    >{{ $prog_i->nom_prog }}</option>
                                    @endforeach
                                </select>
                                
                                @if ($errors->has('cod_prog'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('cod_prog') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                       <div class="form-group row">
                            <label for="flex" class="col-md-4 col-form-label text-md-right">{{ __('Flexibilidad') }}</label>

                            <div class="col-md-6">
                                <select id="flex" class="form-control{{ $errors->has('flex') ? ' is-invalid' : '' }}" name="flex" required autofocus>
                                    <option
                                    @if (count($errors)>0)
                                        @if (old('flex')=="SI") {{ __(' selected ') }} @endif;
                                    @else
                                        @if ($asigc->flex=="SI") {{ __(' selected ') }} @endif;
                                    @endif;
                                    >SI</option>
                                    <option
                                    @if ($errors->has('flex'))
                                        @if (old('flex')!="SI") {{ __(' selected ') }} @endif;
                                    @else
                                        @if ($asigc->flex!="SI") {{ __(' selected ') }} @endif;
                                    @endif;
                                    >NO</option>
                                </select>

                                @if ($errors->has('flex'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('flex') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="grupo" class="col-md-4 col-form-label text-md-right">{{ __('Grupo') }}</label>

                            <div class="col-md-6">
                                <?php
                                $sug_grupo = $asigc->grupo;
                                ?>
                                <input placeholder="Ejemplo:  Grupo 1" id="grupo" type="text" class="form-control{{ $errors->has('grupo') ? ' is-invalid' : '' }}" name="grupo" value="{{ count($errors)>0 ? old('grupo') : $sug_grupo }}" required autofocus>
                                @if ($errors->has('grupo'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('grupo') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                       <div class="form-group row">
                            <label for="observaciones" class="col-md-4 col-form-label text-md-right">{{ __('Observaciones') }}</label>

                            <div class="col-md-6">
                                <textarea placeholder="" id="observaciones" class="form-control{{ $errors->has('observaciones') ? ' is-invalid' : '' }}" name="observaciones"  >{{ count($errors)>0 ? old('observaciones') : $asigc->observaciones }}</textarea>
                                
                                @if ($errors->has('observaciones'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('observaciones') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

<div class="form-group row">
  <input type="hidden" name="notificar" value="NO"> 
  <input id="notificarid" type="checkbox" name="notificar" value="SI">
  <label for="notificarid">Enviar notificación al guardar</label>
</div>
                        <div class="form-group row mb-0">
                             <div class="col-md-6 offset-md-4">
                                <a href="{{ route('admin.asignacion.index') }}" class="btn btn-secondary">
                                    {{ __('Regresar') }}
                                </a>
                            </div>
                            <div class="col-md-6 offset-md-4">
                                <input type="submit" name="submit" value="Actualizar" class="btn btn-primary">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
