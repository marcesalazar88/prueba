@extends('layouts.app')
@section('barra_buscar')
<li><a onclick="window.print()" title="Imprimir">
<span class="glyphicon glyphicon-print"></span></a></li>
@endsection
@section('ruta_de_migas')
<ol class="breadcrumb">
    <li><a href="{{ route('home') }}"> Inicio</a></li>
    <li><a href="{{ route('admin.asignacion.index') }}"> Asignación de asignaturas</a></li>
    <li class="active"> Detalles</li>
</ol>
@endsection
@section('scripts')
@include('reportes.css')
@endsection
@section('content')
<span id="reporte">
<center>
<table border="1">
    <tr>
        <th>
            <center><img src="{{ secure_asset('img/logo-udenar.png')}}"></center>
            <span id="titulo_logo"><center>Universidad <span class="m10">de</span></center><center><strong>Nariño</strong></center></span>
        </th>
        <th>
            <div class="titulo negrita"><strong>IDENTIFICACIÓN DE LA ASIGNATURA:</strong></div>
        </th>
    </tr>

</table></center>

<table border="1">
    <tr>
        <td class="alineado_izquierda"><strong>NOMBRE DE LA ASIGNATURA EN CURSO: </strong> {{ $asigc->asigt->nom_asigt }}</td>
    </tr>
</table>
<br>
<center>
<table border="1">
    <tr>
        <td>Código de la Asignatura:</td>
        <td colspan="4"> {{ $asigc->asigt->codigo_asigt }}</td>
    </tr>
    <tr>
        <td>Semestres a los Cuales se ofrece:</td>
        <td colspan="4"> {{ $asigc->asigt->sem_ofrece_asigt }}</td>
    </tr>
    <tr>
        <td>Intensidad Horaria Semanal:</td>
        <td>Número de Créditos:</td>
        <td colspan="4">{{ $asigc->asigt->ihs_n_cred_asigt }}</td>
    </tr>
</table>
</center>
<br>
<table border="1">
    <tr>
        <td class="alineado_izquierda"><strong>2. JUSTIFICACIÓN: <br></strong>{{ $asigc->asigt->just_asigt }}</td>
    </tr>
</table>
<br>
<table border="1">
    <tr>
        <td class="alineado_izquierda"><strong>3. OBJETIVOS: <br>
        <p>3.1 Objetivo General</p>
        <p>{{ $asigc->asigt->obj_gen }}</p>
        <p>3.2 Objetivos Específicos</p>
        <p><?php echo  $asigc->asigt->obj_esp ?></p>
        </strong></td>
    </tr>
</table>
<br>
<table border="1">
    <tr>
        <td class="alineado_izquierda"><strong>Area:</strong><br>
        {{ $asigc->asigt->area }}
        </td>
    </tr>
</table>
<br>
<table border="1">
    <tr>
        <td class="alineado_izquierda" colspan="8"><center><strong>Contenido de la Asignatura</strong></center></td>
    </tr>
    <tr>
        <th><center>Horas de contenido</center></th>
        <th colspan="2"><center>Tema o Capítulo</center></th>
        <th colspan="2"><center>Forma de Evaluación</center></th>
        <!--th colspan="2"><center>Acciones</center></th>
        <th colspan="2"><center><a class="btn btn-primary" href="{{ route('admin.contenido.create',[$asigc->asigt->id]) }}">Nuevo</a></center></th-->
    </tr>
    @foreach ($asigc->asigt->contasigts as $cont_asig_i)
    <tr>
        <td>{{ $cont_asig_i->hras_cont_asigt }}</td>
        <td colspan="2"><?php echo $cont_asig_i->tem_cap_cont_asigt ?></td>
        <td colspan="2">{{ $cont_asig_i->form_eva_cont_asigt }}</td>
        <!--td colspan="2"><a class="btn btn-info" href="{{ route('admin.contenido.edit',[$cont_asig_i->id]) }}">Modificar</a></td>
        <td colspan="2"><a class="btn btn-danger"  onclick="return confirm('¿Esta ud seguro que quiere eliminar este contenido?')" href="{{ route('admin.contenido.delete',[$cont_asig_i->id]) }}">Eliminar</a></td-->
    </tr>
    @endforeach
</table>
<br>
<table border="1">
    <tr>
        <td class="alineado_izquierda" colspan="4"><center><strong>Asignación de la Asignatura</strong></center></td>
    </tr>
    <tr>
        <th><center>Facultad / Departamento / Programa</center></th>
        <th><center>Docente</center></th>
        <th><center>Periodo Académico</center></th>
        <th><center>Estudiante</center></th>
    </tr>
   
    <tr>
        <td>{{ $asigc->prog->depto->fac->nom_fac }} / {{ $asigc->prog->depto->nom_depto }} / {{ $asigc->prog->nom_prog }} </td>
        <td>{{ $asigc->ident_docnt }}<br>{{ $asigc->docente_fk->name }}</td>
        <td>{{ $asigc->per_acad }}</td>
        <td>
            @if(isset($asigc->estudiante_fk->name))
                {{ $asigc->estudiante }}<br>{{ $asigc->estudiante_fk->name }}
            @endif
        </td>
    </tr>
    
</table>
<br>
@if (!empty($asigc->observaciones))
    <table border="1">
    <tr>
        <th><center>Observaciones</center></th>
    </tr>
    <tr>
        <td>{{ $asigc->observaciones }}</td>
    </tr>
    </table>
    <br>
@endif
</span>
<a href="{{ route('admin.asignacion.index') }}" class="btn btn-secondary hidden-print">Regresar</a>
@endsection