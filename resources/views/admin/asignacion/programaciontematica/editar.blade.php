@extends('layouts.app')
@section('vars')
{!!
$page_title = 'Programación Temática'; 
$route = 'asignacion'; 
!!}
@endsection
@section('barra_buscar_cuerpo')
@endsection
@section('scripts')
<style>
    table {
    width: 100% !important;
    }
    input[type=submit]{
    width: auto !important;
    }
</style>
<script>
    function sugerir_fechas(select){
        var x = select.selectedIndex;
        var items = select.options;
        if(items[x].value!=""){
            $("#fecha_aprobacion").val($(items[x]).attr('data-fecha_aprobacion'));
            $("#fecha_revision_doc").val($(items[x]).attr('data-fecha_revision_doc'));
            $("#fecha_aprobacion").prop('readonly',true);
            $("#fecha_revision_doc").prop('readonly',true);
            }else{
            $("#fecha_aprobacion").prop('readonly',false);
            $("#fecha_revision_doc").prop('readonly',false);
        }
    }
</script>
@endsection
@section('ruta_de_migas')
<ol class="breadcrumb navbar-left">
    <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
    <li class="active"><i class="fa fa-user"></i> {{ $page_title }}</li>
</ol>
@endsection
@section('botones_guardar')
@if (Auth::user()->rol=='admin' or Auth::user()->rol=='director' or Auth::user()->rol=='docente')
<label><input onchange="onchange_continuar_editando(this)" type="checkbox" id="continuar_editando">Mostrar Reporte</label>
<input style="display:block"  type="submit" name="guardar" id="guardar_continuar_editando"value="Guardar y continuar editando" class="btn btn-primary">
<input style="display:none" type="submit" name="guardar" id="guardar_mostrar"value="Guardar y mostrar" class="btn btn-primary">
<script>
function onchange_continuar_editando(input) {
//console.log(input.checked);
if (input.checked){
document.querySelector("#guardar_continuar_editando").style.display='none';
document.querySelector("#guardar_mostrar").style.display='block';
}else{
document.querySelector("#guardar_continuar_editando").style.display='block';
document.querySelector("#guardar_mostrar").style.display='none';
}
}
</script>
@endif
@endsection
@section('scripts')
<script>
    $(document).ready(function() {
        password_en_formulario("admin_asignacion_create");
        required_en_formulario_for("admin_asignacion_create","red","*")
    });
    </script>
<script src="{{ secure_asset('lib/ckeditor/ckeditor.js') }}"></script>
<script>
document.addEventListener("DOMContentLoaded", function(event) {
var editorElement = CKEDITOR.replace( 'metodologia' );
var editorElement2 = CKEDITOR.replace( 'crit_eva' );
});
</script>
@endsection
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
<span id="reporte">
<center>
<table border="1">
    <tr>
        <th rowspan="4">
            <center><img src="{{ secure_asset('img/logo-udenar.png')}}"></center>
            <span id="titulo_logo"><center>Universidad <span class="m10">de</span></center><center><strong>Nariño</strong></center></span>
        </th>
        <th rowspan="4">
            <div class="titulo">FORMACIÓN ACADÉMICA</div>
            <div class="titulo">FACULTAD {{ mb_strtoupper($asigt->nom_fac,'utf-8') }}</div>
            <div class="titulo">PROGRAMA DE {{ mb_strtoupper($asigt->nom_prog,'utf-8') }}</div>
            <div class="titulo negrita"><strong>PROGRAMACIÓN TEMÁTICA ASIGNATURA</strong></div>
        </th>
        <th>Código: FOA-FR-07</th>
    </tr>
    <tr>
        <th>Página 1 de 2</th>
    </tr>
    <tr>
        <th>Versión: 4</th>
    </tr>
    <tr>
        <th>Vigente a partir de: 2011-01-18</th>
    </tr>

</table></center>
<p><strong>1. IDENTIFICACIÓN DE LA ASIGNATURA:</strong></p>
<center>
<table border="1">
    <tr>
        <td><strong>NOMBRE DEL DOCENTE:</strong> {{ $progTem->asigc->docente_fk->name }}</td>
        <td><strong>IDENTIFICACIÓN No.</strong> {{  $progTem->asigc->docente_fk->ident_usu }}</td>
    </tr>
    <tr>
        <td><strong>Correo Electrónico:</strong> {{ $progTem->asigc->docente_fk->email }}</td>
        <td></td>
    </tr>
</table>
</center>
<br>
<style>table{width:100%;}</style>
<table border="1">
    <tr>
        <td class="alineado_izquierda"><strong>NOMBRE DE LA ASIGNATURA EN CURSO: </strong> {{ $progTem->asigc->asigt->nom_asigt }}</td>
    </tr>
</table>
<br>
<form method="post" id="admin_asignacion_create" action="{{ route('admin.asignacion.programaciontematica.update',[
                    'id' => $progTem->asigc_id,
                    ]) }}">
     <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <input  type="hidden" class="form-control{{ $errors->has('fecha_act_progc_temt') ? ' is-invalid' : '' }}" name="fecha_act_progc_temt" value="{{ $errors->has('fecha_act_progc_temt') ? old('fecha_act_progc_temt') : date('Y-m-d') }}" required>
    <input placeholder="" id="asigc_id" type="hidden" class="form-control" name="asigc_id" value="{{ $progTem->asigc_id }}" required >
<table border="1">
    <tr>
        <td class="alineado_izquierda">
                 <div class="row">
                 <div class="form-group">
                            <label for="control_fecha_id" class="col-md-3 col-form-label text-md-right">{{ __('Control de fechas') }}</label>
       @if (Auth::user()->rol=='admin' or Auth::user()->rol=='director')
                            <div class="col-md-3">
                                <select title="Sugerencias para fechas" onchange="sugerir_fechas(this)" name="control_fecha_id" id="control_fecha_id" class="js-example-basic-single form-control{{ $errors->has('control_fecha_id') ? ' is-invalid' : '' }}" >
                                    <option  value="">Fechas Personalizadas</option>
                                    @foreach($controlfechas as $controlfechas_i)
                                    <option data-fecha_aprobacion = "{{ $controlfechas_i->fecha_aprobacion }}" data-fecha_revision_doc = "{{ $controlfechas_i->fecha_revision_doc }}" title='' value="{{ $controlfechas_i->id }}" 
                                        @if ($errors->has('control_fecha_id'))
                                            @if (old('control_fecha_id')==$controlfechas_i->id) {{ __(' selected ') }} @endif;
                                        @else
                                            @if ($progTem->control_fecha_id==$controlfechas_i->id) {{ __(' selected ') }} @endif;
                                        @endif;
                                    >{{ $controlfechas_i->nombre_control." (".$controlfechas_i->per_acad.")" }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('control_fecha_id'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('control_fecha_id') }}</strong>
                                    </span>
                                @endif
        @endif
                            </div>
                        </div>
                    </div>
        </td>
    </tr>
    <tr>
        <td class="alineado_izquierda">
            <label>Fecha de aprobación</label>
            @if (Auth::user()->rol=='admin' or Auth::user()->rol=='director')
              <input id="fecha_aprobacion" name="fecha_aprobacion" type="date" value="{{$progTem->fecha_aprobacion}}"
              @if ($progTem->control_fecha_id!="")
              readonly="readonly"
              @endif
              >
            @else
                {{$progTem->fecha_aprobacion}}
            @endif
        </td>
    </tr>
    <tr>
        <td class="alineado_izquierda">
                <label>Fecha límite para Revisión docente</label>
            @if (Auth::user()->rol=='admin' or Auth::user()->rol=='director')
              <input id="fecha_revision_doc" name="fecha_revision_doc" type="date" value="{{$progTem->fecha_revision_doc}}" <?php 
              if ($progTem->control_fecha_id!=""):
              echo 'readonly="readonly"';
              endif;
              ?> >
            @else
                {{$progTem->fecha_revision_doc}}
            @endif
        </td>
    </tr>
</table>
@if (Auth::user()->rol=='admin' or Auth::user()->rol=='director')
  @yield('botones_guardar')
@endif
</form>
  
<br>
<center>
<form method="post" id="admin_asignacion_create" action="{{ route('admin.asignacion.programaciontematica.update',[
                    'id' => $progTem->asigc_id,
                    ]) }}">
     <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <input  type="hidden" class="form-control{{ $errors->has('fecha_act_progc_temt') ? ' is-invalid' : '' }}" name="fecha_act_progc_temt" value="{{ $errors->has('fecha_act_progc_temt') ? old('fecha_act_progc_temt') : date('Y-m-d') }}" required>
    <input placeholder="" id="asigc_id" type="hidden" class="form-control" name="asigc_id" value="{{ $progTem->asigc_id }}" required >
<table border="1">
    <tr>
        <td>Código de la Asignatura:</td>
        <td colspan="4"> {{ $progTem->asigc->asigt->codigo_asigt }}</td>
    </tr>
    <tr>
        <td>Semestres a los Cuales se ofrece:</td>
        <td colspan="4"> {{ $progTem->asigc->asigt->sem_ofrece_asigt }}</td>
    </tr>
    <tr>
        <td>Intensidad Horaria Semanal: {{ $progTem->asigc->asigt->ihs_asigt }}, Número de Créditos: {{ $progTem->asigc->asigt->n_cred_asigt }}</td>
        @if (Auth::user()->rol=='admin' or Auth::user()->rol=='director' or Auth::user()->rol=='docente')
        <td>Teórica: <input style="width:45px;" type="number" name="horas_practicas" value="{{ $progTem->horas_practicas }}"></td>
        <td>Práctica: <input style="width:45px;" type="number" name="horas_teoricas" value="{{ $progTem->horas_teoricas }}"></td>
        <td>Adicionales: <input style="width:45px;" type="number" name="horas_adicionales" value="{{ $progTem->horas_adicionales }}"></td>
        @else
        <td>Teórica: {{ $progTem->horas_practicas }}</td>
        <td>Práctica: {{ $progTem->horas_teoricas }}</td>
        <td>Adicionales: {{ $progTem->horas_adicionales }}</td>
        @endif 
        <td>Horas Totales: <span id="horas_totales">{{ $progTem->horas_practicas+$progTem->horas_teoricas+$progTem->horas_adicionales }}</span>
    </tr>
    @if (Auth::user()->rol=='admin')
    <tr>
        <td colspan="5">
        
        </td>
    </tr>
    @endif 
</table>
</center>
<br>
<table border="1">
    <tr>
        <td class="alineado_izquierda"><strong>2. JUSTIFICACIÓN: <br></strong>{{ $progTem->asigc->asigt->just_asigt }}</td>
    </tr>
</table>
<br>
<table border="1">
    <tr>
        <td class="alineado_izquierda"><strong>3. OBJETIVOS: <br>
        <p>3.1 Objetivo General</p>
        <p>{{ $progTem->asigc->asigt->obj_gen }}</p>
        <p>3.2 Objetivos Específicos</p>
        <p><?php echo $progTem->asigc->asigt->obj_esp ?></p>
        </strong></td>
    </tr>
</table>
<br>
<span id="metodologia"></span>
<table border="1">
    <tr>
        <td class="alineado_izquierda"><strong>4. METODOLOGÍA:</strong><br>
        @if (Auth::user()->rol=='admin' or Auth::user()->rol=='docente')
         <textarea placeholder="" id="metodologia" class="form-control{{ $errors->has('metodologia') ? ' is-invalid' : '' }}" name="metodologia" required >{{ $errors->has('metodologia') ? old('metodologia') : $progTem->metodologia }}</textarea>
                                @if ($errors->has('metodologia'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('metodologia') }}</strong>
                                    </span>
                                @endif
            
        @else
        {{ $progTem->metodologia }}
        @endif
        </td>
    </tr>
</table>
<br>
<span id="criteriosevaluacion"></span>
<table border="1">
    <tr>
        <td class="alineado_izquierda"><strong>5. CRITERIOS DE EVALUACIÓN:</strong><br>
        
        @if (Auth::user()->rol=='admin' or Auth::user()->rol=='director' or Auth::user()->rol=='docente')
        <textarea placeholder="" id="crit_eva" class="form-control{{ $errors->has('crit_eva') ? ' is-invalid' : '' }}" name="crit_eva" required >{{ $errors->has('crit_eva') ? old('crit_eva') : $progTem->crit_eva }}</textarea>
                                @if ($errors->has('crit_eva'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('crit_eva') }}</strong>
                                    </span>
                                @endif
         @else
        {{ $progTem->crit_eva }}
        @endif
        </td>
    </tr>
</table>

<div class="form-group row mb-0">
<br>
                            <div class="col-md-6 offset-md-4">
                                @yield('botones_guardar')
                            </div>
                        </div>
</form>
<hr>
<p><strong>6. CONTENIDO DE LA ASIGNATURA:</strong></p>
<table border="1">
    <tr>
        <td><strong>Horas ó Créditos</strong></td>
        <td><strong>Tema ó Capitulo</strong></td>
    </tr>
    <?php 
    $total_horas = 0;
    ?>
        @foreach ($asigt->asigc->asigt->contasigts as $cont_asig_i)
    <tr>
        <td>{{ $cont_asig_i->hras_cont_asigt }}</td>
        <td><?php echo $cont_asig_i->tem_cap_cont_asigt ?></td>
    </tr>
    <?php 
    $total_horas += $cont_asig_i->hras_cont_asigt;
    ?>
    @endforeach
     <tr>
        <td><strong>Total: {{ $total_horas }}</strong></td>
        <td><strong></strong></td>
    </tr>
    
</table>
<br>
  <span id="puntoadicional"></span>
<p><strong>7. PUNTO ADICIONAL Y OPCIONAL QUE APLICA A AQUELLOS PROGRAMAS QUE UTILIZAN OTROS FACTORES EN LA PROGRAMACIÓN TEMÁTICA POR ASIGNATURA, Ej. Competencias, Habilidades, etc</strong></p>
<span id="punto_adic">
                @if (Auth::user()->rol=='admin' or Auth::user()->rol=='director' or Auth::user()->rol=='docente')
                  <form method="post" action="{{ route('admin.asignacion.programaciontematica.puntoadic.store',[$progTem->id])}}">
                  <table border="1">
                  <tr>
                  <td>
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <input required placeholder="Item"  type="text" class="form-control{{ $errors->has('punto_adic') ? ' is-invalid' : '' }}" name="item" value="">
                  </td>
                  <td>
                  <input required placeholder="Descripción"  type="text" class="form-control{{ $errors->has('punto_adic') ? ' is-invalid' : '' }}" name="valor" value="">
                  </td> 
                  <td>
                      <button type="submit" class="btn btn-primary">Agregar</button>
                  </td>
                  </tr>
                 </table>
                  </form>
                  @endif
                 <table>

                @foreach ($progTem->punto_adics as $punto_adic_i)
                    <tr>
                        <td><li>{{ $punto_adic_i->item }}</li></td>
                        <td><li>{{ $punto_adic_i->valor }}</li></td>
                        @if (Auth::user()->rol=='admin' or Auth::user()->rol=='director' or Auth::user()->rol=='docente')
                        <td><a  class="btn btn-danger" href="{{route('admin.asignacion.programaciontematica.puntoadic.delete',[$progTem->asigc_id,$punto_adic_i->id])}}" onclick="return confirm('¿Esta seguro de eliminar este punto adicional?')"><span title="Eliminar" class="glyphicon glyphicon-trash"></span></a></td>
                        @endif
                    </tr>
                 @endforeach
                 </table>
                 <hr>
            </span>
            @if ($errors->has('punto_adic'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('punto_adic') }}</strong>
                </span>
            @endif
<br>
<span id="bibliografia"></span>
<table border="1">
    <tr>
        <td class="alineado_izquierda"><strong>8. REFERENCIAS BIBLIOGRÁFICAS</strong>
      <p>Se recomienda utilizar el formato <a target="_blank" href="https://normasapa.com/"><abbr  title="Ir a https://normasapa.com/">APA</abbr></a> para su bibliogrfía</p>
      </td>
    </tr>
    <tr class="alineado_izquierda">
        <td>
 <span id="refe_biblio">
      @if (Auth::user()->rol=='admin' or Auth::user()->rol=='director' or Auth::user()->rol=='docente')
                                     <form method="post" action="{{ route('admin.asignacion.programaciontematica.refebiblio.store',[$progTem->id])}}">
                                          <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                      <textarea required placeholder="Referencia Bibliográfica"  type="text" class="form-control{{ $errors->has('refe_biblio') ? ' is-invalid' : '' }}" name="refe_biblio" value=""></textarea>
                                      <br><button type="submit" class="btn btn-primary">Agregar</button>
                                      </form>
                                      <hr>
    @endif
                                      <ul>
@foreach ($progTem->refe_biblios as $refe_biblio_i)
    <li>{{ $refe_biblio_i->refe_biblio }}
        @if (Auth::user()->rol=='admin' or Auth::user()->rol=='director' or Auth::user()->rol=='docente')
            &nbsp;&nbsp;<a class="btn btn-danger" href="{{route('admin.asignacion.programaciontematica.refebiblio.delete',[$progTem->id,$refe_biblio_i->id])}}" onclick="return confirm('¿Esta seguro de eliminar esta referencia bibliográfica?')"><span title="Eliminar" class="glyphicon glyphicon-trash"></span></a>
        @endif
    </li>
 @endforeach
                                     </ul>
                                </span>
                                @if ($errors->has('refe_biblio'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('refe_biblio') }}</strong>
                                    </span>
                                @endif
        </td>
    </tr>
</table>
<br>
<br>
</span>
    <span style="display:none;" >

          <input name="estado" value="En Proceso" type="hidden">
          <input name="estado" value="Listo" id="switch-state" type="checkbox" @if($asigt->estado=="Listo") checked @endif >
    {{$asigt->estado}}
    </span>
    <div class="row">
      <div class="col-md-12">
        <center>
        <h2 class="h4">Estado de esta Programación Temática</h2>
        <p>
        </p>
        <div class="btn-group">
          <button type="button" onclick="return estado(this, false)" id="enproceso" class="estado btn btn-warning">En Proceso</button>
          <button type="button" onclick="return estado(this, true)" id="listo" class="estado btn btn-success">Listo</button>
        </div>
          </center>
      </div>
    </div>
    <script>
      function estado(obj, estado){
        var label_estado = estado ?  'Listo' : 'En Proceso';
        if(confirm("Esta seguro que desea cambiar a estado "+label_estado)){
               guardar_estado('prog_tem', 'estado', label_estado, estado,"switch-state","{{route('estado_doc',[$progTem->id,$progTem->asigc_id])}}");
           }else{
             return false;
           }
      }    
      document.addEventListener("DOMContentLoaded", function(event) {
        alplicar_estilos();
      });
     
    </script>
<style>
  .estado{
    /*
    color: #000 !importat;
    background-color: #eeeeee !importat;
    border-color: #eeeeee !importat;
    */
  }
  #enproceso~switch-state::checked{
    color: red !importat;
    /*
    background-color: #cbb956 !importat;
    border-color: #c5b143 !importat;
    */
  }
  #listo ~ #switch-state::checked{
    color: red !importat;
    /*
    background-color: #2ab27b !importat;
    border-color: #259d6d !importat;
    */
  }
</style>
@endsection
