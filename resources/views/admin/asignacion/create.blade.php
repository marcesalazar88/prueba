@extends('layouts.app')
@section('vars')
{!!
$page_title = 'Asignación de asignaturas'; 
$route = 'asignacion'; 
!!}
@endsection
@section('ruta_de_migas')
<ol class="breadcrumb">
    <li><a href="{{ route('home') }}"> Inicio</a></li>
    <li><a href="{{ route('admin.asignacion.index') }}"> {{ $page_title }}</a></li>
    <li class="active"> Registrar</li>
</ol>
@endsection
@section('barra_buscar_cuerpo')
@endsection
@section('scripts')
<script>
    $(document).ready(function() {
        password_en_formulario("admin_asignacion_create");
        required_en_formulario_for("admin_asignacion_create","red","*")

  
          $("#cod_prog").change(function() {
          //var plan = $("#plan").val();
          var cod_prog = $("#cod_prog").val();
          var obj_prog = document.getElementById("cod_prog");
          var nom_prog = obj_prog.options[obj_prog.selectedIndex].text;
          var parametros = {
            "_token": $("meta[name='csrf-token']").attr("content"),
            'cod_prog':cod_prog
          };
                  $.ajax({
                          dataType: 'json',
                          data:  parametros,
                          url:   '{{route('admin.asignacion.consultar_planes_prog')}}',
                          type:  'post',
                          beforeSend: function () {
                              //$("#plan").html(`<tr><td colspan="${num_data}">Procesando, espere por favor...</td></tr>`);
                          },
                          success:  function (response) {
                            //console.log(response);
                              //var datos = response;//ojo Crud devuelve .data con paginate
                              $("#plan").html('<option value="">Seleccione un plan ('+nom_prog+')</option>');
                              for (x in response){
            $('#plan').append('<option value="' + x + '">' + response[x] + '</option>');
                              }

                          }
                  });//ajax
          });//change
});//ready
    </script>

@endsection
@section('fdsfdf')
<script>
$(document).ready(function(){
$("#buscar").keyup(function(){
  buscar_planes_as();
});
$("#buscar").change(function(){
  buscar_planes_as();
});
$("#search-btn").click(function(){
  buscar_planes_as();
});
$("#num_resultados").keyup(function(){
  buscar_planes_as();
});
$("#num_resultados").change(function(){
  buscar_planes_as();
});
$("#menu_planes").addClass('active');
buscar_planes_as();
});
function buscar_planes_as(valor='', page=1){
var a5d546a503db8d=false;
var obj_a5d546a503db8d = document.getElementById('a5d546a503db8d');
if (obj_a5d546a503db8d){ 
  a5d546a503db8d=obj_a5d546a503db8d.value=='true';
}
var modulo = 'planes';
foreign = {
    'cod_prog':{'tabla':'prog', 'campo':'nom_prog'},
};

label_data = {
        'nombre':'Nombre',
        'adjunto':'Acuerdo',
        'fecha':'Fecha',
        'cod_prog':'Programa'
};

response_data = {
        'nombre':'nombre',
        'adjunto':'adjunto',
        'fecha':'fecha',
        'cod_prog':'cod_prog'
};
all_data = {
    pk:'id',
    'label_data':label_data,
    'response_data':response_data,
    'foreign':foreign,
};
detalle = true;
  //
  if (valor=='') 
var obj_valor = document.getElementById('buscar');
if (obj_valor)
var buscar_arreglo = document.getElementById('buscar_arreglo');
if (buscar_arreglo && buscar_arreglo.value=="SI"){
    buscar_arreglo_value = '1';
}else{
    buscar_arreglo_value = '0';
}
valor = obj_valor.value;
var obj_resultados = document.getElementById('num_resultados')
var resultados = (obj_resultados) ? document.getElementById('num_resultados').value : 10;
if (resultados>0){
var num_data = all_data.length;
var parametros = {};
        $.ajax({
                dataType: 'json',
                data:  parametros,
                url:   `?json=1&ba=${buscar_arreglo_value}&page=${page}&resultados=${resultados}&valor_clave=${valor}`,
                type:  'get',
                beforeSend: function () {
                    $("#txt_resultados").html(`<tr><td colspan="${num_data}">Procesando, espere por favor...</td></tr>`);
                },
                success:  function (response) {
                    //var datos = response;//ojo Crud devuelve .data con paginate
                    document.querySelector('#txt_resultados').innerHTML =  tabla(response,modulo,all_data,detalle, a5d546a503db8d);
                    ajustar_pagina(response,modulo);
                    setTimeout(function(){
                        dismiss_alert();
                    },4000);
                }
        });
}
  //
}
</script>
@endsection
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <center><h1>{{ $page_title }}</h1></center>
            <div class="card">
                <div class="card-header"></div>

                <div class="card-body">
                  <center>
              <a class="th-btn btn btn-primary" href="{{ route('admin.asignacion.createindividual') }}" title="Nuevo">Nueva asignación</a>
                  <br> <br>
<form nethod="get" action="{{ route('admin.asignacion.createplan') }}">
  
   <div class="input-group sidebar-form">
      <label for="buscar">Seleccione un programa&nbsp;</label>
    <select style="width:100%;background-color: #FFF;color:black;width:170px;display:inline"  id="cod_prog" class="js-example-basic-single form-control">
      <option value="">Seleccione un programa</option>
      @foreach ($prog as $id => $prog_i)
      @if(Funciones::permiso_depto('cod_prog',$prog_i->cod_prog))
      <option value="{{$prog_i->cod_prog}}">{{$prog_i->nom_prog}}</option>
      @endif
      @endforeach
    </select>
    </div>  
   <div class="input-group sidebar-form">
      <label for="buscar">Seleccione un plan&nbsp;</label>
    <select required style="width:100%;background-color: #FFF;color:black;width:170px;display:inline" name="plan" id="plan" class="js-example-basic-single form-control">
      <option value="">Seleccione un plan</option>
    </select>
    </div>
              <button type="submit" class="th-btn btn btn-secondary"  title="Nuevo">Asignación de Plan de estudios</button>
                    </form>
                </center>
          </div>
            </div>
          <a href="{{ route('admin.asignacion.index') }}" class="btn btn-secondary hidden-print">Regresar</a>
        </div>
    </div>
</div>
@endsection
