@extends('layouts.app')
@section('vars')
{!!
$facultad = "Ciencias Exactas y Naturales";
$programa = "Licenciatura en Informática";
$facultad = mb_strtoupper($facultad,'utf-8');
$programa = mb_strtoupper($programa,'utf-8');
$page_title = 'Seguimiento'; 
!!}
@endsection
@section('ruta_de_migas')
<ol class="breadcrumb navbar-left">
    <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
    <li class="active"><i class="fa fa-user"></i> {{ $page_title }}</li>
</ol>
@endsection
@section('barra_buscar')
<!--li><a onclick="window.print()" title="Imprimir">
<span class="glyphicon glyphicon-print">
</a></li-->
@endsection
@section('scripts')
  <style>
  ul:not(:empty)+.button_mas {
    display: none !important;
}
  </style>
<?php //use Auth; ?>
@include('reportes.css')
<style>
    li.list-group-item:hover{
        background-color: var(--hover_seguimiento);
    }
    .cabecera_seguimiento{
        background-color: var(--cabecera_seguimiento);
    }
</style>

<script>
  $(document).ready(function() {
  //atajos de teclado
$("#btn-input").bind('keydown', function(e) {
tecla=(document.all) ? e.keyCode : e.which;
if (tecla==13){ //Enter
//acciones
  enviar();
}
  });
});
    function sugerir_fechas(select){
        var x = select.selectedIndex;
        var items = select.options;
        if(items[x].value!=""){
            $("#fecha_aprobacion").val($(items[x]).attr('datafechaaprobacion'));
            $("#fecha_revision_doc").val($(items[x]).attr('datafecharevisiondoc'));
            $("#fecha_revision_est").val($(items[x]).attr('datafecharevisionest'));
            //$("#fecha_aprobacion").prop('readonly',true);
            //$("#fecha_revision_doc").prop('readonly',true);
            //$("#fecha_revision_est").prop('readonly',true);
            }else{
            //$("#fecha_aprobacion").prop('readonly',false);
            //$("#fecha_revision_doc").prop('readonly',false);
            //$("#fecha_revision_est").prop('readonly',false);
        }
    }
</script>
<script src="{{ secure_asset('ckeditor/ckeditor.js') }}"></script>
<script>
function alertas(msg,tipo,destino='alerts'){
var resultado ='';
resultado +=`<div class="alert alert-${tipo} alert-dismissible">`;
resultado +='<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
resultado += msg;
resultado += '</div>';
document.getElementById(destino).innerHTML = resultado;
}
function guardar_observaciones(){
    var observ_docnt = $("#observ_docnt");
    if (observ_docnt){
    $("#observ_docnt").change(function(){
    var observ_docnt = $("#observ_docnt").val();
    var parametros = { 'observ_docnt' : observ_docnt };
    guardar_obs(parametros);
    });
    $(".control_fechas").change(function(){
        guardar_fechas();
    }); 
    $("#control_fecha_id").change(function(){
        guardar_fechas();
    }); 
}
    var observ_est = $("#observ_est");
    if (observ_est){
    $("#observ_est").change(function(){
    var observ_est = $("#observ_est").val();
    var parametros = { 'observ_est' : observ_est };
    guardar_obs(parametros);
    });
    }
    //console.log(parametros);
}
function guardar_fechas(){
    var parametros = {
        'control_fecha_id' : $("#control_fecha_id").val(),
        'fecha_aprobacion' : $("#fecha_aprobacion").val(),
        'fecha_revision_doc' : $("#fecha_revision_doc").val(),
        'fecha_revision_est' : $("#fecha_revision_est").val()
    }
    //console.log(parametros);
        $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                dataType: 'json',
                data:  parametros,
                url:   '?obs',
                type:  'post',
                beforeSend: function () {
                    alertas('Procesando, espere por favor...','info','mensajes_control_fecha');
                },
                success:  function (response) {
                  if (response == "1"){
                   //console.log('Exito');
                   alertas('Fechas Actualizadas','success','mensajes_control_fecha');
                  }else{
                    //console.log('Error');
                    alertas('Error al guardar','warning','mensajes_control_fecha');
                  }
                    setTimeout(function(){
                        dismiss_alert();
                    },4000);
                }
        });
}
function guardar_obs(parametros){
        $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                dataType: 'json',
                data:  parametros,
                url:   '?obs',
                type:  'post',
                beforeSend: function () {
                    alertas('Procesando, espere por favor...','info','mensajes_observaciones');
                },
                success:  function (response) {
                  if (response == "1"){
                   console.log('Exito');
                   alertas('Observación Actualizada','success','mensajes_observaciones');
                  }else{
                    console.log('Error');
                    alertas('Error al guardar','warning','mensajes_observaciones');
                  }
                    setTimeout(function(){
                        dismiss_alert();
                    },4000);
                }
        });
}
  
function guardar_por(parametros){
        $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                dataType: 'json',
                data:  parametros,
                url:   '?por',
                type:  'post',
                beforeSend: function () {
                    alertas('Procesando, espere por favor...','info','mensajes_antestabla');
                },
                success:  function (response) {
                   console.log(response);
                  if (response == "1"){
                   console.log('Exito');
                   alertas('Observación Actualizada','success','mensajes_antestabla');
                  }else{
                    console.log('Error');
                    alertas('Error al guardar','warning','mensajes_antestabla');
                  }
                    setTimeout(function(){
                        dismiss_alert();
                    },4000);
                }
        });
}

//push
function push(msg='',timestamp=0){
  parametros={'msg':msg,'timestamp':timestamp};
         $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                dataType: 'json',
                data:  parametros,
                url:   '{{route('push')}}',
                type:  'get',
                beforeSend: function () {
                  if (msg!='') $("#btn-chat").html('Enviando...');
                },
                success:  function (response) {
                  //console.log(response.timestamp);//prueba de funcion a desencadenar
                  lista_mensajes();
                  if (msg!=''){ 
                   $("#btn-chat").html('Enviado');
                    setTimeout(function(){
                        $("#btn-chat").html('Enviar');
                    },4000);
                  }else{
                  push(msg='',response.timestamp);
                  }
                },
                error:function () {
                    console.log('reintentando')
                    push('',timestamp);
                }
        });
}
  function lista_mensajes(){
   
          $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data:  parametros,
                url:   '{{ route('mensaje.show',[$id])}}',
                type:  'get',
            
                beforeSend: function () {
                   //$("#lista_mensajes").html("<li>Procesando, espere por favor...</li>");
                },
                success:  function (response) {
                $("#lista_mensajes").html(atob(response));
                document.querySelector(".mensajes .panel-body").scrollTo(0, 50000);
                 }
        });

  }
  /*
  $(".editar_valoracion").change(function(){
    var ide = $(this).attr('ide');
    var valoracion = $(this).val();
    fn_editar_valoracion(ide,valoracion);
  });

  $(".guardar_valoracion").click(function(){
    guardar_valoracion(this);
  });
  */
  function guardar_valoracion(obj){
    var ide = $(obj).attr('ide');
    //console.log("ok "+ide);
    var valoracion = $('#editar_valoracion_'+ide).val();
    fn_editar_valoracion(ide,valoracion);
    $('#cerrar_'+ide).click();
    
  }
  function eliminar_valoracion(obj){
    var ide = $(obj).attr('ide');
    //console.log("ok "+ide);
    var valoracion = $('#editar_valoracion_'+ide).val();
    fn_eliminar_valoracion(ide);
    $('#cerrar_'+ide).click();
    
  }
  
  function fn_noeditar_valoracion(ide){
     $('#editar_valoracion_'+ide).val($("#valoracion_"+ide).html());
  }
  
  function fn_editar_valoracion(ide,valoracion){
    if (ide!=''){
  var parametros={
    'asigc' : '{{$asigc}}',
    'ide' : ide,
    'valoracion' : valoracion
  };
  //  console.log(parametros);
    
  $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                dataType: 'json',
                data:  parametros,
                url:   '{{ route('admin.seguimieto.actualizar_item',['id' => $seg_id]) }}',
                type:  'post',
                beforeSend: function () {
                    //$("#btn-chat").html('Enviando...');
                },
                error:  function (response) {
                    $('#editar_valoracion_'+ide).val()=$("#valoracion_"+ide).html();
                },
                success:  function (response) {
                  if (response.estado == "1"){
                      //console.log('valoracion_'+ide);
                      $("#valoracion_"+ide).html(valoracion);
                  }else{
                    console.log('Error');
                  }
                }
        });
  }
  }
  function fn_eliminar_valoracion(ide){
    if (ide!=''){
  var parametros={
    'asigc' : '{{$asigc}}',
    'ide' : ide
  };
   // console.log(parametros);
    
  $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                dataType: 'json',
                data:  parametros,
                url:   '{{ route('admin.seguimieto.eliminar_item',['id' => $seg_id]) }}',
                type:  'post',
                beforeSend: function () {
                    //$("#btn-chat").html('Enviando...');
                },
                error:  function (response) {
                    $('#editar_valoracion_'+ide).val()=$("#valoracion_"+ide).html();
                },
                success:  function (response) {
                  if (response.estado == "1"){
                      //console.log('valoracion_'+ide);
                      $("#valoracion_"+ide).html('');
                  }else{
                    console.log('Error');
                  }
                }
        });
  }
  }
  function enviar(texto=''){
  if (texto==''){
    texto = document.getElementById('btn-input').value;
  }
  if (texto!=''){
  var parametros={
    'asigc_id' : {{$id}},
    'de' : {{$user->ident_usu}},
    'texto' : texto
  };
    console.log(parametros);
    
  $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                dataType: 'json',
                data:  parametros,
                url:   '{{route('mensaje.store')}}',
                type:  'post',
                beforeSend: function () {
                    $("#btn-chat").html('Enviando...');
                },
                success:  function (response) {

                  if (response.estado == "1"){
                       push('1');
                       document.getElementById('btn-input').value='';
                       $("#btn-chat").html('Enviado');
                        setTimeout(function(){
                            $("#btn-chat").html('Enviar');
                        },4000);
                  }else{
                    console.log('Error');
                    alertas('Error al guardar','warning','mensajes_footer');
                    setTimeout(function(){
                        dismiss_alert();
                    },4000);
                  }
                }
        });
  }
}

document.addEventListener("DOMContentLoaded", function(event) {
guardar_observaciones();
push();
});
/*
var editorobservaciones = CKEDITOR.replace( 'observaciones' );
var editorvaloracion = CKEDITOR.replace( 'valoracion' );
*/
</script>
 <style>
 .mensajes .chat
{
    list-style: none;
    margin: 0;
    padding: 0;
}

.mensajes .chat li
{
    margin-bottom: 10px;
    padding-bottom: 5px;
    border-bottom: 1px dotted #B3A9A9;
}

.mensajes .chat li.left .chat-body
{
    margin-left: 60px;
}

.mensajes .chat li.right .chat-body
{
    margin-right: 60px;
}


.mensajes .chat li .chat-body p
{
    margin: 0;
    color: #777777;
}

.mensajes .panel .slidedown .glyphicon, .chat .glyphicon
{
    margin-right: 5px;
}

.mensajes .panel-body
{
    overflow-y: scroll;
    height: 250px;
}
/*
::-webkit-scrollbar-track
{
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
    background-color: #F5F5F5;
}

::-webkit-scrollbar
{
    width: 12px;
    background-color: #F5F5F5;
}

::-webkit-scrollbar-thumb
{
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
    background-color: #555;
}
*/
 </style>
@endsection
@section('content')
<?php
$seguimiento = $asigc->progtems()->first()->segs->first();
?>
<span id="reporte">
<table border="1">
    <tr>
        <td class="alineado_izquierda">
                 <div class="row">
                 <div class="form-group">
                            <label for="control_fecha_id" class="col-md-3 col-form-label text-md-right">{{ __('Control de fechas') }}</label>
        @if (Auth::user()->rol=='admin' or Auth::user()->rol=='director')
                            <div class="col-md-3">
                                <select title="Sugerencias para fechas" onchange="sugerir_fechas(this)" name="control_fecha_id" id="control_fecha_id" class="js-example-basic-single form-control{{ $errors->has('control_fecha_id') ? ' is-invalid' : '' }}" >
                                    <option  value="">Fechas Personalizadas</option>
                                    @foreach($controlfechas as $controlfechas_i)
                                    <option datafechaaprobacion = "{{ $controlfechas_i->fecha_aprobacion }}" datafecharevisiondoc = "{{ $controlfechas_i->fecha_revision_doc }}" datafecharevisionest = "{{ $controlfechas_i->fecha_revision_est }}" title='' value="{{ $controlfechas_i->id }}" 
                                        @if ($errors->has('control_fecha_id'))
                                            @if (old('control_fecha_id')==$controlfechas_i->id) {{ __(' selected ') }} @endif;
                                        @else
                                            @if ($seguimiento->control_fecha_id==$controlfechas_i->id) {{ __(' selected ') }} @endif;
                                        @endif;
                                    >{{ $controlfechas_i->nombre_control." (".$controlfechas_i->per_acad.")" }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('control_fecha_id'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('control_fecha_id') }}</strong>
                                    </span>
                                @endif
        @endif
                            </div>
                        </div>
                    </div>
        </td>
    </tr>
    <tr>
        <td class="alineado_izquierda">
            <label>Fecha de aprobación</label>
            @if (Auth::user()->rol=='admin' or Auth::user()->rol=='director')
              <input id="fecha_aprobacion" class="control_fechas form-control" style="width:auto !important"; name="fecha_aprobacion" type="date" value="{{$seguimiento->fecha_aprobacion}}"
              <?php /*
              @if ($seguimiento->control_fecha_id!="")
              readonly="readonly"
              @endif
              */ ?>
              >
            @else
                {{$seguimiento->fecha_aprobacion}}
            @endif
        </td>
    </tr>
    <tr>
        <td class="alineado_izquierda">
                <label>Fecha límite para Revisión docente</label>
            @if (Auth::user()->rol=='admin' or Auth::user()->rol=='director')
              <input id="fecha_revision_doc" class="control_fechas form-control" style="width:auto !important"; name="fecha_revision_doc" type="date" value="{{$seguimiento->fecha_revision_doc}}" <?php 
              /*
              if ($seguimiento->control_fecha_id!=""):
              echo 'readonly="readonly"';
              endif;
              */
              ?> >
            @else
                {{$seguimiento->fecha_revision_doc}}
            @endif
        </td>
    </tr>
    <tr>
        <td class="alineado_izquierda">
                <label>Fecha límite para Revisión estudiante</label>
            @if (Auth::user()->rol=='admin' or Auth::user()->rol=='director')
              <input id="fecha_revision_est" class="control_fechas form-control" style="width:auto !important"; name="fecha_revision_est" type="date" value="{{$seguimiento->fecha_revision_est}}" <?php 
              /*
              if ($seguimiento->control_fecha_id!=""):
              echo 'readonly="readonly"';
              endif;
              */ ?> >
            @else
                {{$seguimiento->fecha_revision_est}}
            @endif
        </td>
    </tr>
</table>
<div id="mensajes_control_fecha"></div>
<br>
<center>
<table border="1">
    <tr>
        <th rowspan="4">
            <center><img src="{{ secure_asset('img/logo-udenar.png')}}"></center>
            <span id="titulo_logo"><center>Universidad <span class="m10">de</span></center><center><strong>Nariño</strong></center></span>
        </th>
        <th rowspan="4">
            <div class="titulo">FORMACIÓN ACADÉMICAs</div>
            <div class="titulo">FACULTAD {{ $facultad }}</div>
            <div class="titulo">PROGRAMA DE {{ $programa }}</div>
            <div class="titulo negrita"><strong>SEGUIMIENTO AL CONTENIDO POR ASIGNATURA</strong></div>
        </th>
        <th>Código: FOA-FR-14</th>
    </tr>
    <tr>
        <th>Página 1 de 1</th>
    </tr>
    <tr>
        <th>Versión: 4</th>
    </tr>
    <tr>
        <th>Vigente a partir de: 2011-01-24</th>
    </tr>

</table></center>
<br>
<div id="mensajes_antestabla">
</div>
<?php
$pa = $criterios['PA'];
unset($criterios['PA']);
?>
  
<!-- Modal -->
<div id="crear_nueva_valoracion" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Nueva Valoración</h4>
      </div>
      <div class="modal-body">
          <form method="post" action="{{ route('admin.seguimieto.guardar_item',[
                    'id' => $seg_id,
                    'id_asigc' => $id,
                    ]) }}" onsubmit="document.getElementById('cerrar_crear_nueva_valoracion').click()">
            <meta name="csrf-token" content="{{ csrf_token() }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <div class="row">
            <input type="hidden" name="seg_id" value="{{ $seg_id }}" required>
       <div class="col-md-12" style="display:none">
                        <label for="">{{ $nombres['tipo_valoracion'] }}
							<div class="wrap">
								<div class="formulario">
									<div class="radio">
									    @foreach ($tipovaloracion as $tipo)
										<div class="col-md-6">
										    <label for="labeltipo{{ $tipo->name }}" title="{{ $tipo->descripcion }}">
    										<input class="labeltipo" id="labeltipo{{ $tipo->name }}" type="radio" name="tipo" value="{{ $tipo->name }}" required>
    										<strong>{{ $tipo->name }}</strong></label>
										</div>
										@endforeach  
									</div>
								</div>
							</div>
						</label>
			</div>
			<div class="col-md-12" style="display:none">
            <label for="">{{ $nombres['criterio'] }}
            {!!
            Form::select('aspecto', $criterios, 
                                ''
                                , [
                                'id' => 'aspecto',
                                'class' => 'form-control',
                                'placeholder' => "Seleccione ".$nombres['criterio']." a valorar",
                                'required',
                                ])
            !!}
            </label>
			</div>
       <div>
         <p id="texto_mostrar_nuevo" style="text-align: center;font-weight: bold;"></p>
        </div>
			<div class="col-md-12" id="ideditorvaloracion">
			     <textarea style="width:100%" id="valoracion" name="valoracion" required></textarea> 
			</div>
			<div class="col-md-12">
			    <center><br>
			    <input class="btn btn-success" type="submit" class="guardar" value="Guardar">
			    </center>
			</div>
		</div>
			</form>
      </div>
      <div class="modal-footer">
        <button type="button" id="cerrar_crear_nueva_valoracion" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>

  </div>
</div><!-- Modal -->
  <script>
function mostrar_nuevo(){
  var labeltipo = $(".labeltipo:checked").val();
  //ar aspecto = $("#aspecto");
  var aspecto = document.getElementById("aspecto");
  $("#texto_mostrar_nuevo").html(labeltipo+" en "+aspecto.options[aspecto.selectedIndex].innerHTML);
}
  </script>
<form method="post"  action="{{ route('admin.seguimieto.guardar_voto') }}">
<meta name="csrf-token" content="{{ csrf_token() }}">
<input type="hidden" name="_token" value="{{ csrf_token() }}">
<table border="1">
    <tr>
        <td colspan="1" style="width:20%;padding: 0px;margin:0px;"></td>
        <td colspan="1" style="width:20%;padding: 0px;margin:0px;"></td>
        <td colspan="1" style="width:20%;padding: 0px;margin:0px;"></td>
        <td colspan="1" style="width:20%;padding: 0px;margin:0px;"></td>
        <td colspan="1" style="width:20%;padding: 0px;margin:0px;"></td>
    </tr>
    
    <tr>
        <td colspan="2"><strong>SEMESTRE</strong></td>
        <td colspan="3"><strong>DOCENTE</strong></td>
    </tr>
    <tr>
        <td colspan="2"><strong>FECHA DE REVISIÓN</strong></td>
        <td colspan="3"><strong>AREA CURRICULAR</strong></td>
    </tr>
    <tr>
        <td><strong>ASIGNATURA:</strong></td>
        <td colspan="4">
            <!--pre><?php #print_r($asigc); ?></pre-->
        </td>
    </tr>
    <tr>
        <td colspan="5">
			<!-- Trigger the modal with a button -->
<?php /*
@if ($user->rol=="docente")
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#crear_nueva_valoracion">Crear Nueva Valoración</button>
@endif
*/ ?>
        </td>
    </tr>
    </table>
  <script>
document.addEventListener("DOMContentLoaded", function(event) {
  var por_avance = document.getElementById('por_avance');
  if(por_avance){
  por_avance.addEventListener('input',function(){
    range_por_avance.innerHTML = this.value;
  });
  por_avance.addEventListener('change',function(){
    console.log(this.value);//observ_est
    var ide = this.getAttribute('ide');
    var parametros = { 'pa' : this.value, 'id' : ide };
    guardar_por(parametros);
    
  });
}
});
  </script>
    <table border="1" class="table cssresponsive">
       <?php
        $seguim = $asigc->progtems()->first()->segs->first()->seg_vals()->get();
        $seguimiento = $asigc->progtems()->first()->segs->first();
        //dd($seguim);
        ?>
         <tr class="">
        <td colspan="1" style="min-width: 250px;">{{$pa}}</td>
        <td colspan="{{ count($tipovaloracion)*2}}">
    @foreach ($seguim as $itemseg)
    @if ('PA'==$itemseg->aspecto)
           <span style="display:inline"><span id="range_por_avance">{{$itemseg->valoracion}}</span>%</span>
            @if (Auth::user()->rol=='docente')
             <input class="ps7" id="por_avance" ide="{{$itemseg->id}}" type="range" style="width:100%;margin-bottom:10px;" class="form-control" name="por_avance" value="{{$itemseg->valoracion}}">
           @endif
    @endif
    @endforeach

      </td>
    </tr>
    <tr class="cabecera_seguimiento">
        <td colspan="1" style="min-width: 250px;"></td>
        @foreach ($tipovaloracion as $itemval)
        <td colspan="2"><strong>{{ $itemval->descripcion }}:</strong><br>(estudiantes y docentes)</td>
        @endforeach
    </tr>
    @foreach ($criterios as $id => $itemcriterio)
       <tr >
        <td colspan="1">{{$itemcriterio}}</td>
    <?php $total_cols=0; ?>
    @foreach ($tipovaloracion as $itemval)
    <?php $total_cols++; ?>
        <td colspan="2">
          <span class="hidden-sm hidden-md hidden-lg">{{$itemval->descripcion}}</span>
    <ul class="list-group list-group-flush">
        <?php
        $seguim = $asigc->progtems()->first()->segs->first()->seg_vals()->get();
        $seguimiento = $asigc->progtems()->first()->segs->first();
        //dd($seguim);
        ?>
                        @foreach ($seguim as $itemseg)
                            @if ($itemval->name==$itemseg->tipo)
                                @if ($id==$itemseg->aspecto)
  <li class="list-group-item" >
                  <!--div style="border:solid 1px #000; margin:15px"--> 
                                    <p>
                                    <!-- Trigger the modal with a button -->
                                    <span  data-toggle="modal" data-target="#valoraciontxt_{{$itemseg->id }}" title="Haga clic para ver más" style="cursor:pointer;padding-right: 10px;" id="valoracion_{{$itemseg->id }}"><?php 
                                    //echo $itemseg->seg_id." ->";
                                    echo Funciones::puntos_suspensivos($itemseg->valoracion,150);
                                    ?></span>

                                    @if ($itemseg->voto=="Pendiente")
                                    <span style="float:right;background-color: orange; color: white;" class="label label-success" title="Pendiente">P</span><br>
                                    @endif
                                    @if ($itemseg->voto=="Aprueba")
                                    <span style="float:right;background-color: green; color: white;" class="label label-success" title="Aprueba">A</span><br>
                                    @endif
                                    @if ($itemseg->voto=="Rechaza")
                                    <span style="float:right;background-color: red; color: white;" class="label label-success" title="Rechaza">R</span><br>
                                    <p><strong>Argumentación:</strong> {{$itemseg->replica}}</p>
                                    @endif
                                    </p>
                                    @if ($user->rol=="estudiante")
                                   <?php #if ($itemseg->voto=="Pendiente"){?>
                                        <input type="hidden" name="id[{{$itemseg->id }}]" value="{{$itemseg->id }}">
                                        <label class="labelaprueba">
                                            <input required type="radio" @if ($itemseg->voto=="Aprueba") checked @endif value="Aprueba" name="voto[{{$itemseg->id }}]" class="aprueba">Aprobar</label>
                                        <label class="labelrechaza">
                                            <input required type="radio" @if ($itemseg->voto=="Rechaza") checked @endif value="Rechaza" name="voto[{{$itemseg->id }}]" class="rechaza">Rechazar
                                        <input type="text" name="replicavoto[{{$itemseg->id }}]" id="replica" placeholder="Argumentación" class="replica form-control" style="display: block;" @if ($itemseg->voto=="Rechaza") value="{{$itemseg->replica}}" @endif>
                                        </label>
                                    <?php #}//@endif ?>
                                    @endif
                                    <!--/div-->
           </li>
<!-- Modal -->
<div id="valoraciontxt_{{$itemseg->id }}" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Detalle de Valoración</h4>
      </div>
      <div class="modal-body">
        <p>
          @if ($itemseg->voto=="Pendiente" and ($user->rol=="admin" or $user->rol=="director" or $user->rol=="docente" ))
          <textarea id="editar_valoracion_{{$itemseg->id }}" class="editar_valoracion" ide="{{$itemseg->id }}" style="width:100%" rows="6"><?php echo $itemseg->valoracion ?></textarea>
          @else
          <?php echo $itemseg->valoracion ?>
          @endif
        </p>
@if ($itemseg->voto=="Rechaza")
<span style="float:right;background-color: red; color: white;" class="label label-success" title="Rechaza">R</span><br>
<p><strong>Argumentación:</strong> {{$itemseg->replica}}</p>
@endif
      </div>
      <div class="modal-footer">
         @if ($itemseg->voto=="Pendiente" and ($user->rol=="admin" or $user->rol=="director" or $user->rol=="docente" ))
        <button type="button" class="btn btn-danger pull-left" class="eliminar_valoracion" ide="{{$itemseg->id }}" onclick=" eliminar_valoracion(this);">Eliminar</button> 
        <button type="button" class="btn btn-primary" class="guardar_valoracion" ide="{{$itemseg->id }}" onclick=" guardar_valoracion(this);">Guardar</button>       
          @endif
        <button type="button" class="btn btn-default" id="cerrar_{{$itemseg->id }}" data-dismiss="modal" onclick="fn_noeditar_valoracion('{{$itemseg->id }}')">Cerrar</button>
      </div>
    </div>

  </div>
</div>
<!-- Modal -->
                                   
                                @endif
                            @endif
                        @endforeach
                                    </ul>
                        @if ($user->rol=="docente")
  <a class="button_mas badge" data-toggle="modal" data-target="#crear_nueva_valoracion" onclick="document.getElementById('labeltipo{{ $itemval->name }}').checked = true;document.getElementById('aspecto').value = '{{ $id }}';mostrar_nuevo();setTimeout(function(){ document.querySelector('#ideditorvaloracion .cke_wysiwyg_frame').contentDocument.body.focus(); }, 1000);" style="    color: #fff;
    background-color: #3097D1 !important;
    border-color: #2a88bd;
    height: 42px;
    width: 42px;
    /*border-radius: 50%;*/
    text-align: center;
    vertical-align: middle;
    vertical-align: -webkit-baseline-middle;
    font-size: 37px;" href="facultad/create" title="Nueva valoración">
    
+
</a>
                        @endif
        </td>
        @endforeach
    </tr>
    @endforeach
    @if (Auth::user()->rol=='estudiante')
    <tr>
        <td colspan="{{ 2*$total_cols+1 }}">
            <input type="submit" value="Guardar">
        </td>
    </tr>
    @endif
</table>
</form>
<br>
<div id="mensajes_observaciones">
</div>

<br>
<table>
 <tr>
   <th colspan="3" class="alineado_izquierda borde_inferior"><strong>Iconografía</strong></th>
</tr>
 <tr>
    <td><span class="label label-success" style="background-color: orange; color: white;">P</span> : Pendiente</td>
    <td><span class="label label-success" style="background-color: red; color: white;">R</span> : Rechazado</td>
    <td><span class="label label-success" style="background-color: green; color: white;">A</span> : Aprobado</td>
</tr>
</table>
<br>
<!--table>
 <tr>
     <td colspan="5" class="alineado_izquierda borde_inferior"><strong>Observaciones del Docente:</strong></td>
 </tr>
 <tr>   
     <td>
        @if (Auth::user()->rol=='docente')
        <textarea class="form-control" cols="90" rows="6" id="observ_docnt" name="observ_docnt">{{ $seguimiento->observ_docnt }}</textarea> 
        @else
        <p>{{ $seguimiento->observ_docnt }}</p>
        @endif
     </td>
 </tr>
 <tr>
     <td colspan="5" class="alineado_izquierda borde_inferior"><strong>Observaciones del estudiante:</strong></td>
 </tr>
 <tr>   
     <td>
        @if (Auth::user()->rol=='estudiante')
        <textarea class="form-control" cols="90" rows="6" id="observ_est" name="observ_est">{{ $seguimiento->observ_est }}</textarea> 
        @else
        <p>{{ $seguimiento->observ_est }}</p>
        @endif
     </td>
 </tr>
</table-->
<br>
</span>
<div class="mensajes">
  <div class="row">
        <div class="col-md-12" style="float:right">
            <div class="panel panel-primary">
                <div class="panel-heading"> Observaciones
                    <!--div class="btn-group pull-right">
                        <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                            <span class="glyphicon glyphicon-chevron-down"></span>
                        </button>
                        <ul class="dropdown-menu slidedown">
                            <li><a href="http://www.jquery2dotnet.com"><span class="glyphicon glyphicon-refresh">
                            </span>Refresh</a></li>
                            <li><a href="http://www.jquery2dotnet.com"><span class="glyphicon glyphicon-ok-sign">
                            </span>Available</a></li>
                            <li><a href="http://www.jquery2dotnet.com"><span class="glyphicon glyphicon-remove">
                            </span>Busy</a></li>
                            <li><a href="http://www.jquery2dotnet.com"><span class="glyphicon glyphicon-time"></span>
                                Away</a></li>
                            <li class="divider"></li>
                            <li><a href="http://www.jquery2dotnet.com"><span class="glyphicon glyphicon-off"></span>
                                Sign Out</a></li>
                        </ul>
                    </div-->
                </div>
                <div class="panel-body">
                    <ul class="chat" id="lista_mensajes">

                    </ul>
                </div>
                <div class="panel-footer">
                    <div class="input-group">
                        <input id="btn-input" type="text" class="form-control input-sm" placeholder="Escriba su mensaje aquí..." />
                        <span class="input-group-btn">
                            <button onclick="enviar();" class="btn btn-warning btn-sm" id="btn-chat">
                                Enviar</button>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>  
</div>

@if (Auth::user()->rol=="admin" or Auth::user()->rol=="docente")
    <span style="display:none;" >

          <input name="estado" value="En Proceso" type="hidden">
          <input name="estado" value="Listo" id="switch-state" type="checkbox" @if($seguimiento->estado=="Listo") checked @endif >
    {{$seguimiento->estado}}
    </span>

    <div class="row">
      <div class="col-md-12">
        <center>
        <h2 class="h4">Estado de Seguimiento @if (Auth::user()->rol=="admin")(Docente) @endif</h2>
        <p>
        </p>
        <div class="btn-group">
          <button type="button" onclick="return estado(this, false)" id="enproceso" class="estado btn btn-warning">En Proceso</button>
          <button type="button" onclick="return estado(this, true)" id="listo" class="estado btn btn-success">Listo</button>
        </div>
          </center>
      </div>
    </div>

    <script>
      document.addEventListener("DOMContentLoaded", function(event) {
        alplicar_estilos();
      });
    </script>
@endif
@if (Auth::user()->rol=="admin" or Auth::user()->rol=="estudiante")
    <span style="display:none;" >

          <input name="estado_est" value="En Proceso" type="hidden">
          <input name="estado_est" value="Listo" id="switch-state_est" type="checkbox" @if($seguimiento->estado_est=="Listo") checked @endif >
    {{$seguimiento->estado_est}}
    </span>
    <div class="row">
      <div class="col-md-12">
        <center>
        <h2 class="h4">Estado de Seguimiento @if (Auth::user()->rol=="admin")(Estudiate) @endif</h2>
        <p>
        </p>
        <div class="btn-group">
          <button type="button" onclick="return estado(this, false, 'estado_est','_est')" id="enproceso_est" class="estado btn btn-warning">En Proceso</button>
          <button type="button" onclick="return estado(this, true, 'estado_est','_est')" id="listo_est" class="estado btn btn-success">Listo</button>
        </div>
          </center>
      </div>
    </div>

    <script>
      document.addEventListener("DOMContentLoaded", function(event) {
        alplicar_estilos("_est");
      });
    </script>

@endif
    <script>
      function estado(obj, estado, campo =  'estado', destino = ""){
        var label_estado = estado ?  'Listo' : 'En Proceso';
        if(confirm("Esta seguro que desea cambiar a estado "+label_estado)){
               guardar_estado('seg', campo, label_estado, estado,"switch-state","{{route('estado_doc',[$seguimiento->id,$asigc->id])}}",destino);
           }else{
             return false;
           }
      }        
     
    </script>
     
<div id="mensajes_footer">
</div>
@endsection
<?php
  /*
      @foreach($mensaje as $mensajei)
                      @if(Auth::user()->ident_usu == $mensajei->de_fk->ident_usu)
                                              <li class="right clearfix"><span class="chat-img pull-right">
                            <img src="https://placehold.it/50/FA6F57/fff&text=YO" alt="User Avatar" class="img-circle" />
                        </span>
                            <div class="chat-body clearfix">
                                <div class="header">
                                    <small class=" text-muted"><span class="glyphicon glyphicon-time"></span>13 mins ago</small>
                                    <strong class="pull-right primary-font">{{$mensajei->de_fk->nombre}} {{$mensajei->de_fk->apellido}}</strong>
                                </div>
                                <p>
                                    {{$mensajei->texto}}
                                </p>
                            </div>
                        </li>
                      @else
                        <li class="left clearfix"><span class="chat-img pull-left">
                            <img src="https://placehold.it/50/55C1E7/fff&text=U" alt="User Avatar" class="img-circle" />
                        </span>
                            <div class="chat-body clearfix">
                                <div class="header">
                                    <strong class="primary-font">{{$mensajei->de_fk->nombre}} {{$mensajei->de_fk->apellido}}</strong> <small class="pull-right text-muted">
                                        <span class="glyphicon glyphicon-time"></span>12 mins ago</small>
                                </div>
                                <p>
                                   {{$mensajei->texto}}
                                </p>
                            </div>
                        </li>
                     @endif
                       @endforeach
  */
  ?>