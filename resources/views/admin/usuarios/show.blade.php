@extends('layouts.app')
@section('vars')
{!!
$page_title = 'Usuarios'; 
$route = 'usuarios'; 
!!}
@endsection
@section('ruta_de_migas')
<ol class="breadcrumb">
    <li><a href="{{ route('home') }}"> Inicio</a></li>
    <li>{{ $page_title }}</li>
    <li class="active"> Detalles</li>
</ol>
@endsection
@section('scripts')
<script> 
  function opselectroles(){
    $('#select1').html(`<option value=""> 
                                  Seleccione un rol predeterminado
                              </option>`);
        $('.chk_roles').each(function( index ) {
          if (this.checked){
            var optionValue = $(this).val();
            var optionText = $("#label_"+optionValue).html();
            var selected = '';
            var rol_actual = $('#rol').val();
            console.log(rol_actual);
            if (rol_actual==optionValue){
                 selected = ' selected ';
                }
            $('#select1').append(`<option ${selected} value="${optionValue}"> 
                                   ${optionText} 
                              </option>`); 
          }
  });
}
$(document).ready(function(){
  opselectroles();
  $('.chk_roles').change(function(){
    opselectroles();
  }); 
});
</script> 

<script>
    $(document).ready(function() {
        //password_en_formulario("admin_usuarios_create");
        required_en_formulario_for("admin_usuarios_create","red","*")
    });
    
    function validar_area_password(obj){
        document.getElementById('area_password').style.display = obj.checked ? 'block' : 'none';
        document.getElementById('password').value='';
        document.getElementById('password-confirm').value='';
        $('#password').prop('required',obj.checked);
        $('#password-confirm').prop('required',obj.checked);
    }
</script>
@endsection
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"></div>

                <div class="card-body">
                    <form method="POST" id="admin_usuarios_create" action="{{ route('admin.usuarios.update',[
                    'usuario' => $user->id,
                    ]) }}">
                        <meta name="csrf-token" content="{{ csrf_token() }}">
                         <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <h1 class="text-center">Modificar Usuario</h1>



        <div class="form-group{{ $errors->has('nombre') ? ' has-error' : '' }}">
            <label for="nombre" class="col-md-4 control-label">Nombre</label>

            <div class="col-md-6">
                <span class="form-control">{{ $user->nombre }}</span>
            </div>
        </div>
        <div class="form-group{{ $errors->has('apellido') ? ' has-error' : '' }}">
            <label for="apellido" class="col-md-4 control-label">Apellido</label>

            <div class="col-md-6">
               <span class="form-control">{{ $user->apellido }}</span>
            </div>
        </div>
        <div class="form-group{{ $errors->has('rol') ? ' has-error' : '' }}">
            <label for="rol" class="col-md-4 control-label">Rol(es)</label>

            <div class="col-md-6">
              <label><?php 
$user_roles = explode(",",$user->roles);
if (in_array('admin',$user_roles)){ ?> <span class="form-control">Administrador</span> <?php }//endif;
                            ?></label>
              <br>
              <label><?php 
$user_roles = explode(",",$user->roles);
if (in_array('director',$user_roles)){ ?> <span class="form-control">Director</span> <?php }//endif;
                            ?></label>
              <br>
              <label><?php 
$user_roles = explode(",",$user->roles);
if (in_array('docente',$user_roles)){ ?> <span class="form-control">Docente</span> <?php }//endif;
                            ?></label>
              <br>
              <label><?php 
$user_roles = explode(",",$user->roles);
if (in_array('estudiante',$user_roles)){ ?> <span class="form-control">Estudiante</span> <?php }//endif;
                            ?></label>
              <br>
            </div>
        </div>
        <div class="form-group{{ $errors->has('tel') ? ' has-error' : '' }}">
            <label for="tel" class="col-md-4 control-label">Teléfono</label>

            <div class="col-md-6">
               <span class="form-control">{{ $user->tel }}</span>
            </div>
        </div>
        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <label for="email" class="col-md-4 control-label">Correo Electrónico</label>

            <div class="col-md-6">
               <span class="form-control">{{ $user->email }}</span>
            </div>
        </div>


                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
<a href="{{ route('admin.usuarios.index') }}" class="btn btn-secondary hidden-print">Regresar</a>
                            </div>
                        </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
