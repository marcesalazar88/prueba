@extends('layouts.app')
@section('vars')
{!!
$page_title = 'Control de Fechas'; 
$route = 'control'; 
!!}
@endsection
@section('ruta_de_migas')
<ol class="breadcrumb">
    <li><a href="{{ route('home') }}"> Inicio</a></li>
    <li><a href="{{ route('admin.control.index') }}"> {{ $page_title }}</a></li>
    <li class="active"> Registrar</li>
</ol>
@endsection
@section('scripts')
<script>
    $(document).ready(function() {
        password_en_formulario("admin_control_create");
    required_en_formulario_for("admin_control_create","red","*")
    });
    </script>
@endsection
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"></div>

                <div class="card-body">
                    <form method="POST" id="admin_control_create" action="{{ route('admin.control.store') }}">
                         <meta name="csrf-token" content="{{ csrf_token() }}">
                         <input type="hidden" name="_token" value="{{ csrf_token() }}">
<div class="form-group row">
      <label for="depto_id" class="col-md-4 col-form-label text-md-right">{{ __('Departamento') }}</label>

      <div class="col-md-6">
          <select id="depto_id" class="js-example-basic-single form-control{{ $errors->has('depto_id') ? ' is-invalid' : '' }}" name="depto_id" required autofocus>
              <option value="">Seleccione un departamento</option>
              @foreach($deptos as $depto)
              <option value="{{ $depto->id }}"
              @if (count($errors))
                  @if (old('depto_id')==$depto->id) 
                      {{ __(' selected ') }} 
                  @endif;                                  
              @endif;
              >{{ $depto->nom_depto }}</option>
              @endforeach
          </select>
          @if ($errors->has('depto_id'))
              <span class="invalid-feedback">
                  <strong>{{ $errors->first('depto_id') }}</strong>
              </span>
          @endif
      </div>
  </div>
<div class="form-group row">
                            <label for="formato" class="col-md-4 col-form-label text-md-right">{{ __('Formato') }}</label>

                            <div class="col-md-6">
                                <?php
                                #$sug_formato = date("Y");
                                #$sug_formato .= (date("m")<=6) ? 'A' : 'B';
                                ?>
                                
                                <select id="formato" onchange="document.getElementById('div_seg_estu').style.display = (this.value=='seg') ? 'block':'none'" class="js-example-basic-single form-control{{ $errors->has('formato') ? ' is-invalid' : '' }}" name="formato" required autofocus>
                                    <option value="">Seleccione un fomato de documento</option>
                                    @foreach($formatos as $id => $formato)
                                    <option value="{{ $id }}"
                                    @if (count($errors))
                                        @if (old('formato')==$id) 
                                            {{ __(' selected ') }} 
                                        @endif;
                                    @endif;
                                    >{{ $formato }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('formato'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('formato') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                       <div class="form-group row">
                            <label for="nombre_control" class="col-md-4 col-form-label text-md-right">{{ __('Nombre') }}</label>

                            <div class="col-md-6">
                                <input id="nombre_control" type="text" class="form-control{{ $errors->has('nombre_control') ? ' is-invalid' : '' }}" name="nombre_control" value="{{ old('nombre_control') }}" required autofocus placeholder="Calendario Ordinario, Extraordinario, Especial, etc.">

                                @if ($errors->has('nombre_control'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('nombre_control') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                      
                       <div class="form-group row">
                            <label for="fecha_aprobacion" class="col-md-4 col-form-label text-md-right">{{ __('Fecha de revision docente Inicial') }}</label>

                            <div class="col-md-6">
                                <input id="fecha_aprobacion" type="date" class="form-control{{ $errors->has('fecha_aprobacion') ? ' is-invalid' : '' }}" name="fecha_aprobacion" value="{{ old('fecha_aprobacion') }}" required autofocus>

                                @if ($errors->has('fecha_aprobacion'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('fecha_aprobacion') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                      
                       <div class="form-group row">
                            <label for="fecha_revision_doc" class="col-md-4 col-form-label text-md-right">{{ __('Fecha de revision docente Final') }}</label>

                            <div class="col-md-6">
                                <input id="fecha_revision_doc" type="date" class="form-control{{ $errors->has('fecha_revision_doc') ? ' is-invalid' : '' }}" name="fecha_revision_doc" value="{{ old('fecha_revision_doc') }}" required autofocus>

                                @if ($errors->has('fecha_revision_doc'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('fecha_revision_doc') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                       <div class="form-group row" id="div_seg_estu" style="diaplay:none;">
                            <label for="fecha_revision_est" class="col-md-4 col-form-label text-md-right">{{ __('Fecha de revison estudiante') }}</label>

                            <div class="col-md-6">
                                <input id="fecha_revision_est" type="date" class="form-control{{ $errors->has('fecha_revision_est') ? ' is-invalid' : '' }}" name="fecha_revision_est" value="{{ old('fecha_revision_est') }}" autofocus>

                                @if ($errors->has('fecha_revision_est'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('fecha_revision_est') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                       <div class="form-group row">
                            <label for="per_acad" class="col-md-4 col-form-label text-md-right">{{ __('Periodo Académico') }}</label>

                            <div class="col-md-6">
                                <?php
                                #$sug_per_acad = date("Y");
                                #$sug_per_acad .= (date("m")<=6) ? 'A' : 'B';
                                ?>
                                
                                <select id="per_acad" class="js-example-basic-single form-control{{ $errors->has('per_acad') ? ' is-invalid' : '' }}" name="per_acad" required autofocus>
                                    <option value="">Seleccione un periodo académico</option>
                                    @foreach($periodos as $periodo)
                                    <option value="{{ $periodo->periodo }}"
                                    @if (count($errors))
                                        @if (old('per_acad')==$periodo->periodo) 
                                            {{ __(' selected ') }} 
                                        @endif;
                                    @else
                                         @if ($periodo_activo->periodo==$periodo->periodo) 
                                            {{ __(' selected ') }} 
                                        @endif;
                                    @endif;
                                    >{{ $periodo->periodo }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('per_acad'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('per_acad') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                       
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
<a href="{{ route('admin.control.index') }}" class="btn btn-secondary hidden-print">Regresar</a>
<input type="submit" name="submit" value="Registrar" class="btn btn-primary">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
