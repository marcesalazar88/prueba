@extends('layouts.app')
@section('vars')
{!!
$page_title = 'Departamento'; 
$route = 'departamento'; 
!!}
@endsection
@section('ruta_de_migas')
<ol class="breadcrumb">
    <li><a href="{{ route('home') }}"> Inicio</a></li>
    <li><a href="{{ secure_asset('admin/departamento') }}"> {{ $page_title }}</a></li>
    <li class="active"> Registrar</li>
</ol>
@endsection
@section('scripts')
<script>
    $(document).ready(function() {
        password_en_formulario("admin_departamento_create");
    required_en_formulario_for("admin_departamento_create","red","*")
    });
    </script>
@endsection
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"></div>

                <div class="card-body">
        <h1>Codigos</h1>
                
<form method="POST" id="admin_codigos_create" action="{{ route('admin.codigos.store') }}">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
            
                        <div class="form-group row">
                            <label for="asigc" class="col-md-4 col-form-label text-md-right">{{ __('Asignación') }}</label>

                            <div class="col-md-6">
                                <input id="asigc" type="text" class="form-control{{ $errors->has('asigc') ? ' is-invalid' : '' }}" name="asigc" value="{{ $errors->has('asigc') ? old('asigc') : $id_asig }}" required autofocus>

                                @if ($errors->has('asigc'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('asigc') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
    
                        
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Registrar') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
