@extends('layouts.app') @section('barra_buscar')
<li><a onclick="window.print()" title="Imprimir">
<span class="glyphicon glyphicon-print"></span></a></li>
@endsection @section('ruta_de_migas')
<ol class="breadcrumb navbar-left">
  <li><a href="{{ route('home') }}"> Inicio</a></li>
  <li><a href="{{ route('admin.programa.index') }}"> Programa</a></li>
  <li><a href="{{ route('admin.programa.show',[$planes->prog->id]) }}"> Detalles {{ $planes->prog->nom_prog }}</a></li>
  <li class="active">{{ $planes->nombre }}</li>
</ol>
@endsection @section('scripts') @include('reportes.css') @endsection @section('content')
<?php use Illuminate\Support\Arr; ?>
<br>
<hr>
<span id="reporte">
<center>

<h1>
  <center><strong>Plan de estudios - Programa de {{ $planes->prog->nom_prog }}</strong></center>
</h1>
<h2>
  <center><strong>Fecha: {{ $planes->fecha }}</strong></center>
</h2>
<h2>
  <center>
    <strong>Acuerdo:</strong><br>
    <a href="<?php echo env('APP_URL').$planes->adjunto ?>" target="_blank"style="
    color: red;    font-size: medium;
"><span class="glyphicon glyphicon-paperclip"></span> Descargar</a>
  </center>
</h2>
  
<p>
  {{ $planes->observaciones }}
</p>
<table border="1" class="table cssresponsive">
  <?php 
  $asignaturas_anteriores=[];
  $masdesegundo = false;
  $semestre = 0;
  $iniciar_con = $periodo_activo == $planes->prog->periodo_ingreso ? 1 : 2;
  ?> @foreach ($planes->planes_asigts as $planes_asigt_i)
  <?php 
  $cont=0;
  $total = count($planes_asigt_i->prerequisitos)>0 ? count($planes_asigt_i->prerequisitos) : 1;
  $num = count($planes_asigt_i->prerequisitos)>0 ? count($planes_asigt_i->prerequisitos) : 1;
  $num = 1;
  ?> @if($semestre != $planes_asigt_i->semestre)
  <thead></thead>
  <tr>
    <th class="hidden-xs" colspan="6">
      <center>Semestre {{ $planes_asigt_i->semestre }}</center>
    </th>
    <td class="hidden-sm hidden-md hidden-lg" data-label="Semestre">
      {{ $planes_asigt_i->semestre }}
    </td>
  </tr>
  <thead>
  <tr>
    <th>
      <center>Código</center>
    </th>
    <th>
      <center>Asignatura</center>
    </th>
    <th>
      <center>I.H.S.</center>
    </th>
    <th>
      <center>Cod.</center>
    </th>
    <th>
      <center>Prerrequisitos</center>
    </th>
    @if (Auth::check()) @if (Auth::user()->rol=='admin')
    <th colspan="1" class="noprint">
      <center><a class="btn btn-primary" onclick="document.getElementById('semestre').value='{{$planes_asigt_i->semestre}}'" data-toggle="modal" data-target="#nueva_asignatura_plan"><span title="Nuevo" class="glyphicon glyphicon-plus"></span></a></center>
    </th>
    @endif @endif
  </tr>
  </thead>
  @endif
  <?php $semestre = $planes_asigt_i->semestre;
    $asignaturas_anteriores[$planes_asigt_i->asigt->codigo_asigt]=[
    'semestre' =>  $planes_asigt_i->semestre,
    'nom_asigt' => $planes_asigt_i->asigt->nom_asigt
  ];
  ?>
  <tr>
    <td data-label="Código" rowspan="{{$num}}">{{ $planes_asigt_i->asigt->codigo_asigt }}</td>
    <td data-label="Asignatura" rowspan="{{$num}}">{{ $planes_asigt_i->asigt->nom_asigt }}</td>
    <td data-label="I.H.S." rowspan="{{$num}}">{{ $planes_asigt_i->asigt->ihs_asigt }}
      <!-- Modal -->
<div id="nuevo_prerrequisito_{{$planes_asigt_i->id}}" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Configurar prerrequisitos de asignatura</h4>
      </div>
      <div class="modal-body">
        <form method="post" action="{{ route('admin.planesasigtpre.store') }}">
          <meta name="csrf-token" content="{{ csrf_token() }}">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <input type="hidden" name="planes_id" value="{{ $planes->id }}">
          <input type="hidden" name="planes_asigt_id" value="{{ $planes_asigt_i->id }}">
           <div class="form-group row">
              <label for="semestre" class="col-md-4 col-form-label text-md-right">{{ __('Asignatura Actual') }}</label>
             <span class="col-md-6 form-control">{{$planes_asigt_i->asigt->nom_asigt}}</span>
              <label for="semestre" class="col-md-4 col-form-label text-md-right">{{ __('Prerrequisitos') }}</label>
             
               <!-- -->
<?php $cont = 0; ?> 
    @foreach ($planes_asigt_i->prerequisitos as $prerequisito_i)
     <?php $prerequisito_i->asigt(); $cont++; ?>
<div class="col-md-6 form-control">
<span style="float: left;margin-left: 15px;">{{ $prerequisito_i->asigt->codigo_asigt }}</span>
      <!--/td>
      <td class="hidden-xs" data-label="Prerrquisitos" rowspan="1"-->

        <span style="
    float: right;
    margin-right: 15px;
">{{ $prerequisito_i->asigt->nom_asigt }} <a class="btn btn-danger btn-xs" onclick="return confirm('¿Esta ud seguro que quiere eliminar {{ $prerequisito_i->asigt->nom_asigt }} como prerrequisito de {{ $planes_asigt_i->asigt->nom_asigt }}?')" href="{{ route('admin.planesasigtpre.delete',[$planes_asigt_i->planes_id, $prerequisito_i->id]) }}"><span title="Eliminar" class="glyphicon glyphicon-trash"></span></a></span>
</div>
    @endforeach
              
          </div>
          <div class="form-group row">
                            <label for="cod_asigt" class="col-md-4 col-form-label text-md-right">{{ __('Agregar prerrequisito') }}</label>
                            <div class="col-md-6">
                                <select style="width: 100%;" id="cod_asigt" class="js-example-basic-single form-control{{ $errors->has('cod_asigt') ? ' is-invalid' : '' }}" name="cod_asigt" required autofocus>
                                    <option value="">Seleccione una asignatura</option>
                                  <?php $existentes = Arr::pluck($planes_asigt_i->prerequisitos, 'pre'); ?>
                                    @foreach($asignaturas_anteriores as $codigo_asigt => $dat_asigt)
                                    @if($dat_asigt['semestre'] <  $planes_asigt_i->semestre)
                                    @if(!in_array($codigo_asigt,$existentes))
                                    <?php $masdesegundo = true; ?>
                                    <option value="{{ $codigo_asigt }}" 
                                    @if (old('cod_asigt')==$codigo_asigt) {{ __(' selected ') }} 
                                    @endif;
                                    >{{ $dat_asigt['nom_asigt']." (".$codigo_asigt.")" }}</option>
                                    @endif
                                    @endif
                                    @endforeach
                                </select>
                                @if ($errors->has('cod_asigt'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('cod_asigt') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
           <div class="col-md-12">
              <center>
                <input type="submit" name="submit" value="Registrar" class="btn btn-primary">
              </center>
          </div>
          <hr>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>

  </div>
</div>
<!-- Modal -->
    </td>
    <td class="hidden-sm hidden-md hidden-lg" rowspan="{{$num}}" colspan="1" class="noprint">
      <a class="btn btn-danger noprint" onclick="return confirm('¿Esta ud seguro que quiere eliminar {{ $planes_asigt_i->id }}?')" href="{{ route('admin.planesasigt.delete',[$planes_asigt_i->id]) }}"><span title="Eliminar" class="glyphicon glyphicon-trash"></span></a>
    </td>
    <td class="hidden-sm hidden-md hidden-lg"><center><strong>Prerrequisitos
      @if($masdesegundo == true)
      &nbsp;&nbsp;
      <a title="Configurar prerrequisitos" class="btn btn-secondary noprint" data-toggle="modal" data-target="#nuevo_prerrequisito_{{$planes_asigt_i->id}}"><span class="glyphicon glyphicon-pencil"></span></a>
     @endif
      </strong></center></td>
    @if(count($planes_asigt_i->prerequisitos)>0)
    <td class="hidden-sm hidden-md hidden-lg" data-label="Código"><strong>ASIGNATURA</strong></td>
   
    
     
    <?php $cont = 0; ?> 
    @foreach ($planes_asigt_i->prerequisitos as $prerequisito_i)
     <?php $prerequisito_i->asigt(); $cont++; ?> 
    @if ($cont==1)
    <td class="hidden-xs" rowspan="{{$num}}" colspan="2">
    @endif 
    @if ($cont>1)
    <!--tr class="hidden-xs"-->
    @endif
      
      <!--td class="hidden-xs" data-label="Cod." rowspan="1"-->
      
        <span style="
    float: left;
    margin-left: 15px;
">{{ $prerequisito_i->asigt->codigo_asigt }}</span>
      <!--/td>
      <td class="hidden-xs" data-label="Prerrquisitos" rowspan="1"-->

        <span style="
    float: right;
    margin-right: 15px;
">{{ $prerequisito_i->asigt->nom_asigt }} </span><br>
      <!--/td-->
     @if ($cont==$total)
    </td>  
    @endif 
    @if ($cont>1)
    <!--/tr--> 
    @endif 
   
    @endforeach
  <?php $cont  = 0; ?>
  @foreach ($planes_asigt_i->prerequisitos as $prerequisito_i)
  <?php $cont++; ?>
      <td class="hidden-sm hidden-md hidden-lg" data-label="{{ $prerequisito_i->asigt->codigo_asigt }}">
      {{ $prerequisito_i->asigt->nom_asigt }}</td>
  @if ($cont <=1) @if (Auth::check()) @if (Auth::user()->rol=='admin')
        <td class="hidden-xs" rowspan="{{$num}}" colspan="1" class="noprint">
@if($masdesegundo == true or $cont <=1)
  <a title="Configurar prerrequisitos" class="btn btn-secondary noprint" data-toggle="modal" data-target="#nuevo_prerrequisito_{{$planes_asigt_i->id}}"><span class="glyphicon glyphicon-pencil"></span></a>
@endif
          <a class="btn btn-danger noprint" onclick="return confirm('¿Esta ud seguro que quiere eliminar {{ $planes_asigt_i->id }}?')" href="{{ route('admin.planesasigt.delete',[$planes_asigt_i->id]) }}"><span title="Eliminar" class="glyphicon glyphicon-trash"></span></a>
</td>
        @endif @endif @endif 
      @endforeach   
    @else
    <td class="hidden-xs" rowspan="{{$num}}" colspan="2">

    </td>
    @if (Auth::check()) @if (Auth::user()->rol=='admin')
    <td class="hidden-xs" rowspan="{{$num}}" colspan="1" class="noprint">
@if($masdesegundo == true)
<a title="Configurar prerrequisitos" class="btn btn-secondary noprint" data-toggle="modal" data-target="#nuevo_prerrequisito_{{$planes_asigt_i->id}}"><span class="glyphicon glyphicon-pencil"></span></a> 
@endif
<a class="btn btn-danger noprint" onclick="return confirm('¿Esta ud seguro que quiere eliminar {{ $planes_asigt_i->id }}?')" href="{{ route('admin.planesasigt.delete',[$planes_asigt_i->id]) }}"><span title="Eliminar" class="glyphicon glyphicon-trash"></span></a>

  </td>
    @endif @endif @endif

  </tr>
  @endforeach
  <tr>
      @if (Auth::check()) @if (Auth::user()->rol=='admin')
    <th colspan="5" class="noprint"></th>
    <th colspan="1" class="noprint">
      <center><a class="btn btn-primary" onclick="document.getElementById('semestre').value='{{$semestre+1}}'" data-toggle="modal" data-target="#nueva_asignatura_plan"><span title="Nuevo" class="glyphicon glyphicon-plus"></span></a></center>
    </th>
    @endif @endif
  </tr>
</table>
  
</span>
<!-- Modal -->
<div id="nueva_asignatura_plan" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Nueva asignatura del plan</h4>
      </div>
      <div class="modal-body">
        
        <form method="post" action="{{ route('admin.planesasigt.store') }}">
          <meta name="csrf-token" content="{{ csrf_token() }}">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <input type="hidden" name="planes_id" value="{{ $planes->id }}">
           <div class="form-group row">
              <label for="semestre" class="col-md-4 col-form-label text-md-right">{{ __('Semestre') }}</label>

              <div class="col-md-6">
                  <?php
                  $sug_semestre = old('semestre');
                  ?>
                  <input  id="semestre" type="number" class="form-control{{ $errors->has('semestre') ? ' is-invalid' : '' }}" name="semestre" value="{{ $errors->has('semestre') ? old('semestre') : $sug_semestre }}" required autofocus>
                  @if ($errors->has('semestre'))
                      <span class="invalid-feedback">
                          <strong>{{ $errors->first('semestre') }}</strong>
                      </span>
                  @endif
              </div>
          </div>
          <div class="form-group row">
                            <label for="cod_asigt" class="col-md-4 col-form-label text-md-right">{{ __('Asignatura') }}</label>
                            <div class="col-md-6">
                                <select style="width: 100%;" id="cod_asigt" class="js-example-basic-single form-control{{ $errors->has('cod_asigt') ? ' is-invalid' : '' }}" name="cod_asigt" required autofocus>
                                    <option value="">Seleccione una asignatura</option>
                                    @foreach($asigts as $asigt_i)
                                    <option value="{{ $asigt_i->codigo_asigt }}" 
                                    @if (old('cod_asigt')==$asigt_i->codigo_asigt) {{ __(' selected ') }} 
                                    @endif;
                                    >{{ $asigt_i->nom_asigt." (".$asigt_i->codigo_asigt.")" }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('cod_asigt'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('cod_asigt') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
           <div class="col-md-12">
              <center>
                <input type="submit" name="submit" value="Registrar" class="btn btn-primary">
              </center>
          </div>
          <hr>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>

  </div>
</div>
<!-- Modal -->
@endsection