@extends('layouts.app')
@section('vars')
{!!
$page_title = 'Enlaces de Interés'; 
$route = 'links'; 
!!}
@endsection
@section('ruta_de_migas')
<ol class="breadcrumb">
    <li><a href="{{ route('home') }}"> Inicio</a></li>
    <li><a href="{{ route('admin.links.index') }}"> {{ $page_title }}</a></li>  
    <li class="active"> Registrar</li>
</ol>
@endsection
@section('scripts')
<script>
    $(document).ready(function() {
        password_en_formulario("admin_links_create");
    required_en_formulario_for("admin_links_create","red","*")
    });
    </script>
@endsection
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"></div>

                <div class="card-body">
                    <form method="POST" id="admin_links_create" action="{{ route('admin.links.store') }}">
                         <meta name="csrf-token" content="{{ csrf_token() }}">
                         <input type="hidden" name="_token" value="{{ csrf_token() }}">

                       <div class="form-group row">
                            <label for="nombre" class="col-md-4 col-form-label text-md-right">{{ __('Nombre') }}</label>

                            <div class="col-md-6">
                                <input id="nombre" type="text" class="form-control{{ $errors->has('nombre') ? ' is-invalid' : '' }}" name="nombre" value="{{ old('nombre') }}" required autofocus>

                                @if ($errors->has('nombre'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('nombre') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="url" class="col-md-4 col-form-label text-md-right">{{ __('Enlace') }}</label>

                            <div class="col-md-6">
                                <input id="url" type="text" class="form-control{{ $errors->has('url') ? ' is-invalid' : '' }}" name="url" value="{{ old('url') }}" required autofocus>

                                @if ($errors->has('url'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('url') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="target" class="col-md-4 col-form-label text-md-right">{{ __('Abrir en') }}</label>

                            <div class="col-md-6">
                              {!!Form::select('target', $target, 
                                ($errors->has('target')) ?  old('target') : ''
                                , [
                                'id' => 'target',
                                'class' => 'js-example-basic-single form-control',
                                'placeholder' => 'Seleccione un destino',
                                'required',
                                ])!!}

                                @if ($errors->has('target'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('target') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="icono" class="col-md-4 col-form-label text-md-right">{{ __('icono') }}</label>

                            <div class="col-md-6">
                               {!!Form::select('icono', $iconos, 
                                ($errors->has('icono')) ?  old('icono') : ''
                                , [
                                'id' => 'icono',
                                'class' => 'js-example-basic-single form-control',
                                'placeholder' => 'Seleccione un Icono',
                                'required',
                                ])!!}
                                @if ($errors->has('icono'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('icono') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
<a href="{{ route('admin.links.index') }}" class="btn btn-secondary hidden-print">Regresar</a>
<input type="submit" name="submit" value="Registrar" class="btn btn-primary">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
