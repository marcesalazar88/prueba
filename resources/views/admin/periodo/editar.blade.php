@extends('layouts.app')
@section('vars')
{!!
$page_title = 'Periodo'; 
$route = 'periodo'; 
!!}
@endsection
@section('ruta_de_migas')
<ol class="breadcrumb">
    <li><a href="{{ route('home') }}"> Inicio</a></li>
    <li><a href="{{ route('admin.periodo.index') }}"> {{ $page_title }}</a></li>
    <li class="active"> Modificar</li>
</ol>
@endsection
@section('scripts')
<script>
    $(document).ready(function() {
        password_en_formulario("admin_periodo_create");
        required_en_formulario_for("admin_periodo_create","red","*")
    });
    
    function validar_area_password(obj){
        document.getElementById('area_password').style.display = obj.checked ? 'block' : 'none';
        document.getElementById('password').value='';
        document.getElementById('password-confirm').value='';
        $('#password').prop('required',obj.checked);
        $('#password-confirm').prop('required',obj.checked);
    }
</script>
@endsection
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"></div>
<?php 
/*
if(count($errors)>0) dd($errors);   
<input id="estado" type="text" class="form-control{{ $errors->has('estado') ? ' is-invalid' : '' }}" name="estado" value="{{
                                ($errors->has('estado')) ?  old('estado') : $periodo->estado }}" required autofocus>
*/
?>
                <div class="card-body">
                    <form method="POST" id="admin_periodo_create" action="{{ route('admin.periodo.update',[
                    'id' => $periodo->periodo,
                    ]) }}">
                        <meta name="csrf-token" content="{{ csrf_token() }}">
                         <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group row">
                            <label for="estado" class="col-md-4 col-form-label text-md-right">{{ __('Estado') }}</label>

                            <div class="col-md-6">
                                <input id="id" type="hidden" class="form-control{{ $errors->has('id') ? ' is-invalid' : '' }}" name="id" value="{{
                                ($errors->has('id')) ?  old('id') : $periodo->id }}" required>
                                
                                <select id="estado" class="form-control{{ $errors->has('estado') ? ' is-invalid' : '' }}" name="estado" required autofocus>
                                    <option value="1"
                                    <?php
                                    if (count($errors)>0) { 
                                        if (old('estado')=="1"){
                                        echo ' selected ';
                                        }
                                    }else{
                                        if ($periodo->estado=="1"){
                                        echo ' selected ';
                                        } 
                                    }
                                    ?>
                                    >Activo</option>
                                  @if($periodo->estado=="0")
                                    <option  value="0"
                                    <?php
                                    if (count($errors)>0) { 
                                        if (old('estado')=="0"){
                                        echo ' selected ';
                                        }
                                    }else{
                                        if ($periodo->estado=="0"){
                                        echo ' selected ';
                                        } 
                                    }
                                    ?>
                                    >Inactivo</option>
                                  @endif
                                </select>
                                <?php
                                /*
                                dd($errors);                                
                                @if ($errors->has('nombre'))
                                @endif
                                */
                                ?>
                                @foreach ($errors->nombre as $error)
                                    <span class="invalid-feedback">
                                        <strong>{{ $error }}</strong>
                                    </span>
                                @endforeach
                            </div>
                        </div>

                        

                        

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
<a href="{{ route('admin.periodo.index') }}" class="btn btn-secondary hidden-print">Regresar</a>
@if($periodo->estado=="0")
<input type="submit" name="submit" class="btn btn-primary" value="Actualizar">
@endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
