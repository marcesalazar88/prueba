@extends('layouts.app')
@section('vars')
{!!
$page_title = 'Periodo'; 
$route = 'periodo'; 
!!}
@endsection
@section('ruta_de_migas')
<ol class="breadcrumb">
    <li><a href="{{ route('home') }}"> Inicio</a></li>
    <li><a href="{{ route('admin.periodo.index') }}"> {{ $page_title }}</a></li>
    <li class="active"> Registrar</li>
</ol>
@endsection
@section('scripts')
<script>
    $(document).ready(function() {
        password_en_formulario("admin_periodo_create");
    required_en_formulario_for("admin_periodo_create","red","*")
    });
    </script>
@endsection
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"></div>

                <div class="card-body">
                    <form method="POST" id="admin_periodo_create" action="{{ route('admin.periodo.store') }}">
                         <meta name="csrf-token" content="{{ csrf_token() }}">
                         <input type="hidden" name="_token" value="{{ csrf_token() }}">

                       <div class="form-group row">
                            <label for="periodo" class="col-md-4 col-form-label text-md-right">{{ __('Periodo') }}</label>

                            <div class="col-md-6">
                                <input id="periodo" type="text" maxlength="5" class="form-control{{ $errors->has('periodo') ? ' is-invalid' : '' }}" name="periodo" value="{{ old('periodo') }}" required autofocus>

                                @if ($errors->has('periodo'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('periodo') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                       <div class="form-group row">
                            <label for="estado" class="col-md-4 col-form-label text-md-right">{{ __('Estado') }}</label>

                            <div class="col-md-6">
                                 <select id="estado" class="form-control{{ $errors->has('estado') ? ' is-invalid' : '' }}" name="estado" required autofocus>
                                    <option value="1"
                                    @if (old('estado')=="1") {{ __(' selected ') }} 
                                    @endif;
                                    >Activo</option>
                                    <option  value="0"
                                    @if (old('estado')=="0") {{ __(' selected ') }} 
                                    @endif;
                                    >Inactivo</option>
                                </select>

                                @if ($errors->has('estado'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('estado') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
<a href="{{ route('admin.periodo.index') }}" class="btn btn-secondary hidden-print">Regresar</a>
<input type="submit" name="submit" value="Registrar" class="btn btn-primary">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
