@extends('layouts.app')
@section('barra_buscar')
<li><a onclick="window.print()" title="Imprimir">
<span class="glyphicon glyphicon-print"></span></a></li>
@endsection
@section('scripts')
@include('reportes.css')
@endsection
@section('ruta_de_migas')
<ol class="breadcrumb">
    <li><a href="{{ route('home') }}"> Inicio</a></li>
  @if (Auth::check())
    <li><a href="{{ route('reportes.asignacion.flexibilidad') }}">Flexibilidad</a></li>
  @if (Auth::user()->rol=="admin" or Auth::user()->rol=="director")
    <li><a href="{{ route('admin.asignatura.index') }}">Asignaturas</a></li>
  @endif
  @else
    <li><a href="{{ route('reportes.asignacion.flexibilidad') }}">Flexibilidad</a></li>
  @endif
    <li class="active"> Contenido</li>
</ol>
@endsection
@section('content')
<span id="reporte">
<center>
<table border="1">
    <tr>
        <th>
            <center><img src="{{ secure_asset('img/logo-udenar.png')}}"></center>
            <span id="titulo_logo"><center>Universidad <span class="m10">de</span></center><center><strong>Nariño</strong></center></span>
        </th>
        <th>
            <div class="titulo negrita"><strong>IDENTIFICACIÓN DE LA ASIGNATURA:</strong></div>
        </th>
    </tr>

</table></center>

<table border="1">
    <tr>
        <td class="alineado_izquierda"><strong>NOMBRE DE LA ASIGNATURA EN CURSO: </strong> {{ $asigt->nom_asigt }}</td>
    </tr>
</table>
<br>
<center>
<table border="1">
    <tr>
        <td>Código de la Asignatura:</td>
        <td colspan="4"> {{ $asigt->codigo_asigt }}</td>
    </tr>
    <tr>
        <td>Semestres a los Cuales se ofrece:</td>
        <td colspan="4"> {{ $asigt->sem_ofrece_asigt }}</td>
    </tr>
    <tr>
        <td>Intensidad Horaria Semanal:</td>
        <td colspan="4">{{ $asigt->ihs_asigt }}</td>
    </tr>
    <tr>
        <td>Número de Créditos:</td>
        <td colspan="4">{{ $asigt->n_cred_asigt }}</td>
    </tr>
</table>
</center>
<br>
<table border="1">
    <tr>
        <td class="alineado_izquierda"><strong>2. JUSTIFICACIÓN: <br></strong>{{ $asigt->just_asigt }}</td>
    </tr>
</table>
<br>
<table border="1">
    <tr>
        <td class="alineado_izquierda"><strong>3. OBJETIVOS: <br>
        <p>3.1 Objetivo General</p>
        <p>{{ $asigt->obj_gen }}</p>
        <p>3.2 Objetivos Específicos</p>
        <p><?php  echo $asigt->obj_esp ?></p>
        </strong></td>
    </tr>
</table>
<br>
<table border="1">
    <tr>
        <td class="alineado_izquierda"><strong>Area:</strong><br>
        {{ $asigt->area }}
        </td>
    </tr>
</table>
<br>
<table border="1">
    <tr>
        <td class="alineado_izquierda" colspan="7"><center><strong>Contenido de la Asignatura</strong></center></td>
    </tr>
    <tr>
        <th><center>Horas de contenido</center></th>
        <th colspan="2"><center>Tema o Capítulo</center></th>
@if (Auth::check())
@if (Auth::user()->rol=='admin')
        <th colspan="2"><center>Acciones</center></th>
        <th colspan="2"><center><a class="btn btn-primary" href="{{ route('admin.contenido.create',[$asigt->id]) }}">Nuevo</a></center></th>
@endif
@endif    
    </tr>
    @foreach ($asigt->contasigts as $cont_asig_i)
    <tr>
        <td>{{ $cont_asig_i->hras_cont_asigt }}</td>
        <td colspan="2"><?php echo $cont_asig_i->tem_cap_cont_asigt ?></td>
@if (Auth::check())
@if (Auth::user()->rol=='admin')
        <td colspan="2"><a class="btn btn-info" href="{{ route('admin.contenido.edit',[$cont_asig_i->id]) }}">Modificar</a></td>
        <td colspan="2"><a class="btn btn-danger"  onclick="return confirm('¿Esta ud seguro que quiere eliminar este contenido?')" href="{{ route('admin.contenido.delete',[$cont_asig_i->id]) }}">Eliminar</a></td>
@endif
@endif 
    </tr>
    @endforeach
</table>
<br>
<table border="1">
    <tr>
        <td class="alineado_izquierda" colspan="4"><center><strong>Asignaciones de la Asignatura</strong></center></td>
    </tr>
    <tr>
        <th><center>Facultad / Departamento / Programa</center></th>
        <th><center>Docente</center></th>
        <th><center>Periodo Académico</center></th>
      @if (Auth::check()) @if (Auth::user()->rol=="admin" or Auth::user()->rol=="director")
        <!--th><center>Estudiante</center></th-->
      @endif @endif
    </tr>
    @foreach ($asigt->asigcs as $asigc_i)
    <tr>
        <td>{{ $asigc_i->prog->depto->fac->nom_fac }} / {{ $asigc_i->prog->depto->nom_depto }} / <strong>{{ $asigc_i->prog->nom_prog }}</strong> </td>
        <td><?php /* {{ $asigc_i->ident_docnt }}<br>*/?>{{ $asigc_i->docente_fk->name }}</td>
        <td>{{ $asigc_i->per_acad }}</td>
       @if (Auth::check()) @if (Auth::user()->rol=="admin" or Auth::user()->rol=="director")
      <!--td>
            @if(isset($asigc_i->estudiante_fk->name))
            <?php /*{{ $asigc_i->estudiante }}<br>*/?>{{ $asigc_i->estudiante_fk->name }}
            @endif        
        </td-->
      @endif @endif
    </tr>
    @endforeach
</table>
<br>
</span>
<a href="{{ route('admin.asignatura.index') }}" class="btn btn-secondary hidden-print">Regresar</a>
@endsection