@extends('layouts.app')
@section('vars')
{!!
$page_title = 'Asignaturas'; 
$route = 'asignatura'; 
!!}
@endsection
@section('ruta_de_migas')
<ol class="breadcrumb">
    <li><a href="{{ route('home') }}"> Inicio</a></li>
    <li><a href="{{ secure_asset('admin/asignatura') }}"> {{ $page_title }}</a></li>
    <li class="active"> Registrar</li>
</ol>
@endsection
@section('scripts')
<script>
    $(document).ready(function() {
        required_en_formulario_for("admin_asignatura_update","red","*")
    });
    </script>
@endsection
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><center><h1>{{ $page_title }}</h1></center></div>

                <div class="card-body">
                    <form method="POST" id="admin_asignatura_update" action="{{ route('admin.asignatura.update',[$asigt->id]) }}">
                       <meta name="csrf-token" content="{{ csrf_token() }}">
                       <input type="hidden" name="_token" value="{{ csrf_token() }}">
                       <input type="hidden" name="asigt_id" value="{{ $asigt->id }}">

                        <div class="form-group row">
                            <label for="codigo_asigt" class="col-md-4 col-form-label text-md-right">{{ __('Código de la Asignatura') }}</label>
                            <div class="col-md-6">
                                <input id="codigo_asigt" type="number" min="0" class="form-control{{ $errors->has('codigo_asigt') ? ' is-invalid' : '' }}" name="codigo_asigt" value="{{ ($errors->has('codigo_asigt')) ?  old('codigo_asigt') : $asigt->codigo_asigt }}" placeholder="Código de la Asignatura" required autofocus>

                                @if ($errors->has('codigo_asigt'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('codigo_asigt') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
    
                        <div class="form-group row">
                            <label for="nom_asigt" class="col-md-4 col-form-label text-md-right">{{ __('Nombre de la Asignatura') }}</label>

                            <div class="col-md-6">
                                <input id="nom_asigt" type="text" class="form-control{{ $errors->has('nom_asigt') ? ' is-invalid' : '' }}" name="nom_asigt" value="{{ ($errors->has('nom_asigt')) ?  old('nom_asigt') : $asigt->nom_asigt }}" placeholder="Nombre de la Asignatura" required autofocus>

                                @if ($errors->has('nom_asigt')) 
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('nom_asigt') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="sem_ofrece_asigt" class="col-md-4 col-form-label text-md-right">{{ __('Semestres a los Cuales se ofrece') }}</label>

                            <div class="col-md-6">
                                
                            <?php $class_sem_ofrece_asigt = 'form-control';
                            $class_sem_ofrece_asigt .= ' js-example-tokenizer ';
                            $class_sem_ofrece_asigt .= $errors->has('sem_ofrece_asigt') ? ' is-invalid' :  '';
                            ?>
                            {!!
                            Form::select('sem_ofrece_asigt[]', [
                                 'Primero'=>'Primero',
                                 'Segundo'=>'Segundo',
                                 'Tercero'=>'Tercero',
                                 'Cuarto'=>'Cuarto',
                                 'Quinto'=>'Quinto',
                                 'Sexto'=>'Sexto',
                                 'Septimo'=>'Septimo',
                                 'Octavo'=>'Octavo',
                                 'Noveno'=>'Noveno',
                                 'Decimo'=>'Decimo',
                                 'Trece'=>'Trece'
                                 ], 
                                ($errors->has('sem_ofrece_asigt')) ?  old('sem_ofrece_asigt') : explode(", ",$asigt->sem_ofrece_asigt)
                                , [
                                'id' => 'sem_ofrece_asigt',
                                'class' => $class_sem_ofrece_asigt,
                                'multiple' => 'multiple',
                                'required',
                                ])!!}
                                
                                @if ($errors->has('sem_ofrece_asigt'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('sem_ofrece_asigt') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="ihs_asigt" class="col-md-4 col-form-label text-md-right">{{ __('Intensidad Horaria Semanal') }}</label>

                            <div class="col-md-6">
                                <input id="ihs_asigt" type="text" class="form-control{{ $errors->has('ihs_asigt') ? ' is-invalid' : '' }}" name="ihs_asigt" value="{{ ($errors->has('ihs_asigt')) ?  old('ihs_asigt') : $asigt->ihs_asigt }}" placeholder="Intensidad Horaria Semanal" required autofocus>

                                @if ($errors->has('ihs_asigt'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('ihs_asigt') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label for="n_cred_asigt" class="col-md-4 col-form-label text-md-right">{{ __('Número de Créditos') }}</label>

                            <div class="col-md-6">
                                <input id="n_cred_asigt" type="text" class="form-control{{ $errors->has('n_cred_asigt') ? ' is-invalid' : '' }}" name="n_cred_asigt" value="{{ ($errors->has('n_cred_asigt')) ?  old('n_cred_asigt') : $asigt->n_cred_asigt }}" placeholder="Intensidad Horaria Semanal ó Número de Créditos" required autofocus>

                                @if ($errors->has('n_cred_asigt'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('n_cred_asigt') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="just_asigt" class="col-md-4 col-form-label text-md-right">{{ __('Justificación') }}</label>

                            <div class="col-md-6">
                                <textarea id="just_asigt" class="form-control{{ $errors->has('just_asigt') ? ' is-invalid' : '' }}" name="just_asigt" placeholder="Justificación" required autofocus>{{ ($errors->has('just_asigt')) ?  old('just_asigt') : $asigt->just_asigt }}</textarea>

                                @if ($errors->has('just_asigt'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('just_asigt') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="obj_gen" class="col-md-4 col-form-label text-md-right">{{ __('Objetivo General') }}</label>

                            <div class="col-md-6">
                                <textarea id="obj_gen" class="form-control{{ $errors->has('obj_gen') ? ' is-invalid' : '' }}" name="obj_gen"  placeholder="Objetivo General" required>{{ ($errors->has('obj_gen')) ?  old('obj_gen') : $asigt->obj_gen }}</textarea>

                                @if ($errors->has('obj_gen'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('obj_gen') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="obj_esp" class="col-md-4 col-form-label text-md-right">{{ __('Objetivos Específicos') }}</label>

                            <div class="col-md-6">
                                <textarea id="obj_esp" type="text" class="form-control{{ $errors->has('obj_esp') ? ' is-invalid' : '' }}" name="obj_esp" placeholder="Objetivos Específicos" required>{{ ($errors->has('obj_esp')) ?  old('obj_esp') : $asigt->obj_esp }}</textarea>
<script src="{{ secure_asset('lib/ckeditor/ckeditor.js') }}"></script>
<script>
document.addEventListener("DOMContentLoaded", function(event) {
var editorElement = CKEDITOR.replace( 'obj_esp' );
});
</script>
                                @if ($errors->has('obj_esp'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('obj_esp') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="area" class="col-md-4 col-form-label text-md-right">{{ __('Área') }}</label>

                            <div class="col-md-6">
                                <input id="area" type="text" class="form-control" name="area" value="{{ ($errors->has('area')) ?  old('area') : $asigt->area }}" placeholder="Área" required>
                                @if ($errors->has('area'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('area') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
    
                        <div class="form-group row">
                            <input type="hidden" name="detalles" value="NO">
                            <input id="detalles" type="checkbox" name="detalles" value="SI" checked>
                            <label for="detalles">Ver Detalles al guardar</label>
                        </div>
                        
    
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                              <a href="{{ route('admin.asignatura.index') }}" class="btn btn-secondary hidden-print">Regresar</a>
<input type="submit" name="submit" value="Modificar" class="btn btn-primary">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
