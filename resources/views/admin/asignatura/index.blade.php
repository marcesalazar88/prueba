@extends('layouts.app')
@section('vars')
{!! $page_title = 'Asignaturas'; 
!!}
@endsection
@section('scripts')
@section('barra_buscar_cuerpo')
<nav class="navbar navbar-default">
<ol class="breadcrumb navbar-left">
    <li><a href="{{ route('home') }}"> Inicio</a></li>
    <li class="active"> {{ $page_title }}</li>
</ol>
<div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
<ul class="nav navbar-nav navbar-right">
    @include('layouts.partials.buscar')
</ul>
</nav>
@endsection
<script>
$(document).ready(function(){
$("#buscar").keyup(function(){
  buscar_asignatura();
});
$("#buscar").change(function(){
  buscar_asignatura();
});
$("#search-btn").click(function(){
  buscar_asignatura();
});
$("#num_resultados").keyup(function(){
  buscar_asignatura();
});
$("#num_resultados").change(function(){
  buscar_asignatura();
});
$("#menu_usuarios").addClass('active');
buscar_asignatura();
});
</script>
@endsection
@section('ruta_de_migas')
@endsection
@section('content')
<center><h1>{{ $page_title }}</h1></center>
<div class='row'>
  <div class='col-md-12'>
  <span id="txt_resultados">
</span>
 <div id="area_pagination"></div>
    
    
  </div><!-- /.col -->
  <div class='col-md-6'>

  </div><!-- /.col -->

</div><!-- /.row -->
@endsection