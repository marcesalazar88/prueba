@extends('layouts.app')
@section('vars')
{!!
$page_title = 'Contenido de asignatura'; 
$route = 'contenido'; 
!!}
@endsection
@section('ruta_de_migas')
<ol class="breadcrumb">
    <li><a href="{{ route('home') }}"> Inicio</a></li>
    <li><a href="{{ route('admin.asignatura.index') }}"> Asignaturas</a></li>
    <li><a href="{{ route('admin.asignatura.show',[$contenido->asigt->id]) }}"> {{ $page_title }}</a></li>
    <li class="active"> Registrar</li>
</ol>
@endsection
@section('scripts')
<script>
    $(document).ready(function() {
        required_en_formulario_for("admin_contenido_create","red","*")
    });
    </script>
@endsection
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><center><h1>{{ $page_title }}</h1></center></div>

                <div class="card-body">
                    <form method="POST" id="admin_contenido_create" action="{{ route('admin.contenido.update',[
                    'usuario' => $contenido->id,
                    ]) }}">
                       <meta name="csrf-token" content="{{ csrf_token() }}">
                       <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <div class="form-group row">
                            <label for="codigo_asigt" class="col-md-4 col-form-label text-md-right">{{ __('Código de la asignatura') }}</label>
                            <div class="col-md-6">
                                <input id="id" type="hidden" name="id" value="{{ (isset($contenido->id)) ? $contenido->id : old('id') }}" required>
                                <input id="cod_asigt" type="hidden" name="cod_asigt" value="{{ (isset($contenido->asigt->codigo_asigt)) ? $contenido->asigt->codigo_asigt : old('cod_asigt') }}" required>
                                <input readonly id="codigo_asigt" type="text" class="form-control" value="{{ (isset($contenido->asigt->codigo_asigt)) ? $contenido->asigt->codigo_asigt : '' }}" placeholder="Código de la contenido" required autofocus>

                                @if ($errors->has('cod_asigt'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('cod_asigt') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
    
                        <div class="form-group row">
                            <label for="nom_asigt" class="col-md-4 col-form-label text-md-right">{{ __('Nombre de la asignatura') }}</label>

                            <div class="col-md-6">
                                <input readonly id="nom_asigt" type="text" class="form-control" value="{{ (isset($contenido->asigt->nom_asigt)) ? $contenido->asigt->nom_asigt : '' }}" placeholder="Nombre de la asignatura" required autofocus>

                                @if ($errors->has('nom_asigt'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('nom_asigt') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="hras_cont_asigt" class="col-md-4 col-form-label text-md-right">{{ __('Horas de contenido') }}</label>

                            <div class="col-md-6">
                                <input id="hras_cont_asigt" type="number" class="form-control{{ $errors->has('hras_cont_asigt') ? ' is-invalid' : '' }}"  name="hras_cont_asigt" value="{{ (isset($contenido->hras_cont_asigt)) ? $contenido->hras_cont_asigt : old('hras_cont_asigt') }}" placeholder="Horas de contenido" required autofocus>

                                @if ($errors->has('hras_cont_asigt'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('hras_cont_asigt') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="tem_cap_cont_asigt" class="col-md-4 col-form-label text-md-right">{{ __('Tema o Capítulo') }}</label>

                            <div class="col-md-6">
                                <textarea id="tem_cap_cont_asigt" class="form-control{{ $errors->has('tem_cap_cont_asigt') ? ' is-invalid' : '' }}" name="tem_cap_cont_asigt" placeholder="Tema o Capítulo" required autofocus>{{ (isset($contenido->tem_cap_cont_asigt)) ? $contenido->tem_cap_cont_asigt : old('tem_cap_cont_asigt') }}</textarea>
<script src="{{ secure_asset('lib/ckeditor/ckeditor.js') }}"></script>
<script>
document.addEventListener("DOMContentLoaded", function(event) {
var editorElement = CKEDITOR.replace( 'tem_cap_cont_asigt' );
});
</script>
                                @if ($errors->has('tem_cap_cont_asigt'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('tem_cap_cont_asigt') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <a class="btn btn-secondary" href="{{ route('admin.asignatura.show',[$contenido->asigt->id]) }}">Regresar</a>
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Modificar') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
