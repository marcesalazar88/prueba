@extends('layouts.app')
@section('vars')
{!! $page_title = 'Detalles'; 
!!}
@endsection
@section('barra_buscar_cuerpo')
<nav class="navbar navbar-default">
<ol class="breadcrumb navbar-left">
    <li><a href="{{ route('home') }}"> Inicio</a></li>
  <li><a href="{{ route('notificaciones.index') }}">Notificaciones</a></li>
    <li class="active"> {{ $page_title }}</li>
</ol>
<div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
<ul class="nav navbar-nav navbar-right">
    @include('layouts.partials.buscar')
</ul>
</nav>
@endsection
@section('scripts')
@endsection
@section('ruta_de_migas')
@endsection
@section('content')
<center><h1>{{ $page_title }}</h1></center>
<div class='row'>
  <div class='col-md-12'>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Mensaje</h3>
      </div>
      <div class="panel-body">
         <?php echo $notificacion->mensaje; ?>
      </div>
      <div class="panel-footer">
        Fecha y hora de envío: {{ Fecha::formato_fecha_hora($notificacion->created_at) }}
      </div>
    </div>
  </div><!-- /.col -->
  <div class='col-md-6'>

  </div><!-- /.col -->

</div><!-- /.row -->
@endsection