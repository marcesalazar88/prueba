@extends('layouts.app')
@section('vars')
{!!
$page_title = 'Facultades'; 
$route = 'facultad'; 
!!}
@endsection
@section('ruta_de_migas')
<ol class="breadcrumb">
    <li><a href="{{ route('home')}}"> Inicio</a></li>
    <li><a href="{{ secure_asset('admin/facultad') }}"> {{ $page_title }}</a></li>
    <li class="active"> Registrar</li>
</ol>
@endsection
@section('scripts')
<script>
    $(document).ready(function() {
        password_en_formulario("admin_facultad_create");
    required_en_formulario_for("admin_facultad_create","red","*")
    });
    </script>
@endsection
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"></div>

                <div class="card-body">
                    <form method="POST" id="admin_facultad_create" action="{{ route('admin.facultad.store') }}">
                         <meta name="csrf-token" content="{{ csrf_token() }}">
                         <input type="hidden" name="_token" value="{{ csrf_token() }}">

                       <div class="form-group row">
                            <label for="id" class="col-md-4 col-form-label text-md-right">{{ __('Código') }}</label>

                            <div class="col-md-6">
                                <input id="id" type="number" class="form-control{{ $errors->has('id') ? ' is-invalid' : '' }}" name="id" value="{{ old('id') }}" required autofocus>

                                @if ($errors->has('id'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                       <div class="form-group row">
                            <label for="nom_fac" class="col-md-4 col-form-label text-md-right">{{ __('Nombre') }}</label>

                            <div class="col-md-6">
                                <input id="nom_fac" type="text" class="form-control{{ $errors->has('nom_fac') ? ' is-invalid' : '' }}" name="nom_fac" value="{{ old('nom_fac') }}" required autofocus>

                                @if ($errors->has('nom_fac'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('nom_fac') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                            <a href="{{ route('admin.facultad.index') }}" class="btn btn-secondary hidden-print">Regresar</a>
                                <input type="submit" name="submit" value="Registrar" class="btn btn-primary">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
