@extends('layouts.app')
@section('vars')
{!!
$facultad = "Ciencias Exactas y Naturales";
$programa = "Licenciatura en Informática";
$facultad = mb_strtoupper($facultad,'utf-8');
$programa = mb_strtoupper($programa,'utf-8');
$page_title = "IDENTIFICACI&Oacute;N DE LOS PROGRAMAS A LOS CUALES SE OFRECE";
!!}
@endsection
@section('style')
<style>
  table tr th, table tr td {
    text-align: center;
}
</style>
@endsection
@section('barra_buscar')
<li><a onclick="window.print()" title="Imprimir">
<span class="glyphicon glyphicon-print"></span>
</a></li>
@endsection
@section('ruta_de_migas')
<ol class="breadcrumb">
    <li><a href="{{ route('home') }}"> Inicio</a></li>
    <li class="active"> Flexibilidad</li>
</ol>
@endsection
@section('scripts')
@include('reportes.css')
@endsection
@section('content')
<center><strong><p>{{ $page_title }}</p></strong></center>
<div class='row'>
  <div class='col-md-12'>
    <table border="1">
      <thead>
      <tr>
        <th colspan="2">Asignatura</th>
        <th>Programa</th>
        <th>Periodo de Ingreso</th>
        <!--th>Periodo acadmico</th-->
      </tr>
    </thead>
    <body>
  @foreach ($asigc as $asigt_i)
    <!--
      <div class="panel panel-default" style="width: 24rem;display:inline-block">
  <div class="panel-body">
    <h5 class="card-title">{{ $asigt_i->nom_asigt }}</h5>
    @if ($asigt_i->flex == 'SI')
    <span class="badge badge-success">Flexibilidad</span>
    @endif
    <h6 class="card-title">
    Docente: ({{ $asigt_i->ident_docnt }}) {{ $asigt_i->name }}
    </h6>
    <p class="card-text">{{ $asigt_i->obj_gen }}</p>
    <!--a href="#" class="btn btn-primary">Go somewhere</a- ->
<nav class="navbar navbar-default">
  <div class="container-fluid">
   <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#"></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling - ->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
            <li><a href="{{ route('reportes.prog_tem',$asigt_i->id) }}">Programación Temática</a></li>
            <li><a href="#">Seguimiento</a></li>
            <li><a href="#">Informe Final</a></li>
      </ul>
    </div><!-- /.navbar-collapse - ->
  </div><!-- /.container-fluid - ->
</nav>
</div>
</div>
-->
<tr>
  <td>{{ $asigt_i->cod_asigt }}</td>
  <td><a href="{{ route('admin.asignatura.show',[$asigt_i->asigt_id]) }}">{{ $asigt_i->nom_asigt }}</a></td>
  <td>{{ $asigt_i->prog->nom_prog }}</td>
  <td>{{ $asigt_i->prog->periodo_ingreso }}</td>
  <!--td>{{ $asigt_i->per_acad}}</td-->
</tr>
@endforeach
</body>
</table>
  </div>
</div>

@endsection