@extends('layouts.app')
@section('vars')
{!!
$facultad = "Ciencias Exactas y Naturales";
$programa = "Licenciatura en Informática";
$facultad = mb_strtoupper($facultad,'utf-8');
$programa = mb_strtoupper($programa,'utf-8');

!!}
@endsection
@section('ruta_de_migas')
<ol class="breadcrumb">
    <li><a href="{{ route('home') }}"> Inicio</a></li>
    <li class="active"> Seguimiento</li>
</ol>
@endsection
@section('barra_buscar')
<li><a onclick="window.print()" title="Imprimir">
<span class="glyphicon glyphicon-print"></span>
</a></li>
@endsection
@section('scripts')
@include('reportes.css')
@endsection
@section('content')
<span id="reporte">
<center>
<table border="1">
    <tr>
        <th rowspan="4">
            <center><img src="{{ secure_asset('img/logo-udenar.png')}}"></center>
            <span id="titulo_logo"><center>Universidad <span class="m10">de</span></center><center><strong>Nariño</strong></center></span>
        </th>
        <th rowspan="4">
            <div class="titulo">FORMACIÓN ACADÉMICA</div>
            <div class="titulo">{{ mb_strtoupper($facultad,'utf-8') }}</div>
            <div class="titulo">PROGRAMA DE {{ mb_strtoupper($programa,'utf-8') }}</div>
            <div class="titulo negrita"><strong>SEGUIMIENTO AL CONTENIDO POR ASIGNATURA</strong></div>
        </th>
        <th>Código: FOA-FR-14</th>
    </tr>
    <tr>
        <th>Página 1 de 1</th>
    </tr>
    <tr>
        <th>Versión: 4</th>
    </tr>
    <tr>
        <th>Vigente a partir de: {{ Fecha::formato_fecha('2011-01-24') }}</th>
    </tr>

</table></center>
<br>
<table border="1" class="cssresponsive">
    <tr class="hidden-xs">
        <td colspan="1" style="width:20%;padding: 0px;margin:0px;"></td>
        <td colspan="1" style="width:20%;padding: 0px;margin:0px;"></td>
        <td colspan="1" style="width:20%;padding: 0px;margin:0px;"></td>
        <td colspan="1" style="width:20%;padding: 0px;margin:0px;"></td>
        <td colspan="1" style="width:20%;padding: 0px;margin:0px;"></td>
    </tr>
        @foreach ($asigc->segs as $seg)
    <tr>
        <td colspan="2"><strong>SEMESTRE:</strong> {{ $asigc->sem_ofrece_asigt }}</td>
        <td colspan="2"><strong>DOCENTE: </strong> {{ $asigc->name }}</td>
        <td rowspan="2"><?php $ruta = route('reportes.buscar')."?codigo=".$seg->codigo; ?>
<center>
<img style="width:35mm;height:35mm;" src="{{ route('qrcode.create',[300])}}?url={{$ruta}}">
            </center></td>
    </tr>
    <tr>
        <td colspan="2"><strong>FECHA DE REVISIÓN DOCENTE: </strong> {{ Fecha::formato_fecha($seg->fecha_revision_doc) }}<br>
      <strong>FECHA DE REVISIÓN ESTUDIANTE: </strong> {{ Fecha::formato_fecha($seg->fecha_revision_doc) }}</td>
        <td colspan="2"><strong>AREA CURRICULAR:</strong>  {{ $asigc->area }}</td>
    </tr>
    <tr>
        <td><strong>ASIGNATURA:</strong></td>
        <td colspan="4">{{ $asigc->nom_asigt }} ({{ $asigc->codigo_asigt }})</td>
    </tr>
    

     <tr class="cabecera_seguimiento">
        <td colspan="1" style="min-width: 250px;"></td>
        @foreach ($tipovaloracion as $itemval)
        <td colspan="2"><strong>{{ $itemval->descripcion }}:</strong><br>(estudiantes y docentes)</td>
        @endforeach
    </tr>
    @foreach ($criterios as $id => $itemcriterio)
    <tr>
        <td colspan="1">{{$itemcriterio}}</td>
    <?php $total_cols=0; ?>
    @foreach ($tipovaloracion as $itemval)
    <?php $total_cols++; ?>
        <td colspan="2">
          <span class="hidden-sm hidden-md hidden-lg">{{$itemval->descripcion}}</span>
    <ul class="list-group list-group-flush">
        <?php
        $seguim = $asigc->segs->first()->seg_vals()->get();
        //dd($seguim);
        ?>
                        @foreach ($seguim as $itemseg)
                            @if ($itemval->name==$itemseg->tipo)
                                @if ($id==$itemseg->aspecto)
  <li class="list-group-item">
                  <!--div style="border:solid 1px #000; margin:15px"--> 
                                    <p>
                                    <!-- Trigger the modal with a button -->
                                    <span><?php 
                                    //echo $itemseg->seg_id." ->";
                                    echo $itemseg->valoracion;
                                    ?></span>

                                    @if ($itemseg->voto=="Pendiente")
                                    <span style="float:right;background-color: orange; color: white;" class="label label-success" title="Pendiente">P</span><br>
                                    @endif
                                    @if ($itemseg->voto=="Aprueba")
                                    <span style="float:right;background-color: green; color: white;" class="label label-success" title="Aprueba">A</span><br>
                                    @endif
                                    @if ($itemseg->voto=="Rechaza")
                                    <span style="float:right;background-color: red; color: white;" class="label label-success" title="Rechaza">R</span><br>
                                    <p><strong>Argumentación:</strong> {{$itemseg->replica}}</p>
                                    @endif
                                    </p>
                                    
                                    <!--/div-->
                                    </li>
                                @endif
                            @endif
                        @endforeach
                                    </ul>
        </td>
        @endforeach
    </tr>
    @endforeach
</table>
<br>
<table>
 <tr>
   <th colspan="3" class="alineado_izquierda borde_inferior"><strong>Iconografía</strong></th>
</tr>
 <tr>
    <td><span class="label label-success" style="background-color: orange; color: white;">P</span> : Pendiente</td>
    <td><span class="label label-success" style="background-color: red; color: white;">R</span> : Rechazado</td>
    <td><span class="label label-success" style="background-color: green; color: white;">A</span> : Aprobado</td>
</tr>
</table>
<br>
<table>
 <tr>
     <td colspan="6" class="alineado_izquierda borde_inferior"><strong>Observaciones</strong>
</tr>
</table>´
<table border="1" class="table cssresponsive">  
  <thead>
  <tr>
<th><center>Fecha</center></th>
<th><center>Usuario</center></th>
<th colspan="4" ><center>Mensaje</center></th>
</tr>
    </thead>
  <tbody>
   @foreach ($mensajes as $mensajei)
<tr>
<td data-label="Fecha" >{{Fecha::formato_fecha_hora($mensajei->created_at)}}</td>
<td data-label="Usuario" > {{$mensajei->de_fk->nombre}} {{$mensajei->de_fk->apellido}} ({{ Funciones::rol($mensajei->rol)}})</td>
<td  data-label="Mensaje" colspan="4" >{{ $mensajei->texto }}</td>
 </tr>
  @endforeach
    </tbody>
<table>
 @endforeach
 
<br><br><br>
 <tr>
        <td colspan="2" style="width:40%;" class="alineado_izquierda borde_superior"><center><strong>{{ Funciones::nombre_usuario($asigc->estudiante) }}</strong><br>REPRESENTANTE ESTUDIANTIL</td>
        <td colspan="1" style="width:20%;"></center></td>
        <td colspan="2" style="width:40%;" class="alineado_izquierda borde_superior"><center><strong>{{ $asigc->name }}</strong><br>DOCENTE</center></td>
    </tr>
</table>
<br>
<footer>
 <p>Para verificar este documento, ingrese a la dirección {{route('reportes.buscar')}} donde podrá escanear el código QR o ingresar el siguiente código: <strong>{{ $seg->codigo }}</strong></p>
</footer>
</span>
@endsection