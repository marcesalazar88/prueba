<title>SEGUIMIENTO AL CONTENIDO POR ASIGNATURA</title>
<span id="reporte">
<center>
<table border="1" style="width: 100%;">
    <tr>
        <th rowspan="4">
            <center><img src="{{ secure_asset('img/logo-udenar.png')}}"></center>
            <span id="titulo_logo"><center>Universidad <span class="m10">de</span></center><center><strong>Nariño</strong></center></span>
        </th>
        <th rowspan="4">
            <div class="titulo">FORMACIÓN ACADÉMICA</div>
            <div class="titulo">{{ mb_strtoupper($facultad,'utf-8') }}</div>
            <div class="titulo">PROGRAMA DE {{ mb_strtoupper($programa,'utf-8') }}</div>
            <div class="titulo negrita"><strong>SEGUIMIENTO AL CONTENIDO POR ASIGNATURA</strong></div>
        </th>
        <th>Código: FOA-FR-14</th>
    </tr>
    <tr>
        <th>Página 1 de 1</th>
    </tr>
    <tr>
        <th>Versión: 4</th>
    </tr>
    <tr>
        <th>Vigente a partir de: {{ Fecha::formato_fecha('2011-01-24') }}</th>
    </tr>

</table></center>
<br>
<table border="1" style="width: 100%;">
    <tr>
        <td colspan="1" style="width:20%;border:0px;padding: 0px;margin:0px;"></td>
        <td colspan="1" style="width:20%;border:0px;padding: 0px;margin:0px;"></td>
        <td colspan="1" style="width:20%;border:0px;padding: 0px;margin:0px;"></td>
        <td colspan="1" style="width:20%;border:0px;padding: 0px;margin:0px;"></td>
        <td colspan="1" style="width:20%;border:0px;padding: 0px;margin:0px;"></td>
    </tr>
        @foreach ($asigc->segs as $seg)
    <tr>
        <td colspan="2"><strong>SEMESTRE:</strong> {{ $asigc->sem_ofrece_asigt }}</td>
        <td colspan="2"><strong>DOCENTE: </strong> {{ $asigc->name }}</td>
        <td rowspan="2"><?php $ruta = route('reportes.buscar')."?codigo=".$seg->codigo; ?>
<center>
<img style="width:35mm;height:35mm;" src="{{ route('qrcode.create',[300])}}?url={{$ruta}}">
            </center></td>
    </tr>
    <tr>
        <td colspan="2"><strong>FECHA DE REVISIÓN DOCENTE: </strong> {{ Fecha::formato_fecha($seg->fecha_revision_doc) }}<br>
      <strong>FECHA DE REVISIÓN ESTUDIANTE: </strong> {{ Fecha::formato_fecha($seg->fecha_revision_doc) }}</td>
        <td colspan="2"><strong>AREA CURRICULAR:</strong>  {{ $asigc->area }}</td>
    </tr>
    <tr>
        <td><strong>ASIGNATURA:</strong></td>
        <td colspan="4">{{ $asigc->nom_asigt }} ({{ $asigc->codigo_asigt }})</td>
    </tr>
    
  </table>
<style>
table{
  border-collapse: collapse;
        text-align:center;
        width:100%;
    }
    table tr th, table tr td{
        padding:5px;
    }
    .titulo{
        text-align:center;
        font-weight:lighter !important;
    }
    .negrita{
         font-weight:bold !important;
    }
    .reporte *{
         font-family: "Humanst521 BT";
         font-weight:lighter;
    }
    #titulo_logo{
        text-align:center;
        margin:0 auto;
        font-weight:lighter !important;
    }
    #titulo_logo strong{
        font-weight:bold;   
    }
    .m10{
        font-weight:lighter;
        font-size:.9em;
    }
    .alineado_izquierda{
        text-align:left;
    }
    .borde_superior{
        border-top:1px solid #969696;
    }
    .borde_inferior{
        border-bottom:1px solid #969696;
    }

.contenedor-tabla {
    display: table;
    width: 100%;
  }
  .contenedor-tabla.pagebreakafteralways {
    page-break-after:always;
    }
.contenedor-tabla.border {
    border: 1px solid #000000;
}
.contenedor-fila{
display: table-row;
}
.contenedor-columna.border{
    border: 1px solid #969696;
}
.contenedor-columna{
display: table-cell;
   }
  ul {
    list-style-type: none;
}
 </style>
<div class="contenedor-tabla border pagebreakafteralways">
     <div class="contenedor-fila cabecera_seguimiento" >
         <div class="contenedor-columna border"  style="width: 20%">
           
       </div>
        @foreach ($tipovaloracion as $itemval)
        <div class="contenedor-columna border" style="width: <?php echo ((100-20)/count($tipovaloracion)) ?>% !important;">
            <strong>{{ $itemval->descripcion }}:</strong><br>(estudiantes y docentes)
          </div>
        @endforeach
    </div>
    @foreach ($criterios as $id => $itemcriterio)
    <div class="contenedor-fila">
      <div class="contenedor-columna border"  style="width: 20%">
        {{$itemcriterio}} 
      </div>
    <?php $total_cols=0; ?>
    @foreach ($tipovaloracion as $itemval)
    <?php $total_cols++; ?>
        <div class="contenedor-columna border" style="width: <?php echo ((100-20)/count($tipovaloracion)) ?>% !important;">
    <ul class="list-group list-group-flush">
        <?php
        $seguim = $asigc->segs->first()->seg_vals()->get();
        //dd($seguim);
        ?>
                        @foreach ($seguim as $itemseg)
                            @if ($itemval->name==$itemseg->tipo)
                                @if ($id==$itemseg->aspecto)
  <li class="list-group-item">
                  <!--div style="border:solid 1px #000; margin:15px"--> 
                                    <p>
                                    <!-- Trigger the modal with a button -->
                                    <span><?php 
                                    //echo $itemseg->seg_id." ->";
                                    echo $itemseg->valoracion;
                                    ?></span>

                                    @if ($itemseg->voto=="Pendiente")
                                    <span style="float:right;background-color: orange; color: white;" class="label label-success" title="Pendiente">P</span><br>
                                    @endif
                                    @if ($itemseg->voto=="Aprueba")
                                    <span style="float:right;background-color: green; color: white;" class="label label-success" title="Aprueba">A</span><br>
                                    @endif
                                    @if ($itemseg->voto=="Rechaza")
                                    <span style="float:right;background-color: red; color: white;" class="label label-success" title="Rechaza">R</span><br>
                                    <p><strong>Argumentación:</strong> {{$itemseg->replica}}</p>
                                    @endif
                                    </p>
                                    
                                    <!--/div-->
                                    </li>
                                @endif
                            @endif
                        @endforeach
                                    </ul>
        </div>
        @endforeach
    </div>

    @endforeach
 <div class="contenedor-fila">
    <div class="contenedor-columna alineado_izquierda borde_inferior"><strong>Observaciones Estudiante</strong>
     <p>{{ $seg->observ_est }}</p>
     </div>
</div>
 <div class="contenedor-fila">
     <div class="contenedor-columna alineado_izquierda borde_inferior"><strong>Observaciones Docente:</strong>
     <p>{{ $seg->observ_docnt }}</p>
     </div>
</div>
<p></p><p></p>
     <div class="contenedor-fila alineado_izquierda borde_superior">
         <div class="contenedor-columna"  style="width: 40%">
           <strong>FIRMA REPRESENTANTE ESTUDIANTIL</strong>
        </div>
         <div class="contenedor-columna"  style="width: 20%">
           
        </div>
         <div class="contenedor-columna"  style="width: 40%">
           <strong>FIRMA DEL DOCENTE</strong>
        </div>
      </div>
</div>
<br>       
<div style="page-break-after:always;">

@endforeach
</span>
<footer>
 <p>Para verificar este documento, ingrese a la dirección {{route('reportes.buscar')}} donde podrá escanear el código QR o ingresar el siguiente código: <strong>{{ $seg->codigo }}</strong></p>
</footer>
</span>