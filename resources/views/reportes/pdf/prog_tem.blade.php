<style>
    /*.table {page-break-before:auto;}*/
    * {
        page-break-after:auto;
    }
</style>
<title>PROGRAMACIÓN TEMÁTICA POR ASIGNATURA</title>
<span id="reporte">
<table border="1" style="width: 100%;">
    <tr>
        <th rowspan="4">
            <center><img src="{{ secure_asset('img/logo-udenar.png')}}"></center>
            <span id="titulo_logo"><center>Universidad <span class="m10">de</span></center><center><strong>Nariño</strong></center></span>
        </th>
        <th rowspan="4">
            <div class="titulo">FORMACIÓN ACADÉMICA</div>
            <div class="titulo">{{ mb_strtoupper($asigt->nom_fac,'utf-8') }}</div>
            <div class="titulo">PROGRAMA DE {{ mb_strtoupper($asigt->nom_prog,'utf-8') }}</div>
            <div class="titulo negrita"><strong>PROGRAMACIÓN TEMÁTICA ASIGNATURA</strong></div>
        </th>
        <th>Código: FOA-FR-07</th>
    </tr>
    <tr>
        <th>Página 1 de 2</th>
    </tr>
    <tr>
        <th>Versión: 4</th>
    </tr>
    <tr>
        <th style="font-weight:bold;">Vigente a partir de: {{ Fecha::formato_fecha('2011-01-18')}}</th>
    </tr>

</table>

<p><strong>1. IDENTIFICACIÓN DE LA ASIGNATURA:</strong></p>

<table border="1" style="width: 100%;">
    <tr>
        <td><strong>NOMBRE DEL DOCENTE:</strong> {{ $asigt->name }}</td>
        <td rowspan="3"><?php if($asigt->codigo and $asigt->codigo!=""){ ?>
           <?php $ruta = route('reportes.buscar')."?codigo=".$asigt->codigo; ?>
          <center>
<img style="width:35mm;height:35mm;" src="{{ route('qrcode.create',[300])}}?url={{$ruta}}">
            </center>
<?php } ?>
</td>
    </tr>
    <tr>
        <td><strong>Correo Electrónico:</strong> {{ $asigt->email }}</td>
    </tr>

    <tr>
        <td colspan="1" class="alineado_izquierda">
          <strong>NOMBRE DE LA ASIGNATURA EN CURSO: </strong> {{ $asigt->nom_asigt }}
        </td>
    </tr>
</table>
<br>
<table border="1" style="width: 100%;">
    <tr>
        <td>Código de la Asignatura:</td>
        <td colspan="4"> {{ $asigt->codigo_asigt }}</td>
    </tr>
    <tr>
        <td>Semestres a los Cuales se ofrece:</td>
        <td colspan="4"> {{ $asigt->sem_ofrece_asigt }}</td>
    </tr>
    <tr>
        <td>Intensidad Horaria Semanal: {{ $asigt->ihs_asigt }}, Número de Créditos: {{ $asigt->n_cred_asigt }}</td>
        <td>Teórica:{{ $asigt->horas_practicas }}</td>
        <td>Práctica:{{ $asigt->horas_teoricas }}</td>
        <td>Adicionales:{{ $asigt->horas_adicionales }}</td>
        <td>Horas Totales:{{ $asigt->horas_practicas + $asigt->horas_teoricas + $asigt->horas_adicionales }}</td>
    </tr>
</table>
<br>
<div class="table" border="1">
    <div>
        <span class="alineado_izquierda"><strong>2. JUSTIFICACIÓN:
        <br></strong>{{ $asigt->just_asigt }}
        </span>
    </div>
</div>
<br>
<div class="table" border="1" style="word-wrap: break-word;">
    <span>
        <span class="alineado_izquierda"><strong>3. OBJETIVOS: </strong>
        <br>
        <p>3.1 Objetivo General</p>
        <p>{{ $asigt->obj_gen }}</p>
        <p>3.2 Objetivos Específicos</p>
        <p><?php echo $asigt->obj_esp ?></p>
        </span>
    </span>
</div>
<br>
<div class="table" border="1">
    <span>
        <span class="alineado_izquierda"><strong>4. METODOLOGÍA:</strong>
        <br>
        <?php echo  $asigt->metodologia ?>
        </span>
    </span>
</div>
<br>
<div border="1">
    <div>
        <span class="alineado_izquierda"><strong>5. CRITERIOS DE EVALUACIÓN:</strong>
        <br>
        <?php echo  $asigt->crit_eva ?>
        </span>
    </div>
</div>
<br>
<p><strong>6. CONTENIDO DE LA ASIGNATURA:</strong></p>
<div border="1">
    <div>
        <span><strong>Horas ó Créditos</strong></span>
        <span><strong>Tema ó Capitulo</strong></span>
        </span>
    </div>
    <?php 
    $total_horas = 0;
    ?>
        @foreach ($asigt->asigc->asigt->contasigts as $cont_asig_i)
    <div>
        <span>{{ $cont_asig_i->hras_cont_asigt }}</span>
        <span><?php echo $cont_asig_i->tem_cap_cont_asigt ?></span>
   
    </div>
    <?php 
    $total_horas += $cont_asig_i->hras_cont_asigt;
    ?>
    @endforeach
     <div>
        <span><strong>Total: {{ $total_horas }}</strong></span>
        <span><strong></strong></span>
    </div>
    
</div>
<br>
<p><strong>7. PUNTO ADICIONAL Y OPCIONAL QUE APLICA A AQUELLOS PROGRAMAS QUE UTILIZAN OTROS FACTORES EN LA PROGRAMACIÓN TEMÁTICA POR ASIGNATURA, Ej. Competencias, Habilidades, etc</strong></p>
@if(count($asigt->punto_adics)>0)
<div border="1">
    <div>
        <span><strong>Item</strong></span>
        <span><strong>Descripción</strong></span>
    </div>
    @foreach ($asigt->punto_adics as $punto_adic)
    <div>
        <span><strong>{{ $punto_adic->item }}</strong></span>
        <span><strong>{{ $punto_adic->valor }}</strong></span>
    </div>
    @endforeach
</div>
@else
<p>No hay puntos adicionales</p>
@endif
<br>
<div border="1">
    <div>
        <span class="alineado_izquierda"><strong>8. REFERENCIAS BIBLIOGRÁFICAS</strong>
        </span>
    </div>
    <div class="alineado_izquierda">
        <span>
            <ul>
              @foreach($asigt->refe_biblios as $biblio)
              <li>{{ $biblio->refe_biblio }}</li>
              @endforeach
            </ul>
        </span>
    </div>
</div>
<!--br>
<center>
<p><strong>FIRMA DEL DOCENTE</strong></p>
</center>
<br>
<center>
<p><strong>FIRMA REPRESENTANTE ESTUDIANTIL</strong></p>
</center>
<br-->
</span>

<footer>
 <p>Para verificar este documento, ingrese a la dirección {{route('reportes.buscar')}} donde podrá escanear el código QR o ingresar el siguiente código: <strong>{{ $asigt->codigo }}</strong></p>
</footer>
</span>