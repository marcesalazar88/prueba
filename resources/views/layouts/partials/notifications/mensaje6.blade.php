Apreciado docente {{$user->name}}.
 
La revisión y aprobación del seguimiento de la asignatura {{$asigc->asigt->nom_asigt}}. grupo {{$asigc->asigt->grupo}}.  del periodo académico {{$periodo}}.  bajo la responsabilidad del representante estudiantil  {{$estudiante->name}}.  ya se encuentra debidamente evaluado.
Para visualizar los aportes del estudiante puede acceder en el siguiente enlace {{env('APP_URL')}}