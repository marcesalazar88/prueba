Apreciado estudiante {{$user->name}},

Como Director del {{$depto->nom_depto}}, le recuerdo el deber de cumplir con los compromisos adquiridos a inicio de este semestre {{$periodo}}, entre ellos está la revision y complementacion de la Seguimiento programación temática por asignatura de la cátedra {{$nom_asigt}} del programa {{$nom_prog}}. Además le recuerdo que ya se encuentra vigente en el sistema Ediweb, al cual puede acceder en el siguiente enlace {{env('APP_URL')}}
Como estudiante representante debe revisa y aprueba los aspectos de la asignatura, hasta la fecha {{$seg->fecha_revision_est}}.

Agrega que esto es un requisito institucional cuyo no cumplimiento puede traer procesos disciplinarios
