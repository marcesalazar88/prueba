
@extends('layouts.app')

@section('style')
<style>
.adminmenu li a{
    color: #000000 !important;
    font-weight: bold !important;
}
</style>
@endsection
@section('ruta_de_migas')
<nav class="navbar navbar-default">
<ol class="breadcrumb navbar-left">
    <li><a href="{{ route('home') }}"> Inicio</a></li>
    <li class="active"> Configuraciones</li>
</ol>
<div class="collapse navbar-collapse" id="app-navbar-collapse">
</nav>
@endsection
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">Administración</div>

                <div class="panel-body">
                   Configuraciones
                  <br>
                  
                  <ul class="nav navbar-nav adminmenu center-xs">
                        @if (Auth::check())
                           
                            @if (Auth::user()->rol=="admin" or Auth::user()->rol=="director")
                                      @if (Auth::user()->rol=="admin")
                                        <li>
                                            <a href="{{ secure_asset('admin/facultad') }}">
                                                <span class="glyphicon glyphicon-list"></span>
                                                Facultades
                                                </a>
                                        </li>
                                        <li>
                                            <a href="{{ secure_asset('admin/departamento') }}">
                                                <span class="glyphicon glyphicon-list-alt"></span>
                                                Departamento
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('admin.programa.index') }}">
                                                <span class="glyphicon glyphicon-list-alt"></span>
                                                Programa
                                            </a>
                                        </li>
                                        
                                        
                                        <li>
                                            <a href="{{ secure_asset('admin/asignatura') }}">
                                                <span class="glyphicon glyphicon-blackboard"></span>
                                                Asignaturas
                                                </a>
                                        </li>
                                      @endif
                                      <li id="menu_config_planes">
                                            <a href="{{ route('admin.planes.index') }}">
                                           <span class="glyphicon glyphicon-blackboard"></span>
                                          Planes
                                          </a>
                                        </li>
                                        <li id="menu_asignacion">
                                            <a href="{{ secure_asset('admin/asignacion') }}">
                                                <span class="glyphicon glyphicon-tasks"></span>
                                                Asignación
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ secure_asset('admin/control') }}">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                                Control de Fechas
                                            </a>
                                        </li>
                                      @if (Auth::user()->rol=="admin")
                                        <li>
                                            <a href="{{ secure_asset('admin/usuarios') }}">
                                                <span class="glyphicon glyphicon-user"></span>
                                                Usuarios
                                            </a>
                                        </li>
                                        <li class="divider"></li>
                                        <li>
                                            <a href="{{ secure_asset('admin/links') }}">
                                                <span class="glyphicon glyphicon-link"></span>
                                                Enlaces
                                                </a>
                                        </li>
                                        <li>
                                            <a href="{{ secure_asset('admin/periodo') }}">
                                                <span class="glyphicon glyphicon-list-alt"></span>
                                                Periodos
                                                </a>
                                        </li>
                                        @endif
                                    
                            @endif

                        @endif

                      
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
