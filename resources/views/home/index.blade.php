@extends('layouts.app')
@section('vars')
@php
$page_title = 'Inicio'; 
use App\User;
@endphp
@endsection
@section('style')
<style>
  @media screen and (max-width: 767px){
    table.cssresponsive td:before {
        max-width: 180px !important;
    }
  }
    .container .table .glyphicon{
        margin: 0px 0px;
    }
    ul li .badge{
        left: -30px;
        margin-right: -30px;
        position: relative;
    }
    .color-danger {
        color: #bf5329 !important;
    }
    .color-success {
        color: #196c4b !important;
    }
    .color-warning {
        color: #cbb956 !important;
    }
    .iconos_estado{
      margin:5px 5px 5px -20px !important;
    }
  .reloj{
    display:none;
  }

.input-buscar{
    height: 27px;
    background-color: rgb(255, 255, 255);
    color: black;
    width: 170px;
    border: 1px solid rgb(170, 170, 170);
    margin-right: 1px;
    border-radius: 4px;
}
</style>
@endsection
@section('botones_guardar')
@if (Auth::user()->rol=='admin' or Auth::user()->rol=='director' or Auth::user()->rol=='docente')
<label><input onchange="onchange_continuar_editando(this)" type="checkbox" id="continuar_editando">Mostrar Reporte</label>
<input style="display:block"  type="submit" name="guardar" id="guardar_continuar_editando"value="Guardar y continuar editando" class="btn btn-primary">
<input style="display:none" type="submit" name="guardar" id="guardar_mostrar"value="Guardar y mostrar" class="btn btn-primary">
<script>
function onchange_continuar_editando(input) {
//console.log(input.checked);
if (input.checked){
document.querySelector("#guardar_continuar_editando").style.display='none';
document.querySelector("#guardar_mostrar").style.display='block';
}else{
document.querySelector("#guardar_continuar_editando").style.display='block';
document.querySelector("#guardar_mostrar").style.display='none';
}
}
</script>
@endif
@endsection
@section('barra_buscar_cuerpo')
<nav class="navbar navbar-default">
<div class="" id="app-navbar-collapse">
<ol class="breadcrumb navbar-left style:float:left;">
    <li class="active"> {{ $page_title }}</li>
</ol>
<?php $ruta = \Request::route()->getName(); ?>
<form method="post" action="{{ route($ruta) }}">
<meta name="csrf-token" content="{{ csrf_token() }}">
<input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <!-- Left Side Of Navbar -->
<ul class="navbar-nav navbar-right" style="list-style:none;text-align:right;text-align:center;">
    <li class="left_btn area_buscar_mas" style="display:none"><a style="margin: 0; padding: 8px;">
    <div class="center-xs input-group sidebar-form">
    <label for="buscar">Buscar por asignatura&nbsp;</label><br>
    <select style="background-color: #FFF;color:black;width:170px;display:inline-flex;text-align:center;" id="buscar_asignaturas" name="buscar_asignaturas" class="js-example-basic-single form-control">
      <option value="">Todas las asignaturas</option>
      @foreach ($asignaturas as $id => $asignaturas_i)
      @if(Auth::user()->rol=="admin" or in_array($asignaturas_i->codigo_asigt,$progs_asigct))
      <option value="{{$asignaturas_i->codigo_asigt}}" <?php 
      if (isset($_POST['buscar_asignaturas']) and $_POST['buscar_asignaturas'] == $asignaturas_i->codigo_asigt){
      echo " selected ";
    } ?> >{{$asignaturas_i->nom_asigt}}</option>
      @endif
      @endforeach
    </select>
    </div>
    </a></li>
  @if(Auth::user()->rol=="admin" or Auth::user()->rol=="director")
    <li class="left_btn area_buscar_mas" style="display:none"><a style="margin: 0; padding: 8px;">
    <div class="center-xs input-group sidebar-form">
    <label for="buscar">Buscar por docente&nbsp;</label><br>
    <select style="background-color: #FFF;color:black;width:170px;display:inline" id="buscar_docentes" name="buscar_docentes" class="js-example-basic-single form-control">
      <option value="">Todos los docentes</option>
      @foreach ($docentes as $id => $docentes_i)
      @if(Auth::user()->rol=="admin" or in_array($docentes_i->ident_usu,$progs_docentes))
      <option value="{{$docentes_i->id}}" <?php 
      if (isset($_POST['buscar_docentes']) and $_POST['buscar_docentes'] == $docentes_i->id){
      echo " selected ";
    } ?> >{{$docentes_i->nombre}} {{$docentes_i->apellido}}</option>
      @endif
      @endforeach
    </select>
    </div>
    </a></li>
  @endif
    <li class="left_btn area_buscar_mas" style="display:none"><a style="margin: 0; padding: 8px;">
    <div class="center-xs input-group sidebar-form">
    <label for="buscar">Buscar por periodo&nbsp;</label><br>
    <select style="background-color: #FFF;color:black;width:170px;display:inline" id="buscar_periodos" name="buscar_periodos" class="js-example-basic-single form-control">
      <option value="">Todos los periodos</option>
      @foreach ($periodos as $id => $periodos_i)
      <option value="{{$periodos_i->periodo}}" <?php 
      if (isset($_POST['buscar_periodos']) and $_POST['buscar_periodos'] == $periodos_i->periodo){
      echo " selected ";
      }
      if (empty($_POST) and $periodo_activo->periodo == $periodos_i->periodo){
        echo " selected ";
      }
      ?>>{{$periodos_i->periodo}}</option>
      @endforeach
    </select>
    </div>
    </a></li>
  @if(Auth::user()->rol=="admin" or Auth::user()->rol=="director")
    <li class="left_btn area_buscar_mas" style="display:none"><a style="margin: 0; padding: 8px;">
    <div class="center-xs input-group sidebar-form">
    <label for="buscar">Buscar por programa&nbsp;</label><br>
    <select style="background-color: #FFF;color:black;width:170px;display:inline" id="buscar_programa" name="buscar_programa" class="js-example-basic-single form-control">
      <option value="">Todos los programas</option>
      @foreach ($progs as $id => $prog_i)
      @if(Auth::user()->rol=="admin" or in_array($prog_i->cod_prog,$progs_asigc))
      <option value="{{$prog_i->cod_prog}}" <?php 
      if (isset($_POST['buscar_programa']) and $_POST['buscar_programa'] == $prog_i->cod_prog){
      echo " selected ";
    } ?> >{{$prog_i->nom_prog}}</option>
      @endif
      @endforeach
    </select>
    </div>
    </a></li>
  @endif
  
 <!---->
    <li class="left_btn inlinegroup2" style="
    
    
                                  "><a style="margin: 0; padding: 8px;">
    <div class="center-xs input-group sidebar-form">
    <input id="buscar_mas" type="checkbox" onchange="area_buscar_mas(this)" hidden>
    <label for="buscar_mas"><span id="icono_buscar_mas" class="glyphicon glyphicon-chevron-left"></span></label><label for="buscar">Buscar</label><br>
    <input type="search" style="background-color: #FFF;color:black;width:170px;display:inline;" id="buscar" name="buscar" class="input-buscar" value="{{isset($_POST['buscar']) ? $_POST['buscar'] : ''}}"></div>
    </a></li>
    <li class="left_btn inlinegroup2" style="
    
    
                                  ">
      <div class="center-xs input-group sidebar-form">
          <label for="num_resultados" style="margin-top: 22px;" title="Resultados por página">Resultados&nbsp;</label><br>
          <span class="input-group-btn" style="display:inline">
          <input title="Resultados por página" class="form-control" style="height: 27px;
        background-color: rgb(255, 255, 255);color: black;width: 70px;border-bottom: 1px none transparent;border-left: 0px solid black;margin-right: 1px;border-right: 1px solid #ccd0d2;border-bottom: 1px solid #ccd0d2;background-color: #fff;
    border: 1px solid #aaa;
    border-radius: 4px;" min="1" id="num_resultados" name="num_resultados"  type="number" value="<?php 
    if (isset($_POST['num_resultados'])){
      echo $_POST['num_resultados'];
    }else{
      echo 8;
    }
    ?>" solonumeros>
          </span>
      </div>
    </li>
    <li class="left_btn inlinegroup" style="
    
    
                                  
    margin: 0px 15px 0px 0px;
    padding: 5px;
">
      <div class="input-group sidebar-form">
          <label for="btn"></label><br>
          <span class="input-group-btn" style="display:inline">
      <span class="select2-selection select2-selection--single">
          <button class="botondebusquedahome btn btn-primary btn-xs" id="btn"  type="submit" title="Buscar"><span class="glyphicon glyphicon-search"></span></button>
          </span>
      </span>
      </div>
    </li>
</ul>
</form>
</div>
</nav>
@endsection
@section('content')
<div class="container">
<?php if (\Request::route()->getName()=="home"){ ?>
  <div class="row">
      @if (Auth::check())
        <div class="col-md-9" style="min-height: 500px;">
            <h1>Asignación de Asignaturas:</h1>
<div class="col-md-12" style="text-align: center;"></div>
<style>
   .left_btn{
          /*margin-left: 25px;*/
     text-align:center;
    }
  .icono_home:hover{
    color:blue !important;
    font-size: +10px !important;
  } 
  .inlinegroup{
      display:inline-flex;
  }
</style>
<table class="table fill-table csstable cssresponsive">
  <thead>
    <th>Asignatura</th>
    <th>Docente</th>
    <th style="min-width: 175px;">Programación Temática</th>
    <th style="min-width: 135px;">Seguimiento</th>
    <th style="min-width: 135px;">Informe Final</th>
  </thead>
  <tbody>
@foreach ($asigc as $asigt_i)

  <tr>

<?php $fecha = date("Y-m-d");?>
    <td data-label="Asignatura">{{ $asigt_i->nom_asigt }} (Grupo {{ $asigt_i_des[$asigt_i->id]->grupo }})@if($asigt_i_des[$asigt_i->id]->progtem_estado==1)
     @if($asigt_i_des[$asigt_i->id]->seguimiento_estado == 1)
            @if($asigt_i_des[$asigt_i->id]->infofinal_estado==1)
            @else
      <br class="hidden-sm hidden-md hidden-lg"><small title="Fecha límite para Informe Final" style="float:right; color:{{ (strtotime($asigt_i_des[$asigt_i->id]->fecha_revision_doc_if) < strtotime($fecha)) ? "red" : "var(--verde)" }};">{{ Fecha::formato_fecha_mes($asigt_i_des[$asigt_i->id]->fecha_revision_doc) }}</small><p class="hidden-sm hidden-md hidden-lg"></p>
            @endif
     @else
       <?php 
//if(Auth::user()->rol=="docente" or Auth::user()->rol=="director" or Auth::user()->rol=="admin"){ 
if(Auth::user()->rol=="docente" or Auth::user()->rol=="director" or Auth::user()->rol=="admin"){ 
?><br class="hidden-sm hidden-md hidden-lg"><small title="Fecha límite para Seguimiento" style="float:right; color:{{ (strtotime($asigt_i_des[$asigt_i->id]->fecha_revision_doc_se) < strtotime($fecha)) ? "red" : "var(--verde)" }};">{{ Fecha::formato_fecha_mes($asigt_i_des[$asigt_i->id]->fecha_revision_doc) }}</small><?php 
      }
if(Auth::user()->rol=="estudiante" or Auth::user()->rol=="docente" or Auth::user()->rol=="director" or Auth::user()->rol=="admin"){
  if(Auth::user()->rol=="docente" or Auth::user()->rol=="director" or Auth::user()->rol=="admin"){ 
  ?><br class="hidden-xs"><?php
}
?><br class="hidden-sm hidden-md hidden-lg"><small title="Fecha límite para Seguimiento" style="float:right; color:{{ (strtotime($asigt_i_des[$asigt_i->id]->fecha_revision_est_se) < strtotime($fecha)) ? "red" : "var(--verde)" }};">{{ (Auth::user()->rol=="docente" or Auth::user()->rol=="director" or Auth::user()->rol=="admin") 
  ? "Estudiante:" : "" }}{{ Fecha::formato_fecha_mes($asigt_i_des[$asigt_i->id]->fecha_revision_est) }}</small><?php 
}      ?><p class="hidden-sm hidden-md hidden-lg"></p>
    @endif
  @else
      <br class="hidden-sm hidden-md hidden-lg"><small title="Fecha límite para Programación Temática" style="float:right; color:{{ (strtotime($asigt_i_des[$asigt_i->id]->fecha_revision_doc_pt) < strtotime($fecha)) ? "red" : "var(--verde)" }};">{{ Fecha::formato_fecha_mes($asigt_i_des[$asigt_i->id]->fecha_revision_doc) }}</small><p class="hidden-sm hidden-md hidden-lg"></p>
  @endif</td>
    <td data-label="Docente" style="min-width: 210px;">{{ $asigt_i->name }}</td>
    <td data-label="Programación Temática"><!--{{$asigt_i_des[$asigt_i->id]->progtem_estado}}

-->
<!-- Modal -->
<div id="myModal_PT{{$asigt_i->id}}" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Control de Fechas</h4>
      </div>
      <div class="modal-body">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>

  </div>
</div>

    <a title='Programación Temática {{ $asigt_i_des[$asigt_i->id]->permiso_fecha_pt ? "" : " - No se encuentra con fecha vigente" }}'><span style="margin: 5px 5px 5px 5px !important;"><?php echo $iconos[$asigt_i_des[$asigt_i->id]->progtem_estado]; ?></span></a>
  <i data-toggle="modal" data-target="#myModal_PT{{$asigt_i->id}}" class="fas fa-stopwatch reloj" title='{{ $asigt_i_des[$asigt_i->id]->permiso_fecha_pt ? "Se encuentra con fecha vigente" : "No se encuentra con fecha vigente" }}' style='font-size: 12px;color:  {{ $asigt_i_des[$asigt_i->id]->permiso_fecha_pt ? "var(--verde)" : "var(--light2)" }};  '></i>
@php
$permisos_home_pt = (($asigt_i_des[$asigt_i->id]->permiso_fecha_pt or Auth::user()->rol=='admin' or Auth::user()->rol=='director') and $asigt_i_des[$asigt_i->id]->progtem_estado==2 or $asigt_i_des[$asigt_i->id]->progtem_estado==1);
@endphp
@if ($permisos_home_pt)
<span style="margin:5px">
<a title="Ver Documento" href="{{ route('reportes.prog_tem',[$asigt_i->id]) }}">
<span style="color: var(--verde) !important; font-size: large !important;" class="icono_home glyphicon glyphicon-search"><p style="display:inline;    font-size: 14px; font-family:Arial;font-weight: bold;">P</p>
</span>
</a>
</span>
@else
<span style="margin:5px">
<a title='La vista previa del documento estará disponible cuando el documento se encuentre "En Proceso" o "Listo"' href="#"  class="disbled">
<span style="color: var(--light2) !important; font-size: large !important;" class="icono_home glyphicon glyphicon-search"><p style="display:inline;    font-size: 14px; font-family:Arial;font-weight: bold;">P</p>
</span>
</a>
</span>
@endif
@php
$permisos_home_pt_print = Auth::user()->rol=='admin' or ($asigt_i_des[$asigt_i->id]->permiso_fecha_pt and $asigt_i_des[$asigt_i->id]->progtem_estado==2);
@endphp
@if ($permisos_home_pt_print or Auth::user()->rol=='admin')
<!--
<span style="margin:5px" class="hidden-xs">
<a title="Imprimir Documento" href="{{ route('reportes.prog_tem',[$asigt_i->id,'imprimir']) }}">
    <span style="color: var(--verde) !important; font-size: large !important;" class="icono_home glyphicon glyphicon-print">
      <p style="display:inline;    font-size: 14px; font-family:Arial;font-weight: bold;">P</p>
    </span>
  </a>
</span>
-->
@endif

@if (Auth::user()->rol!='estudiante')
@php
$permisos_home_pt_edit = ($asigt_i_des[$asigt_i->id]->permiso_fecha_pt or Auth::user()->rol=='admin' or Auth::user()->rol=='director') and ($asigt_i_des[$asigt_i->id]->progtem_estado==0 or $asigt_i_des[$asigt_i->id]->progtem_estado==2 or ($asigt_i_des[$asigt_i->id]->progtem_estado==1 and $asigt_i_des[$asigt_i->id]->seguimiento_estado==0));
@endphp
  @if ($permisos_home_pt_edit or Auth::user()->rol=='admin')
  <span style="margin:5px">
    <a title="Diligenciar Formato" href="{{ route('admin.asignacion.programaciontematica.edit',$asigt_i->id) }}">
      <span style="color: var(--verde) !important;font-size: large !important;" class="icono_home glyphicon glyphicon-pencil">
        <p style="display:inline;    font-size: 14px; font-family:Arial;font-weight: bold;">P</p>
      </span>
    </a>
  </span>
@else
      <span style="margin:5px">
    <a title='La edición del documento estará disponible si el seguimiento aún esta "Pediente" y dentro de las fechas establecidas' href="#">
      <span style="color: var(--light2) !important;font-size: large !important;" class="icono_home glyphicon glyphicon-pencil">
        <p style="display:inline;    font-size: 14px; font-family:Arial;font-weight: bold;">P</p>
      </span>
    </a>
  </span>
  @endif
@endif
      <p class="hidden-sm hidden-md hidden-lg"><br></p>
    </td>
    <td data-label="Seguimiento">
    <span style="margin: 5px 5px 5px 5px !important;" title="Seguimiento{{ $asigt_i_des[$asigt_i->id]->permiso_fecha_se ? "" : " - No se encuentra con fecha vigente" }}"><?php echo $iconos[$asigt_i_des[$asigt_i->id]->seguimiento_estado]; ?></span>
<i class="fas fa-stopwatch reloj" title=" {{ $asigt_i_des[$asigt_i->id]->permiso_fecha_se ? "Se encuentra con fecha vigente" : "No se encuentra con fecha vigente" }}" style="font-size: 12px;color:  {{ $asigt_i_des[$asigt_i->id]->permiso_fecha_se ? "var(--verde)" : "var(--light2)" }};  "></i>
  @if ($asigt_i_des[$asigt_i->id]->progtem_estado==1 or Auth::user()->rol=='admin')
            @if ((($asigt_i_des[$asigt_i->id]->permiso_fecha_se or Auth::user()->rol=='admin' or Auth::user()->rol=='director') and ($asigt_i_des[$asigt_i->id]->seguimiento_estado==2 or Auth::user()->rol=='admin')) or $asigt_i_des[$asigt_i->id]->seguimiento_estado==1)
            <a title="Ver Documento" href="{{ route('reportes.seguimiento',$asigt_i->id) }}">
              <span style="color: var(--cafehome) !important; font-size: large !important;" class="glyphicon glyphicon-search"><p style="display:inline; font-size: 14px; font-family:Arial;font-weight: bold;">S</p></span>
            </a>
            @else 
            <a title="Ver Documento" href="#">
              <span style="color: var(--light2) !important; font-size: large !important;" class="glyphicon glyphicon-search"><p style="display:inline; font-size: 14px; font-family:Arial;font-weight: bold;">S</p></span>
            </a>
            @endif
@php
$permisos_home_se_edit = (($asigt_i_des[$asigt_i->id]->permiso_fecha_se) and $asigt_i_des[$asigt_i->id]->infofinal_estado==0 and ($asigt_i_des[$asigt_i->id]->progtem_estado==1 and ($asigt_i_des[$asigt_i->id]->progtems->first()->segs()->first()->estado=="Listo" or (($asigt_i_des[$asigt_i->id]->seguimiento_estado==2 or $asigt_i_des[$asigt_i->id]->seguimiento_estado==0) and Auth::user()->rol=='docente')))) or Auth::user()->rol=='admin' or Auth::user()->rol=='director';
@endphp
@if ($permisos_home_se_edit or Auth::user()->rol=='admin')
            <a  title="Diligenciar Formato" href="{{ route('admin.asignacion.seguimiento.edit',$asigt_i->id) }}">
              <span style="color: var(--cafehome) !important; font-size: large !important;" class="glyphicon glyphicon-pencil"><p style="display:inline; font-size: 14px; font-family:Arial;font-weight: bold;">S</p></span>
            </a>
       @else 
         <a  title="Diligenciar Formato" href="#">
              <span style="color: var(--light2) !important; font-size: large !important;" class="glyphicon glyphicon-pencil"><p style="display:inline; font-size: 14px; font-family:Arial;font-weight: bold;">S</p></span>
            </a>
       @endif
            @if (Auth::user()->rol!='estudiante')
            @if ($asigt_i_des[$asigt_i->id]->codigo != "")
            @if ($asigt_i_des[$asigt_i->id]->seguimiento_estado==2)
            <a>
            <span style="color: var(--cafehome) !important; font-size: large !important;" class="js-tooltip js-copy glyphicon glyphicon-copy" data-toggle="tooltip" data-placement="bottom" data-copy="{{ $asigt_i_des[$asigt_i->id]->codigo }}" title="Copiar a portapapeles el código: {{ $asigt_i_des[$asigt_i->id]->codigo }}"><p style="display:inline; font-size: 14px; font-family:Arial;font-weight: bold;">S</p></span>
            </a>
            @endif
            @elseif($asigt_i_des[$asigt_i->id]->estudiante!="")
            @if ($asigt_i_des[$asigt_i->id]->seguimiento_estado!=1)
             <a>
              <span title="{{$asigt_i_des[$asigt_i->id]->estado_est}}" style="color: var(--cafehome) !important; font-size: large !important;" class="js-tooltip <?php
if($asigt_i_des[$asigt_i->id]->estado_est=="Pendiente"){
echo " far fa-user ";
}else{
echo " fas fa-user ";  
}
?>" data-toggle="tooltip" data-placement="bottom" title="{{ $asigt_i_des[$asigt_i->id]->estudiante }}, Estudiante quién evaluará la asignatura"><p style="display:inline;    font-size: 14px; font-family:Arial;font-weight: bold;">S</p></span>
            </a>
            @endif
            @endif
    
            @if ($asigt_i_des[$asigt_i->id]->seguimiento_estado==2 and $asigt_i_des[$asigt_i->id]->estado_seg_doc=="Listo" and $asigt_i_des[$asigt_i->id]->hay_votos_est==false)
               
               @if($asigt_i_des[$asigt_i->id]->estudiante!="")
              <a href='admin/codigos/{{ $asigt_i->id }}/create'  onclick="return confirm('Esta usted seguro que desea generar un código, actualmente ya se encuentra el estudiante {{ $asigt_i_des[$asigt_i->id]->estudiante }}?')">
              @else
              <a href='admin/codigos/{{ $asigt_i->id }}/create'  onclick="return confirm('Esta usted seguro que desea generar un código?')">
              @endif
               
              <span style="color: var(--cafehome) !important; font-size: large !important;" class="js-tooltip glyphicon glyphicon-qrcode" data-toggle="tooltip" data-placement="bottom" title="Generar Código"><p style="display:inline;    font-size: 14px; font-family:Arial;font-weight: bold;">S</p></span>
            </a>
             @endif
                
             @endif
     @else 
            <a  title="Ver Documento" href="#">
              <span style="color: var(--light2) !important; font-size: large !important;" class="glyphicon glyphicon-search"><p style="display:inline;    font-size: 14px; font-family:Arial;font-weight: bold;">S</p></span>
            </a>
             <a  title="Diligenciar Formato" href="">
              <span style="color: var(--light2) !important; font-size: large !important;" class="glyphicon glyphicon-pencil"><p style="display:inline;    font-size: 14px; font-family:Arial;font-weight: bold;">S</p></span>
            </a>
    @endif            
    </td>
    <td data-label="Informe Final">
    <span style="margin: 5px 5px 5px 5px !important;"  title="Informe Final{{ $asigt_i_des[$asigt_i->id]->permiso_fecha_if ? "" : " - No se encuentra con fecha vigente" }}"><?php echo $iconos[$asigt_i_des[$asigt_i->id]->infofinal_estado]; ?></span>
      <i class="fas fa-stopwatch reloj" title=" {{ $asigt_i_des[$asigt_i->id]->permiso_fecha_if ? "Se encuentra con fecha vigente" : "No se encuentra con fecha vigente" }}" style="font-size: 12px;color:  {{ $asigt_i_des[$asigt_i->id]->permiso_fecha_if ? "var(--verde)" : "var(--light2)" }};  "></i>
   
      @if ($asigt_i_des[$asigt_i->id]->infofinal_estado==1)
            <a title="Ver Documento" href="{{ route('reportes.inf_final',$asigt_i->id) }}">
              <span style="color: var(--azul) !important; font-size: large !important;" class="glyphicon glyphicon-search"><p style="display:inline;    font-size: 14px; font-family:Arial;font-weight: bold;">F</p></span>
            </a>
      @else
                 <a title="Ver Documento" href="#">
              <span style="color: var(--light2) !important; font-size: large !important;" class="glyphicon glyphicon-search"><p style="display:inline;    font-size: 14px; font-family:Arial;font-weight: bold;">F</p></span>
            </a>
      @endif
      @if (Auth::user()->rol!='estudiante')
@php
$permisos_home_if_edit = ($asigt_i_des[$asigt_i->id]->permiso_fecha_if or Auth::user()->rol=='admin' or Auth::user()->rol=='director') and  $asigt_i_des[$asigt_i->id]->seguimiento_estado==1;
@endphp
@if ($permisos_home_if_edit or Auth::user()->rol=='admin')
            <a title="Diligenciar Formato" href="{{ route('admin.asignacion.informefinal.edit',$asigt_i->id) }}">
              <span style="color: var(--azul) !important; font-size: large !important;" class="glyphicon glyphicon-pencil"><p style="display:inline;    font-size: 14px; font-family:Arial;font-weight: bold;">F</p></span>
            </a>
    @else
      <a title="Diligenciar Formato" href="#">
              <span style="color: var(--light2) !important; font-size: large !important;" class="glyphicon glyphicon-pencil"><p style="display:inline;    font-size: 14px; font-family:Arial;font-weight: bold;">F</p></span>
            </a>
       @endif
       @endif
    
    </td>
    <!--{{ $asigt_i->periodo }}-->
  </tr>
@endforeach
  </tbody>
</table>
<div class="col-md-12" style="text-align: center;">
<?php
$asigc->route = \Request::route()->getName(); ?>
{{ $asigc->links('layouts.paginate_post') }}
</div>

        </div>
        <div class="col-md-3">
@if (Auth::user()->rol=='estudiante')
<!-- modal ficha cofigo -->
<!-- Trigger the modal with a button -->
<button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModalfichacodigo">Ingresar Código</button>
<br>
<p>
  
          </p>
<!-- Modal -->
<div id="myModalfichacodigo" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Nueva Asignatura</h4>
      </div>
      <div class="modal-body">
    <h5 class="card-title"><strong>Nueva Asignatura</strong></h5>
    <h6 class="card-title">
    Estudiante: {{ Auth::user()->name }}
    </h6>
<form method="POST" action="{{ route('codigo') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div id="div_token" class="form-group{{ $errors->has('codigo_token') ? ' has-error' : '' }}">
                <p>Para agregar una nueva asignatura ingrese el código del seguimiento.</p>
                <label for="codigo_token" class="col-md-4 control-label">Ingresar Código</label>
                <br>
                <div class="col-md-12">
                    <input id="codigo_token" autofocus type="text" class="form-control" name="codigo_token">
    
                    @if ($errors->has('codigo_token'))
                        <span class="help-block">
                            <strong>{{ $errors->first('codigo_token') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group">
                <div class="col-md-8 col-md-offset-4">
                <br>
                <button type="submit" class="btn btn-primary">
                Enviar código
                </button>
                </div>
                </div>
            </div>
 </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>

  </div>
</div>
<!-- modal ficha codigo -->
@endif
          <!--p>Periodo activo: 
            <label class="label label-success" title="Periodo Activo">{{ $periodo_activo->periodo }}</label>
          </p-->
          
<!--inicio html-->
    <div id="popover-progtem" class="hide">
      <ul style="list-style:none;">
        <li><span class="icono_home glyphicon glyphicon-search" style="color: var(--verde) !important; font-size: large !important;"><p style="display: inline; font-size: 14px; font-family: Arial; font-weight: bold;">P</p></span> Ver documento</li><li><span class="icono_home glyphicon glyphicon-print" style="color: var(--verde) !important; font-size: large !important;"><p style="display: inline; font-size: 14px; font-family: Arial; font-weight: bold;">P</p></span> Imprimir documento</li><li><span class="icono_home glyphicon glyphicon-pencil" style="color: var(--verde) !important; font-size: large !important;"><p style="display: inline; font-size: 14px; font-family: Arial; font-weight: bold;">P</p></span> Editar documento</li>
      </ul>
</div>
<!--fin html-->
<!--inicio html-->
    <div id="popover-seg" class="hide">
      <ul style="list-style:none;">
        <li><span class="icono_home glyphicon glyphicon-search" style="color: var(--cafehome) !important; font-size: large !important;"><p style="display: inline; font-size: 14px; font-family: Arial; font-weight: bold;">S</p></span> Ver documento</li>
        <li><span class="icono_home glyphicon glyphicon-print" style="color: var(--cafehome) !important; font-size: large !important;"><p style="display: inline; font-size: 14px; font-family: Arial; font-weight: bold;">S</p></span> Imprimir documento</li>
        <li><span class="icono_home glyphicon glyphicon-pencil" style="color: var(--cafehome) !important; font-size: large !important;"><p style="display: inline; font-size: 14px; font-family: Arial; font-weight: bold;">S</p></span> Editar documento</li>
        <li><span class="icono_home glyphicon glyphicon-qrcode" style="color: var(--cafehome) !important; font-size: large !important;"><p style="display: inline; font-size: 14px; font-family: Arial; font-weight: bold;">S</p></span> Generar Código</li>
        <li><span class="icono_home glyphicon glyphicon-copy" style="color: var(--cafehome) !important; font-size: large !important;"><p style="display: inline; font-size: 14px; font-family: Arial; font-weight: bold;">S</p></span> Copiar Código</li>
        <li><span class="icono_home far fa-user" style="color: var(--cafehome) !important; font-size: large !important;"><p style="display: inline; font-size: 14px; font-family: Arial; font-weight: bold;">S</p></span> Sin revisión de Estudiante</li>
        <li><span class="icono_home fas fa-user" style="color: var(--cafehome) !important; font-size: large !important;"><p style="display: inline; font-size: 14px; font-family: Arial; font-weight: bold;">S</p></span> Con revisión de Estudiante</li>
      </ul>
</div>
<!--fin html-->
<!--inicio html-->
    <div id="popover-infofin" class="hide">
      <ul style="list-style:none;">
        <li><span class="icono_home glyphicon glyphicon-search" style="color: var(--azul) !important; font-size: large !important;"><p style="display: inline; font-size: 14px; font-family: Arial; font-weight: bold;">F</p></span> Ver documento</li>
        <li><span class="icono_home glyphicon glyphicon-print" style="color: var(--azul) !important; font-size: large !important;"><p style="display: inline; font-size: 14px; font-family: Arial; font-weight: bold;">F</p></span> Imprimir documento</li>
        <li><span class="icono_home glyphicon glyphicon-pencil" style="color: var(--azul) !important; font-size: large !important;"><p style="display: inline; font-size: 14px; font-family: Arial; font-weight: bold;">F</p></span> Editar documento</li>
      </ul>
</div>
<!--fin html-->
<!--inicio html-->
    <div id="popover-estados" class="hide">
      <ul style="list-style:none;">
        <li><span class="color-badges color-success glyphicon glyphicon-ok"></span> Listo</li>
        <li><span class="color-badges color-warning glyphicon glyphicon-wrench"></span> En Proceso</li>
        <li><span class="color-badges color-danger glyphicon glyphicon-remove"></span> Pendiente</li>
        <li><span style="color: var(--light2) !important; font-size: large !important;" class="icono_home glyphicon glyphicon-search"></span> Vista Previa Inactiva</li>
        <li><span style="color: var(--light2) !important; font-size: large !important;" class="icono_home glyphicon glyphicon-pencil"></span> Edición Inactiva</li>
      </ul>
</div>
<!--fin html-->
          <br>
          @if(Auth::user()->rol=='admin' or Auth::user()->rol=='director' or Auth::user()->rol=='docente')
          <span style="color: var(--verde) !important; font-weight: bold; cursor:pointer !important; font-size: 10pt !important;" data-placement="bottom" data-toggle="popover" data-container="body" data-placement="left" data-trigger="hover" type="button" data-html="true" id="progtem"><span class="glyphicon glyphicon-list" style=""></span> Programación Temática</span> 
          <span class="badge" style="background-color:green">{{ $estados['progtem_estado1'] }}</span>
          <span class="badge" style="background-color:orange">{{ $estados['progtem_estado2'] }}</span>
          <span class="badge" style="background-color:red">{{ $estados['progtem_estado0'] }}</span><br><br>
          @endif
          
         <span style="color: var(--cafehome) !important; font-weight: bold; cursor:pointer !important; font-size: 10pt !important;" data-placement="bottom" data-toggle="popover" data-container="body" data-placement="left" data-trigger="hover" type="button" data-html="true" id="seg"><span class="glyphicon glyphicon-list" style=""></span> Seguimiento</span>
          <span class="badge" style="background-color:green">{{ $estados['seg_estado1'] }}</span>
          <span class="badge" style="background-color:orange">{{ $estados['seg_estado2'] }}</span>
          <span class="badge" style="background-color:red">{{ $estados['seg_estado0'] }}</span><br><br>
          @if(Auth::user()->rol=='admin' or Auth::user()->rol=='director' or Auth::user()->rol=='docente')
          <span style="color: var(--azul) !important; font-weight: bold; cursor:pointer !important; font-size: 10pt !important;" data-placement="bottom" data-toggle="popover" data-container="body" data-placement="left" data-trigger="hover" type="button" data-html="true" id="infofin"><span class="glyphicon glyphicon-list" style=""></span> Informe Final</span>
          <span class="badge" style="background-color:green">{{ $estados['infofin_estado1'] }}</span>
          <span class="badge" style="background-color:orange">{{ $estados['infofin_estado2'] }}</span>
          <span class="badge" style="background-color:red">{{ $estados['infofin_estado0'] }}</span><br><br>
            @endif
          <span style="color: var(--gray-dark) !important; font-weight: bold; cursor:pointer !important; font-size: 10pt !important;" data-placement="bottom" data-toggle="popover" data-container="body" data-placement="left" data-trigger="hover" type="button" data-html="true" id="estados"><span class="glyphicon glyphicon-list" style=""></span> Estados</span>
  <br><br>
          <hr>
          <div id="cont_datepicker" align="left">
              <center>
            <div class="calendario" id="datepicker"></div>
            </center>
          </div>
        </div>
        <div class="col-md-3">
  <!-- Calendario-->
          <?php
          /*
          https://medium.com/@krissanawat/how-to-implement-jquery-fullcalendar-in-laravel-5-5-554d3deeb7c6
          */
          ?>
        </div>
    <center>
    <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModallistado">Ver listado de fechas</button>
    </center>
  <!-- Modal -->

<div id="myModallistado" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Listado de Fechas</h4>
      </div>
      <div class="modal-body">
       <table class="table">
         <thead>
          <tr>
            <th>Fecha</th>
            <th>Detalle</th>
            <th>Asignatura(s)</th>
          </tr>
         </thead>
         <tbody><?php 
$meses=["","ene","feb","mar","abr","may","jun","jul","ago","sep","oct","nov","dic"];
		foreach ($fechas_calendario as $id => $fechas_calendarioi)
		{
      $fecha = $fechas_calendarioi['fecha']; 
      
      $anio = "";
      $mes = "";
      $dia = "";
      if($fechas_calendarioi['fecha']=!"" and $fechas_calendarioi['fecha']=!NULL){
        $fechaComoEntero = strtotime($fecha);
        $anio = date("Y", $fechaComoEntero);
        $mes = date("m", $fechaComoEntero)+0;
        $dia = date("d", $fechaComoEntero)+0;
      }
      
      ?><tr>
            <td><?php echo Fecha::formato_fecha_mes($id)?></td>
            <td><?php echo implode(", ",array_values($fechas_calendarioi['mensaje'])) ?></td>
            <td><?php echo implode(", ",array_values($fechas_calendarioi['nom_asigt'])) ?></td>
        </tr><?php
    }
        ?></tbody>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>

  </div>
</div>
     <center>
     <button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModalenc">Encuesta de Satisfacción del usuario</button>
    </center>
  <!-- Modal -->

<div id="myModalenc" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Encuesta de Satisfacción del usuario</h4>
      </div>
      <div class="modal-body">
         <div class="enc">
  <ul>
@foreach($preg_enc as $preg_enc_i)
<li>{{ $preg_enc_i->pregunta }}<br>
@php
$resp_enc = \App\RespEnc::where('preg_enc_id',$preg_enc_i->id)->where('usu_id',Auth::user()->ident_usu)->get()->first();
@endphp
<label style="color:#01c100"><input class="radio" id="radio-1-{{$preg_enc_i->id}}" ide="{{$preg_enc_i->id}}" name="enc-{{$preg_enc_i->id}}" type="radio" value="Excelente" @if(isset($resp_enc->respuesta) and $resp_enc->respuesta=="Excelente") checked @endif >Excelente</label><!-- 8ae64a -->
<label style="color:#0826c1"><input class="radio" id="radio-2-{{$preg_enc_i->id}}" ide="{{$preg_enc_i->id}}" name="enc-{{$preg_enc_i->id}}" type="radio" value="Bueno" @if(isset($resp_enc->respuesta) and $resp_enc->respuesta=="Bueno") checked @endif >Bueno</label>
<label style="color:#feda00"><input class="radio" id="radio-3-{{$preg_enc_i->id}}" ide="{{$preg_enc_i->id}}" name="enc-{{$preg_enc_i->id}}" type="radio" value="Regular" @if(isset($resp_enc->respuesta) and $resp_enc->respuesta=="Regular") checked @endif >Regular</label>
<label style="color:#e76f00"><input class="radio" id="radio-4-{{$preg_enc_i->id}}" ide="{{$preg_enc_i->id}}" name="enc-{{$preg_enc_i->id}}" type="radio" value="Malo" @if(isset($resp_enc->respuesta) and $resp_enc->respuesta=="Malo") checked @endif >Malo</label>
</li>
<br>
@endforeach
  </ul>
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModalencresumen">Resumen de la Encuesta</button>
</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>

  </div>
</div>

      
<div id="myModalencresumen" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Encuesta de Satisfacción del usuario</h4>
      </div>
      <div class="modal-body">
@php
$resp_enc = new \App\RespEnc();
$resp_enc = $resp_enc->all();
$resultados = [];
$resultados['Excelente']=0;
$resultados['Bueno']=0;
$resultados['Regular']=0;
$resultados['Malo']=0;
$resultados['Total']=0;
foreach ($resp_enc as $resp_enc_i){
$resultados[$resp_enc_i->respuesta]=$resultados[$resp_enc_i->respuesta]+1;
$resultados['Total']=$resultados['Total']+1;
}        
@endphp
        @if($resultados['Total']>0)
      Excelente: {{number_format(($resultados['Excelente']*100/$resultados['Total']), 2, '.', '')}}%<br>
      Bueno: {{number_format(($resultados['Bueno']*100/$resultados['Total']), 2, '.', '')}}%<br>
      Regular: {{number_format(($resultados['Regular']*100/$resultados['Total']), 2, '.', '')}}%<br>
      Malo: {{number_format(($resultados['Malo']*100/$resultados['Total']), 2, '.', '')}}%<br>
      Total: {{$resultados['Total']}}<br>
        @endif
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>

  </div>
</div>
<!-- modal ficha cofigo -->
        <!--div class="col-md-3">
            <h1>Ejemplo:</h1>
<div class="panel panel-default" style="width: 24rem;display:inline-block">
  <div class="panel-body">
    <h5 class="card-title">Matemáticas Generales<span class="badge badge-success">Flexibilidad</span></h5>
    <h6 class="card-title">Departamento de Matemáticas</h6>
    <p class="card-text">Aquí una breve descripción de la materia o de la asignación correspondiente.</p>
    <!--a href="#" class="btn btn-primary">Go somewhere</a- ->
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Collect the nav links, forms, and other content for toggling - ->
    <div class="" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Acciones<span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#">Ver Documento</a></li>
            <li><a href="#">Diligenciar Formato</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">Ver Más</a></li>
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse - ->
  </div><!-- /.container-fluid - ->
</nav>
  </div>
</div>
        </div-->
    @else<center>
    <div class="title m-b-md">
                    <a href="home">
                <img src="{{ secure_asset('img/logo.jpg') }}" rel="stylesheet"></a>
                    <!--EDIV-->
                </div>
                <h1 class="text-center">PROGRAMACIÓN TEMÁTICA</h1>
                <h4 class="text-center">Gestión y Seguimiento</h4>
                <h4>
               Bienvenido, aquí usted podrá encontrar información acerca de planes, flexibilidad y enlaces de interés.
                </h4></center>
    @endif
    </div>
</div>
</div>
    <!--
    <div class="row">
        <div class="col-md-9col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">Programación Temática</div>

                <div class="panel-body">
                    <h2><a href="{{asset('/home')}}">Facultades</a></h2>
                    <ul>
                   @foreach ($datos['fac'] as $item_fac)
                   <li>
                       <a href="?fac={{ $item_fac->id }}">{{ $item_fac->nom_fac }}</a>
                           @if (($request->has('fac') and $request->fac == $item_fac->id) or $request->has('prog'))
                           <p>Programas</p>
                           <ul>
                            @foreach ($datos['prog'] as $progs)
                                <li><a href="?prog={{ $progs->id }}">{{ $progs->nom_prog }}</a></li>
                           @endforeach
                           </ul>
                            @endif
                       </li>
                   @endforeach
                        
                    </ul>
                    @if ($request->has('prog'))
                     <h2><a href="{{ asset('admin/asignatura') }}">Asignaturas</a> por <a href="{{ asset('admin/asignacion') }}">Asignación</a></h2>
                      <table border="1">
                          {!! $aux = ''; !!}
                      @foreach ($datos['asigc'] as $item_asigc)
                          @if ($aux != $item_asigc->per_acad)
                          <tr><th colspan="4">
                          {!! $aux = $item_asigc->per_acad; !!}
                          </th></tr>
                          @endif
                          <tr>
                            <td>
                                  <li><a href="{{ route('reportes.prog_tem',$item_asigc->id) }}">{{ $item_asigc->nom_asigt }}. Docente: {{ $item_asigc->name }}</a></li>
                            </td>
                            <td>
                            Asignación
                            </td>
                            <td>
                                Editar Docente
                            </td>
                            <td>
                                Editar Asignatura
                            </td>
                        </tr>
                      @endforeach
                      </table> 
                      @endif
                </div>
            </div>
        </div>
    </div>
    -->
<?php }//endif ?>
<?php if (\Request::route()->getName()=="reportes.general"){ ?>
<!-- Styles -->
<style>
#chartdiv {
  width: 100%;
  height: 500px;
}
</style>
<!-- Chart code -->
<script>
var datos_pt = [
<?php foreach($asignaturas_grafico as $asignatura){?>  
  {
    category: "<?php echo $asignatura['nom_asigt'] ?>",
    value1: <?php echo floor(100*$asignatura['progtem_estado0']/$asignatura['progtem_estadot']); ?>,
    value2: <?php echo floor(100*$asignatura['progtem_estado1']/$asignatura['progtem_estadot']); ?>,
    value3: <?php echo floor(100*$asignatura['progtem_estado2']/$asignatura['progtem_estadot']); ?>
  },
<?php } ?>
];
var nom_series_pt = ["Programación Temática Pendiente","Programación Temática en trámite","Programación Temática Finalizado"];
graficar("chartdivpt",datos_pt,nom_series_pt);

var datos_se = [
<?php foreach($asignaturas_grafico as $asignatura){?>  
  {
    category: "<?php echo $asignatura['nom_asigt'] ?>",
    value1: <?php echo floor(100*$asignatura['seguimiento_estado0']/$asignatura['seguimiento_estadot']); ?>,
    value2: <?php echo floor(100*$asignatura['seguimiento_estado2']/$asignatura['seguimiento_estadot']); ?>,
    value3: <?php echo floor(100*$asignatura['seguimiento_estado1']/$asignatura['seguimiento_estadot']); ?>
  },
<?php } ?>
];
var nom_series_se = ["Seguimiento Pendiente","Seguimiento en trámite","Seguimiento Finalizado"];
graficar("chartdivse",datos_se,nom_series_se);

var datos_if = [
<?php foreach($asignaturas_grafico as $asignatura){?>  
  {
    category: "<?php echo $asignatura['nom_asigt'] ?>",
    value1: <?php echo floor(100*$asignatura['infofinal_estado0']/$asignatura['infofinal_estadot']); ?>,
    value2: <?php echo floor(100*$asignatura['infofinal_estado1']/$asignatura['infofinal_estadot']); ?>,
    value3: <?php echo floor(100*$asignatura['infofinal_estado2']/$asignatura['infofinal_estadot']); ?>
  },
<?php } ?>
];
var nom_series_if = ["Seguimiento Pendiente","Seguimiento en trámite","Seguimiento Finalizado"];
graficar("chartdivif",datos_if,nom_series_if);

function graficar(id_div,datos,nom_series){
am4core.ready(function() {
// Themes begin
am4core.useTheme(am4themes_animated);
// Themes end

var chart = am4core.create(id_div, am4charts.XYChart);
chart.hiddenState.properties.opacity = 0; // this creates initial fade-in

chart.data = datos;

chart.colors.step = 2;
chart.padding(30, 30, 10, 30);
chart.legend = new am4charts.Legend();

var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
categoryAxis.dataFields.category = "category";
categoryAxis.renderer.grid.template.location = 0;

var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
valueAxis.min = 0;
valueAxis.max = 100;
valueAxis.strictMinMax = true;
valueAxis.calculateTotals = true;
valueAxis.renderer.minWidth = 50;


var series1 = chart.series.push(new am4charts.ColumnSeries());
series1.columns.template.width = am4core.percent(80);
series1.columns.template.tooltipText =
  "{name}: {valueY.totalPercent.formatNumber('#.00')}%";
series1.name = nom_series[0];
series1.dataFields.categoryX = "category";
series1.dataFields.valueY = "value1";
series1.dataFields.valueYShow = "totalPercent";
series1.dataItems.template.locations.categoryX = 0.5;
series1.stacked = true;
series1.tooltip.pointerOrientation = "vertical";

var bullet1 = series1.bullets.push(new am4charts.LabelBullet());
bullet1.interactionsEnabled = false;
bullet1.label.text = "{valueY.totalPercent.formatNumber('#.00')}%";
bullet1.label.fill = am4core.color("#ffffff");
bullet1.locationY = 0.5;

var series2 = chart.series.push(new am4charts.ColumnSeries());
series2.columns.template.width = am4core.percent(80);
series2.columns.template.tooltipText =
  "{name}: {valueY.totalPercent.formatNumber('#.00')}%";
series2.name = nom_series[1];
series2.dataFields.categoryX = "category";
series2.dataFields.valueY = "value2";
series2.dataFields.valueYShow = "totalPercent";
series2.dataItems.template.locations.categoryX = 0.5;
series2.stacked = true;
series2.tooltip.pointerOrientation = "vertical";

var bullet2 = series2.bullets.push(new am4charts.LabelBullet());
bullet2.interactionsEnabled = false;
bullet2.label.text = "{valueY.totalPercent.formatNumber('#.00')}%";
bullet2.locationY = 0.5;
bullet2.label.fill = am4core.color("#ffffff");

var series3 = chart.series.push(new am4charts.ColumnSeries());
series3.columns.template.width = am4core.percent(80);
series3.columns.template.tooltipText =
  "{name}: {valueY.totalPercent.formatNumber('#.00')}%";
series3.name = nom_series[2];
series3.dataFields.categoryX = "category";
series3.dataFields.valueY = "value3";
series3.dataFields.valueYShow = "totalPercent";
series3.dataItems.template.locations.categoryX = 0.5;
series3.stacked = true;
series3.tooltip.pointerOrientation = "vertical";

var bullet3 = series3.bullets.push(new am4charts.LabelBullet());
bullet3.interactionsEnabled = false;
bullet3.label.text = "{valueY.totalPercent.formatNumber('#.00')}%";
bullet3.locationY = 0.5;
bullet3.label.fill = am4core.color("#ffffff");

chart.scrollbarX = new am4core.Scrollbar();

}); // end am4core.ready()
}
</script>
<h1>Reporte General</h1>
<ul class="nav nav-tabs">
  <li class="active"><a data-toggle="tab" href="#home">Resultados</a></li>
  <li><a data-toggle="tab" href="#menu1">Gráfico Programación Temática</a></li>
  <li><a data-toggle="tab" href="#menu2">Gráfico Seguimiento</a></li>
  <li><a data-toggle="tab" href="#menu3">Gráfico Informe Final</a></li>
</ul>

<div class="tab-content">
  <div id="home" class="tab-pane fade in active">
<div id="repotegeneral">
<span id="reporte">
        <table class="table table-striped cssresponsive">
        <thead>
        <tr>
            <th>Nombre</th>
            <th>Sede</th>
            <th>Programa</th>
            <th><span class="hidden-print">Semestre(s)</span><span class="visible-print">Sem</span></th>
            <th>Código de Asignatura</th>
            <th>Nombre de Asignatura</th>
            <th>Grupo</th>
            <th><span class="hidden-print">Horas Prácticas</span><span class="visible-print">H/P</span></th>
            <th><span class="hidden-print">Horas Teóricas</span><span class="visible-print">H/T</span></th>
            <th><span class="hidden-print">Programación Temática</span><span class="visible-print">PT</span></th>
            <th><span class="hidden-print">Seguimiento</span><span class="visible-print">S</span></th>
            <th><span class="hidden-print">Informe Final</span><span class="visible-print">IF</span></th>
        </tr>
          </thead>
  <tbody>
          @foreach ($asigc as $asigt_i)
        <tr>
            <td data-label="Nombre">{{ $asigt_i_des[$asigt_i->id]->docente_fk->nombre }} {{ $asigt_i_des[$asigt_i->id]->docente_fk->apellido }}</td>
            <td data-label="Sede">{{ $asigt_i_des[$asigt_i->id]->prog->sede }}</td>
            <td data-label="Programa">{{ $asigt_i_des[$asigt_i->id]->prog->nom_prog }}</td>
            <td data-label="Semestre(s)">{{ $asigt_i_des[$asigt_i->id]->nums_semestre }}</td>
            <td data-label="Código de Asignatura">{{ $asigt_i_des[$asigt_i->id]->cod_asigt }}</td>
            <td data-label="Nombre de Asignatura"><p class="hidden-sm hidden-md hidden-lg"></p>{{ $asigt_i_des[$asigt_i->id]->asigt->nom_asigt }}</td>
            <td data-label="Grupo">{{ $asigt_i_des[$asigt_i->id]->grupo }}</td>
            <td data-label="Horas Prácticas">{{ $asigt_i_des[$asigt_i->id]->horas_practicas }}</td>
            <td data-label="Horas Teóricas">{{ $asigt_i_des[$asigt_i->id]->horas_teoricas }}</td>
            <td data-label="Programación Temática"><?php echo $iconos[$asigt_i_des[$asigt_i->id]->progtem_estado] ?></td>
            <td data-label="Seguimiento"><?php echo $iconos[$asigt_i_des[$asigt_i->id]->seguimiento_estado] ?></td>
            <td data-label="Informe Final"><?php echo $iconos[$asigt_i_des[$asigt_i->id]->infofinal_estado] ?></td>
        </tr>
        @endforeach 
          </tbody>
            </table>
        </span>
    </center>
</div><!-- fin reporte general tabla-->
  </div>
  <div id="menu1" class="tab-pane fade">
<div id="chartdivpt" class="hidden-print chartdiv" style="height: 500px;"></div>
  </div>
  <div id="menu2" class="tab-pane fade">
<div id="chartdivse" class="hidden-print chartdiv" style="height: 500px;"></div>
  </div>
  <div id="menu3" class="tab-pane fade">
    <div id="chartdivif" class="hidden-print chartdiv" style="height: 500px;"></div>
  </div>
</div>
<center>
<span id="txt_resultados"></span>
<span id="area_pagination">
{{ $asigc->links('layouts.paginate_post') }}
</span>
</center>
<?php }//endif ?>
</div>
@endsection
@section('scripts')
<script>
   
$("#progtem[data-toggle=popover]").popover({
    html: true, 
	content: function() {
          return $('#popover-progtem').html();
        }
});
$("#seg[data-toggle=popover]").popover({
    html: true, 
	content: function() {
          return $('#popover-seg').html();
        }
});
$("#infofin[data-toggle=popover]").popover({
    html: true, 
	content: function() {
          return $('#popover-infofin').html();
        }
});
$("#estados[data-toggle=popover]").popover({
    html: true, 
	content: function() {
          return $('#popover-estados').html();
        }
});

$('.enc .radio').change(function(){
  var parametros = {
    preg_enc_id : $(this).attr('ide'),
    respuesta: $(this).val()
    }
   //alert(parametros);
   guardar_enc(parametros);
});
function guardar_enc(parametros){
      $.ajax({
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              dataType: 'json',
              data:  parametros,
              url:   '{{route ('encuesta.responder')}}',
              type:  'post',
              beforeSend: function () {
                  ///#$(".alertas").html('Procesando, espere por favor...','info','mensajes_observaciones');
              },
              success:  function (response) {
                if (response == "1"){
                 console.log('Exito');
                 ///#$(".alertas").html('Encuesta Actualizada','success','mensajes_observaciones');
                }else{
                  console.log('Error');
                  ///#$(".alertas").html('Error al guardar','warning','mensajes_observaciones');
                }
                  setTimeout(function(){
                      dismiss_alert();
                  },4000);
              }
      });
}
</script>
  <style>
    /*
  .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
    border-top: 1px solid #808080 !important;
    border-bottom: 1px solid #808080 !important;
    border-right: 1px solid #bbbaba !important;
    border-left: 1px solid #bbbaba !important;
    */
}
  </style>
<link href="{{ secure_asset('lib/jquery-ui/jquery-ui.css') }}" rel="stylesheet">
<script src="{{ secure_asset('lib/jquery-ui/jquery-ui.js') }}"></script>
<script src="{{ secure_asset('lib/datepicker/i18n/datepicker-es.js') }}"></script>
<script>
function area_buscar_mas(obj){
  var st = obj.checked ? 'block' : 'none';
    $('.area_buscar_mas').css('display',st);
  if (obj.checked){
    $("#icono_buscar_mas").addClass('glyphicon-chevron-right');
    $("#icono_buscar_mas").removeClass('glyphicon-chevron-left');
  }else{  
    $("#icono_buscar_mas").removeClass('glyphicon-chevron-right');
    $("#icono_buscar_mas").addClass('glyphicon-chevron-left');
  }
  }

	$( "#datepicker" ).datepicker({

	
		beforeShowDay: function( date ) { <?php 
		foreach ($fechas_calendario as $fecha => $fechas_calendarioi)
		{ 
    $fechaComoEntero = strtotime($fecha);
    $anio = date("Y", $fechaComoEntero);
    $mes = date("m", $fechaComoEntero)-1;
    $dia = date("d", $fechaComoEntero)+0;
    ?>
    if ( date.getFullYear() == <?php echo $anio; ?> && date.getDate() == <?php echo $dia; ?> && date.getMonth() == <?php echo $mes; ?> ) {
    return [ true, "ui-festivo", "<?php echo implode(", ",array_values($fechas_calendarioi['mensaje']))." ".implode(", ",array_values($fechas_calendarioi['nom_asigt'])); ?>" ];
    }
    <?php }//endforeach ?>
    return [ true ];
		}
	})
	.datepicker( "option", $.datepicker.regional['es'] );
</script>
<style>
.ui-festivo *{
  border: 1px solid #dad55e !important;
    background: #bf5329 !important;
    color: #ffffff !important;
}
</style>

@endsection