@extends('layouts.app')

@section('scripts')
<script>
        $(".contra[type=text]").prop("type","password");
</script>
@endsection
@section('content')
<script>
function validar_area_password(obj){
        document.getElementById('area_password').style.display = obj.checked ? 'block' : 'none';
        document.getElementById('nocomplete2').value='';
        document.getElementById('nocomplete3').value='';
        $('#nocomplete2').prop('required',obj.checked);
        $('#nocomplete3').prop('required',obj.checked);
    }
</script>
<form class="form-horizontal" role="form" method="POST" action="{{ route('usuarios.cambiar_clave') }}" autocomplete="off">
<meta name="csrf-token" content="{{ csrf_token() }}">
<input type="hidden" name="_token" value="{{ csrf_token() }}">
    <h1 class="text-center">Mi Perfíl</h1>

    

<div class="form-group{{ $errors->has('ident_usu') ? ' has-error' : '' }}">
    <label for="ident_usu" class="col-md-4 control-label">Identificación</label>

    <div class="col-md-6">
      <span class="form-control" readonly >{{ Auth::user()->ident_usu }}</span>
    </div>
</div>
<div class="form-group{{ $errors->has('nombre') ? ' has-error' : '' }}">
    <label for="nombre" class="col-md-4 control-label">Nombre</label>

    <div class="col-md-6">
        <input id="nombre" type="text" class="form-control" name="nombre" value="{{ Auth::user()->nombre}}" required autofocus>

        @if ($errors->has('nombre'))
            <span class="help-block">
                <strong>{{ $errors->first('nombre') }}</strong>
            </span>
        @endif
    </div>
</div>
  
<div class="form-group{{ $errors->has('apellido') ? ' has-error' : '' }}">
    <label for="apellido" class="col-md-4 control-label">Apellido</label>

    <div class="col-md-6">
        <input autocomplete="ÑÖcompletes" id="apellido" type="text" class="form-control" name="apellido" value="{{ Auth::user()->apellido }}" required autofocus>

        @if ($errors->has('apellido'))
            <span class="help-block">
                <strong>{{ $errors->first('apellido') }}</strong>
            </span>
        @endif
    </div>
</div>
  
<div class="form-group{{ $errors->has('tel') ? ' has-error' : '' }}">
    <label for="tel" class="col-md-4 control-label">Teléfono</label>

    <div class="col-md-6">
        <input autocomplete="ÑÖcompletes" id="tel" type="text" class="form-control" name="tel" value="{{ Auth::user()->tel}}" required autofocus>

        @if ($errors->has('tel'))
            <span class="help-block">
                <strong>{{ $errors->first('tel') }}</strong>
            </span>
        @endif
    </div>
</div>
  
<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
    <label for="email" class="col-md-4 control-label">Correo Electrónico</label>

    <div class="col-md-6">
        <input autocomplete="ÑÖcompletes" id="email" type="text" class="form-control" name="email" value="{{ Auth::user()->email}}" required autofocus>
        @if ($errors->has('email'))
            <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
    </div>
</div>
  <p class="text-center">Para confirmar los cambios en su perfil, por favor escriba su contraseña</p>
        <div class="form-group{{ $errors->has('nocomplete1') ? ' has-error' : '' }}">
            <label for="nocomplete1" class="col-md-4 control-label">Contraseña Actual</label>

            <div class="col-md-6">
                <input autocomplete="ÑÖcompletes" id="nocomplete1" type="text" class="contra form-control" name="nocomplete1" value="{{ $nocomplete1 or old('nocomplete1') }}" required autofocus>

                @if ($errors->has('nocomplete1'))
                    <span class="help-block">
                        <strong>{{ $errors->first('nocomplete1') }}</strong>
                    </span>
                @endif
            </div>
        </div>
  <div class="form-group">
    
  <div class="col-md-4">
      <br>
  </div>
    <div class="col-md-6">
<label><input type="checkbox" onclick="validar_area_password(this);"> Cambiar Contraseña</label>
    </div>
  </div>
<span id="area_password" style="display:none">
        <div class="form-group{{ $errors->has('nocomplete2') ? ' has-error' : '' }}">
            <label for="nocomplete2" class="col-md-4 control-label">Nueva Contraseña</label>

            <div class="col-md-6">
                <input autocomplete="ÑÖcompletes" id="nocomplete2" type="text" class="contra form-control" name="nocomplete2" >

                @if ($errors->has('nocomplete2'))
                    <span class="help-block">
                        <strong>{{ $errors->first('nocomplete2') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('nocomplete3') ? ' has-error' : '' }}">
            <label for="password-confirm" class="col-md-4 control-label">Confirmar Nueva Contraseña</label>
            <div class="col-md-6">
                <input autocomplete="ÑÖcompletes" id="password-confirm" type="text" class="contra form-control" name="nocomplete3" >

                @if ($errors->has('nocomplete3'))
                    <span class="help-block">
                        <strong>{{ $errors->first('nocomplete3') }}</strong>
                    </span>
                @endif
            </div>
        </div>
</span>
        <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
                <button type="submit" class="btn btn-primary">
                    Guardar
                </button>
            </div>
        </div>
    </form>
@endsection
