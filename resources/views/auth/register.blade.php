@extends('layouts.app')
@section('ruta_de_migas')
<ol class="breadcrumb">
    <li><a href="{{ route('home') }}"> Inicio</a></li>
    @if (Auth::check()) @if (Auth::user()->rol=="admin")
    <li><a href="{{ secure_asset('admin/usuarios') }}"> Usuarios</a></li>
  @endif  @endif
    <li class="active">Registrar</li>
</ol>
@endsection
@section('scripts')
<script> 
  function opselectroles(){
    $('#select1').html(`<option value=""> 
                                  Seleccione un rol predeterminado
                              </option>`);
        $('.chk_roles').each(function( index ) {
          if (this.checked){
            var optionValue = $(this).val();
            var optionText = $("#label_"+optionValue).html();
            var selected = '';
            var rol_actual = $('#rol').val();
            console.log(rol_actual);
            if (rol_actual==optionValue){
                 selected = ' selected ';
                }
            $('#select1').append(`<option ${selected} value="${optionValue}"> 
                                   ${optionText} 
                              </option>`); 
          }
  });
}
$(document).ready(function(){
  opselectroles();
  $('.chk_roles').change(function(){
    opselectroles();
  }); 
});
</script> 
@endsection
@section('content')

    <h1 class="text-center">Registrar Usuario</h1>

        @if (Auth::check())
    <form class="form-horizontal" role="form" method="POST" store action="{{ route('admin.usuarios.store') }}">
        @else
    <form class="form-horizontal" role="form" method="POST" register action="{{ route('register') }}">
        @endif
        {{ csrf_field() }}
        <div class="form-group{{ $errors->has('ident_usu') ? ' has-error' : '' }}">
            <label for="ident_usu" class="col-md-4 control-label">Identificación</label>

            <div class="col-md-6">
                <input id="ident_usu" type="text" class="form-control" name="ident_usu" value="{{ old('ident_usu') }}" required autofocus>
                @if ($errors->has('ident_usu'))
                    <span class="help-block">
                        <strong>{{ $errors->first('ident_usu') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="form-group{{ $errors->has('nombre') ? ' has-error' : '' }}">
            <label for="nombre" class="col-md-4 control-label">Nombre</label>

            <div class="col-md-6">
                <input id="nombre" type="text" class="form-control" name="nombre" value="{{ old('nombre') }}" required autofocus>

                @if ($errors->has('nombre'))
                    <span class="help-block">
                        <strong>{{ $errors->first('nombre') }}</strong>
                    </span>
                @endif
            </div>
        </div>
      
        <div class="form-group{{ $errors->has('apellido') ? ' has-error' : '' }}">
            <label for="apellido" class="col-md-4 control-label">Apellido</label>

            <div class="col-md-6">
                <input id="apellido" type="text" class="form-control" name="apellido" value="{{ old('apellido') }}" required autofocus>

                @if ($errors->has('apellido'))
                    <span class="help-block">
                        <strong>{{ $errors->first('apellido') }}</strong>
                    </span>
                @endif
            </div>
        </div>
      
        <div class="form-group{{ $errors->has('rol') ? ' has-error' : '' }}">
            <label for="rol" class="col-md-4 control-label">
              @if (Auth::check())
              Rol(es)
              @else
              Rol
              @endif
          </label>

            <div class="col-md-6">
        
                    @if (Auth::check())
              <label><input type="checkbox" name="roles[]" class="chk_roles" id="roles_admin" value="admin"><span id="label_admin">Administrador</span></label>
              <br>
              <label><input type="checkbox" name="roles[]" class="chk_roles" id="roles_director" value="director"><span id="label_director">Director</span></label>
              <br>
              <label><input type="checkbox" name="roles[]" class="chk_roles" id="roles_docente" value="docente"><span id="label_docente">Docente</span></label>
              <br>
              <label><input type="checkbox" name="roles[]" class="chk_roles" id="roles_estudiante" value="estudiante"><span id="label_estudiante">Estudiante</span></label>
              <input type="hidden" id="rol"value="{{ old('rol') }}">
              <select name="rol" id="select1" class="form-control" required>
              @else
 <input type="hidden" id="rol_estudiante" class="form-control2" name="roles[]" value="estudiante" checked required>
              <select id="rol" class="form-control" name="rol" value="{{ old('rol') }}" required >
                    <option value ="estudiante">Estudiante</option>
              </select>
                     @endif
              </select>
                @if ($errors->has('rol'))
                    <span class="help-block">
                        <strong>{{ $errors->first('rol') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        @if (!Auth::check())
        <div class="form-group{{ $errors->has('codigo') ? ' has-error' : '' }}">
            <label for="codigo" class="col-md-4 control-label">Código para Evaluación</label>

            <div class="col-md-6">
                <input id="codigo" type="text" class="form-control" name="codigo" value="{{ old('codigo') }}" required autofocus>

                @if ($errors->has('codigo'))
                    <span class="help-block">
                        <strong>{{ $errors->first('codigo') }}</strong>
                    </span>
                @endif
            </div>
        </div>
         @endif
        <div class="form-group{{ $errors->has('tel') ? ' has-error' : '' }}">
            <label for="tel" class="col-md-4 control-label">Teléfono</label>

            <div class="col-md-6">
                <input id="tel" type="text" class="form-control" name="tel" value="{{ old('tel') }}" required autofocus>

                @if ($errors->has('tel'))
                    <span class="help-block">
                        <strong>{{ $errors->first('tel') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <label for="email" class="col-md-4 control-label">Correo Electrónico</label>

            <div class="col-md-6">
                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
            <label for="password" class="col-md-4 control-label">Contraseña</label>

            <div class="col-md-6">
                <input id="password" type="password" class="form-control" name="password" required>

                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <label for="password-confirm" class="col-md-4 control-label">Confirmar Contraseña</label>

            <div class="col-md-6">
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
<a href="{{ route('admin.usuarios.index') }}" class="btn btn-secondary hidden-print">Regresar</a>
<input type="submit" name="submit" value="Registrar" class="btn btn-primary">
            </div>
        </div>
    </form>
@endsection
