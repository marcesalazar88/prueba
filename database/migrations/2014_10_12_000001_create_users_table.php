<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Seeder;
//use DB;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usu', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ident_usu')->length(12)->unique(); 
            $table->enum('rol',['admin','director','docente','estudiante']);
            $table->string('roles'); 
            $table->string('tel'); 
            $table->string('name');
            $table->string('nombre'); 
            $table->string('apellido'); 
            $table->string('email')->length(190)->unique();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
      
        Schema::create('password_resets', function (Blueprint $table) {
           //$table->increments('id');
           $table->string('email')->index();
           $table->string('token');
           $table->timestamp('created_at')->nullable();
       });
       $data_usu[] = [
             'ident_usu'=>'1',
             'rol'=>'admin',
             'roles'=>'admin,director,docente,estudiante',
             'tel'=>'1',
             'nombre'=>'Administrador',
             'apellido'=>'Sistema',
             'name'=>'Administrador'." ".'Sistema',
             'email'=>'admin@correo.com',
             'password'=> bcrypt('admin')
        ];
        \DB::table('usu')->insert($data_usu);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('password_resets');
        Schema::dropIfExists('usu');
    }
}
