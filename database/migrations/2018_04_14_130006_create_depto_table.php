<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeptoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('depto', function (Blueprint $table) {
            $table->integer('id')->length(10)->unsigned();
            $table->string('nom_depto')->length(190)->unique();
            $table->integer('fac_id')->length(10)->unsigned();
            $table->foreign('fac_id')
                    ->references('id')
                    ->on('fac')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
            $table->string('director')->length(12)->index();
            $table->foreign('director')
                    ->references('ident_usu')
                    ->on('usu')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('depto');
    }
}
