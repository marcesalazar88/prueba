<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAsigcTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
 public function up()
    {
         Schema::create('asigc', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ident_docnt')->length(12);
            $table->string('estudiante')->length(12)->nullable();
            $table->string('cod_asigt')->length(10);
            $table->string('per_acad')->length(5);
            $table->string('cod_prog')->length(12);
            $table->enum('flex',['NO','SI'])->default('NO');
            $table->text('observaciones')->nullable();
            $table->string('grupo');
            
            $table->index(['cod_asigt']);
            
            $table->foreign('cod_asigt')
                    ->references('codigo_asigt')
                    ->on('asigt')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
                               
            $table->foreign('cod_prog')
                    ->references('cod_prog')
                    ->on('prog')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
           
            $table->foreign('estudiante')
                    ->references('ident_usu')
                    ->on('usu')
                    ->onDelete('set null')
                    ->onUpdate('cascade');
            
            $table->foreign('ident_docnt')
                    ->references('ident_usu')
                    ->on('usu')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
           
            $table->foreign('per_acad')
                    ->references('periodo')
                    ->on('periodo')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');   
   
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asigc');
    }
}
