<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInfoFinTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('info_fin', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_asigt')->unsigned();
            $table->text('temt_cumpl')->nullable();
            $table->text('temt_n_cumpl')->nullable();
            $table->text('id_proc_eva')->nullable();
            $table->integer('por_cump_desem_gen')->nullable();
            $table->text('id_desllo_comp')->nullable();
           
            #$table->text('id_desem_gen')->nullable();
            $table->text('autoeva')->nullable();
            $table->text('observ')->nullable();
            $table->text('biblio')->comment('Sugerir uso de Normas APA')->nullable();
            $table->text('act_proc_eva')->nullable();
            $table->decimal('porcentaje_proc_eva')->length(8,2)->nullable();
           
           $table->integer('total_est_curso')->nullable();
           $table->integer('total_est_desem_gen')->nullable();
           #$table->decimal('total_est_desem_gen')->length(8,2)->nullable();
           
            $table->decimal('nota_max_desem_gen')->length(8,2)->nullable();
            $table->decimal('nota_min_desem_gen')->length(8,2)->nullable();
            $table->decimal('prom_grup_desem_gen')->length(8,2)->nullable();
           
            $table->decimal('nota_perdieron_desem_gen')->length(8,2)->nullable();
            $table->decimal('nota_est_pasaron_desem_gen')->length(8,2)->nullable();
           
            $table->text('observ_desem_gen')->length(500)->nullable();
            $table->text('comp_desllo_compete')->length(500)->nullable();
            $table->text('aplica_compe')->length(500)->nullable();
            $table->text('act_proce_eva')->length(500)->nullable();
           
            #$table->integer('porcentaje_proce_eva')->length(4)->unsigned()->nullable();
            $table->text('porcentaje_proce_eva')->nullable();
           
            $table->integer('control_fecha_id')->unsigned()->nullable();// `control_fecha_id` INTEGER NULL DEFAULT NULL,
           
            $table->string('codigo')->nullable();
            $table->enum('estado',['Pendiente', 'En Proceso', 'Listo']);
            $table->integer('seg_id')->length(11)->unsigned()->nullable();
           
            $table->date('fecha_aprobacion')->nullable();
            $table->date('fecha_revision_doc')->nullable();
           
           

            
            $table->foreign('id_asigt')
                    ->references('id')
                    ->on('asigc')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
                    
             
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('info_fin');
    }
}
