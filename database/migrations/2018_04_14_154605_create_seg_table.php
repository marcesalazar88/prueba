<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSegTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * 
     */
    public function up()
    {
        Schema::create('seg', function (Blueprint $table) {
          
            $table->increments('id');
            $table->integer('prog_temt_id')->length(10)->unsigned();//`id_prog_temt` INTEGER(12) NULL DEFAULT NULL
            $table->integer('control_fecha_id')->unsigned()->nullable();// `control_fecha_id` INTEGER NULL DEFAULT NULL,
  
            $table->string('estudiante')->length(12)->nullable();
            $table->string('docente')->length(12)->nullable();
            $table->date('fecha_aprobacion')->nullable();
            $table->date('fecha_revision_doc')->nullable();
            $table->date('fecha_revision_est')->nullable();
          
          
            $table->string('codigo')->nullable();
            $table->enum('estado',['Pendiente', 'En Proceso', 'Listo']);
          $table->enum('estado_est',['Pendiente', 'En Proceso', 'Listo']);
            $table->text('observ_est')->length(500)->nullable();
            $table->text('observ_docnt')->length(500)->nullable();
          
            $table->foreign('docente')
                    ->references('ident_usu')
                    ->on('usu')
                    ->onDelete('restrict')
                    ->onUpdate('cascade');
            
            $table->foreign('estudiante')
                    ->references('ident_usu')
                    ->on('usu')
                    ->onDelete('restrict')
                    ->onUpdate('cascade');
                    
            $table->foreign('prog_temt_id')
                    ->references('id')
                    ->on('prog_temt')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
        });
        
        Schema::create('seg_asp_val', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cod')->length(10)->unique();
            $table->string('name')->length(255);
        });
        $data_seg_asp_val[] = [
            'id' => '1',
            'cod' => 'DC',
            'name' => 'Desarrollo de contenidos'
           ];
        $data_seg_asp_val[] = [
            'id' => '2',
            'cod' => 'ME',
            'name' => 'Metodología'
        ];
        $data_seg_asp_val[] = [
            'id' => '3',
            'cod' => 'PE',
            'name' => 'Proceso de Evaluación'
        ];
        $data_seg_asp_val[] = [
            'id' => '4',
            'cod' => 'MG',
            'name' => 'Manejo de la comunicación y desempeño general'
        ];
        \DB::table('seg_asp_val')->insert($data_seg_asp_val);
        Schema::create('tipo_valoracion', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->length(20)->unique();
            $table->text('descripcion')  ;
        });
        $data_tipo_valoracion[] = [
            'id' => '1',
            'name' => 'Fortaleza',
            'descripcion' => 'Fortalezas'
           ];
        $data_tipo_valoracion[] = [
            'id' => '2',
            'name' => 'Dificultad',
            'descripcion' => 'Dificultades por mejorar y compromisos'
        ];
        \DB::table('tipo_valoracion')->insert($data_tipo_valoracion);
        Schema::create('seg_val', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('seg_id')->length(10)->unsigned();
            $table->string('aspecto')->length(10);
            $table->string('tipo')->length(20);
            $table->text('valoracion')  ;
            $table->enum('voto',['Pendiente', 'Aprueba', 'Rechaza'])->default('Pendiente')->nullable();
            $table->text('replica')->nullable();
            
            $table->foreign('tipo')
                    ->references('name')
                    ->on('tipo_valoracion')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
            
            $table->foreign('aspecto')
                    ->references('cod')
                    ->on('seg_asp_val')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
                    
            $table->foreign('seg_id')
                    ->references('id')
                    ->on('seg')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
        });
      
       Schema::create('mensaje', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('asigc_id')->length(10)->unsigned();
            $table->string('de')->length(12);
            $table->text('texto');
            $table->string('rol');
            $table->timestamps();
         /*
        $table->foreign('asigc_id')
                    ->references('id')
                    ->on('asigc')
                    ->onDelete('cascade')
                    ->onUpdate('cascade'); 
         */
         $table->foreign('de')
                    ->references('ident_usu')
                    ->on('usu')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
         
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mensaje');
        Schema::dropIfExists('seg_val');
        Schema::dropIfExists('tipo_valoracion');
        Schema::dropIfExists('seg_asp_val');
        Schema::dropIfExists('seg');
    }
}
