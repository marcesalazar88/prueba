<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('notificaciones', function (Blueprint $table) {
           
            $table->increments('id');
            $table->text('mensaje');
            $table->string('destino')->length(12);
           
            $table->enum('estado',['pendiente', 'leido']);
            $table->timestamps();
            
        });
       
    }

/*
*/

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notificaciones');
    }
}
