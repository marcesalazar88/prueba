<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PregEnc extends Migration
{
    /**
     * Run the migrations.
     *`preg_enc_id`, `respuesta`, `usu_id` resp_enc
     * @return void
     */
    public function up()
    {
         Schema::create('preg_enc', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('preg_enc_id')->length(10)->unsigned();
            $table->foreign('preg_enc_id')
                    ->references('id')
                    ->on('preg_enc')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
            $table->string('respuesta');
            $table->string('usu_id')->length(12)->index();
            $table->foreign('usu_id')
                    ->references('ident_usu')
                    ->on('usu')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
        });
        
    }

/*
*/

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('preg_enc');
    }
}
