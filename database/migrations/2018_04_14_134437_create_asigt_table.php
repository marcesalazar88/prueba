<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAsigtTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('asigt', function (Blueprint $table) {
            $table->increments('id');
            $table->string('codigo_asigt')->length(10);
            $table->string('nom_asigt');
            $table->string('sem_ofrece_asigt');
            $table->string('ihs_asigt');
            $table->string('n_cred_asigt');
            $table->text('just_asigt');
            $table->text('obj_gen');
            $table->text('obj_esp');
            $table->string('area');
           
            $table->unique('codigo_asigt');
        });
      
      Schema::create('cont_asigt', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cod_asigt')->length(10);
            $table->integer('hras_cont_asigt');
            $table->text('tem_cap_cont_asigt');
            $table->string('form_eva_cont_asigt');
            $table->foreign('cod_asigt')
                    ->references('codigo_asigt')
                    ->on('asigt')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cont_asigt');
        Schema::dropIfExists('asigt');
    }
}
