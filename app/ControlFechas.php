<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Fecha;

class ControlFechas extends Model
{
    public $timestamps = false;
    protected $table = 'control_fechas';
    protected $fillable = [
        'nombre_control',
       // 'fecha_aprobacion',
        'fecha_revision_doc',
        'fecha_revision_est',
        'per_acad',
        'depto_id'
    ];
    public $fillcolumn = [
        'nombre_control'=>'Nombre',
       // 'fecha_aprobacion'=>'Fecha de Aprobacion',
        'fecha_revision_doc'=>'Fecha de Revision de Docente',
        'fecha_revision_est'=>'Fecha de Revison de Estudiante',
        'per_acad'=>'Periodo Académico',
        'depto_id'=>'Departamento'
    ];
 public function depto()
    {//Verificado.
        return $this->belongsTo('App\Depto');
    }
     public function segs()
    {//Verificado
        return $this->hasMany('App\Seg');
    }
    public static function estado_dif_fecha_vencimiento($estado_dif_fecha)
    {
        return Fecha::estado_dif_fecha_vencimiento($estado_dif_fecha);
    }
    public static function diferencia_fecha($fecha_i,$fecha_f,$abs = true)
    {
	      return Fecha::diferencia_fecha($fecha_i,$fecha_f,$abs);
    }
    public static function mes_letras($fecha,$letras=false)
    {
	      return Fecha::mes_letras($fecha,$letras);
    }
}
