<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fac extends Model
{
    public $timestamps = false;
    protected $primaryKey = 'id'; // or null
    public $incrementing = false;
    protected $table = 'fac';
    protected $fillable = [
        'id',
        'nom_fac'
    ];
    public $fillcolumn = [
        'id'=>'Código',
        'nom_fac'=>'Nombre'
    ];
public function deptos()
    {//Verificado
        return $this->hasMany('App\Depto');
    }
}
