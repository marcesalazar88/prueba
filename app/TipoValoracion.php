<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class TipoValoracion extends Model
{
    protected $table = 'tipo_valoracion';
    protected $fillable = [
        'name',
        'descripcion' 
    ];
    public $fillcolumn = [
        'name'=>'Nombre',
        'descripcion'=>'Descripción'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
}
