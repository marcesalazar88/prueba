<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SegAspVal extends Model
{
      public $timestamps = false;
    protected $table = 'seg_asp_val';
    protected $fillable = [
        'cod',
        'name'
        ];
    public $fillcolumn = [
        'cod'=>'Código',
        'name'=>'Nombre'
        ];
    public function segvals()
    {//Verificado
        return $this->hasMany('App\SegVal','aspecto','cod');
    }
    
}
