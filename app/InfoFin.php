<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InfoFin extends Model
{
     public $timestamps = false;
    protected $table = 'info_fin';
    protected $fillable = [
        'seg_id',
        'id_asigt',
        'temt_cumpl',
        'temt_n_cumpl',
        'id_proc_eva',
        'por_cump_desem_gen',
        'id_desllo_comp',
        'autoeva',
        'observ',
        'biblio',
        'act_proc_eva',
        'porcentaje_proc_eva',
        'total_est_curso',
        'total_est_desem_gen',
        'nota_max_desem_gen',
        'nota_min_desem_gen',
        'prom_grup_desem_gen',
        'nota_perdieron_desem_gen',
        'nota_est_pasaron_desem_gen',
        'observ_desem_gen',
        'comp_desllo_compete',
        'aplica_compe',
        'act_proce_eva',
        'porcentaje_proce_eva'
    ];
    public $fillcolumn = [
        'id_asigt' => 'id_asigt',
        'temt_cumpl' => 'temt_cumpl',
        'temt_n_cumpl' => 'temt_n_cumpl',
        'id_proc_eva' => 'id_proc_eva',
        'id_desem_gen' => 'id_desem_gen',
        'id_desllo_comp' => 'id_desllo_comp',
        'autoeva' => 'autoeva',
        'observ' => 'observ',
        'biblio' => 'biblio',
        'act_proc_eva' => 'act_proc_eva',
        'porcentaje_proc_eva' => 'porcentaje_proc_eva',
        'total_est_curso' => 'total_est_curso',
        'total_est_desem_gen' => 'total_est_desem_gen',
        'nota_max_desem_gen' => 'nota_max_desem_gen',
        'nota_min_desem_gen' => 'nota_min_desem_gen',
        'prom_grup_desem_gen' => 'prom_grup_desem_gen',
        'nota_perdieron_desem_gen' => 'nota_perdieron_desem_gen',
        'nota_est_pasaron_desem_gen' => 'nota_est_pasaron_desem_gen',
        'observ_desem_gen' => 'observ_desem_gen',
        'comp_desllo_compete' => 'comp_desllo_compete',
        'aplica_compe' => 'aplica_compe',
        'act_proce_eva' => 'act_proce_eva',
        'porcentaje_proce_eva' => 'porcentaje_proce_eva'
    ];
    public function asigc()
    {//Verificado
        return $this->belongsTo('App\Asigc','id_asigt','id');
    }
}
