<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Depto extends Model
{
    public $timestamps = false;
    protected $primaryKey = 'id'; // or null
    public $incrementing = false;
    protected $table = 'depto';
    protected $fillable = [
        'id',
        'nom_depto',
        'fac_id',
        'director'
    ];
    public $fillcolumn = [
        'id'=>'Código',
        'nom_depto'=>'Nombre',
        'fac_id'=>'Facultad',
        'director'=>'Director'
    ];
public function progs()
    {//Verificado
        return $this->hasMany('App\Prog');
    }
public function fac()
    {//Verificado
        return $this->belongsTo('App\Fac');
    }
 public function director_fk()
    {//Verificado
        return $this->belongsTo('App\User','director','ident_usu');
    }
}
