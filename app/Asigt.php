<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Asigt extends Model
{
    public $timestamps = false;
    protected $table = 'asigt';
    protected $fillable = [
        'codigo_asigt',
        'nom_asigt',
        'sem_ofrece_asigt',
        'ihs_asigt',
        'n_cred_asigt',
        'just_asigt',
        'obj_gen',
        'obj_esp',
        'area'
    ];
    public $fillcolumn = [
        'codigo_asigt'=>'Código',
        'nom_asigt'=>'Nombre',
        'sem_ofrece_asigt'=>'Semestre',
        'ihs_asigt'=>'IHS',
        'n_cred_asigt'=>'N° de Créditos',
        'just_asigt'=>'Justificación',
        'obj_gen'=>'Objetivo General',
        'obj_esp'=>'Objetivos Específicos',
        'area'=>'Área'
    ];
     public function asigcs()
    {//Verificando
        return $this->hasMany('App\Asigc','cod_asigt', 'codigo_asigt');
    } 
    public function contasigts()
    {//Verificado
        return $this->hasMany('App\ContAsigt', 'cod_asigt', 'codigo_asigt');
    }
}
