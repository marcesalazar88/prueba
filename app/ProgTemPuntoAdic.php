<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProgTemPuntoAdic extends Model
{
    //
    public $timestamps = false;
    protected $table = 'prog_tem_punto_adic';
    protected $fillable = [
        'prog_tem_id',
        'item',
        'valor'
        ];
    public $fillcolumn = [
        'prog_tem_id'=>'prog_tem_id',
        'item'=>'item',
        'valor'=>'valor'
        ];
        
        public function prog_tem()
    {//Verificado
        return $this->belongsTo('App\ProgTem');
    }
}
