<?php

namespace App;

use Illuminate\Http\Request;
use App\Asigc;
use App\ProgTem;
use App\Seg;
use App\InfoFin;
use App\Periodo;
use App\User;
use App\Notificaciones;
use Auth;
class Funciones
{
        public static function nombre_usuario($ident)
    { 
        if($ident!=""){
          $user = User::where('ident_usu',$ident)->get();
          if (count($user)>0){ 
          $user = $user->first();
          return $user->name;
          }else{
            return "";
          }
        }else{
          return $ident;
        }
  
  
    }
       public static function puntos_suspensivos($string, $length=NULL)
    {
        //Si no se especifica la longitud por defecto es 50
        if ($length == NULL)
            $length = 50;
        //Primero eliminamos las etiquetas html y luego cortamos el string
        $stringDisplay = substr(strip_tags($string), 0, $length);
        //Si el texto es mayor que la longitud se agrega puntos suspensivos
        if (strlen(strip_tags($string)) > $length)
            $stringDisplay .= ' ...';
        return $stringDisplay;
    }   
    
   /**
     * @param string          $validarFechaAprobacion
     * @param asigcId $asigcId
     * @return bool
     */
           public static function validarFechaAprobacion($asigcId,$formato='ProgTem',$fecha='hoy')
    {
            if ($fecha=='hoy') $fecha = date("Y-m-d");
            $resultado = false;
             
            $asigc = Asigc::find($asigcId);
            $progtem = ProgTem::where("asigc_id",$asigcId)->get()->first();
            if (isset($progtem->id) and $progtem->id !="")
            $seg = Seg::where("prog_temt_id",$progtem->id)->get()->first();
            else
            $seg = new Seg;
            $infofin = InfoFin::where("asigc_id",$asigcId)->get()->first();
             
            if ($formato='ProgTem'){
            $dif_fecha_aprobacion = Fecha::diferencia_fecha($asigc->fecha_aprobacion,$fecha,false);
            $dif_fecha_revision_doc = Fecha::diferencia_fecha($fecha, $asigc->fecha_revision_doc,false);
              if ($dif_fecha_aprobacion>=0 and $dif_fecha_revision_doc>=0){
                return true;
              }
            }
             
            if ($formato='Seg'){
            $asigc->fecha_aprobacion;
            $asigc->fecha_revision_doc;
            $asigc->fecha_revision_est;//seg
            }
             
            if ($formato='InfoFin'){
            $asigc->fecha_aprobacion;
            $asigc->fecha_revision_doc;
            }
             
            return $resultado;
    }   
  
     public static function notif_estado($id,$estado='leido')
    {
        $notificaciones = Notificaciones::find($id);
        $notificaciones->estado = $estado;
        $result = $notificaciones->save();
        //return $result;
    }
  
    public static function num()
    {
        $resultado=0;
        $id_user = Auth::user()->ident_usu;
        $notificaciones = Notificaciones::where('estado','pendiente')
          ->where('destino',$id_user)
          ->get();
        //dd($notificaciones);
        $resultado = count($notificaciones);
      
        return $resultado;
    }   
    
    public static function permiso_depto($tipo,$valor)
    {
    if (Auth::check()){
    if (Auth::user()->rol=="admin"){
      return true;
    }else if (Auth::user()->rol=="director"){
        $id_user = Auth::user()->id;
        $user = User::find($id_user);
        $user->deptos();
        $deptos_asigc=[];
        $progs_asigc=[];
        $progs_asigc_id=[];
        $planes_asigc=[];
        foreach($user->deptos as $deptoi){
          $deptos_asigc[]=$deptoi->id;
          $deptoi->progs();
          foreach($deptoi->progs as $progi){
            $progs_asigc[]=$progi->cod_prog;
            $progs_asigc_id[]=$progi->id;
            $progi->planes();
            foreach($progi->planes as $plani){
              $planes_asigc[]=$plani->id;
            }
          }
        }
      if ($tipo == 'plan'){
        $arreglo = $planes_asigc;
      }else if($tipo == 'prog_id'){
        $arreglo = $progs_asigc_id;
      }else if($tipo == 'cod_prog'){
        $arreglo = $progs_asigc;
      }elseif($tipo == 'depto'){
        $arreglo = $deptos_asigc;
      }else{
        return false;
      }
      //dd($arreglo);
      if (in_array($valor,$arreglo)){
          return true;
       }

    }else{
       return false;
    }
      
  }else{
      return false;
    }
    }
  
    public static function permiso_en_fechas($tipo,$valor)
    {
      //dentro de fn permiso_asigc validar para caso true si está dentro de las fechas
    }
  
    public static function permiso_asigc($tipo,$valor)
    {
    if (Auth::check()){
    $id_user = Auth::user()->id;
    $user = User::find($id_user);
    if (Auth::user()->rol=="admin"){
      return true;
    }else if (Auth::user()->rol=="director"){
        
        $user->deptos();
        $deptos_asigc=[];
        $progs_asigc=[];
        $progs_asigc_id=[];
        $planes_asigc=[];
        $asigcs_asigc=[];
        foreach($user->deptos as $deptoi){
          $deptos_asigc[]=$deptoi->id;
          $deptoi->progs();
          foreach($deptoi->progs as $progi){
            $progs_asigc[]=$progi->cod_prog;
            $progs_asigc_id[]=$progi->id;
            $progi->planes();
            foreach($progi->planes as $plani){
              $planes_asigc[]=$plani->id;
            }
            $progi->asigcs();
            foreach($progi->asigcs as $asigcs_i){
              $asigcs_asigc[]=$asigcs_i->id;
            }
          }
        }
      if ($tipo == 'plan'){
        $arreglo = $planes_asigc;
       
      }else if($tipo == 'prog_id'){
        $arreglo = $progs_asigc_id;
      }else if($tipo == 'cod_prog'){
        $arreglo = $progs_asigc;
      }else if($tipo == 'asigc'){
        $arreglo = $asigcs_asigc;
      }elseif($tipo == 'depto'){
        $arreglo = $deptos_asigc;
      }else{
        return false;
      }
      //dd($arreglo);
      if (in_array($valor,$arreglo)){
          return true;
       }else{
          return false;
      }

    }else if (Auth::user()->rol=="docente"){
      if($tipo == 'asigc'){
          $asigc = Asigc::find($valor);
        return ($asigc->ident_docnt == $user->ident_usu);
      }else{
          return false;
      }
    }else if (Auth::user()->rol=="estudiante"){
      if($tipo == 'asigc'){
        $asigc = Asigc::find($valor);
        return ($asigc->estudiante == $user->ident_usu);
      }else{
       return false;
      }
    }else{
       return false;
    }
      
  }else{
      return false;
    }
    }
  
    
       
    public static function rol($rol)
    {
      if ($rol =='admin'){
        return "Administrador";
      }elseif ($rol =='director'){
        return "Director";
      }elseif ($rol =='docente'){
        return "Docente";
      }elseif ($rol =='estudiante'){
        return "Estudiante";
      }else{
        return $rol;
      }
    }   
    
       
    public static function periodo_activo()
    {
      $periodo_activo = Periodo::where('estado','1')->get();
      if (count($periodo_activo)>0){
        $periodo = $periodo_activo->first();
      }else{
        $periodo = new Periodo();
        $periodo->periodo='No hay periodo activo';
      }
      return $periodo;
    }   
    
       
    public static function asignaciones($cod_prog, $cod_asigt, $per_acad)
    {
      $asigcs = Asigc::where(['cod_prog' => $cod_prog, 'cod_asigt' => $cod_asigt, 'per_acad' => $per_acad])->get();
      foreach ($asigcs as $id => $asigcsi){ 
      $asigcs[$id]->docente_fk();
      $asigcsfk = $asigcs[$id]->docente_fk->id;
      }
       return $asigcs;
    }   
    
       
    public static function palabra_a_numero($string, $flex = 'NO')
    {
    /**
    * Funciones::palabra_a_numero($asigc->asigt->sem_ofrece_asigt,'NO')
    */
    if ($flex == 'NO'){
        $palabras = array( "Primero","Segundo", "Tercero", "Cuarto", "Quinto", "Sexto", "Septimo", "Octavo", "Noveno", "Decimo", "Trece");
        $numeros   = array("1", "2", "3","4", "5", "6","7", "8", "9", "10", "13");
        $string = str_replace($palabras, $numeros, $string);
        return $string;
    }else  if ($flex == 'SI'){
        return 13;
    }
    }
    public static function params_post(){
        $salida = '';
        if (!empty($_POST))
        foreach ($_POST as $id_post => $valor_post){ ?>
        <?php if (!is_array($valor_post)){ 
        $salida .= '<input hidden type="text" name="'.$id_post.'" value="'.$valor_post.'">';
        }else{ 
        foreach ($valor_post as $id_vpost => $vvalor_post){
        $tipo = "radio";
        $name = "".$id_vpost."";
        $salida .= '<input hidden type="'.$tipo.'" checked name="'.$name.'" value="'.$vvalor_post.'">';
        }
        }
        }
        return $salida;
    }
}
?>