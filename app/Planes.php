<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Planes extends Model
{
    public $timestamps = false;
    protected $table = 'planes';
    protected $fillable = [
        'nombre',
        'adjunto',
        'fecha',
        'cod_prog',
        'observaciones'
    ];

    public $fillcolumn = [
        'nombre'=>'Nombre',
        'adjunto'=>'Adjunto',
        'fecha'=>'Fecha',
        'cod_prog' => 'Programa',
        'observaciones' => 'Observaciones'
    ];
  public function prog()
    {//Verificado
        return $this->belongsTo('App\Prog','cod_prog','cod_prog');
    }
 public function planes_asigts()
    {//Verificado
        return $this->hasMany('App\PlanesAsigt');
    }
 
}
