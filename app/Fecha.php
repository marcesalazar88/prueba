<?php

namespace App;

use DateTime;
use Illuminate\Http\Request;
class Fecha
{
        public static function formato_fecha($fecha)
    {
          // Fecha::formato_fecha($fecha)
          $salida = "";
          if ($fecha!="" and $fecha!=0){
          $date = new DateTime($fecha);
          $salida=  $date->format('d/m/Y');
          }
          return $salida;
    }
        public static function formato_fecha_mes($fecha)
    {
          $salida = "Fecha pendiente";
          if ($fecha!="" and $fecha!=0){
          $date = new DateTime($fecha);
          $meses = array ('','ene','feb','mar','abr','may','jun','jul','ago','sep','oct','nov','dic');
          $salida =  $date->format('d/'); 
          $salida .=  $meses[$date->format('n')]; 
          $salida .=  $date->format('/Y'); 
          } 
          return $salida;
    }  
  public static function formato_fecha_mes_hora($fecha)
    {
          $salida = "Fecha pendiente";
          if ($fecha!=""){
          
          //$date = new DateTime($fecha);
          $meses = array ('','ene','feb','mar','abr','may','jun','jul','ago','sep','oct','nov','dic');
          //$salida =  $date->format('d/'); 
          $salida =  date("d/",strtotime($fecha));
          $salida .=  $meses[date("n",strtotime($fecha))]; 
          //$salida .=  $meses[$date->format('n')]; 
          $salida .=  date("/Y  H:i:s a",strtotime($fecha));
          #$salida .=  $date->format('/Y  H:i:s a'); 
          }
          #dd($salida);
          return $salida;
    }
   public static function formato_fecha_hora($fecha)
    {
          // Fecha::formato_fecha($fecha)
          $date = new DateTime($fecha);
          $salida=  $date->format('d/m/Y H:i:s a');
          return $salida;
    }
    public static function estado_dif_fecha_vencimiento($estado_dif_fecha) 
      {

          $estado_dif_fecha_s='';
          if ($estado_dif_fecha==""){
              $estado_dif_fecha_s = "Fecha Pendiente";
          }else if ($estado_dif_fecha==0){
              $estado_dif_fecha_s = "Hoy";
          }else if ($estado_dif_fecha>0){
              if (abs($estado_dif_fecha)==1)
              $estado_dif_fecha_s = "Falta ".abs($estado_dif_fecha)." día";
              else
              $estado_dif_fecha_s = "Faltan ".abs($estado_dif_fecha)." días";

          }else if ($estado_dif_fecha<0){
              if (abs($estado_dif_fecha)==1)
              $estado_dif_fecha_s = "Hace ".abs($estado_dif_fecha)." día";
              else
              $estado_dif_fecha_s = "Hace ".abs($estado_dif_fecha)." días";
          }
          #dd($estado_dif_fecha_s);
          return $estado_dif_fecha_s;
      }
   public static function hace($fecha){
  $fecha_unix = strtotime($fecha);
	//obtener la hora en formato unix
	$ahora=time();
	
	//obtener la diferencia de segundos
	$segundos=$ahora-$fecha_unix;
	
	//dias es la division de n segs entre 86400 segundos que representa un dia;
	$dias=floor($segundos/86400);

	//mod_hora es el sobrante, en horas, de la division de días;	
	$mod_hora=$segundos%86400;
	
	//hora es la division entre el sobrante de horas y 3600 segundos que representa una hora;
	$horas=floor($mod_hora/3600);
	
	//mod_minuto es el sobrante, en minutos, de la division de horas;	
	$mod_minuto=$mod_hora%3600;
	
	//minuto es la division entre el sobrante y 60 segundos que representa un minuto;
	$minutos=floor($mod_minuto/60);
	
	if($horas<=0){
		return 'hace '.$minutos.' minutos';
	}elseif($dias<=0){
		return 'hace '.$horas.' y horas '.$minutos.' minutos';
	}else{
		return 'hace '.$dias.' dias';//.$horas.' horas '.$minutos.' minutos';
	}
}
      public static function mes_letras($fecha,$letras=false){
      $meses = array ('','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
      if($fecha<10) $fecha = substr($fecha,1);
      return $salida = $meses[$fecha];    
      }
  /*
      public static function fecha_unix($fecha)
      {   
        return strtotime($fecha);
      }
  */
      public static function diferencia_fecha($fecha_i,$fecha_f,$abs = true)
      {
        $dias='';
        if (($fecha_i!="" or $fecha_i!=0) and ($fecha_f!="" or $fecha_f!=0)){
              $dias	= (strtotime($fecha_i)-strtotime($fecha_f))/86400;
    $dias = floor($dias);		
    if ($abs) $dias	= abs($dias);
        }
    return $dias;
      }
}
?>