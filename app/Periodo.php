<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Periodo extends Model
{
    public $timestamps = false; 
    protected $primaryKey = 'periodo'; // or null
    public $incrementing = false;
    protected $table = 'periodo';
    protected $fillable = [
      //`periodo`, `estado`, `calendario`, `fecha_inicio`, `fecha_fin`, `acuerdo`, `adjunto`
        'periodo',
        'estado',
        'calendario',
        'fecha_inicio',
        'fecha_fin',
        'acuerdo',
        'adjunto'
    ];

    public $fillcolumn = [
        'periodo' => 'periodo',
        'estado' => 'estado'
    ];
    public function asigcs()
    {//Verificado
        return $this->hasMany('App\Asigc');
    }
}
