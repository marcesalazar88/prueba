<?php

namespace App\Http\Controllers;

use App\Planes;
use App\Periodo;
use App\Crud;
use App\Fecha;
use App\Prog;
use App\Asigt;
use Auth;
use App\Funciones;
use App\User;
use App\PlanesAsigt;
use Illuminate\Http\Request;
use App\Http\Requests\PlanesRequest;
use App\Http\Requests\PlanesRequestUpdate;
use Illuminate\Support\Facades\Validator;
use Redirect;
use Illuminate\Support\Facades\Storage;
class PlanesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public $planes;
     public function __construct()
    {
        $this->middleware('auth');//auth|guest
        $this->middleware('rol:admin|director', [
          'only' => ['index','create','store','show','edit','update','destroy']
        ]);//admin|director|docente|estudiante
       /*
$this->middleware('permiso.depto:6', [
          'only' => ['show','edit','update','destroy']
        ]);//admin|director|docente|estudiante
       */
}
  
    public function index(Request $request)
    {
         $crud = new Crud();
         $prog = new Prog();
      
      if (Auth::user()->rol=="admin"){   
          $prog = $prog->all();
      }else if (Auth::user()->rol=="director"){  
          $id_user = Auth::user()->id;
          $user = User::find($id_user);
          $user->deptos();
          foreach($user->deptos as $deptoi){
            $deptoi->progs();
            foreach($deptoi->progs as $progsi){
              $progs_asigc[]=$progsi->cod_prog;
            }
          }
        $prog= Prog::whereIn('cod_prog',$progs_asigc)->get();
      }
         $this->planes = new Planes();
         $resultado = $crud->buscar($request,$this->planes);
         foreach ($resultado as $id => $resultado_i){
            $resultado_i->prog();
            $resultado[$id]->nom_prog = $resultado[$id]->prog->nom_prog;
            $resultado[$id]->fecha = Fecha::formato_fecha($resultado[$id]->fecha);
          }
         $botones = true;
          #dd($resultado->app_url);
          #dd($resultado);
         if (isset($_REQUEST['json'])){
         return $resultado;
         }else{
         return view('admin.planes.index')->with(["resultado"=>$resultado, "prog"=>$prog,'botones'=>$botones]);
         }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($cod_prog='')
    {
      
      if ($cod_prog!=''){
        if (Funciones::permiso_depto('cod_prog',$cod_prog)){
              $prog = Prog::where('cod_prog',$cod_prog)->get();
              return view('admin.planes.create')->with(["prog"=>$prog->first()]);
        }else{ 
          return redirect('home')->with('danger', 'Error, Usted no tiene los permisos para ingresar a este sitio');
        }

      }else{
              return redirect()->back()->with('danger', 'Error, Usted está intentando ingresar de manera incorrecta, verifique su información. el código de programa no existe');
      }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PlanesRequest $request)
    {
if (Funciones::permiso_depto('cod_prog',$request->cod_prog)){
        $fileName_adjunto = "plan_".$request->cod_prog."_".date("YmdHis").'.'.request()->adjunto->getClientOriginalExtension();
        $request->adjunto->storeAs('planes',$fileName_adjunto, 'publico');
        $planes = new Planes;
        $planes->nombre = $request->nombre;
        $planes->adjunto = "/planes/".$fileName_adjunto;
        $planes->fecha = $request->fecha;
        $planes->observaciones = $request->observaciones;
        $planes->cod_prog = $request->cod_prog;
        $result = $planes->save();
        $prog = Prog::where('cod_prog',$planes->cod_prog)->get()->first();
        if($result){
        return redirect()->route('admin.planes.show', ['id' => $planes->id])->with('success', 'El plan ha sido registrado correctamente.');
        }else{
        return redirect()->route('admin.planes.show', ['id' => $planes->id])->with('danger', 'Error, El plan no ha sido registrado.');
        }
}else{ 
          return redirect('home')->with('danger', 'Error, Usted no tiene los permisos para ingresar a este sitio');
}
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Fac  $planes
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
if (!Funciones::permiso_depto('plan',$id)){
          return redirect('home')->with('danger', 'Error, Usted no tiene los permisos para ingresar a este sitio');
}
      #dd($id);
      $periodo_activo = Periodo::where('estado','1')->get()->first();
      $planes = Planes::find($id);
      if (isset($planes->id)){
      $planes->prog();
            $planes->prog->planes();
            $planes->prog->depto();
            $planes->prog->depto->fac();
            $fac = $planes->prog->depto->fac;
            $planes->prog->asigcs();
            $planes_asigts = PlanesAsigt::where('planes_id',$id)->orderBy('semestre', 'ASC')->get();
            $planes->planes_asigts = $planes_asigts;
         foreach ($planes->planes_asigts as $id => $asigc){
           $planes->planes_asigts[$id]->asigt();
           $planes->planes_asigts[$id]->prerequisitos();
         }
            //dd($planes->prog);
        /*
           
            asigt
                $planes->prog->asigcs[$id]->docente_fk();
                $docente_fk = $planes->prog->asigcs[$id]->docente_fk;
                $planes->prog->asigcs[$id]->estudiante_fk();
                $estudiante_fk = $planes->prog->asigcs[$id]->estudiante_fk;
                $planes->prog->asigcs[$id]->asigt();
                $asigt = $planes->prog->asigcs[$id]->asigt;
            }
            */
            $asigts = new Asigt();
            return view('admin.planes.show')->with(['planes'=>$planes,'periodo_activo'=>$periodo_activo,'asigts'=>$asigts->all()]);
        }else{
            return Redirect::to('admin/programa/planes')->with('danger', 'No existen registros de este programa.');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Fac  $planes
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      if (Funciones::permiso_depto('plan',$id)){
        $planes = Planes::find($id);
        return view("admin.planes.editar")->with(["planes"=>$planes]);
      }else{ 
        return redirect('home')->with('danger', 'Error, Usted está intentando ingresar de manera incorrecta, verifique su información.');
      }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Fac  $planes
     * @return \Illuminate\Http\Response
     */
    public function update(PlanesRequest $request, $id)
    {
if (!Funciones::permiso_depto('plan',$id)){
          return redirect('home')->with('danger', 'Error, Usted no tiene los permisos para ingresar a este sitio');
}
        //dd($request);
        $planes = Planes::find($id);
        $planes->prog();
        $planes->nombre = $request->nombre;
        if ($request->modificar_adjunto == "SI")
        {
          Storage::disk('publico')->delete($planes->adjunto);//borrar el anterior
          $fileName_adjunto = "plan_".$request->cod_prog."_".date("YmdHis").'.'.request()->adjunto->getClientOriginalExtension();
          $request->adjunto->storeAs('planes',$fileName_adjunto, 'publico');
          $planes->nombre = $request->nombre;
          $planes->adjunto = "/planes/".$fileName_adjunto;
        }
        $planes->fecha = $request->fecha;
        $planes->observaciones = $request->observaciones;
        $result = $planes->save();
        if($result)
        {
        return redirect()->route('admin.programa.show', [$planes->prog->id])->with('success', 'El plan ha sido actualizado correctamente.');
        }else{
        return redirect()->route('admin.programa.show', [$planes->prog->id])->with('danger', 'Error, El plan no ha sido actualizado.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Fac  $planes
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
if (!Funciones::permiso_depto('plan',$id)){
          return redirect('home')->with('danger', 'Error, Usted no tiene los permisos para ingresar a este sitio');
}
    $planes = Planes::find($id);
    if($planes):
    $prog = Prog::where('cod_prog',$planes->cod_prog)->get()->first();
    Storage::disk('publico')->delete($planes->adjunto);
    $planes->delete();
        if ($planes->exists === false){
        return redirect()->route('admin.planes.index')->with('success', 'El plan ha sido eliminado correctamente.');
        }else{
        return redirect()->route('admin.planes.index')->with('danger', 'Error, El plan no ha sido eliminado.');
        }
    else:
    return redirect()->back()->with('danger', 'Error, El plan no ha sido eliminado.');
    endif;
    }
}
