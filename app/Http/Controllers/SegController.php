<?php

namespace App\Http\Controllers;
use Auth;
use App\Seg;
use App\Asigc;
use App\ProgTem;
use App\Depto;
use App\SegVal;
use App\Mensaje;
use App\SegAspVal;
use App\TipoValoracion;
use App\ControlFechas;
use App\Funciones;
use Illuminate\Http\Request;
use Redirect;
use App\Http\Controllers\AsigcController;
class SegController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function __construct()
    {
        $this->middleware('auth');//auth|guest
    }
    public function index()
    {
        return  redirect()->back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return  redirect()->back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return  redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Seg  $seg
     * @return \Illuminate\Http\Response
     */
    public function actualizar_item(Request $request, $id)
    {
        $asigc = $request->asigc;
        if (Auth::user()->rol=="estudiante"){
        #if (!Funciones::permiso_asigc('asigc',$asigc)){
          return ['estado'=>0];
        }
        $ide = $request->ide;
        $segVal = SegVal::find($ide);
        $segVal->valoracion = $request->valoracion;
        $result = $segVal->save();
        if($result){
          return ['estado'=>1];
        }else{
          return ['estado'=>0];
        }
    }
    public function eliminar_item(Request $request, $id)
    {
        $asigc = $request->asigc;
        if (Auth::user()->rol=="estudiante"){
        #if (!Funciones::permiso_asigc('asigc',$asigc)){
          return ['estado'=>0];
        }
        
        $ide = $request->ide;
        $segVal = SegVal::find($ide);
        $segVal->delete();
        if ($segVal->exists === false){
          return ['estado'=>1];
        }else{
          return ['estado'=>0];
        }
    }
    public function guardar_item(Request $request, $id, $id_asigc)
    {
        //dd($request->all());
        $segVal = new SegVal();
        $segVal->seg_id = $request->seg_id;
        $segVal->aspecto = $request->aspecto;
        $segVal->tipo = $request->tipo;
        $segVal->valoracion = $request->valoracion;
        $segVal->save();
        if ($segVal->exists === true)
        return redirect()->route('admin.asignacion.seguimiento.edit',['id' => $id_asigc])->with('notice', 'La valoración ha sido correctamente.');
        else
        return redirect()->route('admin.asignacion.seguimiento.edit',['id' => $id_asigc])->with('notice', 'Error, La valoración no ha sido guardada.');
    }
    public function guardar_voto()
    {
        //dd($_POST);
      if(isset($_POST['id'])){
        foreach ($_POST['id'] as $ids):
            $segVal = SegVal::find($ids);
            $segVal->voto = $_POST['voto'][$ids];
            $segVal->replica = $_POST['replicavoto'][$ids];
            $segVal->save();
            $id = $segVal->seg();
            $id = $segVal->seg->prog_tem();
            $id = $segVal->seg->prog_tem->asigc_id;
            //dd($id);
        endforeach;
        return redirect()->route('admin.asignacion.seguimiento.edit',['id' => $id])->with('notice', 'La valoración ha sido correctamente.');
      }else{
         return redirect()->back();
      }
    }
    public function show($id)
    {
        return redirect()->route('reportes.seguimiento',['id' => $id]);
    }
     public function edit($id)
    {
        if (!Funciones::permiso_asigc('asigc',$id)){
        return Redirect::to('home')->with('danger', 'Error, No tiene permisos para ingresar.');
        }
          $asigc_id = $id;
        
        $asigc = Asigc::find($id);
        $auxasigcAsigc = new AsigcController();
        $auxasigcAsigc->crear_registros($asigc);
        $asigc->progtems();
        $asigc->codigos();
        foreach ($asigc->progtems as $id => $progtem){
        #$asigc->progtems->each(function ($progtem) {
            #if ($id == $progtem->asigc_id){
        //    if (prog_temt_id == id){
                $asigc->progtems[$id]->segs();
                /*$progtem->segs->all();*/
                foreach ($asigc->progtems[$id]->segs as $id2 => $seg){
                #$progtem->segs->each(function ($seg) {
                    
           # if ($progtem->id == $seg->progtem_id){
                    $asigc->progtems[$id]->segs[$id2]->seg_vals();
                    //$seg->seg_vals->all();
                    foreach ($asigc->progtems[$id]->segs[$id2]->seg_vals as $id3 => $seg_vals){
                    #$seg->seg_vals->each(function ($seg_vals) {
                        #if ($seg->id == $seg_vals->seg_id){
                        $seg_id = $asigc->progtems[$id]->segs[$id2]->id;
                        $asigc->progtems[$id]->segs[$id2]->seg_vals[$id3]->segaspval();//OJO relación
                        $name = $asigc->progtems[$id]->segs[$id2]->seg_vals[$id3]->segaspval->name;
                        //dd($seg_vals);
                       # }
                    };  
            #}
                };  
            #}
        };
        #dd($asigc);
        #$asigc->progs();
        #echo $prog->seg->id;
        #$asigc->progtems();
        #$asigc->progtems()->first();
        #$codigo = !empty($asigc->codigos()) ? $asigc->codigos()->first()->token : '' ;
        #$asigc->codigo = $codigo;
        #->seg();
        $seg = [];
        try{ 
        if ($seg = $asigc->progtems()->first()->segs->first()->seg_vals->first()->get()){
            
        }else{
            throw new Exception("Algún mensaje de error"); 
        }
            
        } catch(Exception $e) { 
            echo $e->getMessage();
        }
        //dd($seg);
        //observ_est
    
        #$seg->where('seg.asigc_id',$id)
                        //->join('asigc', 'asigc.id', '=', 'seg.id_asigc')
                        //->join('asigt', 'asigc.cod_asigt', '=', 'asigt.codigo_asigt')
                        //->join('usu', 'asigc.ident_docnt', '=', 'usu.ident_usu')
                        //->join('prog', 'asigc.cod_prog', '=', 'prog.cod_prog')
                        //->join('depto', 'depto.id', '=', 'prog.depto_id')
                        //->join('fac', 'fac.id', '=', 'depto.fac_id')
                        //->select('asigt.*','usu.*','asigc.*','prog.*','fac.*','seg.*')
                        //->get()
                        //->first();
                        //->toSql();
                        /**
                        Es una consulta para un reporte, no es muy diferente a un SQL puro
                        */
         #   $seg->seg->seg_vals();
           # dd($seg);
            $todos = count($asigc);
            $criterios = SegAspVal::orderBy('name')->pluck('name', 'cod');
            $tipovaloracion = TipoValoracion::orderBy('id')->get();
            $user = Auth::user();
$nombres = [
            'criterio' =>  env('criterio'),
            'tipo_valoracion'=> env('tipo_valoracion')
    ];
            if ($todos>0){
                #$seg = $seg->first();
                //$arreglo = json_decode($seg->punto_adic);
                //var_dump($arreglo);
                //$var = ['Criterio','Contenido'];
                //dd(json_encode($var));
                //dd("Fin");
                #$seg->asigc();
                #seg->asigc->progtems()->first();
                #$seg->asigc->progtem->refe_biblios();
                #$seg->asigc->progtem->punto_adics();
                #$seg->asigc->asigt();
                #$seg->asigc->asigt->contasigts();
                #$seg->asigc->docente_fk();
                #dd($seg->asigc->asigt->contasigts);
                #$seg->asigc->asigt->contasigts();
                //dd($seg_id);
              //dd($asigc);
               $asigc->prog();
                $asigc->prog->depto();
                $depto_id = $asigc->prog->depto->id;
                $controlfechas = ControlFechas::where('formato','seg')->where('depto_id',$depto_id)->get();
              
              $seguimiento = $asigc->progtems()->first()->segs->first();
              $segestado = Seg::find($seguimiento->id);
        if (Auth::user()->rol=="docente"){
          if($segestado->estado=="Pendiente"){
          $segestado->estado = "En Proceso";             
          $segestado->save();
          }   
        }
        if (Auth::user()->rol=="estudiante"){
          if($segestado->estado_est=="Pendiente"){
          $segestado->estado_est = "En Proceso";             
          $segestado->save();
          }
        }

                return view('admin.asignacion.seguimiento.editar')->with(['seg_id'=>$seg_id,'id'=>$asigc_id,'asigc'=>$asigc,'user'=>$user,'seg'=>$seg,'nombres'=>$nombres,'tipovaloracion'=>$tipovaloracion,'criterios'=>$criterios,'controlfechas'=>$controlfechas->all(),'seguimiento'=>$seguimiento]);
                //return view('reportes.seg_prog')->with(['asigc'=>$asigc]);
            }else{
                return Redirect::to('home');
            }
    }
  /*
   public function edit2(Request $request, $id)
    {
        if ($request->has('mensaje')){
         $dato = $request->get('mensaje');
         //dd($dato);
         //return 22;
         $retorno = Mensaje::where('asigc_id',$dato)->orderBy('id', 'ASC')->get();
         $datos = [];
         foreach ($retorno as $retornoi){
           $datos[] = $retornoi->id."|".$retornoi->de."|".$retornoi->para."|".$retornoi->texto;
         }
         //$datos2 = "<li>" . implode("</li><li>", $datos) . "</li>";
         echo base64_encode($datos);die();
       }
        $asigc_id = $id;
        $mensaje = Mensaje::where('asigc_id',$id)->get();
       foreach ($mensaje as $id => $mensajei)
       {
          $de = $mensaje[$id]->de_fk();
       }
        $asigc = Asigc::find($id);
        $auxasigcAsigc = new AsigcController();
        $auxasigcAsigc->crear_registros($asigc);
        $asigc->progtems();
        $asigc->codigos();
        foreach ($asigc->progtems as $id => $progtem){
        #$asigc->progtems->each(function ($progtem) {
            #if ($id == $progtem->asigc_id){
        //    if (prog_temt_id == id){
                $asigc->progtems[$id]->segs();
                /*$progtem->segs->all();* /
                foreach ($asigc->progtems[$id]->segs as $id2 => $seg){
                #$progtem->segs->each(function ($seg) {
                    
           # if ($progtem->id == $seg->progtem_id){
                    $asigc->progtems[$id]->segs[$id2]->seg_vals();
                    //$seg->seg_vals->all();
                    foreach ($asigc->progtems[$id]->segs[$id2]->seg_vals as $id3 => $seg_vals){
                    #$seg->seg_vals->each(function ($seg_vals) {
                        #if ($seg->id == $seg_vals->seg_id){
                        $seg_id = $asigc->progtems[$id]->segs[$id2]->id;
                        $asigc->progtems[$id]->segs[$id2]->seg_vals[$id3]->segaspval();//OJO relación
                        $name = $asigc->progtems[$id]->segs[$id2]->seg_vals[$id3]->segaspval->name;
                        //dd($seg_vals);
                       # }
                    };  
            #}
                };  
            #}
        };
        #dd($asigc);
        #$asigc->progs();
        #echo $prog->seg->id;
        #$asigc->progtems();
        #$asigc->progtems()->first();
        #$codigo = !empty($asigc->codigos()) ? $asigc->codigos()->first()->token : '' ;
        #$asigc->codigo = $codigo;
        #->seg();
        $seg = [];
        try{ 
        if ($seg = $asigc->progtems()->first()->segs->first()->seg_vals->first()->get()){
            
        }else{
            throw new Exception("Algún mensaje de error"); 
        }
            
        } catch(Exception $e) { 
            echo $e->getMessage();
        }
        //dd($seg);
        //observ_est
    
        #$seg->where('seg.asigc_id',$id)
                        //->join('asigc', 'asigc.id', '=', 'seg.id_asigc')
                        //->join('asigt', 'asigc.cod_asigt', '=', 'asigt.codigo_asigt')
                        //->join('usu', 'asigc.ident_docnt', '=', 'usu.ident_usu')
                        //->join('prog', 'asigc.cod_prog', '=', 'prog.cod_prog')
                        //->join('depto', 'depto.id', '=', 'prog.depto_id')
                        //->join('fac', 'fac.id', '=', 'depto.fac_id')
                        //->select('asigt.*','usu.*','asigc.*','prog.*','fac.*','seg.*')
                        //->get()
                        //->first();
                        //->toSql();
                        /**
                        Es una consulta para un reporte, no es muy diferente a un SQL puro
                        * /
         #   $seg->seg->seg_vals();
           # dd($seg);
            $todos = count($asigc);
            $criterios = SegAspVal::orderBy('id')->pluck('name', 'cod');
            $tipovaloracion = TipoValoracion::orderBy('id')->get();
            $user = Auth::user();
$nombres = [
            'criterio' =>  env('criterio'),
            'tipo_valoracion'=> env('tipo_valoracion')
    ];
            if ($todos>0){
                #$seg = $seg->first();
                //$arreglo = json_decode($seg->punto_adic);
                //var_dump($arreglo);
                //$var = ['Criterio','Contenido'];
                //dd(json_encode($var));
                //dd("Fin");
                #$seg->asigc();
                #seg->asigc->progtems()->first();
                #$seg->asigc->progtem->refe_biblios();
                #$seg->asigc->progtem->punto_adics();
                #$seg->asigc->asigt();
                #$seg->asigc->asigt->contasigts();
                #$seg->asigc->docente_fk();
                #dd($seg->asigc->asigt->contasigts);
                #$seg->asigc->asigt->contasigts();
                //dd($seg_id);
                $asigc->prog();
                $asigc->prog->depto();
                $depto_id = $asigc->prog->depto->id;
                $controlfechas = ControlFechas::where('formato','seg')->where('depto_id',$depto_id)->get();
              
                $seguimiento = $asigc->progtems()->first()->segs->first();
                return view('admin.asignacion.seguimiento.editar')->with(['seg_id'=>$seg_id,'id'=>$asigc_id,'asigc'=>$asigc,'user'=>$user,'seg'=>$seg,'nombres'=>$nombres,'tipovaloracion'=>$tipovaloracion,'criterios'=>$criterios,'controlfechas'=>$controlfechas,'mensaje'=>$mensaje,'seguimiento'=>$seguimiento]);
                //return view('reportes.seg_prog')->with(['asigc'=>$asigc]);
            }else{
                return Redirect::to('home');
            }
    }
*/
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Seg  $seg
     * @return \Illuminate\Http\Response
     */
    public function edit_post(Request $request, $id)
    {
        //dd($request->all());
      if (isset($_GET['por'])){
          $segval = SegVal::find($request->id);
          $segval->valoracion = $request->pa;
          if ($segval->save()){
            return "1";
          }else{
            return "0";
          }
        //die();
      }
      
        $prog_temt = ProgTem::where('asigc_id',$id)->get()->first();
        $prog_temt_id = $prog_temt->id; 
        $asigc = Seg::where('prog_temt_id',$prog_temt_id)->get()->first();
        if (Auth::user()->rol=='docente')
            if (isset($request->observ_docnt)) 
                $asigc->observ_docnt = $request->observ_docnt;
        if (Auth::user()->rol=='estudiante')
            if (isset($request->observ_est)) 
                $asigc->observ_est = $request->observ_est;
        if (Auth::user()->rol=='admin' or Auth::user()->rol=='director'){
          
        if (isset($request->fecha_aprobacion)){
                        $asigc->fecha_aprobacion = $request->fecha_aprobacion;  
                        $asigc->control_fecha_id = $request->control_fecha_id;                
        }
        if (isset($request->fecha_revision_doc)) 
                        $asigc->fecha_revision_doc = $request->fecha_revision_doc;  
        if (isset($request->fecha_revision_est)) 
                        $asigc->fecha_revision_est = $request->fecha_revision_est;                
        }

        if ($asigc->save()){
            return "1";
        }else{
            return "0";
        }
    }
    public function edit22($id)
    {
        return  redirect()->back();
        $asigc = Asigc::find($id);
        $asigc->progtems();
        $asigc->progtems->each(function ($progtem) {
            $progtem->segs();
            /*$progtem->segs->all();*/
            $progtem->segs->each(function ($seg) {
                $seg->seg_vals();
                //$seg->seg_vals->all();
                $seg->seg_vals->each(function ($seg_vals) {
                $seg_vals->segaspval();
                $name = $seg_vals->segaspval->name;
                #dd($seg_vals);
                });  
            });  
        });
         $todos = count($asigc);
            if ($todos>0){
                //dd($asigc);
                //return view('admin.asignacion.seguimiento.editar')->with(['asigc'=>$asigc]);
            }else{
                return Redirect::to('home');
            }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Seg  $seg
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return  redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Seg  $seg
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return  redirect()->back();
    }
}