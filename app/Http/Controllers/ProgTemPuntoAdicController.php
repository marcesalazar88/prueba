<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProgTemPuntoAdic;
use App\ProgTem;
use Redirect;
use App\Http\Requests\PuntoAdRequest;
class ProgTemPuntoAdicController extends Controller
{
    public function store(PuntoAdRequest $request, $id)
    {
        #dd($request->all());
        $puntoadic = new ProgTemPuntoAdic();
        $puntoadic->prog_tem_id = $id;
        $puntoadic->item = $request->item;
        $puntoadic->valor = $request->valor;
        $puntoadic->save();
        $progtem = ProgTem::find($id);
        $asigc_id = $progtem->asigc_id;
        if ($puntoadic->exists === true){
          return redirect(route('admin.asignacion.programaciontematica.edit', [$asigc_id]). '#puntoadicional')->with('notice', 'El punto adicional ha sido creada correctamente.');
        }else{
      return redirect(route('admin.asignacion.programaciontematica.edit', [$asigc_id]). '#puntoadicional')->with('notice', 'Error, El punto adicional no ha sido creada.');
        }
    }
  /*
puntoadicional
bibliografia*/
    public function destroy($id, $id2)
    {
        $puntoadic = ProgTemPuntoAdic::find($id2);
        if ($puntoadic->exists === true) $puntoadic->delete();
        if ($puntoadic->exists === false){
 return redirect(route('admin.asignacion.programaciontematica.edit', [$id]). '#puntoadicional')->with('notice', 'La referencia bibliográfica ha sido eliminada correctamente.');
        }else{
 return redirect(route('admin.asignacion.programaciontematica.edit', [$id]). '#puntoadicional')->with('danger', 'Error, La referencia bibliográfica no ha sido eliminada.');
    }
}
}
