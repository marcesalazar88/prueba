<?php

namespace App\Http\Controllers;
use Redirect;
use App\ProgTem;
use App\User;
use App\Asigc;
use App\Prog;
use App\Funciones;
use App\Asigt;
use App\Http\Controllers\AsigcController;
use App\ControlFechas;
use Illuminate\Http\Request;
use Auth;

class ProgTemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   public function __construct()
    {
        $this->middleware('auth');//auth|guest
        //$this->middleware('rol:admin|director|docente');//admin|director|docente|estudiante
    }
    public function index()
    {
        $this->middleware('auth', ['only' => ['index','create','edit','destroy','store','update']]);//auth|guest
        $this->middleware('rol:admin', ['only' => ['index','create','edit','store','update']]);//admin|docente|estudiante

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //dd('create programacion temática');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$asigc_id)
    {
        $this->middleware('auth');//auth|guest
        $progtem = new ProgTem();
        $progtem->asigc_id = $asigc_id;
        $progtem->fecha_act_progc_temt = "0000-00-00";
        $progtem->metodologia = '';
        $progtem->crit_eva = '';
        $progtem->rev_direct = NULL;
        $progtem->firm_docnt = NULL;
        $progtem->firm_est = NULL;
        $result = $progtem->save();
        if($result){
            return redirect()->route('admin.asignacion.programaciontematica.show',[$asigc_id])->with('success', 'La programación temática ha sido registrada correctamente.');
        }else{
            return  redirect()->route('admin.asignacion.programaciontematica.show',[$asigc_id])->with('danger', 'Error, La programación temática no ha sido registrada.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProgTem  $progTem
     * @return \Illuminate\Http\Response
     */
    public function show(ProgTem $progTem, $id)
    {
      //dd($id);
      if (!Funciones::permiso_asigc('asigc',$id)){
          return Redirect::to('home')->with('danger', 'Error, No tiene permisos para ingresar.');
        }
        #echo "ID ".$id."<br>";
        #echo "elegir "."<br>";
        #echo 'show programacion temática'."<br>";
        $asigt = ProgTem::where('asigc.id',$id)
                        ->join('asigc', 'asigc.id', '=', 'prog_temt.asigc_id')
                        ->join('asigt', 'asigc.cod_asigt', '=', 'asigt.codigo_asigt')
                        ->join('usu', 'asigc.ident_docnt', '=', 'usu.ident_usu')
                        ->join('prog', 'asigc.cod_prog', '=', 'prog.cod_prog')
                        ->join('depto', 'depto.id', '=', 'prog.depto_id')
                        ->join('fac', 'fac.id', '=', 'depto.fac_id')
                        ->select('asigt.*','usu.*','asigc.*','prog.*','fac.*','prog_temt.*')
                        ->get();
                        //->toSql();
                        /**
                        Es una consulta para un reporte, no es muy diferente a un SQL puro
                        */
            //dd($asigt);
            //$todos = count($asigt);
            $asigt = $asigt[0];
            if ($asigt->exists){
                //$arreglo = json_decode($asigt->punto_adic);
                //var_dump($arreglo);
                //$var = ['Criterio','Contenido'];
                //dd(json_encode($var));
                //dd("Fin");
                $asigt->refe_biblios();
                $n = $asigt->refe_biblios;
                $asigt->punto_adics();
                $n = $asigt->punto_adics;
                $asigt->asigc();
                $n = $asigt->asigc;
                $asigt->asigc->asigt();
                $n = $asigt->asigc->asigt;
                $asigt->asigc->asigt->contasigts();
                $n = $asigt->asigc->asigt->contasigts;
                $asigt->asigc->docente_fk();
                $n = $asigt->asigc->docente_fk;
              
                      $progTem = ProgTem::where('asigc_id',$id)->get();
                    $progTem = $progTem->first();
                    if($progTem->codigo==""){
                      $progTem->codigo = uniqid("PT");             
                      $progTem->save();
                    }

                    
                #dd($asigt->asigc->asigt->contasigts);
                #$asigt->asigc->asigt->contasigts();
                #dd($asigt);
                return view('reportes.prog_tem')->with(['asigt'=>$asigt]);
            }else{
                return Redirect::to('home');
            }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProgTem  $progTem
     * @return \Illuminate\Http\Response
     */
    public function edit(ProgTem $progTem, $id){
        $auth = Auth::check();
        if ($auth){
        //if (!Funciones::permiso_asigc('asigc',$id)){
          //return Redirect::to('home')->with('danger', 'Error, No tiene permisos para ingresar.');
        //}
            //$progTem = ProgTem::find($id);
            $progTem = ProgTem::where('asigc_id',$id)->get();       
            $asigc = Asigc::find($id);
            if (count($progTem)==0){
            $auxasigcAsigc = new AsigcController();
            $auxasigcAsigc->crear_registros($asigc);
            $progTem = ProgTem::where('asigc_id',$id)->get();
            }
            $progTem = $progTem->first();
           if($progTem->codigo==""){
                $progTem->codigo = uniqid("PT");             
                $progTem->save();
             }
           if (Auth::user()->rol=='docente'){
              if($progTem->estado=="Pendiente"){
              $progTem->estado = "En Proceso";             
              $progTem->save();
              }
           }
            $user = new User();
            
            //dd($progTem->asigc_id);
            $progs = new Prog();
            $asigts = new Asigt();
            $usuarios = new User();
            $controlfechas = new ControlFechas();
            //dd($progTem);
            $progTem->refe_biblios();
            $progTem->asigc();
            $progTem->asigc->docente_fk();
            $progTem->asigc->asigt();
            $progTem->punto_adic = json_decode($progTem->punto_adic);//decodifica json en arreglo
            #$progTem->refe_biblio = json_decode($progTem->refe_biblio);//decodifica json en arreglo
            return view('admin.asignacion.programaciontematica.editar')->with(["progTem"=>$progTem,"user"=>$user,"asigc"=>$asigc,"progs"=>$progs->all(),"asigts"=>$asigts->all(),"controlfechas"=>$controlfechas->all(),"usuarios"=>$usuarios->all(),'asigt'=>$progTem]);
        }else{
            return Redirect::to('home')->with('danger', 'Error, No tiene permisos para ingresar.');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProgTem  $progTem
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        #dd($request->guardar);
        #$progtem = ProgTem::find($id);
        $progtem = ProgTem::where('asigc_id',$id)->get()->first();
        $asigc_id = $progtem->asigc_id;
        $progtem->asigc_id = $progtem->asigc_id;
        $progtem->fecha_act_progc_temt = $request->fecha_act_progc_temt;
        if(isset($request->metodologia)) $progtem->metodologia = $request->metodologia;
        if(isset($request->crit_eva)) $progtem->crit_eva = $request->crit_eva;
        if (Auth::user()->rol=='admin' or (Auth::user()->rol=='director' and Funciones::permiso_asigc('asigc',$id))){
            $progtem->control_fecha_id = $request->control_fecha_id;
            if(isset($request->fecha_aprobacion)) $progtem->fecha_aprobacion = $request->fecha_aprobacion;
            if(isset($request->fecha_revision_doc)) $progtem->fecha_revision_doc = $request->fecha_revision_doc;
            if(isset($request->horas_practicas)) $progtem->horas_practicas = $request->horas_practicas;
            if(isset($request->horas_teoricas)) $progtem->horas_teoricas = $request->horas_teoricas;
            if(isset($request->horas_adicionales)) $progtem->horas_adicionales = $request->horas_adicionales;
        }

        $progtem->rev_direct = NULL;
        $progtem->firm_docnt = NULL;
        $progtem->firm_est = NULL;
        $result = $progtem->save();
        if ($request->guardar=="Guardar y mostrar"){
            if($result){
                return redirect()->route('admin.asignacion.programaciontematica.show',[$asigc_id])->with('success', 'La programación temática ha sido registrada correctamente.');
            }else{
                return  redirect()->route('admin.asignacion.programaciontematica.show',[$asigc_id])->with('danger', 'Error, La programación temática no ha sido registrada.');
            }
        }else if ($request->guardar=="Guardar y continuar editando"){
             if($result){
                return redirect(route('admin.asignacion.programaciontematica.edit', [$asigc_id]). '#metodologia')->with('success', 'La programación temática ha sido registrada correctamente.');
            }else{
               return redirect(route('admin.asignacion.programaciontematica.edit', [$asigc_id]). '#metodologia')->with('danger', 'Error, La programación temática no ha sido registrada.');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProgTem  $progTem
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProgTem $progTem)
    {
        //dd('destroy programacion temática');
    }
}
