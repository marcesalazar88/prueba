<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail; //Importante incluir la clase Mail, que será la encargada del envío
use App\Funciones;
use App\Periodo;
use App\Depto;
use App\User;
use App\Asigc;
use App\ProgTem;
use App\Seg;
use App\InfoFin;
use App\Http\Controllers\NotificacionesController as NotificacionesController;


class EmailController extends Controller
{//alerta1
    public $depto;
    public $director;
    public $periodo;
  
    public function mensaje1($asigc_id){
      $asigc = Asigc::find($asigc_id);
      $prog_temt = ProgTem::where('asigc_id',$asigc_id)->first();
      $info_final = InfoFin::where('id_asigt',$asigc_id)->first();
      $asigc->docente_fk();
      $user = User::findOrFail($asigc->docente_fk->id);
      $asigc->asigt();
      $asigc->prog();
      $asigc->prog->depto();
      $depto_id = $asigc->prog->depto->id;
      
      $nom_asigt = $asigc->asigt->nom_asigt;
      /*
      grupo
      observaciones
      echo $asigc->prog->nom_prog;
      echo $asigc->prog->depto->nom_depto;
      echo $depto_id;
      dd($asigc);
      */
      $texto="";
      $periodo = Funciones::periodo_activo();
      //$periodo = Periodo::where("periodo",$periodo_activo)->get()->first();
      //dd($periodo);
      $depto = Depto::find($depto_id);
      $director_id= $depto->director;
      if($director_id!=""){
      $director = User::where('ident_usu',$director_id)->get()->first();
        
      $this->depto = $depto;
      $this->director = $director;
      $this->periodo = $periodo;

      $subject = "Bienvenido a Ediweb!";
      $de_nombre = $depto->nom_depto;
      $de_correo = env('MAIL_FROM_ADDRESS');

      
        
      $view =  \View::make('layouts.partials.notifications.mensaje1', compact('depto','director','periodo','prog_temt','info_final','asigc'))->render();
        
      $notif= new NotificacionesController();
      $notif->enviar_notificacion($user->ident_usu, $view);

      Mail::send('layouts.partials.notifications.mensaje1', ['user' => $user, 'depto' => $depto, 'director' => $director, 'periodo' => $periodo, 'prog_temt' => $prog_temt, 'info_final' => $info_final, 'asigc' => $asigc], function ($m) use ($user, $de_nombre, $de_correo, $subject) {
                  $m->from($de_correo, $de_nombre);
                  $m->to($user->email, $user->name)->subject($subject);
              });
        
      }
       // return redirect()->back();
    }
  
  
    public function mensaje2(){
       $progtems = new ProgTem();
      $progtems = $progtems->all();
      //echo date("Y-m-d");
      #echo "<pre>";
      foreach ($progtems as $prog_tem){
        if ($prog_tem->fecha_aprobacion!=null){
        if ($prog_tem->fecha_aprobacion==date("Y-m-d")){//OJO estado
        $asigc_id = $prog_tem->asigc_id;
        $asigc = Asigc::find($asigc_id);
        $asigc->docente_fk();
        $user = User::find($asigc->docente_fk->id);
        $asigc->asigt();
        $nom_asigt = $asigc->asigt->nom_asigt;
        $periodo = $asigc->per_acad;
        $asigc->prog();
        $asigc->prog->depto();
        $depto_id = $asigc->prog->depto->id;
        $depto = Depto::find($depto_id);
        $nom_prog = $asigc->prog->nom_prog;
        $fecha_revision_doc = $prog_tem->fecha_revision_doc;
        
        $view =  \View::make('layouts.partials.notifications.mensaje2', compact('nom_asigt','periodo','nom_prog','user', 'depto', 'prog_tem', 'fecha_revision_doc'))->render();
        $notif= new NotificacionesController();
        $notif->enviar_notificacion($user->ident_usu, $view);
         
        $de_nombre = $depto->nom_depto;
        $de_correo = env('MAIL_FROM_ADDRESS');
      Mail::send('layouts.partials.notifications.mensaje2', ['nom_asigt' => $nom_asigt, 'periodo' => $periodo, 'nom_prog' => $nom_prog, 'user' => $user, 'depto' => $depto, 'prog_tem' => $prog_tem, 'fecha_revision_doc' => $fecha_revision_doc], function ($m) use ($user, $de_nombre, $de_correo) {
                  $m->from($de_correo, $de_nombre);
                  $m->to($user->email, $user->name)->subject('Información de plazo para diligenciar Programación Temática');
              });

        }
        }
      }//end foreach


    }
    public function mensaje3(){
      
        $seg = new Seg();
      $seg = $seg->all();
      //echo date("Y-m-d");
      #echo "<pre>";
      foreach ($seg as $seg_i){
        #dd();
        if ($seg_i->fecha_aprobacion!=null){
        if ($seg_i->fecha_aprobacion==date("Y-m-d")){
        $prog_tem = ProgTem::find($seg_i->prog_temt_id);
        $asigc_id = $prog_tem->asigc_id;
        $asigc = Asigc::find($asigc_id);
        $asigc->docente_fk();
        $user = User::find($asigc->docente_fk->id);
        $asigc->asigt();
        $nom_asigt = $asigc->asigt->nom_asigt;
        $periodo = $asigc->per_acad;
        $asigc->prog();
        $asigc->prog->depto();
        $depto_id = $asigc->prog->depto->id;
        $depto = Depto::find($depto_id);
        $nom_prog = $asigc->prog->nom_prog;
        $fecha_revision_doc = $prog_tem->fecha_revision_doc;
        $seg = $seg_i;
        $view =  \View::make('layouts.partials.notifications.mensaje3', compact('nom_asigt','periodo','nom_prog','user', 'depto', 'seg', 'fecha_revision_doc'))->render();
        $notif= new NotificacionesController();
        $notif->enviar_notificacion($user->ident_usu, $view);
         
        $de_nombre = $depto->nom_depto;
        $de_correo = env('MAIL_FROM_ADDRESS');
      Mail::send('layouts.partials.notifications.mensaje3', ['nom_asigt' => $nom_asigt, 'periodo' => $periodo, 'nom_prog' => $nom_prog, 'user' => $user, 'depto' => $depto, 'seg' => $seg, 'fecha_revision_doc' => $fecha_revision_doc], function ($m) use ($user, $de_nombre, $de_correo) {
                  $m->from($de_correo, $de_nombre);
                  $m->to($user->email, $user->name)->subject('Información de plazo para diligenciar Programación Temática');
              });

        }
        }
      }//end foreach

    }
    public function mensaje4(){
      /*
      user
depto
info_fin
      */
      $info_fins = new InfoFin();
      $info_fins = $info_fins->all();
      //echo date("Y-m-d");
      #echo "<pre>";
      foreach ($info_fins as $info_fin){
        if ($info_fin->fecha_aprobacion!=null){
        if ($info_fin->fecha_aprobacion==date("Y-m-d")){//OJO estado
        $asigc_id = $info_fin->id_asigt;
        $asigc = Asigc::find($asigc_id);
        $asigc->docente_fk();
        $user = User::find($asigc->docente_fk->id);
        $asigc->asigt();
        $nom_asigt = $asigc->asigt->nom_asigt;
        $periodo = $asigc->per_acad;
        $asigc->prog();
        $asigc->prog->depto();
        $depto_id = $asigc->prog->depto->id;
        $depto = Depto::find($depto_id);
        $nom_prog = $asigc->prog->nom_prog;
        $fecha_revision_doc = $info_fin->fecha_revision_doc;
        
        $view =  \View::make('layouts.partials.notifications.mensaje2', compact('nom_asigt','periodo','nom_prog','user', 'depto', 'info_fin', 'fecha_revision_doc'))->render();
        $notif= new NotificacionesController();
        $notif->enviar_notificacion($user->ident_usu, $view);
         
        $de_nombre = $depto->nom_depto;
        $de_correo = env('MAIL_FROM_ADDRESS');
      Mail::send('layouts.partials.notifications.mensaje2', ['nom_asigt' => $nom_asigt, 'periodo' => $periodo, 'nom_prog' => $nom_prog, 'user' => $user, 'depto' => $depto, 'info_fin' => $info_fin, 'fecha_revision_doc' => $fecha_revision_doc], function ($m) use ($user, $de_nombre, $de_correo) {
                  $m->from($de_correo, $de_nombre);
                  $m->to($user->email, $user->name)->subject('Información de plazo para diligenciar Programación Temática');
              });

        }
        }
      }//end foreach

        
      
    }
  public function mensaje5($asigc_id = 1){
      $asigc = Asigc::find($asigc_id);
      #$depto_id = 3015;
      $asigc->prog();
      $asigc->prog->depto();
      $depto_id = $asigc->prog->depto->id;
        
    $texto="";
      $periodo = Funciones::periodo_activo();
      //$periodo = Periodo::where("periodo",$periodo_activo)->get()->first();
      //dd($periodo);
      $depto = Depto::find($depto_id);
      $director_id= $depto->director;
      if($director_id!=""){
      $director = User::where('ident_usu',$director_id)->get()->first();
        
      $this->depto = $depto;
      $this->director = $director;
      $this->periodo = $periodo;

      $subject = "Asignatura Aprobada!";
      $de_nombre = $depto->nom_depto;
      $de_correo = env('MAIL_FROM_ADDRESS');
/*
$periodo
$asigt
$prog
*/
      #$user = User::findOrFail(79);
      $asigc->docente_fk();
        #dd($asigc);
      $user = User::find($asigc->docente_fk->id);
        
      $view =  \View::make('layouts.partials.notifications.mensaje5', compact('user', 'asigc','depto','director' , 'periodo'))->render();
      $notif= new NotificacionesController();
      $notif->enviar_notificacion($user->ident_usu, $view);

      Mail::send('layouts.partials.notifications.mensaje5', ['user' => $user, 'depto' => $depto, 'director' => $director, 'periodo' => $periodo, 'asigc' => $asigc], function ($m) use ($user, $subject, $de_nombre, $de_correo) {
                  $m->from($de_correo, $de_nombre);
                  $m->to($user->email, $user->name)->subject($subject);
              });
        
      }
       // return redirect()->back();
    }
  
    public function mensaje6($asigc_id){
      $asigc = Asigc::find($asigc_id);
      /*
      user
      depto
      seg
      estudiante
      */
      #$depto_id = 3015;
      $asigc->prog();
      $asigc->prog->depto();
      $depto_id = $asigc->prog->depto->id;
        
    $texto="";
     
      $depto = Depto::find($depto_id);
      $director_id= $depto->director;
      if($director_id!=""){
      $director = User::where('ident_usu',$director_id)->get()->first();
      $periodo = $asigc->per_acad;
      $subject = "Asignatura Aprobada!";
      $de_nombre = $depto->nom_depto;
      $de_correo = env('MAIL_FROM_ADDRESS');
/*
user
asigc
periodo
estudiante
*/
      #$user = User::findOrFail(79);
      $asigc->docente_fk();
        #dd($asigc);
      $user = User::find($asigc->docente_fk->id);
      $estudiante = User::find($asigc->estudiante_fk->id);
        
      $view =  \View::make('layouts.partials.notifications.mensaje6', compact('user', 'asigc','periodo','estudiante'))->render();
      $notif= new NotificacionesController();
      $notif->enviar_notificacion($user->ident_usu, $view);

      Mail::send('layouts.partials.notifications.mensaje6', ['user' => $user, 'periodo' => $periodo, 'estudiante' => $estudiante, 'asigc' => $asigc], function ($m) use ($user, $subject, $de_nombre, $de_correo) {
                  $m->from($de_correo, $de_nombre);
                  $m->to($user->email, $user->name)->subject($subject);
              });
        
      }
      
    }
    public function mensaje7(){
      
        $seg = new Seg();
      $seg = $seg->all();
      //echo date("Y-m-d");
      #echo "<pre>";
      foreach ($seg as $seg_i){
        #dd();
        if ($seg_i->fecha_revision_doc!=null){
        if ($seg_i->fecha_revision_doc==date("Y-m-d")){
        $prog_tem = ProgTem::find($seg_i->prog_temt_id);
        $asigc_id = $prog_tem->asigc_id;
        $asigc = Asigc::find($asigc_id);
        $asigc->estudiante_fk();
        $user = User::find($asigc->estudiante_fk->id);
        $asigc->asigt();
        $nom_asigt = $asigc->asigt->nom_asigt;
        $periodo = $asigc->per_acad;
        $asigc->prog();
        $asigc->prog->depto();
        $depto_id = $asigc->prog->depto->id;
        $depto = Depto::find($depto_id);
        $nom_prog = $asigc->prog->nom_prog;
        $fecha_revision_doc = $prog_tem->fecha_revision_doc;
        $seg = $seg_i;
        $view =  \View::make('layouts.partials.notifications.mensaje7', compact('nom_asigt','periodo','nom_prog','user', 'depto', 'seg', 'fecha_revision_doc'))->render();
        $notif= new NotificacionesController();
        $notif->enviar_notificacion($user->ident_usu, $view);
         
        $de_nombre = $depto->nom_depto;
        $de_correo = env('MAIL_FROM_ADDRESS');
          
      Mail::send('layouts.partials.notifications.mensaje7', ['nom_asigt' => $nom_asigt, 'periodo' => $periodo, 'nom_prog' => $nom_prog, 'user' => $user, 'depto' => $depto, 'seg' => $seg, 'fecha_revision_doc' => $fecha_revision_doc], function ($m) use ($user, $de_nombre, $de_correo) {
                  $m->from($de_correo, $de_nombre);
                  $m->to($user->email, $user->name)->subject('Información de plazo para diligenciar Programación Temática');
              });

        }
        }
      }//end foreach

    }
    public function alerta1(){
      /**
      Alerta cuando el docente aún no ha cumplido la programación temática
      y el plazo se vence hoy
      **/
      $progtem = new ProgTem();
      $progtem = $progtem->all();
      //echo date("Y-m-d");
      #echo "<pre>";
      foreach ($progtem as $progtem_i){
        if ($progtem_i->fecha_revision_doc!=null){
        if ($progtem_i->fecha_revision_doc==date("Y-m-d") and ($progtem_i->estado == "En Proceso" or $progtem_i->estado == "Pendiente")){
        $asigc_id = $progtem_i->asigc_id;
        $asigc = Asigc::find($asigc_id);
        $asigc->docente_fk();
        $user = User::findOrFail($asigc->docente_fk->id);
        $asigc->asigt();
        $asigc->prog();
        $asigc->prog->depto();
        $depto_id = $asigc->prog->depto->id;
        $depto = Depto::find($depto_id);
/*
        echo $asigc_id;
        echo " - ";
        echo $asigc->ident_docnt;
        echo " - ";
        echo $progtem_i->fecha_revision_doc;
        echo "<br>";
          */
        $view =  \View::make('layouts.partials.notifications.alerta1', compact('user', 'depto'))->render();
        $notif= new NotificacionesController();
        $notif->enviar_notificacion($user->ident_usu, $view);
        $de_nombre = $depto->nom_depto;
        $de_correo = env('MAIL_FROM_ADDRESS');
      Mail::send('layouts.partials.notifications.alerta1', ['user' => $user, 'depto' => $depto], function ($m) use ($user, $de_nombre, $de_correo) {
                  $m->from($de_correo, $de_nombre);
                  $m->to($user->email, $user->name)->subject('Último día para diligenciar Programación Temática');
              });
          
        }
        }
      }//end foreach
    }
   public function alerta2(){
     /*
            $user = User::find(1);
            $view = 'Notificación alerta2 de prueba: '.date("d/m/Y h:i:s a");
            $notif= new NotificacionesController();
            $notif->enviar_notificacion($user->ident_usu, $view);
            */
     
      /**
      Alerta cuando el docente aún no ha cumplido el seguimiento
      y el plazo se vence hoy
      **/
      $seg = new Seg();
      $seg = $seg->all();
      //echo date("Y-m-d");
      #echo "<pre>";
      foreach ($seg as $seg_i){
        #dd();
        if ($seg_i->fecha_revision_doc!=null){
        if ($seg_i->fecha_revision_doc==date("Y-m-d") and ($seg_i->estado == "En Proceso" or $seg_i->estado == "Pendiente")){
        $progtem_i = ProgTem::find($seg_i->prog_temt_id);
        $asigc_id = $progtem_i->asigc_id;
        $asigc = Asigc::find($asigc_id);
          
        $asigc->docente_fk();
        $user = User::findOrFail($asigc->docente_fk->id);
        $asigc->asigt();
        $asigc->prog();
        $asigc->prog->depto();
        $depto_id = $asigc->prog->depto->id;
        $depto = Depto::find($depto_id);
        /*
        echo $asigc_id;
        echo " - ";
        echo $asigc->ident_docnt;
        echo " - ";
        echo $progtem_i->fecha_revision_doc;
        echo "<br>";
        */
        $view =  \View::make('layouts.partials.notifications.alerta2', compact('user', 'depto'))->render();
        $notif= new NotificacionesController();
        $notif->enviar_notificacion($user->ident_usu, $view);
        $de_nombre = $depto->nom_depto;
        $de_correo = env('MAIL_FROM_ADDRESS');
      Mail::send('layouts.partials.notifications.alerta2', ['user' => $user, 'depto' => $depto], function ($m) use ($user, $de_nombre, $de_correo) {
                  $m->from($de_correo, $de_nombre);
                  $m->to($user->email, $user->name)->subject('Último día para diligenciar Seguimiento a la Programación Temática');
              });
          
        }
        }
      }
    }
   public function alerta3(){
     /*
                  $user = User::find(1);
            $view = 'Notificación alerta3 de prueba: '.date("d/m/Y h:i:s a");
            $notif= new NotificacionesController();
            $notif->enviar_notificacion($user->ident_usu, $view);
   */ 
     /*
            $user = User::find(1);
            $view = 'Notificación alerta2 de prueba: '.date("d/m/Y h:i:s a");
            $notif= new NotificacionesController();
            $notif->enviar_notificacion($user->ident_usu, $view);
            */
     
      /**
      Alerta para el docente cuando el estudiante aún no ha cumplido el seguimiento
      y el plazo se vence hoy
      **/
     $seg = new Seg();
      $seg = $seg->all();
      //echo date("Y-m-d");
      #echo "<pre>";
      foreach ($seg as $seg_i){
        #dd();
        if ($seg_i->fecha_revision_est!=null){
        if ($seg_i->fecha_revision_est==date("Y-m-d") and ($seg_i->estado_est == "En Proceso" or $seg_i->estado_est == "Pendiente")){
        $progtem_i = ProgTem::find($seg_i->prog_temt_id);
        $asigc_id = $progtem_i->asigc_id;
        $asigc = Asigc::find($asigc_id);
          
        $asigc->docente_fk();
        $user = User::findOrFail($asigc->docente_fk->id);
        $asigc->asigt();
        $asigc->prog();
        $asigc->prog->depto();
        $depto_id = $asigc->prog->depto->id;
        $depto = Depto::find($depto_id);
        /*
        echo $asigc_id;
        echo " - ";
        echo $asigc->ident_docnt;
        echo " - ";
        echo $progtem_i->fecha_revision_doc;
        echo "<br>";
        */
        $view =  \View::make('layouts.partials.notifications.alerta3', compact('user', 'depto'))->render();
        $notif= new NotificacionesController();
        $notif->enviar_notificacion($user->ident_usu, $view);
        $de_nombre = $depto->nom_depto;
        $de_correo = env('MAIL_FROM_ADDRESS');
      Mail::send('layouts.partials.notifications.alerta3', ['user' => $user, 'depto' => $depto], function ($m) use ($user, $de_nombre, $de_correo) {
                  $m->from($de_correo, $de_nombre);
                  $m->to($user->email, $user->name)->subject('Último día para diligenciar Seguimiento a la Programación Temática');
              });
          
        }
        }
      }
    
   }
   public function alerta4(){
      /*
       $user = User::find(1);
              $view = 'Notificación alerta4 de prueba: '.date("d/m/Y h:i:s a");
              $notif= new NotificacionesController();
              $notif->enviar_notificacion($user->ident_usu, $view);
      */
      /**
      Alerta para el estudiante cuando el estudiante aún no ha cumplido el seguimiento
      y el plazo se vence hoy
      **/
      $seg = new Seg();
      $seg = $seg->all();
      //echo date("Y-m-d");
      #echo "<pre>";
      foreach ($seg as $seg_i){
        #dd();
        if ($seg_i->fecha_revision_est!=null){
        if ($seg_i->fecha_revision_est==date("Y-m-d")  and ($seg_i->estado_est == "En Proceso" or $seg_i->estado_est == "Pendiente")){
        $progtem_i = ProgTem::find($seg_i->prog_temt_id);
        $asigc_id = $progtem_i->asigc_id;
        $asigc = Asigc::find($asigc_id);

        $asigc->estudiante_fk();
        $user = User::findOrFail($asigc->estudiante_fk->id);
        $asigc->asigt();
        $asigc->prog();
        $asigc->prog->depto();
        $depto_id = $asigc->prog->depto->id;
        $depto = Depto::find($depto_id);
        /*
        echo $asigc_id;
        echo " - ";
        echo $asigc->ident_docnt;
        echo " - ";
        echo $progtem_i->fecha_revision_doc;
        echo "<br>";
        */
        $view =  \View::make('layouts.partials.notifications.alerta4', compact('user', 'depto'))->render();
        $notif= new NotificacionesController();
        $notif->enviar_notificacion($user->ident_usu, $view);
        
      Mail::send('layouts.partials.notifications.alerta4', ['user' => $user, 'depto' => $depto], function ($m) use ($user, $de_nombre, $de_correo) {
                  $m->from($de_correo, $de_nombre);
                  $m->to($user->email, $user->name)->subject($subject);
              });
          
        }
        }
      }
    
   }
   public function alerta5(){
      /*
       $user = User::find(1);
              $view = 'Notificación alerta5 de prueba: '.date("d/m/Y h:i:s a");
              $notif= new NotificacionesController();
              $notif->enviar_notificacion($user->ident_usu, $view);
      */
      /**
      Alerta para el docente aún no ha cumplido el informe final
      y el plazo se vence hoy
      **/
      $infofin = new InfoFin();
      $infofin = $infofin->all();
      //echo date("Y-m-d");
      #echo "<pre>";
     
      foreach ($infofin as $infofin_i){
        if ($infofin_i->fecha_revision_doc!=null){
        if ($infofin_i->fecha_revision_doc==date("Y-m-d")  and ($infofin_i->estado == "En Proceso" or $infofin_i->estado == "Pendiente")){
        $asigc_id = $infofin_i->id_asigt;
        $asigc = Asigc::find($asigc_id);
        $asigc->docente_fk();
        $user = User::findOrFail($asigc->docente_fk->id);
        $asigc->asigt();
        $asigc->prog();
        $asigc->prog->depto();
        $depto_id = $asigc->prog->depto->id;
        $depto = Depto::find($depto_id);
        /*
        echo $asigc_id;
        echo " - ";
        echo $asigc->ident_docnt;
        echo " - ";
        echo $infofin_i->fecha_revision_doc;
        echo "<br>";
        */
        $view =  \View::make('layouts.partials.notifications.alerta5', compact('user', 'depto'))->render();
        $notif= new NotificacionesController();
        $notif->enviar_notificacion($user->ident_usu, $view);
        
      Mail::send('layouts.partials.notifications.alerta5', ['user' => $user, 'depto' => $depto], function ($m) use ($user, $de_nombre, $de_correo) {
                  $m->from($de_correo, $de_nombre);
                  $m->to($user->email, $user->name)->subject($subject);
              });
          
        }
        }
      }
    
   }
}