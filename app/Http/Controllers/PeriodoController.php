<?php

namespace App\Http\Controllers;

use App\Asigc;
use App\Periodo;
use App\Crud;
use App\Funciones;
use Illuminate\Http\Request;
use App\Http\Requests\PeriodoRequest;
use Illuminate\Support\Periodoades\Validator;
use Redirect;
class PeriodoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public $periodo;
     public $activo;
     public function __construct()
    {
        $this->middleware('auth');//auth|guest
        //$this->middleware('rol:admin');//admin|docente|estudiante
        //$this->activo();
    }
    public function index(Request $request)
    {
         $crud = new Crud();
         $this->Periodo = new Periodo();
         $resultado = $crud->buscar($request,$this->Periodo);
         foreach ($resultado as $i => $item){
             if ($resultado[$i]->estado == "1"){
             $resultado[$i]->estado = "Activo";
             }else if ($resultado[$i]->estado == "0"){
             $resultado[$i]->estado = "Inactivo";
             }
         }
         if (isset($_REQUEST['json'])){
         return $resultado;
         }else{
         return view('admin.periodo.index')->with(["resultado"=>$resultado]);
         }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $periodo_activo = Funciones::periodo_activo();
      //dd($periodo_activo);
        return view("admin.periodo.create")->with(["periodo_activo"=>$periodo_activo]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function reset()
    {
        $periodos = Periodo::where('estado','1')->get();
        $periodos->all();
        //dd($periodos);
            foreach ($periodos as $periodosi){
                $periodosi->estado = "0";
                $periodosi->save();
            }
    }
    
    public function activo()
    {
      $periodo_activo = Funciones::periodo_activo();

        $this->activo = $periodo_activo;
    }
    public function store(PeriodoRequest $request)
    {
        $periodo = new Periodo;
        $periodo->periodo = $request->periodo;
        $periodo->estado = $request->estado;
        $periodo->estado = $request->estado;
        if ($request->estado=="1") $this->reset();
        $result = $periodo->save();
        if($result)
        return Redirect::to('admin/periodo')->with('success', 'El Periodo ha sido registrada correctamente.');
        else
        return Redirect::to('admin/periodo')->with('danger', 'Error, El Periodo no ha sido registrada.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Periodo  $periodo
     * @return \Illuminate\Http\Response
     */
    public function show(Periodo $periodo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Periodo  $periodo
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $periodo_activo = Funciones::periodo_activo();
        $periodo = Periodo::find($id);
        return view("admin.periodo.editar")->with(["periodo"=>$periodo, "periodo_activo"=>$periodo_activo]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Periodo  $periodo
     * @return \Illuminate\Http\Response
     */
    public function update(PeriodoRequest $request, $id)
    {
      $periodo_activo = Funciones::periodo_activo();
      if ($periodo_activo->id != $id){
        
      $periodo = Periodo::find($id);
        //$periodo->periodo = $id;
        $periodo->estado = $request->estado;
        if ($request->estado=="1") $this->reset();
        $result = $periodo->save();
        if($result)
        return Redirect::to('admin/periodo')->with('success', 'El Periodo ha sido mdificada correctamente.');
        else
        return Redirect::to('admin/periodo')->with('danger', 'Error, El Periodo no ha sido mdificado.');
      }else{ 
        return Redirect::to('admin/periodo')->with('danger', 'Error, El Periodo no ha sido mdificado.');
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Periodo  $periodo
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
     $periodo_activo = Funciones::periodo_activo();
    if ($periodo_activo->id != $id){
    $periodo = Periodo::find($id);
        if ($periodo->estado !='1'){
            $asigc_activas = Asigc::where('per_acad',$id)->get();
            if (count($asigc_activas)==0){
            $periodo->delete();
            if ($periodo->exists === false){
            return Redirect::to('admin/periodo')->with('success', 'El Periodo ha sido eliminado correctamente.');
            }else{
            return Redirect::to('admin/periodo')->with('danger', 'Error, El Periodo no ha sido eliminado.');
            }
            }else{
            return Redirect::to('admin/periodo')->with('danger', 'Error, El Periodo no puede ser eliminado. Tiene '.count($asigc_activas).' asignaciones activas.');
            }
        }else{
            return Redirect::to('admin/periodo')->with('danger', 'Error, El Periodo activo no puede ser eliminado.');
        }
        }else{
            return Redirect::to('admin/periodo')->with('danger', 'Error, El Periodo activo no puede ser eliminado.');
        }
    }
}
