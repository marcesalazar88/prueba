<?php

namespace App\Http\Controllers;

use Auth;
use App\Periodo;
use App\Asigc;
use Illuminate\Http\Request;
use App\Http\Requests\AsigcRequest;
use App\Crud;
use App\Funciones;
use App\Prog;
use App\Asigt;
use App\Planes;
use App\PlanesAsigt;
use App\User;
use App\ProgTem;
use App\Seg;
use App\SegVal;
use App\InfoFin;
use Redirect;
use App\Http\Controllers\CodigosController;
use App\Http\Controllers\EmailController as EmailController;
class AsigcController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function __construct()
    {
        $this->middleware('auth');//auth|guest
        $this->middleware('rol:admin|director');//admin|docente|estudiante
    }

    public static function semaforo($id)
    {
            $resultado = Asigc::find($id);
            $resultado->prog();
            $resultado->asigt();
            $resultado->docente_fk();
            $resultado->nom_asigt = $resultado->asigt->nom_asigt;
            $resultado->nom_prog = $resultado->prog->nom_prog;
            $resultado->docente = $resultado->docente_fk->name." (".$resultado->ident_docnt.")";
            if ($resultado->estudiante != "" or $resultado->estudiante == null){
                $resultado->estudiante_fk();
                $resultado->estudiante = $resultado->estudiante_fk->name." (".$resultado->estudiante_fk->ident_usu.")";
            }else{
                $resultado->estudiante = '';
            }
            if ($resultado->estudiante == 'null'){
                $resultado->estudiante = '';
            }
            $codigo = isset($resultado->codigos()->first()->token) ? $resultado->codigos()->first()->token : '' ;
            $resultado->codigo = $codigo;
            $pt = $resultado->progtems();
            if ($resultado->progtems->first()){
            $resultado->progtem_estado = 1;
            $sgm =$resultado->progtems->first()->segs();
            if ($resultado->progtems->first()->segs()->first()){
                $resultado->seguimiento_estado = 1;
                $resultado->progtems->first()->segs()->first()->seg_vals();
                if ($resultado->progtems->first()->segs()->first()->seg_vals()->first() and $resultado->progtems->first()->segs()->first()->seg_vals()->first()->voto =="Pendiente"){
                    $resultado->seguimiento_estado = 2;
                }else if ($resultado->progtems->first()->segs()->first()->seg_vals()->first() and ($resultado->progtems->first()->segs()->first()->seg_vals()->first()->voto =="Aprueba" or $resultado->progtems->first()->segs()->first()->seg_vals()->first()->voto =="Rechaza")){
                    $resultado->seguimiento_estado = 1;
                }
                $resultado->infofinal_estado = 0;//pendiente
            }else{
                $resultado->seguimiento_estado = 0;
                $resultado->infofinal_estado = 0;
            }
            $resultado->horas_practicas = $resultado->progtems->first()->horas_practicas;
            $resultado->horas_teoricas = $resultado->progtems->first()->horas_teoricas;
            }else{
            $resultado->progtem_estado = 0;
            $resultado->seguimiento_estado = 0;
            $resultado->infofinal_estado = 0;
            $resultado->horas_practicas = 0;
            $resultado->horas_teoricas = 0;
            }
            $resultado->iconos = [
            0 => '<span class="color-danger glyphicon glyphicon-remove"></span>',
            1 => '<span class="color-success glyphicon glyphicon-ok"></span>',
            2 => '<span class="color-warning glyphicon glyphicon-warning-sign"></span>'
            ];
         return $resultado;
    }

    public function index(Request $request)
    {
      $periodos = new Periodo;
      $cod_progs=[];//VALIDAR CUANDO NO TENGA DEPARTAMEMTOS ASIGNADOS
      $resultados = 10;
      if (isset($_GET['resultados'])){
          $resultados = $_GET['resultados'];
      }
      if (Auth::user()->rol=="admin"){
                $progs= new Prog();
                $progs = $progs->all();
                foreach($progs as $progsi){
                    $cod_progs[]=$progsi->cod_prog;
                  }
      $progs = new Prog; $progs = $progs->all();
      $docentes = User::where('rol','docente')->get();
      $asignaturas = new Asigt; $asignaturas=$asignaturas->all();
      $estudiantes = User::where('rol','estudiante')->get();
        
      }else if (Auth::user()->rol=="director"){
        
                $id_user = Auth::user()->id;
                $user = User::find($id_user);
                $user->deptos();
                foreach($user->deptos as $deptoi){
                  $deptoi->progs();
                  foreach($deptoi->progs as $progsi){
                    $cod_progs[]=$progsi->cod_prog;
                  }
                }
        
      $asigct= Asigc::whereIn('cod_prog',$cod_progs)->get();
      $progs_asigc=[];
      $docentes_asigc=[];
      $asigts_asigc=[];
      $estudiantes_asigc=[];
      foreach($asigct as $asigcti){
        $asigts_asigc[]=$asigcti->cod_asigt;
        $progs_asigc[]=$asigcti->cod_prog;
        $docentes_asigc[]=$asigcti->ident_docnt;
        $estudiantes_asigc[]=$asigcti->estudiante;
      }
      $progs = Prog::whereIn('cod_prog',$progs_asigc)->get();
      //asignaciones en donde se encuentren estos docentesrelacionado con el departamento
      $docentes = User::where('rol','docente')
        ->whereIn('ident_usu',$docentes_asigc)
        ->get();
      $asignaturas = Asigt::whereIn('codigo_asigt',$asigts_asigc)->get();
      $estudiantes = User::where('rol','estudiante')
        ->whereIn('ident_usu',$estudiantes_asigc)
        ->get();
        
       }
        
      
         $crud = new Crud();
         $periodo_activo = Funciones::periodo_activo();
         $this->asigc = new Asigc();
         $_GET['paginate']=false;
         $resultado1 = $crud->buscar($request,$this->asigc);
         $resultado_id=[];
         foreach ($resultado1 as $id => $resultado1_i){
          $resultado_id[]=$resultado1[$id]->id;
         }
         $resultado= Asigc::whereIn('cod_prog',$cod_progs)
           ->whereIn('id',$resultado_id)
           ->paginate($resultados);
      
         foreach ($resultado as $id => $resultado_i){
            $resultado[$id]->prog();
            $resultado[$id]->asigt();
            $resultado[$id]->docente_fk();
            $resultado[$id]->nom_asigt = $resultado[$id]->asigt->nom_asigt." (".$resultado[$id]->cod_asigt.")";
            if ($resultado[$id]->flex=="SI"){
            $resultado[$id]->nom_asigt .= '<br><span class="badge" style="background-color: rgb(25, 108, 75);">Flexibilidad</span>';
            }
            $resultado[$id]->nom_prog = $resultado[$id]->prog->nom_prog;
            #$resultado[$id]->docente = $resultado[$id]->docente_fk->name." (".$resultado[$id]->ident_docnt.")";
            $resultado[$id]->docente = $resultado[$id]->docente_fk->name;
            if ($resultado[$id]->estudiante!=""){
            $resultado[$id]->estudiante_fk();
            #$resultado[$id]->estudiante = $resultado[$id]->estudiante_fk->name." (".$resultado[$id]->estudiante_fk->ident_usu.")";
            $resultado[$id]->estudiante = $resultado[$id]->estudiante_fk->name;
            }
            $codigo = isset($resultado[$id]->codigos()->first()->token) ? $resultado[$id]->codigos()->first()->token : '' ;
            $resultado[$id]->codigo = $codigo;
         }
         
         $json = $request->get('json');
         if ($json){
         return $resultado;
         }else{
         return view('admin.asignacion.index')->with(["resultado"=>$resultado,'progs'=>$progs, 'periodos'=>$periodos->all(), 'estudiantes'=>$estudiantes->all(), 'docentes'=>$docentes->all(), 'asignaturas'=>$asignaturas, 'periodo_activo'=>$periodo_activo]);
         }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create_individual()
    {
       $progs = new Prog();
        $asigts = new Asigt();
        $periodos = new Periodo();
        $usuarios = new User();
        return view('admin.asignacion.create_individual')->with(["progs"=>$progs->all(),"asigts"=>$asigts->all(),"usuarios"=>$usuarios->all(),"periodos"=>$periodos->all()]);
    }
    public function consultar_planes_prog(Request $request)
    {
      $resultado = Planes::where('cod_prog',$request->cod_prog)->get();
      $salida = array();
      foreach ($resultado as $id => $resultado_i){
        $salida[$resultado_i->id] = $resultado_i->nombre;
      }
      echo json_encode($salida);
    }
    public function create_plan(Request $request)
    {
         $id = $request->plan;
         $periodo_activo = Funciones::periodo_activo();
         $plan = Planes::find($id);
         if (isset($plan->id)){
      $plan->prog();
            $plan->prog->planes();
            $plan->prog->depto();
            $plan->prog->depto->fac();
            $fac = $plan->prog->depto->fac;
            $plan->prog->asigcs();
            $plan_asigts = PlanesAsigt::where('planes_id',$id)->orderBy('semestre', 'ASC')->get();
            $plan->planes_asigts = $plan_asigts;
         foreach ($plan->planes_asigts as $id => $asigc){
           $plan->planes_asigts[$id]->asigt();
           $plan->planes_asigts[$id]->prerequisitos();
         }

        $usuarios = new User();
            $asigts = new Asigt();
            return view('admin.asignacion.create_plan')->with(['planes'=>$plan,'periodo_activo'=>$periodo_activo,'asigts'=>$asigts->all(),'usuarios'=>$usuarios->all()]);
        }else{
        return redirect()->back()->with('warning', 'Por favor, Seleccione un plan.');
         }
    }

    public function create(Request $request)
    {
        #echo "Hola planes - mostrar el listado de planes y crear modulo";//hacer tabla
        #$planes = new Planes();
        #dd($planes->all());
         $crud = new Crud();
         $prog = new Prog();
         $prog = $prog->all();/*
         $this->planes = new Planes();
         $resultado = $crud->buscar($request,$this->planes,['cod_prog'=>'Programa']);
      */
      if(isset($request->valor_clave)){
      $resultado = Planes::where('cod_prog',$request->valor_clave)->pluck('nombre', 'id');
      }else{
      $resultado = Planes::pluck('nombre', 'id');
      }/*
      foreach ($resultado as $id => $resultado_i){
        $resultado[$id]->prog();
        $resultado[$id]->nom_prog = $resultado[$id]->prog->nom_prog;
      }
      */
     //dd($resultado);
      $botones = false;    
        $progs = new Prog();
        $asigts = new Asigt();
        $periodos = new Periodo();
        $usuarios = new User();
      if (isset($_REQUEST['json'])){
         return $resultado;
         }else{
        return view('admin.asignacion.create')->with(["resultado"=>$resultado,"botones"=>$botones,"prog"=>$progs->all(),"asigts"=>$asigts->all(),"usuarios"=>$usuarios->all(),"periodos"=>$periodos->all()]);
    }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AsigcRequest $request)
    {
    //dd($request->all());
    $asigc = new Asigc;
    $asigc->ident_docnt = $request->ident_docnt;
    $asigc->estudiante = isset($request->estudiante) ? $request->estudiante : null;
    $asigc->cod_asigt = $request->cod_asigt;
    $asigc->per_acad = $request->per_acad;
    $asigc->cod_prog = $request->cod_prog;
    $asigc->flex = $request->flex;
    $asigc->grupo = $request->grupo;
    $asigc->observaciones = $request->observaciones;
    $result = $asigc->save();
    $cod = new CodigosController();
    $cod->create($asigc->id);
    if($result){
        $this->crear_registros($asigc);
      //use Notifiable;
        if(isset($request->notificar) and $request->notificar=="SI"){ 
          $email = new EmailController();
          $email->mensaje5($asigc->id);
        }
        if ($request->redirect=='back'){
        return redirect()->back()->with('success', 'La asignación ha sido registrada correctamente.');
        }else{
        return redirect()->route('admin.asignacion.index')->with('success', 'La asignación ha sido registrada correctamente.');
        }
    }else{
        if ($request->redirect=='back'){
        return redirect()->back()->with('danger', 'Error, La asignación no ha sido registrada.');
        }else{
        return  redirect()->route('admin.asignacion.index')->with('danger', 'Error, La asignación no ha sido registrada.');
        }
    }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Asigc  $asigc
     * @return \Illuminate\Http\Response
     */
    public function crear_registros($asigc)
    {

            $progtems = $asigc->progtems();
            $progtem = $asigc->progtems()->first();
            if($progtem){
                
            }else{
            $progtem = new ProgTem;
            $progtem->asigc_id = $asigc->id;
            $progtem->fecha_act_progc_temt = date("Y-m-d");
            $progtem->metodologia = '';
            $progtem->crit_eva = '';
            $progtem->horas_practicas = NULL;
            $progtem->horas_teoricas = NULL;
            $progtem->horas_adicionales = NULL;
            $progtem->rev_direct = NULL;
            $progtem->firm_docnt = $asigc->ident_docnt;
            $progtem->firm_est = NULL;
            //$progtem->control_fecha_id = NULL;
            $progtem->fecha_aprobacion = NULL;
            $progtem->fecha_revision_doc = NULL;
            //$progtem->fecha_revision_est = NULL;
              $progtem->codigo = uniqid("PT");             
            $result2 = $progtem->save();
            }
            $progtem->segs();
            $seg=$progtem->segs->first();
            if($seg){

            }else{
                $seg = new Seg;
                $seg->prog_temt_id = $progtem->id;
                $seg->observ_est = "";
                $seg->observ_docnt = "";
                $seg->estudiante = NULL;
                $seg->fecha_aprobacion = NULL;
                $seg->fecha_revision_doc = NULL;
                $seg->fecha_revision_est = NULL;
                $seg->docente = $asigc->ident_docnt;
                $seg->codigo = uniqid("SE");
                $seg->save();
            }
            $seg->seg_vals();
            $segVal=$seg->seg_vals->first();
            if($segVal){
                
            }else{
                    $segVal = new SegVal();
                    $segVal->seg_id = $seg->id;
                    $segVal->aspecto = "PA";
                    $segVal->tipo = "Fortaleza";
                    $segVal->valoracion = '0';
                    $segVal->save();
            }
            $infofin = InfoFin::where('id_asigt',$asigc->id)->get()->first();
            if(!$infofin){
                    $infofin = new InfoFin();
                    $infofin->id_asigt =  $asigc->id;
                    /*
                    if (isset($seg->id)){
                    $infofin->seg_id =  $seg->id;
                    }*/
                    $infofin->codigo = uniqid("IF");
                    $infofin->save();
            }
            //
            //
    }
    public function show($id)
    {
        $asigc = Asigc::find($id);
    if (isset($asigc->ident_docnt)){
            $asigc->asigt();
            $asigc->asigt->contasigts();
            $asigc->docente_fk();
            $asigc->estudiante_fk();
            $asigc->prog();
            $asigc->prog->depto();
            $asigc->prog->depto->fac();
        return view('admin.asignacion.show')->with(['asigc'=>$asigc]);
    }else{
        return Redirect::to('admin/asignacion')->with('danger', 'No existen registros de esta asignatura.');
    }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Asigc  $asigc
     * @return \Illuminate\Http\Response
     */
    public function edit(Asigc $asigc, $id)
    {
        $asigc = Asigc::find($id);
        $progs = new Prog();
        $asigts = new Asigt();
        $periodos = new Periodo();
        $usuarios = new User();
        return view('admin.asignacion.editar')->with(["asigc"=>$asigc,"progs"=>$progs->all(),"asigts"=>$asigts->all(),"periodos"=>$periodos->all(),"usuarios"=>$usuarios->all()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Asigc  $asigc
     * @return \Illuminate\Http\Response
     */
    public function update(AsigcRequest $request, $id)
    {
        $asigc = Asigc::find($id);
        $asigc->ident_docnt = $request->ident_docnt;
        $asigc->estudiante = isset($request->estudiante) ? $request->estudiante : $asigc->estudiante;
        $asigc->cod_asigt = $request->cod_asigt;
        $asigc->per_acad = $request->per_acad;
        $asigc->cod_prog = $request->cod_prog;
        $asigc->flex = $request->flex;
        $asigc->grupo = $request->grupo;
        $asigc->observaciones = $request->observaciones;
        #dd($asigc);
        $result = $asigc->save();
        if($result){
        if(isset($request->notificar) and $request->notificar=="SI"){ 
          $email = new EmailController();
          $email->mensaje5($asigc->id);
        }
          return redirect()->route('admin.asignacion.index')->with('success', 'La asignación ha sido registrada correctamente.');
        }else{
            return  redirect()->route('admin.asignacion.index')->with('danger', 'Error, La asignación no ha sido registrada.');
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Asigc  $asigc
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $asigc = Asigc::find($id);
        if(!empty($asigc)){
        $asigc->delete();
        if ($asigc->exists === false)
        return redirect()->back()->with('notice', 'La asignación ha sido eliminada correctamente.');
        else
        return redirect()->back()->with('notice', 'Error, La asignación no ha sido eliminada.');
        }else{
        return redirect()->back()->with('notice', 'Error, La asignación no ha sido eliminada.');
        }
    }
}
