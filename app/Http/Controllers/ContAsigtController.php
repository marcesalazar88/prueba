<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Crud;
use App\Asigt;
use App\ContAsigt;
use App\User;
use App\Prog;
use App\Depto;
use App\Fac;
use App\Http\Requests\ContAsigtRequest;
use Redirect;
class ContAsigtController extends Controller
{

public function __construct()
{
    $this->middleware('auth');//auth|guest
    $this->middleware('rol:admin|director');//admin|director|docente|estudiante
}

public function index(Request $request){
     $crud = new Crud();
     $this->asigt = new Asigt();
     $resultado = $crud->buscar($request,$this->asigt);
     $json = $request->get('json');
     if ($json){
     return $resultado;
     }else{
     return view('admin.asignatura.index')->with(["resultado"=>$resultado]);
     }
}
public function create($asigt_id){
     $asigt = Asigt::find($asigt_id);
     return view('admin.asignatura.contenido.create')->with(["asigt"=>$asigt]);
}
public function show($id){
    $asigt = Asigt::find($id);
    if (isset($asigt->codigo_asigt)){
        $asigt->contasigts();
        $asigt->asigcs();
        foreach ($asigt->asigcs as $id => $asigc){
            $asigt->asigcs[$id]->docente_fk();
            $asigt->asigcs[$id]->estudiante_fk();
            $asigt->asigcs[$id]->prog();
            $asigt->asigcs[$id]->prog->depto();
            $asigt->asigcs[$id]->prog->depto->fac();
        }
        return view('admin.asignatura.show')->with(['asigt'=>$asigt]);
    }else{
        return Redirect::to('admin/asignatura')->with('danger', 'No existen registros de esta asignatura.');
    }
}
public function destroy($id){
    $contasig = ContAsigt::find($id);
  if (!empty($contasig)){
    $contasig->asigt();
    #dd($contasig->asigt->id);
    $contasig->delete();
    if ($contasig->exists === false)
    return redirect()->route('admin.asignatura.show', [$contasig->asigt->id])->with('notice', 'El contenido de asignatura ha sido eliminado correctamente.');
    else
    return redirect()->route('admin.asignatura.show', [$contasig->asigt->id])->with('danger', 'Error, El contenido de asignatura no ha sido eliminado.');
  }else{
    return redirect()->back()->with('danger', 'Error, El contenido de asignatura no ha sido eliminado.');
  }
}
public function edit($id){
    $contasig = ContAsigt::find($id);
    $contasig->asigt();
    $tipo_identificacion = array();
    return view('admin.asignatura.contenido.editar')->with(["contenido"=>$contasig]);
}
public function store(ContAsigtRequest $request){
    //dd($request->all());
    $contasig = new ContAsigt();
    $contasig->cod_asigt = $request->cod_asigt;
    $contasig->hras_cont_asigt = $request->hras_cont_asigt;
    $contasig->tem_cap_cont_asigt = $request->tem_cap_cont_asigt;
    $result = $contasig->save();
    if($result)
    return redirect()->route('admin.asignatura.show', [$request->asigt_id])->with('success', 'El Contenido de la Asignatura ha sido registrado correctamente.');
    else
    return redirect()->route('admin.asignatura.show', [$request->asigt_id])->with('danger', 'Error, El Contenido de la Asignatura no ha sido registrado.');
}
public function update(ContAsigtRequest $request){
    $contasig = ContAsigt::find($request->id);
    $contasig->asigt();
    $contasig->cod_asigt = $request->cod_asigt;
    $contasig->hras_cont_asigt = $request->hras_cont_asigt;
    $contasig->tem_cap_cont_asigt = $request->tem_cap_cont_asigt;
    $result = $contasig->save();
    if($result)
    return redirect()->route('admin.asignatura.show',  [$contasig->asigt])->with('success', 'El Contenido de la Asignatura ha sido modificado correctamente.');
    else
    return redirect()->route('admin.asignatura.show',  [$contasig->asigt])->with('danger', 'Error, El Contenido de la Asignatura no ha sido modificado.');

}

}