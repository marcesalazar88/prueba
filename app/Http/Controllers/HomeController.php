<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Fac;
use App\Prog;
use App\Asigc;
use App\ProgTem;
use App\Seg;
use App\SegVal;
use App\InfoFin;
use App\Asigt;
use App\Links;
use App\Periodo;
use App\Funciones;
use App\User;
use App\ControlFechas;
use App\Fecha;
use App\PregEnc;
use App\RespEnc;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\URL;
use Auth;
use Redirect;
/**/
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        URL::forceScheme('https');
        $this->middleware('auth');
    }
    public function estado_doc(Request $request, $id, $asigc_id)
    {
    /**
    * formato : prog_tem|seg|info_fin
    * campo : estado|estado_est
    * valor : Pendiente|En Proceso|Listo
    **/
 //validar si esta en las fechas
      
      if($request->valor=="Pendiente" or $request->valor=="En Proceso" or $request->valor=="Listo"){
          if($request->campo=="estado" or $request->campo=="estado_est"){
              if($request->formato=="prog_tem"){
                $prog_tem = ProgTem::find($id);
                   if($request->campo=="estado")
                $prog_tem->estado = $request->valor;
                    if($prog_tem->save()){
                  return "1";
                  }else{
                    return "0";
                  }
                
              }elseif($request->formato=="seg"){
                $seg = Seg::find($id);
                   if($request->campo=="estado")
                $seg->estado = $request->valor;
                   if($request->campo=="estado_est")
                $seg->estado_est = $request->valor;
                    if($seg->save()){
                  return "1";
                  }else{
                    return "0";
                  }
                
              }elseif($request->formato=="info_fin"){
                $info_fin = InfoFin::find($id);
                   if($request->campo=="estado")
                    $info_fin->estado = $request->valor;
                    if($info_fin->save()){
                  return "1";
                  }else{
                    return "0";
                  }
                
              }else{
                return "0";
              }
          }else{
            return "0";
          }
      }else{
        return "0";
      }
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function estados_reporte(&$estados,&$asigc,&$asigt_i_des)
    {
      $progtem_estado0=0;
      $progtem_estado1=0;
      $progtem_estado2=0;
      $seg_estado0=0;
      $seg_estado2=0;
      $seg_estado1=0;
      $infofin_estado0=0;
      $infofin_estado1=0;
      $infofin_estado2=0;
    
foreach ($asigc as $asigt_i){
  if ($asigt_i_des[$asigt_i->id]->progtem_estado==0){

  }
  if ($asigt_i_des[$asigt_i->id]->seguimiento_estado==0){

  }
  if ($asigt_i_des[$asigt_i->id]->seguimiento_estado==0){

  }
  if ($asigt_i_des[$asigt_i->id]->progtem_estad==1){

  }
  if ($asigt_i_des[$asigt_i->id]->seguimiento_estad==1){

  }
  if ($asigt_i_des[$asigt_i->id]->seguimiento_estad==1){

  }
  if ($asigt_i_des[$asigt_i->id]->progtem_estado==2){

  }
  if ($asigt_i_des[$asigt_i->id]->seguimiento_estado==2){

  }
  if ($asigt_i_des[$asigt_i->id]->seguimiento_estado==2){

  }
}
        foreach ($asigc as $asigt_i){
          
if ($asigt_i_des[$asigt_i->id]->progtem_estado==0){
  if (isset($progtem_estado0))
  $progtem_estado0++;
  else
  $progtem_estado0=1;
}
if ($asigt_i_des[$asigt_i->id]->progtem_estado==1){
  if (isset($progtem_estado1))
  $progtem_estado1++;
  else
  $progtem_estado1=1;
}
if ($asigt_i_des[$asigt_i->id]->progtem_estado==2){
  if (isset($progtem_estado2))
  $progtem_estado2++;
  else
  $progtem_estado2=1;
}
//$seg_estado
if ($asigt_i_des[$asigt_i->id]->seguimiento_estado==0){
  if (isset($seg_estado0))
  $seg_estado0++;
  else
  $seg_estado0=1;
}
if ($asigt_i_des[$asigt_i->id]->seguimiento_estado==1){
  if (isset($seg_estado1))
  $seg_estado1++;
  else
  $seg_estado1=1;
}
if ($asigt_i_des[$asigt_i->id]->seguimiento_estado==2){
  if (isset($seg_estado2))
  $seg_estado2++;
  else
  $seg_estado2=1;
}
// $infofin_estado
if ($asigt_i_des[$asigt_i->id]->infofinal_estado==0){
  if (isset($infofin_estado0))
  $infofin_estado0++;
  else
  $infofin_estado0=1;
}
if ($asigt_i_des[$asigt_i->id]->infofinal_estado==1){
  if (isset($infofin_estado1))
  $infofin_estado1++;
  else
  $infofin_estado1=1;
}
if ($asigt_i_des[$asigt_i->id]->infofinal_estado==2){
  if (isset($infofin_estado2))
  $infofin_estado2++;
  else
  $infofin_estado2=1;
}
        }

$estados = ['progtem_estado0' => $progtem_estado0,
'progtem_estado1' => $progtem_estado1,
'progtem_estado2' => $progtem_estado2,
'seg_estado0' => $seg_estado0,
'seg_estado1' => $seg_estado1,
'seg_estado2' => $seg_estado2,
'infofin_estado0'=>$infofin_estado0,
'infofin_estado1'=>$infofin_estado1,
'infofin_estado2'=>$infofin_estado2];
    }
    
    public function detalle_reporte($asigc_id)
    {
      $resultado = Asigc::find($asigc_id);
          $resultado->fecha_aprobacion = "";
          $resultado->fecha_revision_doc = "";
          $resultado->fecha_revision_est = "";
      $fecha = date("Y-m-d");
      $permiso_fecha_pt=false; 
      $permiso_fecha_se=false; 
      $permiso_fecha_if=false; 
      $permiso_fecha_est_se=false;
      $fecha_revision_doc_if=0;
      $fecha_revision_doc_se=0;
      $fecha_revision_est_se=0;
      $fecha_revision_doc_pt=0;  
      
      $fecha_aprobacon_pt=0; 
      $fecha_aprobacon_se=0; 
      $fecha_aprobacon_if=0;         
            $resultado->prog();
            $resultado->asigt();
            $resultado->docente_fk();
            $resultado->nom_asigt = $resultado->asigt->nom_asigt;
            #dd($resultado->cod_prog);
            #dd($resultado->prog->nom_prog);
            $resultado->nom_prog = $resultado->prog->nom_prog;
            //$resultado->docente = $resultado->docente_fk->name." (".$resultado->ident_docnt.")";
            $resultado->docente = $resultado->docente_fk->name;
            if ($resultado->estudiante!=""){
            $resultado->estudiante_fk();
                if (isset($resultado->estudiante_fk->name)){
                //$resultado->estudiante = $resultado->estudiante_fk->name." (".$resultado->estudiante_fk->ident_usu.")";
                $resultado->estudiante = $resultado->estudiante_fk->name;
                }else{
                $resultado->estudiante = "";
                }
            }
            $codigo = isset($resultado->codigos()->first()->token) ? $resultado->codigos()->first()->token : '' ;
            $resultado->codigo = $codigo;
      //validar estados
            $resultado->estado_est="Pendiente";
            $pt = $resultado->progtems();
            if ($resultado->progtems->first()){    
                
                if($resultado->progtems->first()->estado=="Pendiente"){
                  $resultado->progtem_estado = 0;
                }
                if($resultado->progtems->first()->estado=="En Proceso"){
                  $resultado->progtem_estado = 2;
                }
                if($resultado->progtems->first()->estado=="Listo"){
                  $resultado->progtem_estado = 1;
                }
            $resultado->fecha_aprobacion = $resultado->progtems->first()->fecha_aprobacion;
            $resultado->fecha_revision_doc = $resultado->progtems->first()->fecha_revision_doc;
            

$fecha_aprobacon_pt = $resultado->progtems->first()->fecha_aprobacion;
$fecha_revision_doc_pt = $resultado->progtems->first()->fecha_revision_doc;
if($fecha_aprobacon_pt !="" and $fecha_revision_doc_pt !="" and (strtotime($fecha_aprobacon_pt) <= strtotime($fecha) and strtotime($fecha) <= strtotime($fecha_revision_doc_pt))){
$permiso_fecha_pt = true;
}//endif; 
              
            $sgm =$resultado->progtems->first()->segs();
            if ($resultado->progtems->first()->segs()->first()){
              /* revisar seg validacion*/
                $resultado->seguimiento_estado = 0;
                $resultado->progtems->first()->segs()->first()->seg_vals();
                $resultado->estado_est=$resultado->progtems->first()->segs()->first()->estado_est;
                $resultado->estado_seg_doc=$resultado->progtems->first()->segs()->first()->estado;
                //dd(count($resultado->progtems->first()->segs()->first()->seg_vals())>1);
$fecha_aprobacon_se = $resultado->progtems->first()->segs->first()->fecha_aprobacion;
$fecha_revision_doc_se = $resultado->progtems->first()->segs->first()->fecha_revision_doc;
$fecha_revision_est_se = $resultado->progtems->first()->segs->first()->fecha_revision_est;
        /*
    
      #print(;
      dd($fecha_aprobacon_se !="" and $fecha_revision_doc_se !="" and (strtotime($fecha_aprobacon_se) <= strtotime($fecha) and strtotime($fecha) <= strtotime($fecha_revision_doc_se)));
      #dd($fecha_aprobacon_se." menor que ".$fecha." menor que ".$fecha_revision_doc_se);
           */   
if(Auth::user()->rol=='docente'){
    if($fecha_aprobacon_se !="" and $fecha_revision_doc_se !="" and (strtotime($fecha_aprobacon_se) <= strtotime($fecha) and strtotime($fecha) <= strtotime($fecha_revision_doc_se))){ 
  
          $permiso_fecha_se = true;
    }//endif; 
}else if(Auth::user()->rol=='estudiante'){
    if($fecha_aprobacon_se !="" and $fecha_revision_doc_se !="" and (strtotime($fecha_revision_doc_se) <= strtotime($fecha) and strtotime($fecha) <= strtotime($fecha_revision_est_se) )){ 
          $permiso_fecha_se = true;
    }//endif;
}

               /* Inicio Valida si el estudiante ya voto*/
              $resultado->hay_votos_est = false;
                  #$a = $resultado->progtems->first()->segs()->first()->seg_vals();
                  $seg_id = $resultado->progtems->first()->segs()->first()->id;
                  $segvals = SegVal::where('seg_id',$seg_id)->get();
                  if(count($segvals)>1 and $segvals->get(1)->voto == "Pendiente"){
                    $resultado->hay_votos_est = false;
                  }else{
                     if(count($segvals)>1 and ($segvals->get(1)->voto == "Aprueba" or $segvals->get(1)->voto == "Rechaza")){
                        $resultado->hay_votos_est = true;
                      }else{
                        $resultado->hay_votos_est = false;
                      }
                  }
                     /* Fin Valida si el estudiante ya voto*/
                
              /*
                if (count($resultado->progtems->first()->segs()->first()->seg_vals())>1 and $resultado->progtems->first()->segs()->first()->seg_vals()->latest()->first()->voto =="Pendiente"){
                    $resultado->hay_votos_est = "NO";
                }else if (count($resultado->progtems->first()->segs()->first()->seg_vals())>1 and ($resultado->progtems->first()->segs()->first()->seg_vals()->latest()->first()->voto =="Aprueba" or $resultado->progtems->first()->segs()->first()->seg_vals()->latest()->first()->voto =="Rechaza")){
                    $resultado->hay_votos_est = "SI";
                }
              */
              /* revisar seg validacion*/
              /*
              $resultado->fecha_revision_doc = $resultado->progtems->first()->segs()->first()->fecha_revision_doc;
              $resultado->progtems->first()->segs()->first()->fecha_revision_est;
              */
              if($resultado->progtems->first()->segs()->first()->estado=="Listo" and $resultado->progtems->first()->segs()->first()->estado_est=="Listo"){
                  $resultado->seguimiento_estado = 1;
                }
              if($resultado->progtems->first()->segs()->first()->estado=="Listo" and $resultado->progtems->first()->segs()->first()->estado_est=="En Proceso"){
                  $resultado->seguimiento_estado = 2;
                }
              if($resultado->progtems->first()->segs()->first()->estado=="Listo" and $resultado->progtems->first()->segs()->first()->estado_est=="Pendiente"){
                  $resultado->seguimiento_estado = 2;
                }
              
              if($resultado->progtems->first()->segs()->first()->estado=="Pendiente" and $resultado->progtems->first()->segs()->first()->estado_est=="Listo"){
                  $resultado->seguimiento_estado = 0;
                }
              if($resultado->progtems->first()->segs()->first()->estado=="Pendiente" and $resultado->progtems->first()->segs()->first()->estado_est=="En Proceso"){
                  $resultado->seguimiento_estado = 0;
                }
              if($resultado->progtems->first()->segs()->first()->estado=="Pendiente" and $resultado->progtems->first()->segs()->first()->estado_est=="Pendiente"){
                  $resultado->seguimiento_estado = 0;
                }
              
              if($resultado->progtems->first()->segs()->first()->estado=="En Proceso" and $resultado->progtems->first()->segs()->first()->estado_est=="Listo"){
                  $resultado->seguimiento_estado = 2;
                }
              if($resultado->progtems->first()->segs()->first()->estado=="En Proceso" and $resultado->progtems->first()->segs()->first()->estado_est=="En Proceso"){
                  $resultado->seguimiento_estado = 2;
                }
              if($resultado->progtems->first()->segs()->first()->estado=="En Proceso" and $resultado->progtems->first()->segs()->first()->estado_est=="Pendiente"){
                  $resultado->seguimiento_estado = 2;
                }
                if($resultado->seguimiento_estado != 1 and $resultado->progtem_estado == 1){ 
                      $resultado->fecha_aprobacion = $resultado->progtems->first()->segs()->first()->fecha_aprobacion;
                      $resultado->fecha_revision_doc = $resultado->progtems->first()->segs()->first()->fecha_revision_doc;
                      $resultado->fecha_revision_est = $resultado->progtems->first()->segs()->first()->fecha_revision_est;
                 }         
              
                
                $infofins = $resultado->infofins();
                $fecha_aprobacon_if = $resultado->infofins->first()->fecha_aprobacion;
                $fecha_revision_doc_if = $resultado->infofins->first()->fecha_revision_doc;
                if($fecha_aprobacon_if !="" and $fecha_revision_doc_if !="" and (strtotime($fecha_aprobacon_if) <= strtotime($fecha) and strtotime($fecha) <= strtotime($fecha_revision_doc_if))){ 
                $permiso_fecha_if = true;
                }//endif;   
                if ($resultado->infofins->first()){
                  if($resultado->infofins->first()->estado=="Pendiente"){
                     $resultado->infofinal_estado = 0;
                  }
                  if($resultado->infofins->first()->estado=="En Proceso"){
                     $resultado->infofinal_estado = 2;
                  }
                  if($resultado->infofins->first()->estado=="Listo"){
                     $resultado->infofinal_estado = 1;
                  }else{
                    if($resultado->seguimiento_estado == 1 and $resultado->progtem_estado == 1){
                      $resultado->fecha_aprobacion = $resultado->infofins->first()->fecha_aprobacion;
                      $resultado->fecha_revision_doc = $resultado->infofins->first()->fecha_revision_doc;
                    }
                  }
                  
                }else{
                    $resultado->infofinal_estado = 0;//pendiente
                }
                            
            }else{
                $resultado->seguimiento_estado = 0;
                $resultado->infofinal_estado = 0;
            }
             
            if($resultado->progtems->first()->horas_practicas==NULL){
              $resultado->horas_practicas = 0;
            }else{
              $resultado->horas_practicas = $resultado->progtems->first()->horas_practicas;
            }
              if($resultado->progtems->first()->horas_teoricas==NULL){
                $resultado->horas_teoricas = 0;
              }else{
                $resultado->horas_teoricas = $resultado->progtems->first()->horas_teoricas;
              }
            }else{
            $resultado->progtem_estado = 0;
            $resultado->seguimiento_estado = 0;
            $resultado->infofinal_estado = 0;
            $resultado->horas_practicas = 0;
            $resultado->horas_teoricas = 0;
            $resultado->fecha_revision_doc = "";
            }
      $resultado->nums_semestre = Funciones::palabra_a_numero($resultado->asigt->sem_ofrece_asigt,$resultado->flex);
      $resultado->permiso_fecha_pt = $permiso_fecha_pt;
      $resultado->permiso_fecha_se = $permiso_fecha_se;
      $resultado->permiso_fecha_if = $permiso_fecha_if;
      //$resultado->permiso_fecha_est_se = $permiso_fecha_est_se;
      
      $resultado->fecha_aprobacon_pt = $fecha_aprobacon_pt;
      $resultado->fecha_aprobacon_se = $fecha_aprobacon_se;
      $resultado->fecha_aprobacon_if = $fecha_aprobacon_if;
        
      $resultado->fecha_revision_doc_if = $fecha_revision_doc_if;
      $resultado->fecha_revision_doc_se = $fecha_revision_doc_se;
      $resultado->fecha_revision_est_se = $fecha_revision_est_se;
      $resultado->fecha_revision_doc_pt = $fecha_revision_doc_pt;
       //validar estados
      return $resultado;
    }


    
    public function info_reporte(Request $request)
    {
      
       /**
     * Muestra la informacion de los reportes Proramación Temática, Seguimiento e  Informe Final
     * Detalla los estados Pendiente, En Proceso y Listo
     * @return asinaciones de acuerdo al rol para poder ver o editar cada uno de los reportes
     */
    if (Auth::check()){
        if (Auth::user()->rol=="admin"){  
          $asigct= new Asigc();
          $asigct = $asigct->all();
        }else if (Auth::user()->rol=="director"){
          
        }else if (Auth::user()->rol=="docente"){
          
        }else if (Auth::user()->rol=="estudiante"){
          
        }else{//(Auth::user()->rol
          
        }// else (Auth::user()->rol
      }else{// if (Auth::check())
      
      }// else (Auth::check())
    }//info_reporte
  
    public function index(Request $request)
    {
    
        //if (!empty($_POST)) dd($request->all());
        /*

        if(isset($_SERVER['HTTP_X_FORWARDED_PROTO']) and $_SERVER['HTTP_X_FORWARDED_PROTO']=="https") {
            
        } else {
            return Redirect::to('home');
        }
        */
        if ($request->num_resultados == null){
            $request->num_resultados = 8;
        }
        $periodo_activo = Funciones::periodo_activo();
        //return view('admin.codigos.codigo');
        $fac = new Fac();
        $fac = $fac->all();
        #dd($links[1]->url);
        $prog = [];
        $asigc = [];
        $resp_enc = RespEnc::where('usu_id',Auth::user()->ident_usu)->get();
        $preg_enc = PregEnc::where('lugar','home')->get();
        #$preg_enc = new PregEnc();
        #$preg_enc = $preg_enc->all();
        #dd($preg_enc);
        $asigt = Asigt::get();
          
        if (Auth::check()){
        $progs_asigc=[];
        $progs_asigct = [];
        $progs_docentes = [];
        if (Auth::user()->rol=="admin" or Auth::user()->rol=="director"){
            if (Auth::user()->rol=="admin"){  
                $asigct= new Asigc();
                $asigct = $asigct->all();
            }else if (Auth::user()->rol=="director"){      
                $id_user = Auth::user()->id;
                $user = User::find($id_user);
                $user->deptos();
                foreach($user->deptos as $deptoi){
                  $deptoi->progs();
                  foreach($deptoi->progs as $progsi){
                    $progs_asigc[]=$progsi->cod_prog;
                  }
                }
              $asigct= Asigc::whereIn('cod_prog',$progs_asigc)->get();
            }
      
      foreach($asigct as $asigct_i){
        $progs_asigct[] = $asigct_i->cod_asigt;
        $progs_docentes[] = $asigct_i->ident_docnt;
      }
        
          //
            $asigc = Asigc::select('asigt.*','usu.*','asigc.*')
                    //->where('usu.id',Auth::user()->id)
                   ->where(function($q) use ($request,$progs_asigc) {
                      if (Auth::user()->rol=="director"){
                        $q->whereIn('cod_prog',$progs_asigc);
                      }
                   })
                    
                    ->where(function($q) use ($request) {
                        //`id`, `ident_docnt`, `estudiante`, `cod_asigt`, `per_acad`, `cod_prog`, `flex`, `observaciones`, `grupo`
                        if ($request->buscar_docentes != null){
                        $q->where('usu.id',$request->buscar_docentes);
                        }
                        if ($request->buscar_asignaturas != null){
                        $q->where('asigc.cod_asigt',$request->buscar_asignaturas);
                        }
                        if ($request->buscar_periodos != null){
                        $q->where('asigc.per_acad',$request->buscar_periodos);
                        }
                        if ($request->buscar_programa != null){
                        $q->where('asigc.cod_prog',$request->buscar_programa);
                        }
                        if ($request->buscar!= null){
                          $q->where(function($q) use ($request) {
                            foreach (explode(" ",$request->buscar) as $idacuscar => $valoravuscar){
                               $q->where(function($q) use ($request,$valoravuscar) {
                                  $q->where('asigt.nom_asigt','LIKE','%'.$valoravuscar.'%');
                                  $q->orwhere('usu.nombre','LIKE','%'.$valoravuscar.'%');
                                  $q->orwhere('usu.apellido','LIKE','%'.$valoravuscar.'%');
                               });
                            }
                          });
                        }
                    })
                    ->join('usu', 'asigc.ident_docnt', '=', 'usu.ident_usu')
                    ->join('asigt', 'asigc.cod_asigt', '=', 'asigt.codigo_asigt')
                    //->where('per_acad',$periodo_activo->periodo)
                    ->paginate($request->num_resultados);
           if ($request->buscar!= null){
          //dd($asigc->all());
           }
        }
        if (Auth::user()->rol=="docente"){
            $asigc = Asigc::where('usu.id',Auth::user()->id)
                    //->where('per_acad',$periodo_activo->periodo)
                    ->join('usu', 'asigc.ident_docnt', '=', 'usu.ident_usu')
                    ->join('asigt', 'asigc.cod_asigt', '=', 'asigt.codigo_asigt')
                    ->select('asigt.*','usu.*','asigc.*')
              ->where(function($q) use ($request) {
                          if ($request->buscar_asignaturas != null){
                          $q->where('asigc.cod_asigt',$request->buscar_asignaturas);
                          }
                          if ($request->buscar_periodos != null){
                          $q->where('asigc.per_acad',$request->buscar_periodos);
                          } 
                        })
                    ->paginate($request->num_resultados);
        }
        if (Auth::user()->rol=="estudiante"){
            $asigc = Asigc::where('usu2.id',Auth::user()->id)
                    //->where('per_acad',$periodo_activo->periodo)
                    ->join('usu as usu2', 'asigc.estudiante', '=', 'usu2.ident_usu')
                    ->join('usu', 'asigc.ident_docnt', '=', 'usu.ident_usu')
                    ->join('asigt', 'asigc.cod_asigt', '=', 'asigt.codigo_asigt')
                    ->select('asigt.*','usu.*','asigc.*')
              ->where(function($q) use ($request) {
                          if ($request->buscar_asignaturas != null){
                          $q->where('asigc.cod_asigt',$request->buscar_asignaturas);
                          }
                          if ($request->buscar_periodos != null){
                          $q->where('asigc.per_acad',$request->buscar_periodos);
                          } 
                        })
                    ->paginate($request->num_resultados);
        }
        if (Auth::user()->rol=="docente" or Auth::user()->rol=="estudiante"){
          foreach($asigc as $asigct_i){
            $progs_asigct[] = $asigct_i->cod_asigt;
          }
        }

        }//endif;
        $asigt_i_des=[];
        foreach ($asigc as $asigt_i){
          $resultado = $this->detalle_reporte($asigt_i->id);    
          $asigt_i_des[$asigt_i->id] = $resultado;
        }
      $fechas_calendario = $this->fechas_calenario($asigt_i_des);
      $estados = [];
$this->estados_reporte($estados,$asigc,$asigt_i_des);
      
          $asignaturas_grafico = []; 
      foreach ($asigt_i_des as $id => $resultado_i){
          if (!isset($asignaturas_grafico[$asigt_i_des[$id]->cod_asigt])){
        $asignaturas_grafico[$asigt_i_des[$id]->cod_asigt] = [];
        $asignaturas_grafico[$asigt_i_des[$id]->cod_asigt] = [
            'nom_asigt' => $asigt_i_des[$id]->asigt->nom_asigt,
            'progtem_estado0' => 0,
            'progtem_estado1' => 0,
            'progtem_estado2' => 0,
            'progtem_estadot' => 0,
            'seguimiento_estado0' => 0,
            'seguimiento_estado1' => 0,
            'seguimiento_estado2' => 0,
            'seguimiento_estadot' => 0,
            'infofinal_estado0' => 0,
            'infofinal_estado1' => 0,
            'infofinal_estado2' => 0,
            'infofinal_estadot' => 0
            ];
        }
            
        
        if (isset($asignaturas_grafico[$asigt_i_des[$id]->cod_asigt])){
            $ant = $asignaturas_grafico[$asigt_i_des[$id]->cod_asigt]['progtem_estado'.$asigt_i_des[$id]->progtem_estado];$ant = $ant+1;
            $asignaturas_grafico[$asigt_i_des[$id]->cod_asigt]['progtem_estado'.$asigt_i_des[$id]->progtem_estado] = $ant;
            
            $ant = $asignaturas_grafico[$asigt_i_des[$id]->cod_asigt]['seguimiento_estado'.$asigt_i_des[$id]->seguimiento_estado];$ant = $ant+1;
            $asignaturas_grafico[$asigt_i_des[$id]->cod_asigt]['seguimiento_estado'.$asigt_i_des[$id]->seguimiento_estado] = $ant;
            
            $ant = $asignaturas_grafico[$asigt_i_des[$id]->cod_asigt]['infofinal_estado'.$asigt_i_des[$id]->infofinal_estado];$ant = $ant+1;
            $asignaturas_grafico[$asigt_i_des[$id]->cod_asigt]['infofinal_estado'.$asigt_i_des[$id]->infofinal_estado] = $ant;
            
            $ant = $asignaturas_grafico[$asigt_i_des[$id]->cod_asigt]['progtem_estadot'];$ant = $ant+1;
            $asignaturas_grafico[$asigt_i_des[$id]->cod_asigt]['progtem_estadot'] = $ant;
            
            $ant = $asignaturas_grafico[$asigt_i_des[$id]->cod_asigt]['seguimiento_estadot'];$ant = $ant+1;
            $asignaturas_grafico[$asigt_i_des[$id]->cod_asigt]['seguimiento_estadot'] = $ant;
            
            $ant = $asignaturas_grafico[$asigt_i_des[$id]->cod_asigt]['infofinal_estadot'];$ant = $ant+1;
            $asignaturas_grafico[$asigt_i_des[$id]->cod_asigt]['infofinal_estadot'] = $ant;
        }
        }//fin foreach

        #dd($asigt_i_des);
        $iconos = [
            0 => '<span class="color-badges color-danger glyphicon glyphicon-remove"></span>',
            1 => '<span class="color-badges color-success glyphicon glyphicon-ok"></span>',
            2 => '<span class="color-badges color-warning glyphicon glyphicon-wrench"></span>'
            ];
        $progs = new Prog;
        $periodos = new Periodo;
        $docentes = User::where('rol','docente')->get();
        $estudiantes = User::where('rol','estudiante')->get();
        $asignaturas = new Asigt;
        //dd($docentes->all());
     

      $controlfechas = new ControlFechas();
      
      
      $params = ["datos"=>['fac'=>$fac,'prog'=>$prog,'asigc'=>$asigc],'request'=>$request,'asigc'=>$asigc,'iconos'=>$iconos,'asigt_i_des'=>$asigt_i_des,'periodo_activo'=>$periodo_activo,'progs'=>$progs->all(),'periodos'=>$periodos->all(),'docentes'=>$docentes->all(),'asignaturas'=>$asignaturas->all(),'progs_asigc'=>$progs_asigc,'progs_asigct'=>$progs_asigct,'progs_docentes'=>$progs_docentes,
'estados' => $estados,
'fechas_calendario'=>$fechas_calendario,
"controlfechas"=>$controlfechas->all(),
"asignaturas_grafico"=>$asignaturas_grafico,
"resp_enc"=>$resp_enc,
"preg_enc"=>$preg_enc
                                        ];
        return view('home.index')->with($params);
        //endif;
    }

 public function fechas_calenario($asigt_i_des){
  
  $fechas_calendario = [];
foreach ($asigt_i_des as $id=> $asigt_i_desi){
  /*
fecha_aprobacon_pt
fecha_aprobacon_se
fecha_aprobacon_if
fecha_revision_doc_if
fecha_revision_doc_se
fecha_revision_est_se
fecha_revision_doc_pt
  dd($asigt_i_desi);
*/
  if(Auth::user()->rol=='admin' or Auth::user()->rol=='director' or Auth::user()->rol=='docente'){
    $fechas_calendario[$asigt_i_desi['fecha_revision_doc_pt']]=[
        'fecha'=>$asigt_i_desi['fecha_revision_doc_pt'],
        'dias'=>Fecha::diferencia_fecha(date("Y-m-d"),$asigt_i_desi['fecha_revision_doc_pt'],false),
        'mensaje' => [$asigt_i_desi['cod_asigt']=> "Revisión de Programación Temática: ".Fecha::estado_dif_fecha_vencimiento(Fecha::diferencia_fecha($asigt_i_desi['fecha_revision_doc_pt'],date("Y-m-d"),false))],
        'nom_asigt' => [$asigt_i_desi['cod_asigt']=>$asigt_i_desi['nom_asigt']]
    ];
    
      
      $fechas_calendario[$asigt_i_desi['fecha_revision_doc_se']]=[
        'fecha'=>$asigt_i_desi['fecha_revision_doc_se'],
        'dias'=>Fecha::diferencia_fecha(date("Y-m-d"),$asigt_i_desi['fecha_revision_doc_se'],false),
        'mensaje' => [$asigt_i_desi['cod_asigt']=> "Revisión de Seguimiento: ".Fecha::estado_dif_fecha_vencimiento(Fecha::diferencia_fecha($asigt_i_desi['fecha_revision_doc_se'],date("Y-m-d"),false))],
        'nom_asigt' => [$asigt_i_desi['cod_asigt']=>$asigt_i_desi['nom_asigt']]
    ];
$fechas_calendario[$asigt_i_desi['fecha_revision_doc_if']]=[
        'fecha'=>$asigt_i_desi['fecha_revision_doc_if'],
        'dias'=>Fecha::diferencia_fecha(date("Y-m-d"),$asigt_i_desi['fecha_revision_doc_if'],false),
        'mensaje' => [$asigt_i_desi['cod_asigt']=> "Revisión de Informe Final: ".Fecha::estado_dif_fecha_vencimiento(Fecha::diferencia_fecha($asigt_i_desi['fecha_revision_doc_if'],date("Y-m-d"),false))],
        'nom_asigt' => [$asigt_i_desi['cod_asigt']=>$asigt_i_desi['nom_asigt']]
    ];
  }
if(Auth::user()->rol=='admin' or Auth::user()->rol=='director' or Auth::user()->rol=='docente' or Auth::user()->rol=='estudiante'){
 $fechas_calendario[$asigt_i_desi['fecha_revision_est_se']]=[
        'fecha'=>$asigt_i_desi['fecha_revision_est_se'],
        'dias'=>Fecha::diferencia_fecha(date("Y-m-d"),$asigt_i_desi['fecha_revision_est_se'],false),
        'mensaje' => [$asigt_i_desi['cod_asigt'] => "Revisión de Seguimiento (Estudiante): ".Fecha::estado_dif_fecha_vencimiento(Fecha::diferencia_fecha($asigt_i_desi['fecha_revision_est_se'],date("Y-m-d"),false))],
        'nom_asigt' => [$asigt_i_desi['cod_asigt']=>$asigt_i_desi['nom_asigt']]
    ];
}
 
}
  return $fechas_calendario;
}
}