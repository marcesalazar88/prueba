<?php

namespace App\Http\Controllers;
use Redirect;
use Illuminate\Http\Request;
use App\ProgTem;
use App\Seg;
use App\TipoValoracion;
use App\SegAspVal;
use App\Planes;
use App\Asigc;
use App\InfoFin;
use App\Crud;
use App\Fecha;
use App\Prog;
use App\Periodo;
use App\Asigt;
use App\ContAsigt;
use App\User;
use App\Funciones;
use Auth;
use PDF;
use DB;
use QrCode;
use App\Mensaje;

class PdfController extends Controller
{
  public function __construct()
{
    $this->middleware('auth', ['only' => ['general']]);//auth|guest
    $this->middleware('rol:admin|director|docente', ['only' => ['general']]);//admin|docente|estudiante
  //function show -> permitido para todos
}
  public function qrcode(Request $request, $id)
    {
    $im='';
    if($request->has('url')){  
    header('Content-Type: image/png');
    $im = QrCode::format('png')->size($id)->generate($request->url);
    }
    return $im;
    }
     public function general(Request $request) 
{
    $resultado=[];
    $grafico_ordenado = [];
    $asignaturas_grafico = []; 
    $dato='';
    if (Auth::check() and (Auth::user()->rol == 'admin' or Auth::user()->rol == 'director' or Auth::user()->rol == 'docente')){
        $asigc = new Asigc();
        $resultados = 10;
        if (isset($_GET['resultados'])){
            $resultados = $_GET['resultados'];
        }
        $valor_clave='';
        if (isset($_GET['ba'],$_GET['valor_clave']) and $_GET['ba']=='1'){
            $valor_clave = base64_decode($_GET['valor_clave']);
            $valor_clave = json_decode($valor_clave,true);
         //dd($valor_clave);
        }
        $resultado = $asigc->select('*')
                    ->join('asigt', 'asigc.cod_asigt', '=', 'asigt.codigo_asigt')
                    ->join('prog', 'asigc.cod_prog', '=', 'prog.cod_prog')
                    ->join('depto', 'depto.id', '=', 'prog.depto_id')
                    ->join('prog_temt', 'asigc.id', '=', 'prog_temt.asigc_id')
                    ->where(function($q) use ($valor_clave) {
                        if (Auth::user()->rol == 'docente')
                        $q->where('ident_docnt', Auth::user()->ident_usu);
                        if (Auth::user()->rol == 'director')
                        $q->where('depto.director', Auth::user()->ident_usu);
                        if (is_array($valor_clave)){
                            foreach ($valor_clave as $id => $valor){
                                if ($valor!= ""){
                                $q->where(function($query) use ($id, $valor){
                                        #$query->whereRaw('LOWER('.$id.') LIKE ? ',[''.$valor.'']);
                                        $query->whereRaw('LOWER('.$id.') LIKE ? ',['%'.$valor.'%']);
                                    });
                                }
                            }//foreach
                        }
                    })->paginate($resultados);
        $resultado->iconos = [
                0 => '<span class="color-danger glyphicon glyphicon-remove"></span>',
                1 => '<span class="color-success glyphicon glyphicon-ok"></span>',
                2 => '<span class="color-warning glyphicon glyphicon-warning-sign"></span>',
            ];
        
         foreach ($resultado as $id => $resultado_i){
            $resultado[$id]->prog();
            $resultado[$id]->asigt();
            $resultado[$id]->docente_fk();
            $resultado[$id]->nom_asigt = $resultado[$id]->asigt->nom_asigt;
            $resultado[$id]->nom_prog = $resultado[$id]->prog->nom_prog;
            $resultado[$id]->docente = $resultado[$id]->docente_fk->name." (".$resultado[$id]->ident_docnt.")";
            $pt = $resultado[$id]->progtems();
            if ($resultado[$id]->progtems->first()){
            $resultado[$id]->progtem_estado = 1;
            $sgm =$resultado[$id]->progtems->first()->segs();
            if ($resultado[$id]->progtems->first()->segs()->first()){
                $resultado[$id]->seguimiento_estado = 1;
                $resultado[$id]->progtems->first()->segs()->first()->seg_vals();
                if ($resultado[$id]->progtems->first()->segs()->first()->seg_vals()->first() and $resultado[$id]->progtems->first()->segs()->first()->seg_vals()->first()->voto =="Pendiente"){
                    $resultado[$id]->seguimiento_estado = 2;
                }else if ($resultado[$id]->progtems->first()->segs()->first()->seg_vals()->first() and ($resultado[$id]->progtems->first()->segs()->first()->seg_vals()->first()->voto =="Aprueba" or $resultado[$id]->progtems->first()->segs()->first()->seg_vals()->first()->voto =="Rechaza")){
                    $resultado[$id]->seguimiento_estado = 1;
                }
                $resultado[$id]->infofinal_estado = 0;//pendiente
            }else{
                $resultado[$id]->seguimiento_estado = 0;
                $resultado[$id]->infofinal_estado = 0;
            }
            $resultado[$id]->horas_practicas = $resultado[$id]->progtems->first()->horas_practicas;
            $resultado[$id]->horas_teoricas = $resultado[$id]->progtems->first()->horas_teoricas;
            }else{
            $resultado[$id]->progtem_estado = 0;
            $resultado[$id]->seguimiento_estado = 0;
            $resultado[$id]->infofinal_estado = 0;
            $resultado[$id]->horas_practicas = 0;
            $resultado[$id]->horas_teoricas = 0;
            $resultado[$id]->nums_semestre = Funciones::palabra_a_numero($resultado[$id]->asigt->sem_ofrece_asigt,$resultado[$id]->flex);
            $resultado[$id]->iconos_progtem_estado = $resultado->iconos[$resultado[$id]->progtem_estado];
            $resultado[$id]->iconos_seguimiento_estado = $resultado->iconos[$resultado[$id]->seguimiento_estado];
            $resultado[$id]->iconos_infofinal_estado = $resultado->iconos[$resultado[$id]->infofinal_estado];
            }
        if (!isset($asignaturas_grafico[$resultado[$id]->cod_asigt])){
        $asignaturas_grafico[$resultado[$id]->cod_asigt] = [];
        $asignaturas_grafico[$resultado[$id]->cod_asigt] = [
            'nom_asigt' => $resultado[$id]->asigt->nom_asigt,
            'progtem_estado0' => 0,
            'progtem_estado1' => 0,
            'progtem_estado2' => 0,
            'progtem_estadot' => 0,
            'seguimiento_estado0' => 0,
            'seguimiento_estado1' => 0,
            'seguimiento_estado2' => 0,
            'seguimiento_estadot' => 0,
            'infofinal_estado0' => 0,
            'infofinal_estado1' => 0,
            'infofinal_estado2' => 0,
            'infofinal_estadot' => 0
            ];
        }
            
        
        if (isset($asignaturas_grafico[$resultado[$id]->cod_asigt])){
            $ant = $asignaturas_grafico[$resultado[$id]->cod_asigt]['progtem_estado'.$resultado[$id]->progtem_estado];$ant = $ant+1;
            $asignaturas_grafico[$resultado[$id]->cod_asigt]['progtem_estado'.$resultado[$id]->progtem_estado] = $ant;
            
            $ant = $asignaturas_grafico[$resultado[$id]->cod_asigt]['seguimiento_estado'.$resultado[$id]->seguimiento_estado];$ant = $ant+1;
            $asignaturas_grafico[$resultado[$id]->cod_asigt]['seguimiento_estado'.$resultado[$id]->seguimiento_estado] = $ant;
            
            $ant = $asignaturas_grafico[$resultado[$id]->cod_asigt]['infofinal_estado'.$resultado[$id]->infofinal_estado];$ant = $ant+1;
            $asignaturas_grafico[$resultado[$id]->cod_asigt]['infofinal_estado'.$resultado[$id]->infofinal_estado] = $ant;
            
            $ant = $asignaturas_grafico[$resultado[$id]->cod_asigt]['progtem_estadot'];$ant = $ant+1;
            $asignaturas_grafico[$resultado[$id]->cod_asigt]['progtem_estadot'] = $ant;
            
            $ant = $asignaturas_grafico[$resultado[$id]->cod_asigt]['seguimiento_estadot'];$ant = $ant+1;
            $asignaturas_grafico[$resultado[$id]->cod_asigt]['seguimiento_estadot'] = $ant;
            
            $ant = $asignaturas_grafico[$resultado[$id]->cod_asigt]['infofinal_estadot'];$ant = $ant+1;
            $asignaturas_grafico[$resultado[$id]->cod_asigt]['infofinal_estadot'] = $ant;
        }
        }//endforeach;
        $grafico_ordenado = ['datos_pt' => [], 'datos_se' => [], 'datos_if' => []];
        foreach($asignaturas_grafico as $i => $asignatura){
        $grafico_ordenado['datos_pt'][] = [
                    'category' => $asignatura['nom_asigt'],
                    'value1' => floor(100*$asignatura['progtem_estado0']/$asignatura['progtem_estadot']),
                    'value2' => floor(100*$asignatura['progtem_estado1']/$asignatura['progtem_estadot']),
                    'value3' => floor(100*$asignatura['progtem_estado2']/$asignatura['progtem_estadot'])
                   ];
        $grafico_ordenado['datos_se'][] = [
                    'category' => $asignatura['nom_asigt'],
                    'value1' => floor(100*$asignatura['seguimiento_estado0']/$asignatura['seguimiento_estadot']),
                    'value2' => floor(100*$asignatura['seguimiento_estado1']/$asignatura['seguimiento_estadot']),
                    'value3' => floor(100*$asignatura['seguimiento_estado2']/$asignatura['seguimiento_estadot'])
                    ];
        $grafico_ordenado['datos_if'][] = [
                    'category' => $asignatura['nom_asigt'],
                    'value1' => floor(100*$asignatura['infofinal_estado0']/$asignatura['infofinal_estadot']),
                    'value2' => floor(100*$asignatura['infofinal_estado1']/$asignatura['infofinal_estadot']),
                    'value3' => floor(100*$asignatura['infofinal_estado2']/$asignatura['infofinal_estadot'])
                    ];
        }//endforeach;
         /**/
            /*
            $codigo = isset($resultado[$id]->codigos()->first()->token) ? $resultado[$id]->codigos()->first()->token : '' ;
            end$resforeach;ultado[$id]->codigo = $codigo;
            */
         $progs = new Prog;
         $periodos = new Periodo;
         $periodo_activo = Funciones::periodo_activo();
         $docentes = User::where('rol','docente')->get();
         $asignaturas = new Asigt;
         $estudiantes = User::where('rol','estudiante')->get();
         $resultado2 = [
                'resultado'=>$resultado,
                'grafico_ordenado' => $grafico_ordenado
                ];
         $json = $request->get('json');
         if ($json){
         return $resultado2;
         }else{
         return view('reportes.general')->with(["asignaturas_grafico"=>$asignaturas_grafico,"resultado"=>$resultado2,'periodo_activo'=>$periodo_activo,'progs'=>$progs->all(),'periodos'=>$periodos->all(),'docentes'=>$docentes->all(),'asignaturas'=>$asignaturas->all(),'estudiantes'=>$estudiantes->all()]);
         }
         
     }else{
     return redirect('home'); 
     }//endif;
}//fin reporte general
     public function invoice() 
    {
        $data = $this->getData();
        $date = date('Y-m-d');
        $invoice = "2222";
        $view =  \View::make('pdf.invoice', compact('data', 'date', 'invoice'))->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);
        return $pdf->stream('invoice');
    }

    public function getData() 
    {
        $data =  [
            'quantity'      => '1' ,
            'description'   => 'some ramdom text',
            'price'   => '500',
            'total'     => '500'
        ];
        return $data;
    }
    public function flexibilidad(Request $request)
    {
      $periodo_activo = Funciones::periodo_activo();
      //dd($periodo_activo);
      $asigc = Asigc::where('asigc.flex','SI')
                ->where('asigc.per_acad',$periodo_activo->periodo)
                ->join('usu', 'asigc.ident_docnt', '=', 'usu.ident_usu')
                ->join('asigt', 'asigc.cod_asigt', '=', 'asigt.codigo_asigt')
                ->select('asigt.*','usu.*','asigc.*','asigt.id as asigt_id')
                //->groupBy('asigc.cod_asigt')
                ->get();
        #echo "Hola flexibilidad - hacer tabla";
        #dd($asigc->all());
      $datos = [];
      foreach ($asigc as $i => $asigci){
        $datos[$asigci->cod_asigt] = $asigci;
      }
        return view('reportes.flexibilidad')->with(['asigc'=>$datos,'periodo_activo'=>$periodo_activo]);
    }
    public function buscar()
    {
      /*
// Instanciar la clase dompdf
$pdf = new PDF();

// Algunas configuraciones
$pdf->setPaper('mediaA4', 'portrait');

// Leer o cargar el html
$pdf->loadHtml($view);

// Render the HTML as PDF
$pdf->render();

// Generar el PDF al navegador
return $pdf->stream($nombre_archivo);
*/
                /*
echo $view;
die();
$pdf->loadHTML('<h1>Test</h1>');

        $pdf = \App::make('dompdf.wrapper');
        return $pdf->stream('invoice');
            */
        /*
        $progtem = new ProgTem();
        $progtem = $progtem->all();
        foreach ($progtem as $progtem_i){
            $progtema = ProgTem::find($progtem_i->id);
            $progtema->codigo = uniqid("PT");
            $progtema->save();
        }
        PT5d82d32ad40cc
        */
        if(isset($_REQUEST['codigo'])){
          
              $codigo = $_REQUEST['codigo'];
              $tipo = substr($codigo,0,2);

              if ($tipo=="PT"){
                    $progtem = ProgTem::where('codigo',$codigo)->get();
                    if ($progtem->first()){
                    $progtem=$progtem->first();
                    $asigt = ProgTem::where('prog_temt.id',$progtem->id)
                                    ->join('asigc', 'asigc.id', '=', 'prog_temt.asigc_id')
                                    ->join('asigt', 'asigc.cod_asigt', '=', 'asigt.codigo_asigt')
                                    ->join('usu', 'asigc.ident_docnt', '=', 'usu.ident_usu')
                                    ->join('prog', 'asigc.cod_prog', '=', 'prog.cod_prog')
                                    ->join('depto', 'depto.id', '=', 'prog.depto_id')
                                    ->join('fac', 'fac.id', '=', 'depto.fac_id')
                                    ->select('asigt.*','usu.*','asigc.*','prog.*','fac.*','prog_temt.*')
                                    ->get();
                                    //->toSql();
                                    /**
                                    Es una consulta para un reporte, no es muy diferente a un SQL puro
                                    */
                        //dd($asigt);
                        $todos = count($asigt->all());
                        if ($todos>0){
                            $asigt = $asigt->first();
                            $asigt->punto_adics();
                            $asigt->refe_biblios();
                            $view =  \View::make('reportes.pdf.prog_tem', compact('asigt'))->render();
                            $pdf = \App::make('dompdf.wrapper');
                            $pdf->loadHTML($view);
                            return $view;
                            ##return $pdf->stream();
                        }
                    }else{
                        return Redirect::to('reportes');
                    }
              }else if ($tipo=="SE"){
                  $seg = Seg::where('codigo',$codigo)->get();
                  if ($seg->first()){
                    $seg=$seg->first();
                    $seg->prog_tem();
                    $asigt = ProgTem::where('prog_temt.id',$seg->prog_tem->id)
                    /*
                    dd($seg->prog_tem->id);
if ($id != ''){
            //leftJoin
            $asigt = ProgTem::where('asigc.id',$id)
            */
                        ->join('asigc', 'asigc.id', '=', 'prog_temt.asigc_id')
                        ->join('asigt', 'asigc.cod_asigt', '=', 'asigt.codigo_asigt')
                        ->join('usu', 'asigc.ident_docnt', '=', 'usu.ident_usu')
                        ->join('prog', 'asigc.cod_prog', '=', 'prog.cod_prog')
                        ->join('depto', 'depto.id', '=', 'prog.depto_id')
                        ->join('fac', 'fac.id', '=', 'depto.fac_id')
                        ->select('asigt.*','usu.*','asigc.*','prog.*','fac.*','prog_temt.*')
                        ->get();
                        //->toSql();
                        //Es una consulta para un reporte, no es muy diferente a un SQL puro
                        //dd($asigt);
                        $todos = count($asigt->all());
                        if ($todos>0){
                            $asigt = $asigt->first();
                            $asigt->segs();
                            $asigt->segs->first()->seg_vals();
                            $seg = $asigt->segs->first()->seg_vals->first()->get();
                            $asigt->punto_adics();
                            $asigt->refe_biblios();
                            $tipovaloracion = TipoValoracion::orderBy('id')->get();
                            $criterios = SegAspVal::orderBy('name')->pluck('name', 'cod');
                            $user = Auth::user();
                          
                            $mi_asigc = Asigc::find($asigt->asigc_id);
                            $mi_asigc->prog();
                            $mi_asigc->prog->depto();
                            $facultad = $mi_asigc->prog->depto->fac->nom_fac;
                            $programa = $mi_asigc->prog->nom_prog;
                            $asigc = $asigt;
/*
                          'asigc'=>$asigt,'tipovaloracion'=>$tipovaloracion,'user'=>$user,'criterios'=>$criterios
*/
                          /*
                          $view = "1";
                          */
                            $view =  \View::make('reportes.pdf.seg_prog', compact(['asigc', 'tipovaloracion', 'user', 'criterios','facultad','programa']))->render();
                          $pdf = \App::make('dompdf.wrapper');
                          $pdf->loadHTML($view);
                          $pdf->setPaper('letter', 'landscape');
                          return $view;
                          ##return $pdf->stream();
                          /*
return view('reportes.seg_prog')->with(['asigc'=>$asigt,'tipovaloracion'=>$tipovaloracion,'user'=>$user,'criterios'=>$criterios]);
                          */
                        }
                        }
                   return Redirect::to('reportes');
              }else if ($tipo=="IF"){
                /*
                    dd($tipo);
                */
                $infofin = InfoFin::where('codigo',$codigo)->get()->first();
                $id = $infofin->id_asigt;
                //$infofin = InfoFin::where('id_asigt',$id)->get()->first();
                if($infofin){
                    #dd($infofin);
                    $infofin_cont = Asigc::find($id);
                    $contasigts = ContAsigt::where('cod_asigt',$infofin_cont->cod_asigt)->get();

                    $asigc = Asigc::find($id);

                    $asigc->docente_fk();
                    $nom_usu = $asigc->docente_fk->nom_usu;

                    $asigc->prog();
                    $programa = $asigc->prog->nom_prog;
                    $asigc->prog->depto();
                    $nom_depto = $asigc->prog->depto->nom_depto;
                    $asigc->prog->depto->fac();
                    $facultad = $asigc->prog->depto->fac->nom_fac;
                    $view =  \View::make('reportes.pdf.inf_final', compact(['infofin','asigc','contasigts',
                      'programa','facultad']))->render();
                    $pdf = \App::make('dompdf.wrapper');
                    $pdf->loadHTML($view);
                    $pdf->setPaper('letter', 'portrait');
                    return $view;
                    #return $pdf->stream();
                  
                    }else{
                    return Redirect::to('reportes');
                    }
                /**/
              }else{
                return Redirect::to('reportes');
              }
        }else{
            return Redirect::to('reportes');
        }
    }
    public function planes(Request $request)
    {
        #echo "Hola planes - mostrar el listado de planes y crear modulo";//hacer tabla
        #$planes = new Planes();
        #dd($planes->all());
         $crud = new Crud();
         $prog = new Prog();
         $prog = $prog->all();
         $this->planes = new Planes();
         $resultado = $crud->buscar($request,$this->planes,['cod_prog'=>'Programa']);
          foreach ($resultado as $id => $resultado_i){
            $resultado[$id]->prog();
            $resultado[$id]->nom_prog = $resultado[$id]->prog->nom_prog;
            $resultado[$id]->fecha = Fecha::formato_fecha($resultado[$id]->fecha);
          }
         #dd($resultado);
         $botones = false;      
         if (isset($_REQUEST['json'])){
         return $resultado;
         }else{
         return view('reportes.planes')->with(["resultado"=>$resultado, "prog"=>$prog,'botones'=>$botones]);
         }
    }
    public function prog_tem($id = '') {
        if ($id != ''){
                  $progTem = ProgTem::where('asigc_id',$id)->get();
                    $progTem = $progTem->first();
                    if($progTem->codigo==""){
                      $progTem->codigo = uniqid("PT");             
                      $progTem->save();
                    }
                    
            //leftJoin
            $asigt = ProgTem::where('asigc.id',$id)
                        ->join('asigc', 'asigc.id', '=', 'prog_temt.asigc_id')
                        ->join('asigt', 'asigc.cod_asigt', '=', 'asigt.codigo_asigt')
                        ->join('usu', 'asigc.ident_docnt', '=', 'usu.ident_usu')
                        ->join('prog', 'asigc.cod_prog', '=', 'prog.cod_prog')
                        ->join('depto', 'depto.id', '=', 'prog.depto_id')
                        ->join('fac', 'fac.id', '=', 'depto.fac_id')
                        ->select('asigt.*','usu.*','asigc.*','prog.*','fac.*','prog_temt.*')
                        ->get();
                        //->toSql();
                        /**
                        Es una consulta para un reporte, no es muy diferente a un SQL puro
                        */
            //dd($asigt);
            $todos = count($asigt->all());
            if ($todos>0){
                $asigt = $asigt->first();
                $asigt->punto_adics();
                $asigt->refe_biblios();
                return view('reportes.prog_tem')->with(['asigt'=>$asigt]);
            }else{
                return Redirect::to('home');
            }
        }else{
        return Redirect::to('home');
        }
    }
    
    public function seguimiento($id = '') {
       /*
       $seg = new Seg();
       $seg = $seg->all();
        foreach ($seg as $seg_i){
            $sega = Seg::find($seg_i->id);
            $sega->codigo = uniqid("SE");
            $sega->save();
          var_dump($sega);
        }
      */
        if ($id != ''){
            //leftJoin
            $asigt = ProgTem::where('asigc.id',$id)
                        ->join('asigc', 'asigc.id', '=', 'prog_temt.asigc_id')
                        ->join('asigt', 'asigc.cod_asigt', '=', 'asigt.codigo_asigt')
                        ->join('usu', 'asigc.ident_docnt', '=', 'usu.ident_usu')
                        ->join('prog', 'asigc.cod_prog', '=', 'prog.cod_prog')
                        ->join('depto', 'depto.id', '=', 'prog.depto_id')
                        ->join('fac', 'fac.id', '=', 'depto.fac_id')
                        ->select('asigt.*','usu.*','asigc.*','prog.*','fac.*','prog_temt.*')
                        ->get();
                        //->toSql();
                        /**
                        Es una consulta para un reporte, no es muy diferente a un SQL puro
                        */
            //dd($asigt);
            $todos = count($asigt->all());
            if ($todos>0){
                $asigt = $asigt->first();
                $asigt->segs();
                $asigt->segs->first()->seg_vals();
                $seg = $asigt->segs->first()->seg_vals->first()->get();
                $asigt->punto_adics();
                $asigt->refe_biblios();
                $tipovaloracion = TipoValoracion::orderBy('id')->get();
                $criterios = SegAspVal::orderBy('name')->pluck('name', 'cod');
                $user = Auth::user();
                $mi_asigc = Asigc::find($id);
                $mi_asigc->prog();
                $mi_asigc->prog->depto();
                $facultad = $mi_asigc->prog->depto->fac->nom_fac;
                $programa = $mi_asigc->prog->nom_prog;
                $mensajes =  Mensaje::where('asigc_id',$id)->orderBy('id', 'ASC')->get();
                //dd($mi_asigc);
             // dd($facultad);
                return view('reportes.seg_prog')->with(['asigc'=>$asigt,'tipovaloracion'=>$tipovaloracion,'user'=>$user,'criterios'=>$criterios,'facultad'=>$facultad
,'programa'=>$programa
,'mensajes'=>$mensajes]);
            }else{
                return Redirect::to('home');
            }
        }else{
        return Redirect::to('home');
        }
    }
    public function inf_final($id = '') {
          /*
       $infofin = new InfoFin();
       $infofin = $infofin->all();
        foreach ($infofin as $infofin_i){
            $infofina = InfoFin::find($infofin_i->id);
            $infofina->codigo = uniqid("IF");
            $infofina->save();
        }
         */
      
        $infofin = InfoFin::where('id_asigt',$id)->get()->first();
    if($infofin){
        #dd($infofin);
        $infofin_cont = Asigc::find($id);
        $contasigts = ContAsigt::where('cod_asigt',$infofin_cont->cod_asigt)->get();

        $asigc = Asigc::find($id);
        
        $asigc->docente_fk();
        $nom_usu = $asigc->docente_fk->nom_usu;

        $asigc->prog();
        $nom_prog = $asigc->prog->nom_prog;
        $asigc->prog->depto();
        $nom_depto = $asigc->prog->depto->nom_depto;
        $asigc->prog->depto->fac();
        $nom_fac = $asigc->prog->depto->fac->nom_fac;

        return view('reportes.inf_final')->with(['infofin'=>$infofin,'asigc'=>$asigc,'contasigts'=>$contasigts,
          'programa'=>$nom_prog,
          'facultad'=>$nom_fac]);
    }else{
         return redirect()->back();
         /*
        $infofin = new InfoFin();
        $infofin->id_asigt =  $id;
        $infofin->save();
        return view('reportes.inf_final')->with(['infofin'=>$infofin]);
         */
    }
    }
}
