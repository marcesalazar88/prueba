<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Http\Requests\UserRequest;
use App\Http\Requests\UserRegister;
use App\Codigos;
use App\Asigc;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        if (env('AUTOREGISTRO')==true){
            //$this->middleware('guest');
        }else{
           $this->middleware('auth');
        }
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
     /*
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'nombre' => 'required|string|max:255',
            'apellido' => 'required|string|max:255',
            'ident_usu' => 'required|number|unique:users',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }
*/
    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    public function register(UserRegister $request)
    {
       //dd($request);
       if ($request->rol=='estudiante'){
       $codigos = Codigos::where('token','=',$request->codigo)->get();
       if($codigos->first()){
       $asig = Asigc::find($codigos->first()->asigc_id);
           $asig->estudiante = $request->ident_usu;
            $user = new User;
            $user->ident_usu = $request->ident_usu;
            $user->rol = $request->rol;
            $user->roles = $request->rol;
            $user->tel = $request->tel;
            $user->nombre = $request->nombre;
            $user->apellido = $request->apellido;
            $user->name = $request->nombre." ".$request->apellido;
            $user->email = $request->email;
            $user->password = bcrypt($request->password);//password_confirmation middleware
                if ($user->save()){
                  $codigos->first()->delete();
                  $asig->save();
                return redirect()->to('login')->with(['notice'=>'El Usuario se ha registrado satisfactoriamente']);
                }else{
                return redirect()->back()->with(['notice'=>'Error, El Usuario no se ha registrado']);
                }
            
       }else{
       return redirect()->back()->with('warning', 'Error, El código ingresado no es correcto, o ya ha sido utilizado.');
       }
       
        }else{
        return redirect()->back()->with(['notice'=>'Error, El Usuario no se ha registrado, el rol debe ser estudiante']);
        }
    }
}
