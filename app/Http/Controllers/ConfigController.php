<?php

namespace App\Http\Controllers;


use App\Codigos;
use App\Config;
use Illuminate\Http\Request;
use App\User;
use Auth;
class ConfigController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');//auth|guest
        $this->middleware('rol:admin');//admin|docente|estudiante
    }
    public function index()
    {
      $config = Config::find(1);
     return view('admin.config.show')->with(['config'=>$config]);
    //dd('codigos');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
   
    public function update(Request $request, $id)
    {
      $config = Config::find(1);
      $config->hora_notificacion = $request->hora_notificacion;
      $config->save();
      return redirect()->back();
      //dd($request->hora_notificacion);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Codigos  $codigos
     * @return \Illuminate\Http\Response
     */
   
}
