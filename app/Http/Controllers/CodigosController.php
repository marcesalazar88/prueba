<?php

namespace App\Http\Controllers;


use App\Codigos;
use App\Asigc;
use Illuminate\Http\Request;
use App\User;
use Auth;
class CodigosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');//auth|guest
        $this->middleware('rol:admin|docente', ['only' => ['create']]);//admin|docente|estudiante
    }
    public function codigo_token(Request $request)
    {
       $codigos = Codigos::where('token','=',$request->codigo_token)->get();
       if($codigos->first()){
       $asig = Asigc::find($codigos->first()->asigc_id);
       $user = User::find(Auth::user()->id);
       #dd($user);
       $asig->estudiante = $user->ident_usu;
       $asig->save();
       $codigos->first()->delete();
       return redirect()->action('HomeController@index')->with('success', 'Felicitaciones, El c&oacute;digo ha sido ingresado correctamente.');
       #return redirect()->back()->with('success', 'Felicitaciones, El c��digo ha sido ingresado correctamente.');
       //dd($asig->estudiante);
       //eliminar token o bloquear
       }else{
       return redirect()->action('HomeController@index')->with('warning', 'Error, El c&oacute;digo ingresado no es correcto, o ya ha sido utilizado.');
       #return redirect()->back()->with('warning', 'Error, El c��digo ingresado no es correcto, o ya ha sido utilizado.');
       //dd('no hay resultados');
       //redireccionar a home
       }
    }
    public function index()
    {
     return redirect()->back();
    //dd('codigos');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id_asig)
    {
        $mensaje = 'Felicidades, El c&oacute;digo ha sido creado.';
        $codigo_viejo = Codigos::where('asigc_id','=',$id_asig)->get();
        if($codigo_viejo->first()){
        $codigo_viejo->first()->delete();
        $mensaje = 'Felicidades, El c&oacute;digo ha sido actualizado.';
        }
        $asig = Asigc::find($id_asig);
        $asig->estudiante = null;
        $asig->save();
        $user = User::find(Auth::user()->id);
        $codigos = new Codigos;
        $codigos->asigc_id = $id_asig;
        $codigos->user_id = $user->id;
        $codigos->token = uniqid();
        $result = $codigos->save();
        #return redirect()->to('admin/asignacion');
        return redirect()->back()->with('success', $mensaje);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = User::find(Auth::user()->id);
        $codigos = new Codigos;
        $codigos->asigc_id = $request->asigc_id;
        $codigos->user_id = $user->id;
        $codigos->token = uniqid();
        $result = $codigos->save();
         return redirect()->to('admin/asignacion');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Codigos  $codigos
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Codigos  $codigos
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Codigos  $codigos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Codigos  $codigos
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         return redirect()->back();
    }
}
