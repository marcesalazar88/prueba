<?php

namespace App\Http\Controllers;

use App\InfoFin;
use Illuminate\Http\Request;
use App\ControlFechas;
use App\Asigc;
use App\ContAsigt;
use Auth;

class InfoFinController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
  
       public function __construct()
    {
        $this->middleware('auth');//auth|guest
        $this->middleware('rol:admin|director|docente');//admin|docente|estudiante
    }
    public function index()
    {
        return redirect()->back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return redirect()->back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\InfoFin  $infoFin
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect()->route('reportes.inf_final',['id' => $id]);
     
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\InfoFin  $infoFin
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
       $infofin = InfoFin::where('id_asigt',$id)->get()->first();
     if($infofin){
        #dd($infofin);
     }else{
         $infofin = new InfoFin();
         $infofin->id_asigt = $id;
     }
    $asigc = Asigc::find($id);
    $asigc->docente_fk();
    $nom_usu = $asigc->docente_fk->nom_usu;
    
    $asigc->prog();
    $nom_prog = $asigc->prog->nom_prog;
    $asigc->prog->depto();
    $nom_depto = $asigc->prog->depto->nom_depto;
    $asigc->prog->depto->fac();
    $nom_fac = $asigc->prog->depto->fac->nom_fac;
    //dd($asigc);
    $contasigts = ContAsigt::where('cod_asigt',$asigc->cod_asigt)->get();
    $controlfechas = new ControlFechas();
    if (Auth::user()->rol=='docente'){ 
      if($infofin->estado=="Pendiente"){
          $infofin->estado = "En Proceso";             
          $infofin->save();
      }
    }
    return view('admin.asignacion.informefinal.editar')->with(['infofin'=>$infofin,'asigc'=>$asigc,'contasigts'=>$contasigts,"controlfechas"=>$controlfechas->all()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\InfoFin  $infoFin
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($request);
        $infofin = InfoFin::where('id_asigt',$id)->get()->first();
         if($infofin){
            #dd($infofin);
         }else{
             $infofin = new InfoFin();
             $infofin->id_asigt = $id;
         }
         
     
        
        $infofin->temt_cumpl = $request->temt_cumpl;
        $infofin->temt_n_cumpl = $request->temt_n_cumpl;
       // dd($request->all());
        $infofin->id_proc_eva = $request->id_proc_eva;
        $infofin->por_cump_desem_gen = $request->por_cump_desem_gen;
        $infofin->id_desllo_comp = $request->id_desllo_comp;
        $infofin->autoeva = $request->autoeva;
        $infofin->observ = $request->observ;
        $infofin->biblio = $request->biblio;
        
        $infofin->porcentaje_proc_eva = $request->porcentaje_proc_eva;
        $infofin->total_est_curso = $request->total_est_curso;
        $infofin->total_est_desem_gen = $request->total_est_desem_gen;
        $infofin->nota_max_desem_gen = $request->nota_max_desem_gen;
        $infofin->nota_min_desem_gen = $request->nota_min_desem_gen;
        $infofin->prom_grup_desem_gen = $request->prom_grup_desem_gen;
        $infofin->nota_perdieron_desem_gen = $request->nota_perdieron_desem_gen;
        $infofin->nota_est_pasaron_desem_gen = $request->nota_est_pasaron_desem_gen;
        $infofin->observ_desem_gen = $request->observ_desem_gen;
        $infofin->comp_desllo_compete = $request->comp_desllo_compete;
        $infofin->aplica_compe = $request->aplica_compe;
      
        $infofin->act_proc_eva = json_encode($request->act_proc_eva);
        $infofin->porcentaje_proce_eva = json_encode($request->porcentaje_proce_eva);
          
        if (Auth::user()->rol=='admin' or Auth::user()->rol=='director'){
            $infofin->control_fecha_id = $request->control_fecha_id;
            $infofin->fecha_aprobacion = $request->fecha_aprobacion;
            $infofin->fecha_revision_doc = $request->fecha_revision_doc;
        }
        $infofin->save();
        $controlfechas = new ControlFechas();
        //return redirect()->route('reportes.inf_final',['id' => $id]);
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\InfoFin  $infoFin
     * @return \Illuminate\Http\Response
     */
    public function destroy(InfoFin $infoFin)
    {
        return redirect()->back();
    }
}
