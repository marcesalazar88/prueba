<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProgTemRefeBiblio;
use Redirect;
use Auth;
use App\Http\Requests\BiblioRequest;
class ProgTemRefeBiblioController extends Controller
{
    public function index()
    {
        $this->middleware('auth');//auth|guest
        $this->middleware('rol:admin|docente');//admin|docente|estudiante

    }
   public function store(BiblioRequest $request, $id)
    {
        if (Auth::user()->rol=='admin' or Auth::user()->rol=='docente'):
        $refebiblio = new ProgTemRefeBiblio();
        $refebiblio->prog_tem_id = $id;
        $refebiblio->refe_biblio = $request->refe_biblio;
        $refebiblio->save();
        $refebiblio->prog_tem();
        $asigc_id = $refebiblio->prog_tem->asigc_id;
        if ($refebiblio->exists === true){
            return redirect(route('admin.asignacion.programaciontematica.edit', [$asigc_id]). '#bibliografia')->with('success', 'La referencia bibliográfica ha sido creada correctamente.');
        }else{
           return redirect(route('admin.asignacion.programaciontematica.edit', [$asigc_id]). '#bibliografia')->with('warning', 'Error, La referencia bibliográfica no ha sido creada.');
        }
        else:
        return redirect()->back()->with('danger', 'Error, La referencia bibliográfica no ha sido creada.');
        endif;
    }
   public function destroy($id,$id2)
    {
        if (Auth::user()->rol=='admin' or Auth::user()->rol=='docente'):
        $refebiblio = ProgTemRefeBiblio::find($id2);
        $refebiblio =ProgTemRefeBiblio::where('id',$id2)->first();
        $refebiblio->prog_tem();
        $asigc_id = $refebiblio->prog_tem->asigc_id;
        if ($refebiblio->exists === true) $refebiblio->delete();
        if ($refebiblio->exists === false){
          
          return redirect(route('admin.asignacion.programaciontematica.edit', [$asigc_id]). '#bibliografia')->with('success', 'La referencia bibliográfica ha sido eliminada correctamente.');
        }else{
        return redirect()->back()->with('danger', 'Error, La referencia bibliográfica no ha sido eliminada.');
        }
        else:
        return redirect()->back()->with('danger', 'Error, La referencia bibliográfica no ha sido eliminada.');
        endif;
    }
}
