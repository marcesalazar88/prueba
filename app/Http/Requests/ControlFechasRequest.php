<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use App\User;

class ControlFechasRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
        #return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch (Request::get('submit')) {
            case 'Actualizar':
                if (Request::get('formato')=='seg') {
                        $rules = [
                    'formato'=> 'required',
                    'nombre_control'=> 'required',
                    'fecha_aprobacion'=> 'required|date',
                    'fecha_revision_doc'=> 'required|date',
                    'fecha_revision_est'=> 'required|date', 
                    'per_acad' => 'required|exists:periodo,periodo'
                ]; 
                }else{
                $rules = [
                    'formato'=> 'required',
                    'nombre_control'=> 'required',
                    'fecha_aprobacion'=> 'required|date',
                    'fecha_revision_doc'=> 'required|date',
                    'per_acad' => 'required|exists:periodo,periodo'
                ]; 
                }
                break;
        
            default:
            if (Request::get('formato')=='seg') {
                $rules = [
        'formato'=> 'required',
        'nombre_control'=> 'required',
        'fecha_aprobacion'=> 'required|date',
        'fecha_revision_doc'=> 'required|date',
        'fecha_revision_est'=> 'required|date', 
        'per_acad' => 'required|exists:periodo,periodo'
            ];
                }else{
               $rules = [
        'formato'=> 'required',
        'nombre_control'=> 'required',
        'fecha_aprobacion'=> 'required|date',
        'fecha_revision_doc'=> 'required|date',
        'per_acad' => 'required|exists:periodo,periodo'
            ];
            }
                break;
        }

        return $rules;
        
    }

    public function messages()
    {
        return [
            'formato.required'  => 'El formato es obligatorio',
            'nombre_control.required'  => 'El nombre del control es obligatorio',
            'fecha_aprobacion.required' => 'La fecha_aprobacion es obligatoria',
            'fecha_revision_doc.required' => 'El fecha_revision_doc académico es obligatorio',
            'fecha_revision_est.required' => 'El fecha_revision_est es obligatorio',
            'per_acad.required' => 'El campo per_acad es obligatorio',
            'per_acad.exists' => 'El valor del campo per_acad no es válido'
        ];
    }
}
