<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;

class ProgRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch (Request::get('submit')) {
            case 'Actualizar':
                $rules = [
                   'nom_prog' => 'required|string|max:255',
                    'depto_id' => 'required',
                    'sede' => 'required',
                ];
                break;
        
            default:
                $rules = [
                        'nom_prog' => 'required|string|max:255|unique:prog',
                        'depto_id' => 'required',
                        'sede' => 'required',
                    ];
                break;
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'nom_prog.required'  => 'El nombre es obligatorio',
            'nom_prog.max' => 'El nombre no puede tener más de 255 caracteres',
            'nom_prog.unique' => 'El Registro de este programa ya existe',
            'depto_id.required'  => 'El campo departamento es obligatorio',
            'sede.required'  => 'El campo sede es obligatorio',
        ];
    }
}
