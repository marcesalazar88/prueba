<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;

class DptoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        
       switch (Request::get('submit')) {
            case 'Actualizar':
                $rules = [
                    'nom_depto' => 'required|string|max:255|unique:depto,nom_depto,'.\Request::get('id'),
                    'fac_id' => 'required',
                ];
                break;
        
            default:
                $rules = [
                    'id' => 'required|numeric|unique:depto',
                    'nom_depto' => 'required|string|max:255|unique:depto',
                    'fac_id' => 'required',
                ];
                break;
        }

        return $rules;
    }

    public function messages()
    {
        switch (Request::get('submit')) {
            case 'Actualizar':
                $rules = [
            'id.required' => 'El código es obligatorio',
            'id.numeric' => 'El códibo debe ser de tipo número',
            'id.unique' => 'El Registro de este código ya existe',
            'nom_depto.required'  => 'El nombre es obligatorio',
            'nom_depto.max' => 'El nombre no puede tener más de 255 caracteres',
            'nom_depto.unique' => 'El Registro de este departamento ya existe',
            'fac_id.required'  => 'El campo facultad es obligatorio',
        ];
                break;
        
            default:
                $rules = [
            'id.required' => 'El código es obligatorio',
            'id.numeric' => 'El códibo debe ser de tipo número',
            'id.unique' => 'El Registro de este código ya existe',
            'nom_depto.unique' => 'El Registro de este departamento ya existe',
            'nom_depto.required'  => 'El nombre es obligatorio',
            'nom_depto.max' => 'El nombre no puede tener más de 255 caracteres',
            'fac_id.required'  => 'El campo facultad es obligatorio',
        ];
                break;
        }
    return $rules;
    }
}
