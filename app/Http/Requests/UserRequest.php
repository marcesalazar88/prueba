<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ident_usu' => 'required|integer|unique:usu',
            'nombre' => 'required|string|max:255',
            'apellido' => 'required|string|max:255',
            'rol' => 'required',Rule::in(['admin','director','docente','estudiante']),
            'tel' => 'required',
            'email' => 'required|string|email|max:255|unique:usu',
            'password' => 'required|string|min:6|confirmed',
        ];
    }
/*
  "password" => "123456"
  "password_confirmation" => "123456"
*/
    public function messages()
    {
        return [
            'ident_usu.required'  => 'La Identificación es obligatoria',
            'ident_usu.integer' => 'El valor de identificación debe ser numérico.',
            'ident_usu.unique' => 'El valor de identificación ya está en uso.',
            
            'nombre.required'  => 'El nombre es obligatorio',
            'nombre.string'  => 'El nombre debe ser un texto',
            'nombre.max' => 'El nombre no puede tener más de 255 caracteres',
            
            
            'apellido.required'  => 'El apellido es obligatorio',
            'apellido.string'  => 'El apellido debe ser un texto',
            'apellido.max' => 'El apellido no puede tener más de 255 caracteres',
            
            'rol.required'  => 'El rol es obligatorio',
            'rol.in'  => 'El rol debe ser válido',
            
            'tel.required'  => 'El teléfono es obligatorio',
            
            'email.required'  => 'El email es obligatorio',
            'email.email'  => 'El email debe tener el formato válido como ejemplo@dominio.com o similar',
            'email.max'  => 'El email no puede tener más de 255 caracteres',
            'email.unique'  => 'El email ya está en uso',
            
            'password.required'  => 'La contraseña es obligatorio',
            'password.string'  => 'La contraseña debe ser un texto',
            'password.max' => 'La contraseña no puede tener más de 255 caracteres',
            'password.confirmed'  => 'La confirmación de la contraseña no coincide',
            
        ];
    }
}
