<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class BiblioRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'refe_biblio' => 'required|string|max:255'
        ];
    }
/*
  "password" => "123456"
  "password_confirmation" => "123456"
*/
    public function messages()
    {
        return [            
            'refe_biblio.required'  => 'El contenido es obligatorio',
            'refe_biblio.string'  => 'El contenido debe ser un texto',
            'refe_biblio.max' => 'El contenido no puede tener más de 255 caracteres'
        ];
    }
}
