<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use App\Planes;

class PlanesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      switch (Request::get('submit')) {
            case 'Actualizar':
                 if (Request::get('modificar_adjunto') == "SI")
                 {
                   $rules = [
                  'nombre' => 'required|string|max:255',
                  'adjunto' => 'required|file|mimes:pdf|max:10124',
                  'fecha' => 'required|date'
              ];
                 }else{
                      $rules = [
                  'nombre' => 'required|string|max:255',
                  'fecha' => 'required|date'
              ];
                 }
                break;
        
            default:
                $rules = [
            'nombre' => 'required|string|max:255|unique:planes',
            'adjunto' => 'required|file|mimes:pdf|max:10124',
            'fecha' => 'required|date'
        ];
                break;
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'nombre.required'=>'El nombre es obligatorio',
            'nombre.string'=>'El nombre debe ser un texto',
            'nombre.max'=>'Debe contener másimo 255 caracteres',
            'nombre.unique'=>'El nombre debe ser único',
            'adjunto.required' => 'El adjunto es obligatorio',
            'adjunto.file' => 'Debe ser un archivo',
            'adjunto.max' => 'El tamaño máximo debe ser de 5 Mb',
            'fecha' => 'La fecha es obligatoria',
            'fecha' => 'Debe ser una fecha válida'
            /*
            'nom_fac.required'  => 'El nombre es obligatorio',
            'nom_fac.max' => 'El nombre no puede tener más de 255 caracteres',
            'nom_fac.unique' => 'El Registro de esta facultad ya existe',
            'type.required' => 'El campo Tipo es requerido',
            'type.in' => 'El valor del campo tipo no es válido',
            'movement_date.required' => 'El campo Fecha es requerido',
            'movement_date.date' => 'La Fecha no es válida',
            'category_id.required' => 'La categoría es obligatoria',
            'description.required' => 'La descripción es obligatoria',
            'description.min' => 'La descripción debe tener tres caracteres o más',
            'description.max' => 'La descripción no puede tener más de 1000 caracteres',
            'money_decimal.required' => 'El monto es obligatorio',
            'money_decimal.numeric' => 'El monto debe ser un número',
            'money_decimal.min' => 'El monto debe ser mayor a cero',
            'image.image' => 'El archivo adjunto no es una imagen válida'*/
        ];
    }
}
