<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;

class FacRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch (Request::get('submit')) {
            case 'Actualizar':
                $rules = [
            'nom_fac' => 'required|string|max:255',
        ];
                break;
        
            default:
                $rules = [
            'id' => 'required|numeric|unique:fac',
            'nom_fac' => 'required|string|max:255|unique:fac',
        ];
                break;
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'id.required' => 'EL código es obligatorio',
            'id-numeric' => 'El código debe ser de tipo número',
            'id.unique' => 'El registro de código ya existe',
            'nom_fac.required'  => 'El nombre es obligatorio',
            'nom_fac.max' => 'El nombre no puede tener más de 255 caracteres',
            'nom_fac.unique' => 'El Registro de esta facultad ya existe',
        ];
    }
}
