<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class AsigtRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'codigo_asigt' => 'required|integer|min:1|unique:asigt',
            'nom_asigt' => 'required|string|max:255',
            'sem_ofrece_asigt' => 'required',
            'ihs_asigt' => 'required',
            'n_cred_asigt' => 'required',
            'just_asigt' => 'required',
            'obj_gen' => 'required',
            'obj_esp' => 'required',
            'area' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'codigo_asigt.required'  => 'El campo Código es obligatorio',
            'codigo_asigt.integer'  => 'El código debe ser un número entero mayor a cero.',
            'codigo_asigt.min'  => 'El código debe ser un número entero mayor a cero.',
            'codigo_asigt.unique' => 'El código de asignatura ya existe',
            
            'nom_asigt.required'  => 'El campo Nombre de la Asignatura es obligatorio',
            'nom_asigt.unique' => 'El Nombre de asignatura ya existe',
            
            'sem_ofrece_asigt.required'  => 'El campo Semestres a los Cuales se ofrece es obligatorio',
            'ihs_asigt.required'  => 'El campo Intensidad Horaria Semanal es obligatorio',
            'n_cred_asigt.required'  => 'El campo Número de Créditos es obligatorio',
            'just_asigt.required'  => 'El campo Justificación es obligatorio',
            'obj_gen.required'  => 'El campo Objetivo General es obligatorio',
            'obj_esp.required'  => 'El campo Objetivos Específicos es obligatorio',
            'area.required'  => 'El campo Área es obligatorio',
            

        ];
    }
}
