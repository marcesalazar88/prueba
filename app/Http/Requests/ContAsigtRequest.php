<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;

class ContAsigtRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch (Request::get('submit')) {
            case 'Actualizar':
                $rules = [
            'cod_asigt' => 'required',
            'hras_cont_asigt' => 'required',
            'tem_cap_cont_asigt' => 'required'
        ];
                break;
        
            default:
                $rules = [
            'cod_asigt' => 'required',
            'hras_cont_asigt' => 'required',
            'tem_cap_cont_asigt' => 'required'
        ];
                break;
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'cod_asigt.required' => 'EL campo adignatura es obligatorio',
            'hras_cont_asigt.required' => 'EL campo horas de contenido es obligatorio',
            'tem_cap_cont_asigt.required' => 'EL campo tema o capitulo  es obligatorio',
            'form_eva_cont_asigt.required' => 'EL campo form a de evaluación es obligatorio'
        ];
    }
}
