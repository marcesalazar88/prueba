<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        if (Auth::check()){
            $roles = explode("|",$role);
            #dd(in_array(Auth::user()->rol, $roles));
            #if (Auth::user()->rol != $role) {//admin|docente|estudiante
            if (!in_array(Auth::user()->rol, $roles)) {//admin|docente|estudiante
                return redirect('home');
            }
        }
    return $next($request);
    }
}
