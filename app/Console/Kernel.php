<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Http\Controllers\NotificacionesController as NotificacionesController;
use App\User as User;
use App\Config as Config;
use App\Http\Controllers\EmailController as EmailController;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
        $config = Config::find(1);
        $hora = (string)date("G:i", strtotime($config->hora_notificacion));
        $schedule->call(function () use($hora) {
          
          $email = new \App\Http\Controllers\EmailController();
          //$email->mensaje1();
          $email->mensaje2();
          $email->mensaje3();
          //$email->mensaje4();
          //$email->mensaje5();
          $email->alerta1();
          $email->alerta2();
          $email->alerta3();
          $email->alerta4();
          $email->alerta5();
          })->dailyAt($hora);
      /*
           $config->hora_notificacion
          //->timezone('America/Bogota')
          //->hourly();
          //->daily();
          //->dailyAt('13:00');
      */
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
