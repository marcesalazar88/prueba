<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mensaje extends Model
{
    public $timestamps = true;
    protected $primaryKey = 'id'; // or null
    public $incrementing = true;
    protected $table = 'mensaje';
    protected $fillable = [
        'id', 'asigc_id', 'de', 'para', 'texto', 'rol', 'created_at'
    ];
    public $fillcolumn = [
        'id'=>'ID',
        'asigc_id' => 'asigc_id', 
        'de' => 'de', 
        'texto' => 'texto', 
        'rol' => 'rol'
    ];
      public function de_fk()
    {//Verificado
        return $this->belongsTo('App\User','de','ident_usu');
    }
}
