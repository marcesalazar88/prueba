<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProgTemRefeBiblio extends Model
{
    //
    public $timestamps = false;
    protected $table = 'prog_tem_refe_biblio';
    protected $fillable = [
        'prog_tem_id',
        'refe_biblio'
        ];
    public $fillcolumn = [
        'prog_tem_id'=>'prog_tem_id',
        'refe_biblio'=>'refe_biblio'
        ];
        
        public function prog_tem()
    {//Verificado
        return $this->belongsTo('App\ProgTem');
    }
}
