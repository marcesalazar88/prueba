<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notificaciones extends Model
{
    public $timestamps = true;
    protected $primaryKey = 'id'; // or null
    public $incrementing = true;
protected $table = 'notificaciones';
    protected $fillable = [
        'id', 'mensaje', 'destino', 'estado', 'created_at', 'updated_at'
    ];
  
    public $fillcolumn = [
        'id'=>'ID',
        'mensaje'=>'Mensaje',
        'destino'=>'Destino',
        'estado'=>'Estado'
    ];

 public function destino_fk()
    {//Verificado
        return $this->belongsTo('App\User','destino','ident_usu');
    }
}
